#include "../../interface/TopTrigger/BasicTriggerObjectPlots.h"

truicear::BasicTriggerObjectPlots::BasicTriggerObjectPlots()
{

}

truicear::BasicTriggerObjectPlots::~BasicTriggerObjectPlots()
{
}


void truicear::BasicTriggerObjectPlots::plot(std::vector<mor::TriggerObject> *trigger_objects, std::vector<mor::Jet> *jets, mor::EventInformation *event_information)
{
	int ntrigg_obj_barrel = 0,ntrigg_obj_overlap = 0,ntrigg_obj_endcap = 0;
	int ntrigg_obj_pt5to12 = 0,ntrigg_obj_pt20to30 = 0,ntrigg_obj_pt30plus = 0;
	for(std::vector<mor::TriggerObject>::iterator trigger_obj = trigger_objects->begin();
		trigger_obj != trigger_objects->end();
		++trigger_obj){

		double trigg_obj_pt = trigger_obj->Pt();
		double trigg_obj_eta = trigger_obj->Eta();
		histos1d[pre+"overall_efficiency_trig_obj"+id]->Fill(1);
		histos1d[pre+"pt_trig_obj"+id]->Fill(trigg_obj_pt);
		histos1d[pre+"pt_Manuel_trig_obj"+id]->Fill(trigg_obj_pt);
		histos1d[pre+"eta_trig_obj"+id]->Fill(trigg_obj_eta);
		histos1d[pre+"phi_trig_obj"+id]->Fill(trigger_obj->Phi());
		histos2d[pre+"eta_vs_phi_trig_obj"+id]->Fill(trigger_obj->Phi(),trigger_obj->eta());

		//Fill plots for different eta regions
		if(trigg_obj_eta < 0.8){
			ntrigg_obj_barrel++;
			histos1d[pre+"overall_efficiency_trig_obj"+id]->Fill(2);
			histos1d[pre+"pt_barrel_trig_obj"+id]->Fill(trigg_obj_pt);
			histos1d[pre+"phi_barrel_trig_obj"+id]->Fill(trigger_obj->Phi());
		}
		else if (0.8 <= trigg_obj_eta && trigg_obj_eta < 1.2){
			ntrigg_obj_overlap++;
			histos1d[pre+"overall_efficiency_trig_obj"+id]->Fill(3);
			histos1d[pre+"pt_overlap_trig_obj"+id]->Fill(trigg_obj_pt);
			histos1d[pre+"phi_overlap_trig_obj"+id]->Fill(trigger_obj->Phi());
		}
		else if (1.2 <= trigg_obj_eta){
			ntrigg_obj_endcap++;
			histos1d[pre+"overall_efficiency_trig_obj"+id]->Fill(4);
			histos1d[pre+"pt_endcap_trig_obj"+id]->Fill(trigg_obj_pt);
			histos1d[pre+"phi_endcap_trig_obj"+id]->Fill(trigger_obj->Phi());
		}
		//Fill plots for different pt regions
		if(5 < trigg_obj_pt && trigg_obj_pt <= 12){			
			ntrigg_obj_pt5to12++;
			histos1d[pre+"overall_efficiency_trig_obj"+id]->Fill(5);
			histos1d[pre+"eta_pt5to12_trig_obj"+id]->Fill(trigg_obj_eta);
			histos1d[pre+"phi_pt5to12_trig_obj"+id]->Fill(trigger_obj->Phi());
		}
		if(20. < trigg_obj_pt && trigg_obj_pt <= 30){
			ntrigg_obj_pt20to30++;
			histos1d[pre+"overall_efficiency_trig_obj"+id]->Fill(6);
			histos1d[pre+"eta_pt20to30_trig_obj"+id]->Fill(trigg_obj_eta);
			histos1d[pre+"phi_pt20to30_trig_obj"+id]->Fill(trigger_obj->Phi());
		}
		else if(30 < trigg_obj_pt){
			ntrigg_obj_pt30plus++;
			histos1d[pre+"overall_efficiency_trig_obj"+id]->Fill(7);
			histos1d[pre+"eta_pt30plus_trig_obj"+id]->Fill(trigg_obj_eta);
			histos1d[pre+"phi_pt30plus_trig_obj"+id]->Fill(trigger_obj->Phi());
		}
	}

	if(trigger_objects->size() > 0 ) histos1d[pre+"number_trig_obj"+id]->Fill(trigger_objects->size());
	if(ntrigg_obj_barrel > 0) histos1d[pre+"number_barrel_trig_obj"+id]->Fill(ntrigg_obj_barrel);
	if(ntrigg_obj_overlap > 0) histos1d[pre+"number_overlap_trig_obj"+id]->Fill(ntrigg_obj_overlap);
	if(ntrigg_obj_endcap > 0) histos1d[pre+"number_endcap_trig_obj"+id]->Fill(ntrigg_obj_endcap);
	if(ntrigg_obj_pt5to12 > 0) histos1d[pre+"number_pt5to12_trig_obj"+id]->Fill(ntrigg_obj_pt5to12);
	if(ntrigg_obj_pt20to30 > 0) histos1d[pre+"number_pt20to30_trig_obj"+id]->Fill(ntrigg_obj_pt20to30);
	if(ntrigg_obj_pt30plus > 0) histos1d[pre+"number_pt30plus_trig_obj"+id]->Fill(ntrigg_obj_pt30plus);
}

void truicear::BasicTriggerObjectPlots::book_histos()
{

	int nptBins = 14;
	float  ptBins[100] = { 0,5,6,7,8,9,10,11,13,16,20,25,40,60,100 };
	int netaBins = 12 ;
	//float  etaBins[100] = { 0, 0.8, 1.2,1.6, 2.4 };
	float  etaBins[100] = { -2.4, -2.1, -1.6, -1.2, -0.9, -0.4, 0., 0.4,  0.9, 1.2, 1.6, 2.1, 2.4};
	//float  etaBins[100] = { -2.1, -1.2, -0.8, 0, 0.8, 1.2, 2.1 };

	histos1d[pre+"overall_efficiency_trig_obj"+id]=histo_writer->create_1d(pre+"overall_efficiency_trig_obj"+id,"overall efficiency "+pre,7,0.5,7.5, "Overall Efficiency in different eta pt regions");	
	
	histos1d[pre+"number_trig_obj"+id]=histo_writer->create_1d(pre+"number_trig_obj"+id,"trigger object multiplicity "+pre,10,-0.5,9.5, "trigger object Multiplicity");
	histos1d[pre+"pt_trig_obj"+id]=histo_writer->create_1d(pre+"pt_trig_obj"+id,"p_{t} trigger objects "+pre,nptBins,ptBins, "p_{t} [GeV]");
	histos1d[pre+"pt_Manuel_trig_obj"+id]=histo_writer->create_1d(pre+"pt_Manuel_trig_obj"+id,"p_{t} trigger objects "+pre,50,0.,25., "p_{t} [GeV]");
	histos1d[pre+"eta_trig_obj"+id]=histo_writer->create_1d(pre+"eta_trig_obj"+id,"#eta trigger objects "+pre,netaBins,etaBins, "#eta");
	histos1d[pre+"phi_trig_obj"+id]=histo_writer->create_1d(pre+"phi_trig_obj"+id,"#phi trigger objects "+pre,18,-3.3,3.3, "#phi");
	histos2d[pre+"eta_vs_phi_trig_obj"+id]=histo_writer->create_2d(pre+"eta_vs_phi_trig_obj"+id,"#eta vs #phi trigger objects "+pre,36,-3.3,3.3,30,-3.0,3.0, "#phi","#eta");
	
	std::vector<std::string> eta_regions;
	eta_regions.push_back("barrel");
	eta_regions.push_back("overlap");
	eta_regions.push_back("endcap");

	for(std::vector<std::string>::iterator eta_region = eta_regions.begin();eta_region != eta_regions.end();eta_region++){

	histos1d[pre+"number_"+*eta_region+"_trig_obj"+id]=histo_writer->create_1d(pre+"number_"+*eta_region+"_trig_obj"+id,"trigger object multiplicity "+*eta_region+" "+pre,10,-0.5,9.5, "trigger object Multiplicity");
	histos1d[pre+"pt_"+*eta_region+"_trig_obj"+id]=histo_writer->create_1d(pre+"pt_"+*eta_region+"_trig_obj"+id,"p_{t} "+*eta_region+" trigger objects "+pre,nptBins,ptBins, "p_{t} [GeV]");
	histos1d[pre+"phi_"+*eta_region+"_trig_obj"+id]=histo_writer->create_1d(pre+"phi_"+*eta_region+"_trig_obj"+id,"#phi "+*eta_region+" trigger objects "+pre,18,-3.3,3.3, "#phi");
	}
	
	std::vector<std::string> pt_regions;
	pt_regions.push_back("pt5to12");
	pt_regions.push_back("pt20to30");
	pt_regions.push_back("pt30plus");

	for(std::vector<std::string>::iterator pt_region = pt_regions.begin();pt_region != pt_regions.end();pt_region++){

	histos1d[pre+"number_"+*pt_region+"_trig_obj"+id]=histo_writer->create_1d(pre+"number_"+*pt_region+"_trig_obj"+id,"trigger object multiplicity "+*pt_region+" "+pre,10,-0.5,9.5, "trigger object Multiplicity");
	histos1d[pre+"eta_"+*pt_region+"_trig_obj"+id]=histo_writer->create_1d(pre+"eta_"+*pt_region+"_trig_obj"+id,"#eta "+*pt_region+" trigger objects "+pre,netaBins,etaBins, "#eta");
	histos1d[pre+"phi_"+*pt_region+"_trig_obj"+id]=histo_writer->create_1d(pre+"phi_"+*pt_region+"_trig_obj"+id,"#phi "+*pt_region+" trigger objects "+pre,18,-3.3,3.3, "#phi");
	}
}
