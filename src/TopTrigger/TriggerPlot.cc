#include "../../interface/TopTrigger/TriggerPlot.h"

truicear::TriggerPlot::TriggerPlot()
{
}

truicear::TriggerPlot::~TriggerPlot()
{
}

void truicear::TriggerPlot::set_histo_writer(HistoWriter *histo_writer)
{
	this->histo_writer = histo_writer;
	book_histos();
}

void truicear::TriggerPlot::set_id(std::string ident, std::string trigger_name)
{
	this->id = ident;
	this->pre = trigger_name;
}
