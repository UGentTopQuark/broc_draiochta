#include "../../interface/TopTrigger/TriggerObjectSelector.h"

truicear::TriggerObjectSelector::TriggerObjectSelector(eire::HandleHolder *handle_holder)
{
	this->handle_holder = handle_holder;
	selected_trigger_objects = new std::vector<mor::TriggerObject>();
	min_pt = -1;
	max_eta = -1;
	min_eta = -1;

	module_name = "";
	trigger_name = "";

	module_idx = -1;
	trigger_idx = -1;

	last_module_idx = 987;

	module_set_by_id = false;

	trigger_module_manager = handle_holder->get_trigger_module_manager();
}

truicear::TriggerObjectSelector::~TriggerObjectSelector()
{
	if(selected_trigger_objects){ delete selected_trigger_objects; selected_trigger_objects = NULL; }
}

void truicear::TriggerObjectSelector::set_trigger_name(std::string trigger_name, int module)
{
  	module_set_by_id = true;
	this->trigger_name = trigger_name;

	//	trigger_module_manager->list_modules(trigger_name);

	if(module == -1){
		module_idx = last_module_idx;	// select last idx of modules
//		std::cout<<"last module index : "<<last_module_idx<<std::endl;
	}else{
		module_idx = module;
	}

	trigger_idx = trigger_module_manager->get_trigger_id(trigger_name);
}

void truicear::TriggerObjectSelector::set_trigger_name(std::string trigger_name, std::string module_name)
{
	module_set_by_id = false;
	this->trigger_name = trigger_name;
	this->module_name = module_name;
	//get trigger and module idx
	std::pair<int,int> indices = trigger_module_manager->get_module_ids(trigger_name, module_name);

	trigger_idx = indices.first;
	module_idx = indices.second;
}

std::vector<mor::TriggerObject>* truicear::TriggerObjectSelector::get_selected_objects(std::vector<mor::TriggerObject> *trigger_objects)
{
	std::pair<int,int> indices = trigger_module_manager->get_module_ids(trigger_name, module_name);
	trigger_idx = indices.first;
	if(!module_set_by_id){
		if(module_name == ""){
			module_idx = last_module_idx;
		}else{
			module_idx = indices.second;
		}
	}

	int selected_module_idx = -1;
	if(module_idx == last_module_idx){
		selected_module_idx = trigger_module_manager->get_last_module_index(trigger_name);
	}else{
		selected_module_idx = module_idx;
	}

	selected_trigger_objects->clear();	
	for(std::vector<mor::TriggerObject>::iterator obj = trigger_objects->begin();
		obj != trigger_objects->end();
		++obj){
		if(
		   (obj->pt() > min_pt || min_pt == -1) &&
		   ((fabs(obj->eta()) < max_eta || max_eta == -1) && (fabs(obj->eta()) >= min_eta || min_eta == -1)) &&
		   (obj->triggered(trigger_idx, selected_module_idx))){
			selected_trigger_objects->push_back(*obj);
		}
	}

	return selected_trigger_objects;
}
