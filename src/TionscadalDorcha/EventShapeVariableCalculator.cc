#include "../../interface/TionscadalDorcha/EventShapeVariableCalculator.h"


tionscadaldorcha::EventShapeVariableCalculator::EventShapeVariableCalculator(eire::HandleHolder *handle_holder)
{
	this->handle_holder = handle_holder;

	event_processed = false;
	set_handles();

	electrons = NULL;
	jets = NULL;
	mets = NULL;
	muons = NULL;

	HlT = 0.;
	H3T = 0.;
	Mevent = 0.;
	Mj2nulT = 0.;
	sphericity = 0.;
	aplanarity = 0.;
}

tionscadaldorcha::EventShapeVariableCalculator::~EventShapeVariableCalculator()
{
}

void tionscadaldorcha::EventShapeVariableCalculator::set_handles()
{
	electrons = handle_holder->get_tight_electrons();
	jets = handle_holder->get_tight_jets();
	muons = handle_holder->get_tight_muons();
	mets = handle_holder->get_corrected_mets();
}

void tionscadaldorcha::EventShapeVariableCalculator::calculate_variables()
{
	if(event_processed == true) return;

	/*
	 *	calculate aplanarity, sphericity
	 */
	std::vector<mor::Particle> objects;
	int njet = 0;

	HlT = 0;
	H3T = 0;
	Mevent = 0;
	Mj2nulT = 0;
	mor::Particle event_p4;
	mor::Particle mj2nul_p4;
	for(std::vector<mor::Jet>::iterator jet = handle_holder->get_tight_jets()->begin();
		jet != handle_holder->get_tight_jets()->end();
		++jet){
		if(njet < 5){
			objects.push_back(*jet);
			HlT += jet->pt();
			if(njet > 1){
				H3T += jet->pt();
			}
			event_p4 += jet->p4();
		}
		if(njet == 1){
			mj2nul_p4 += jet->p4();
		}
		++njet;
	}

	mets = handle_holder->get_mets();
	electrons = handle_holder->get_tight_electrons();
	muons = handle_holder->get_tight_muons();
	if(handle_holder->get_tight_electrons()->size() > 0){
		objects.push_back((*electrons)[0]);
		HlT += (*electrons)[0].pt();
		event_p4 += (*electrons)[0].p4();
		mj2nul_p4 += (*electrons)[0].p4();
	}

	if(handle_holder->get_tight_muons()->size() > 0){
		objects.push_back((*muons)[0]);
		HlT += (*muons)[0].pt();
		event_p4 += (*muons)[0].p4();
	}
	if(handle_holder->get_mets()->size() > 0){
		event_p4 += (*mets)[0].p4();
		mj2nul_p4 += (*mets)[0].p4();
	}

	Mevent = event_p4.mass();
	Mj2nulT = mj2nul_p4.mt();

	EventShapeVariables event_shape(objects);
	aplanarity = event_shape.aplanarity();
	sphericity = event_shape.sphericity();

	event_processed = true;
}
