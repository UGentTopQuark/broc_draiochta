#include "../../interface/TionscadalDorcha/ClassificationManager.h"

tionscadaldorcha::ClassificationManager::ClassificationManager(eire::ConfigReader *config_reader, eire::HandleHolder *handle_holder)
{
	this->handle_holder = handle_holder;
	id = "_"+handle_holder->get_ident();
	evt_shape = NULL;
	discriminator_plot = NULL;
	discriminator = -1;

	classify_events = config_reader->get_bool_var("classify_events", "MVA", true);
	create_tree = config_reader->get_bool_var("create_tree", "MVA", true);
	evt_classifier = handle_holder->services()->event_classifier();

	flat_tree_outfile = NULL;
	tree_producer = NULL;

	if(create_tree){
		std::string output_dir = config_reader->get_var("output_directory");
		std::string suffix = config_reader->get_var("outfile_suffix");
		std::string filename = output_dir+"/"+"flat_tree"+id+suffix+".root";
		while(filename.find("|") != std::string::npos)
			filename.replace(filename.find("|"),1,"_");
		flat_tree_outfile = new TFile(filename.c_str(), "RECREATE");
		tree_producer = new tionscadaldorcha::TionscadalDorchaTreeProducer(handle_holder);
		tree_producer->set_outfile(flat_tree_outfile);
		tree_producer->create_tree("tree");
		tree_producer->book_branches();
	}

	mm       = NULL;
	mmweight = 0.;
}

tionscadaldorcha::ClassificationManager::~ClassificationManager()
{
	if(tree_producer){delete tree_producer; tree_producer = NULL;}
	if(flat_tree_outfile){
		delete flat_tree_outfile;
		flat_tree_outfile = NULL;
	}
	if(mm) {delete mm; mm = NULL;}
}

void tionscadaldorcha::ClassificationManager::initialise()
{
	evt_shape = handle_holder->services()->evt_shape();

	if(classify_events){
		discriminator_plot = handle_holder->get_histo_writer()->create_1d(("event_discriminator"+id).c_str(),"Event classification discriminator",320,-1.3,1.3,"event discriminator");
		QCD_discriminator_plot = handle_holder->get_histo_writer()->create_1d(("QCD_event_discriminator"+id).c_str(),"QCD Event classification discriminator",320,-1.3,1.3,"event discriminator"); //jl 10.06.11: for QCD
	}
	//jl 10.06.11: instantiate QCD stuff
	mm = new QCDMMPlots(handle_holder,false);
        mm->set_ht_calc(handle_holder->services()->ht_calc());
        mm->set_mass_reco(handle_holder->services()->mass_reco());

}

void tionscadaldorcha::ClassificationManager::run()
{
	evt_shape->calculate_variables();
	if(classify_events){
		discriminator = evt_classifier->classify();
		mmweight = mm->ComputeMMWeights();
		discriminator_plot->Fill(discriminator);
	 	if(id.find("Data") != std::string::npos) //jl 10.06.11: MM plot for MVA
			QCD_discriminator_plot->Fill(discriminator,mmweight*handle_holder->get_event_weight());
	}
	if(create_tree){
		tree_producer->fill_branches();
	}
}
