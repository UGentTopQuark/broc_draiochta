#include "../../interface/TionscadalDorcha/EventClassifier.h"

tionscadaldorcha::EventClassifier::EventClassifier(eire::HandleHolder *handle_holder)
{
	this->handle_holder = handle_holder;
	this->config_reader = handle_holder->get_config_reader();

	reader = NULL;

	evt_shape = handle_holder->services()->evt_shape();
	tmass = handle_holder->services()->mass_reco();
	ht_calc = handle_holder->services()->ht_calc();

	var_calc = new broc::ClassificationVariableCalculator(handle_holder);

	// initialise variables
	sphericity = 0;
	aplanarity = 0;
	HlT = 0;
	H3T = 0;
	Mevent = 0;
	Mj2nulT = 0;	

	//jl 07.04.11: new variables
	m3        = 0.;
	mu1_pt      = 0.;
	mu1_eta     = 0.;
	mu1_abseta_x_charge = 0;
	dphimmet  = 0.;
	drjm      = 0.;
	detajm    = 0.;
	jet1_pt   = 0.;
	jet2_pt   = 0.;
	jet3_pt   = 0.;
	jet4_pt   = 0.;
	jet1_eta   = 0.;
	jet2_eta   = 0.;
	jet3_eta   = 0.;
	jet4_eta   = 0.;
	njets     = 0;
	MET     = 0;
	drje     = 0;
	dphiemet     = 0;
	e1_abseta_x_charge = 0;
	e1_eta     = 0;
	e1_pt     = 0;
	HT     = 0;
	MHT     = 0;
	mchi2_hadT = 0;
	mchi2_lepT = 0;
	mchi2_prob = 0;
	mchi2_dm = 0;
}

tionscadaldorcha::EventClassifier::~EventClassifier()
{
	if(reader){delete reader; reader=NULL;}
	if(var_calc){delete var_calc; var_calc=NULL;}
}

double tionscadaldorcha::EventClassifier::classify()
{
        std::vector<mor::Muon> *isomu = handle_holder->get_tight_muons();
        std::vector<mor::Electron> *isoe = handle_holder->get_tight_electrons();

        std::vector<mor::Jet>  *jets = handle_holder->get_tight_jets();
        std::vector<mor::MET>  *met = handle_holder->get_selected_mets();

	HT = ht_calc->get_ht();
	MHT = ht_calc->get_missing_ht();

	if(jets->size() > 0){jet1_pt = (*jets)[0].pt(); jet1_eta = (*jets)[0].eta();}
	if(jets->size() > 1){jet2_pt = (*jets)[1].pt(); jet2_eta = (*jets)[1].eta();}
	if(jets->size() > 2){jet3_pt = (*jets)[2].pt(); jet3_eta = (*jets)[2].eta();}
	if(jets->size() > 3){jet4_pt = (*jets)[3].pt(); jet4_eta = (*jets)[3].eta();}
	this->njets = jets->size();

        //      Fill muon variables
	if(isomu->size() > 0){
        	mu1_pt = (*isomu)[0].pt();  
        	mu1_eta = (*isomu)[0].eta();
        	mu1_abseta_x_charge = std::fabs((*isomu)[0].eta())*(*isomu)[0].charge();
	}

        m3 = tmass->calculate_M3();

	drjm     = var_calc->get_drjl<mor::Muon>(isomu, jets);
	detajm   = var_calc->get_detajl<mor::Muon>(isomu, jets);
	dphimmet = var_calc->get_dphilmet<mor::Muon>(isomu);

	evt_shape->calculate_variables();

	aplanarity = evt_shape->get_aplanarity();
	sphericity = evt_shape->get_sphericity();
	Mevent = evt_shape->get_Mevent();
	Mj2nulT = evt_shape->get_Mj2nulT();
	HlT = evt_shape->get_HlT();
	H3T = evt_shape->get_H3T();

	/*
	 *	Fill electron variables
	 */
	if(isoe->size() > 0){
		e1_eta = (*isoe)[0].eta();	
		e1_pt = (*isoe)[0].pt();	
        	e1_abseta_x_charge = std::fabs((*isoe)[0].eta())*(*isoe)[0].charge();
	}

	MET = (met->begin())->pt();

	drje     = var_calc->get_drjl<mor::Electron>(isoe, jets);
	detaje   = var_calc->get_detajl<mor::Electron>(isoe, jets);
	dphiemet = var_calc->get_dphilmet<mor::Electron>(isoe);

	mchi2_hadT = tmass->calculate_chihadTmass();
	mchi2_lepT = tmass->calculate_chilepTmass();
	mchi2_prob = 1-TMath::Prob(tmass->get_chi2(),3);
	mchi2_dm = fabs(mchi2_hadT-mchi2_lepT);

	return reader->EvaluateMVA(method_name.c_str());
}

	//SprAbsTrainedClassifier *ranfor = SprClassifierReader::readTrained("ranfor_tt_vs_wj_mu.spr");
	//assert(ranfor!=0);
        //vector<double> BDTinputs; 
        //BDTinputs.push_back(m3); 
        //BDTinputs.push_back(HlT); 
        //BDTinputs.push_back(lpt); 
        //BDTinputs.push_back(leta);
        //BDTinputs.push_back(dphilmet);
        //double ranfor_output = 0.5;
        //ranfor_output = ranfor->response(BDTinputs);
	//return ranfor_output;

void tionscadaldorcha::EventClassifier::initialise()
{
	// if the module is disabled, but is available through central services, it should not be initialised
	if(!config_reader->get_bool_var("enable_mva_module","MVA", false)) return;

	method_name = config_reader->get_var("method_name","MVA", true);
	std::string weight_file = config_reader->get_var("weights_file","MVA", true);

	reader = new TMVA::Reader("!Color:!Silent");
        reader->AddVariable("sphericity", &sphericity);
        reader->AddVariable("aplanarity", &aplanarity);
//        reader->AddVariable("HlT", &HlT);
//        reader->AddVariable("H3T", &H3T);
///       reader->AddVariable("Mevent", &Mevent);
//        reader->AddVariable("Mj2nulT", &Mj2nulT);
	reader->AddVariable("m3",&m3);
//        reader->AddVariable("HT", &HT);
        reader->AddVariable("MHT", &MHT);
//        reader->AddVariable("MET",&MET);
//	reader->AddVariable("drje",&drje);
//	reader->AddVariable("detaje",&detajm);
//	reader->AddVariable("dphiemet",&dphiemet);
 //       reader->AddVariable("e1_eta",&e1_eta);
//	reader->AddVariable("e1_pt",&e1_pt);
        reader->AddVariable("jet1_pt",&jet1_pt);
        reader->AddVariable("jet2_pt",&jet2_pt);
        reader->AddVariable("jet3_pt",&jet3_pt);
//        reader->AddVariable("jet4_pt",&jet4_pt);
//        reader->AddVariable("jet1_eta",&jet1_eta);
//        reader->AddVariable("jet2_eta",&jet2_eta);
//        reader->AddVariable("jet3_eta",&jet3_eta);
//        reader->AddVariable("jet4_eta",&jet4_eta);
        reader->AddVariable("njets",&njets);
//        reader->AddVariable("drjm",&drjm);
//        reader->AddVariable("detajm",&detajm);
//        reader->AddVariable("dphimmet",&dphimmet);
//        reader->AddVariable("mu1_eta",&mu1_eta);
//        reader->AddVariable("mu1_pt",&mu1_pt);
//        reader->AddVariable("mchi2_hadT",&mchi2_hadT);
//        reader->AddVariable("mchi2_lepT",&mchi2_lepT);
//        reader->AddVariable("mchi2_prob",&mchi2_prob);
//        reader->AddVariable("mchi2_dm",&mchi2_dm);
//        reader->AddVariable("mu1_abseta_x_charge",&mu1_abseta_x_charge);

	reader->BookMVA(method_name, weight_file);
}
