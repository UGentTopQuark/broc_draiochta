#include "../../interface/TionscadalDorcha/TionscadalDorchaTreeProducer.h"

tionscadaldorcha::TionscadalDorchaTreeProducer::TionscadalDorchaTreeProducer(eire::HandleHolder *handle_holder)
{
	id = "_"+handle_holder->get_ident();

	this->handle_holder = handle_holder;

	var_calc = new broc::ClassificationVariableCalculator(handle_holder);

	tmass = handle_holder->services()->mass_reco();
	evt_shape = handle_holder->services()->evt_shape();
	ht_calc = handle_holder->services()->ht_calc();

	jet1_pt = 0;
	jet2_pt = 0;
	jet3_pt = 0;
	jet4_pt = 0;
	jet1_eta = 0;
	jet2_eta = 0;
	jet3_eta = 0;
	jet4_eta = 0;

	njets = 0;

	mu1_pt = 0;
	mu1_eta = 0;
	mu1_abseta_x_charge = 0;
	mu1_charge = 0;
	loose_mu1_pt = 0;
	loose_mu1_eta = 0;

	e1_pt = 0;
	e1_eta = 0;
	e1_abseta_x_charge = 0;
	e1_charge = 0;
	loose_e1_pt = 0;
	loose_e1_eta = 0;

	aplanarity = 0.;
	sphericity = 0.;
	HlT = 0.;
	HT = 0.;
	H3T = 0.;
	MjetT = 0.;
	Mevent = 0.;
	Mj2nulT = 0.;

	mchi2_hadT = 0.;
	mchi2_lepT = 0.;
	mchi2_prob = 0.;
	mchi2_dm = 0.;

	//jl 23.03.11
	MET      = 0.;
	MHT      = 0.;
	m3       = 0.;
	drje     = -1.;
	drjm     = -1.;
	detaje   = -1.;
	detajm   = -1.;
	dphiemet = -1.;
	dphimmet = -1.;

	weight = -1.;
}

tionscadaldorcha::TionscadalDorchaTreeProducer::~TionscadalDorchaTreeProducer()
{
	if(var_calc){delete var_calc; var_calc=NULL;}
}

void tionscadaldorcha::TionscadalDorchaTreeProducer::book_branches()
{
	tree->Branch("jet1_pt",&jet1_pt, "jet1_pt/F");
	tree->Branch("jet2_pt",&jet2_pt, "jet2_pt/F");
	tree->Branch("jet3_pt",&jet3_pt, "jet3_pt/F");
	tree->Branch("jet4_pt",&jet4_pt, "jet4_pt/F");
	tree->Branch("jet1_eta",&jet1_eta, "jet1_eta/F");
	tree->Branch("jet2_eta",&jet2_eta, "jet2_eta/F");
	tree->Branch("jet3_eta",&jet3_eta, "jet3_eta/F");
	tree->Branch("jet4_eta",&jet4_eta, "jet4_eta/F");

	tree->Branch("njets",&njets, "njets/F");

	tree->Branch("mu1_pt",&mu1_pt, "mu1_pt/F");
	tree->Branch("mu1_eta",&mu1_eta, "mu1_eta/F");
	tree->Branch("mu1_abseta_x_charge",&mu1_abseta_x_charge, "mu1_abseta_x_charge/F");
	tree->Branch("mu1_charge",&mu1_charge, "mu1_charge/F");
	//tree->Branch("loose_mu1_pt",&loose_mu1_pt, "loose_mu1_pt/F");
	//tree->Branch("loose_mu1_eta",&loose_mu1_eta, "loose_mu1_eta/F");

	tree->Branch("e1_pt",&e1_pt, "e1_pt/F");
	tree->Branch("e1_eta",&e1_eta, "e1_eta/F");
	tree->Branch("e1_abseta_x_charge",&e1_abseta_x_charge, "e1_abseta_x_charge/F");
	tree->Branch("e1_charge",&e1_charge, "e1_charge/F");
	//tree->Branch("loose_e1_pt",&loose_e1_pt, "loose_e1_pt/F");
	//tree->Branch("loose_e1_eta",&loose_e1_eta, "loose_e1_eta/F");

	tree->Branch("aplanarity",&aplanarity, "aplanarity/F");
	tree->Branch("sphericity",&sphericity, "sphericity/F");
	tree->Branch("HlT",&HlT, "HlT/F");
	tree->Branch("HT",&HT, "HT/F");
	tree->Branch("H3T",&H3T, "H3T/F");
	tree->Branch("MjetT",&MjetT, "MjetT/F");
	tree->Branch("Mevent",&Mevent, "Mevent/F");
	tree->Branch("Mj2nulT",&Mj2nulT, "Mj2nulT/F");

	//jl 23.03.11
	tree->Branch("MET",&MET,"MET/F");
	tree->Branch("MHT",&MHT,"MHT/F");
        tree->Branch("m3",&m3,"m3/F");
        tree->Branch("drje",&drje,"drje/F");
        tree->Branch("drjm",&drjm,"drjm/F");
        tree->Branch("detaje",&detaje,"detaje/F");
        tree->Branch("detajm",&detajm,"detajm/F");
        tree->Branch("dphiemet",&dphiemet,"dphiemet/F");
        tree->Branch("dphimmet",&dphimmet,"dphimmet/F");
	tree->Branch("mchi2_hadT", &mchi2_hadT, "mchi2_hadT/F");
	tree->Branch("mchi2_lepT", &mchi2_lepT, "mchi2_lepT/F");
	tree->Branch("mchi2_prob", &mchi2_prob, "mchi2_prob/F");
	tree->Branch("mchi2_dm", &mchi2_dm, "mchi2_dm/F");

        tree->Branch("weight",&weight,"weight/F");
}

bool tionscadaldorcha::TionscadalDorchaTreeProducer::fill_branches()
{
        std::vector<mor::Muon> *isomu = handle_holder->get_tight_muons();
        std::vector<mor::Electron> *isoe = handle_holder->get_tight_electrons();

        std::vector<mor::Jet>  *jets = handle_holder->get_tight_jets();
        std::vector<mor::MET>  *met = handle_holder->get_selected_mets();

	if(jets->size() > 0){jet1_pt = (*jets)[0].pt(); jet1_eta = (*jets)[0].eta();}
	if(jets->size() > 1){jet2_pt = (*jets)[1].pt(); jet2_eta = (*jets)[1].eta();}
	if(jets->size() > 2){jet3_pt = (*jets)[2].pt(); jet3_eta = (*jets)[2].eta();}
	if(jets->size() > 3){jet4_pt = (*jets)[3].pt(); jet4_eta = (*jets)[3].eta();}
	this->njets = jets->size();

        //      Fill muon variables
	if(isomu->size() > 0){
        	mu1_pt = (*isomu)[0].pt();  
        	mu1_eta = (*isomu)[0].eta();
        	mu1_abseta_x_charge = std::abs((*isomu)[0].eta())*(*isomu)[0].charge();
        	mu1_charge = (*isomu)[0].charge();
	}

        m3 = tmass->calculate_M3();

	drjm     = var_calc->get_drjl<mor::Muon>(isomu, jets);
	detajm   = var_calc->get_detajl<mor::Muon>(isomu, jets);
	dphimmet = var_calc->get_dphilmet<mor::Muon>(isomu);

	evt_shape->calculate_variables();

	aplanarity = evt_shape->get_aplanarity();
	sphericity = evt_shape->get_sphericity();
	Mevent = evt_shape->get_Mevent();
	Mj2nulT = evt_shape->get_Mj2nulT();
	HlT = evt_shape->get_HlT();
	H3T = evt_shape->get_H3T();
	HT = ht_calc->get_ht();
	MHT = ht_calc->get_missing_ht();

	/*
	 *	Fill electron variables
	 */
	if(isoe->size() > 0){
		e1_eta = (*isoe)[0].eta();	
		e1_pt = (*isoe)[0].pt();	
        	e1_abseta_x_charge = std::abs((*isoe)[0].eta())*(*isoe)[0].charge();
        	e1_charge = (*isoe)[0].charge();
	}

	MET = (met->begin())->pt();

	drje     = var_calc->get_drjl<mor::Electron>(isoe, jets);
	detaje   = var_calc->get_detajl<mor::Electron>(isoe, jets);
	dphiemet = var_calc->get_dphilmet<mor::Electron>(isoe);

	mchi2_hadT = tmass->calculate_chihadTmass();
	mchi2_lepT = tmass->calculate_chilepTmass();
	mchi2_prob = 1-TMath::Prob(tmass->get_chi2(),3);
	mchi2_dm = fabs(mchi2_hadT-mchi2_lepT);

	weight = handle_holder->get_event_weight();

	tree->Fill();

	return 0;
}
