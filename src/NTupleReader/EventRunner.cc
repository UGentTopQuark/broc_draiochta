#include "../../interface/NTupleReader/EventRunner.h"

template void EventRunner::assign_collection<beag::TriggerObject>(std::vector<beag::TriggerObject> *&collection, std::string branch_name);
template void EventRunner::assign_collection<beag::Jet>(std::vector<beag::Jet> *&collection, std::string branch_name);
template void EventRunner::assign_collection<beag::MET>(std::vector<beag::MET> *&collection, std::string branch_name);
template void EventRunner::assign_collection<beag::Electron>(std::vector<beag::Electron> *&collection, std::string branch_name);
template void EventRunner::assign_collection<beag::Muon>(std::vector<beag::Muon> *&collection, std::string branch_name);
template void EventRunner::assign_collection<beag::TTbarGenEvent>(std::vector<beag::TTbarGenEvent> *&collection, std::string branch_name);
template void EventRunner::assign_collection<beag::Trigger>(std::vector<beag::Trigger> *&collection, std::string branch_name);
template void EventRunner::assign_collection<beag::PrimaryVertex>(std::vector<beag::PrimaryVertex> *&collection, std::string branch_name);
template void EventRunner::assign_collection<beag::EventInformation>(std::vector<beag::EventInformation> *&collection, std::string branch_name);
template void EventRunner::assign_collection<beag::Particle>(std::vector<beag::Particle> *&collection, std::string branch_name);

EventRunner::EventRunner()
{
        file_runner = new FileRunner();
        current_file_event = 0;
        current_file_max_events = 0;
        overall_current_event = 0;
        new_file = true;
	empty_file_ = false;
}

EventRunner::~EventRunner()
{
}

void EventRunner::set_file_names(std::vector<std::string> *file_names)
{
        file_runner->set_file_names(file_names);
}

FileRunner* EventRunner::get_file_runner()
{
	return file_runner;
}

template <class beagObj>
void EventRunner::assign_collection(typename std::vector<beagObj> *&collection, std::string branch_name)
{
        if(new_file){
		current_file_max_events = 0;
		empty_file_ = false;
		while(current_file_max_events == 0 && file_runner->has_next()){
                	tree = file_runner->get_next_tree("tree");
                	file_runner->cd_infile();
                	current_file_max_events = tree->GetEntries();
		}
                if(verbose) std::cout << "entries in tree: " << current_file_max_events << std::endl;
                current_file_event = 0;
                new_file = false;

		if(current_file_max_events == 0) empty_file_ = true;
        }

	if(!empty_file_) tree->SetBranchAddress(branch_name.c_str(),&collection);
}

bool EventRunner::has_next()
{
        if((current_file_event < current_file_max_events) || 
                file_runner->has_next())
                return true;
        else
                return false;
}

bool EventRunner::end_of_file()
{
        if(current_file_event < current_file_max_events)
                return false;
        else{
                new_file = true;
                return true;
        }
}

void EventRunner::next_event()
{
        if((current_file_event < current_file_max_events)){
                tree->GetEntry(current_file_event);
                ++current_file_event;
                ++overall_current_event;
        }
}

int EventRunner::current_event()
{
        return overall_current_event;
}
