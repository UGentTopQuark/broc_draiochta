#include "../../interface/NTupleReader/MIDChecker.h"

int mor::IDChecker::error_counter = 0;

template double mor::IDChecker::id_value(std::string ID_name, std::vector<double> *id_values);
template std::vector<double> mor::IDChecker::id_value(std::string ID_name, std::vector<std::vector<double> > *id_values);

template unsigned int mor::IDChecker::id_value(std::string ID_name, std::vector<unsigned int> *id_values);
template std::vector<unsigned int> mor::IDChecker::id_value(std::string ID_name, std::vector<std::vector<unsigned int> > *id_values);

mor::IDChecker::IDChecker()
{
	names_vec = NULL;
}

mor::IDChecker::~IDChecker()
{
}

bool mor::IDChecker::id_passed(std::string ID_name, unsigned long long lepton_id)
{
	if(id_map.find(ID_name) != id_map.end()){
		return (lepton_id & ((unsigned long long) 1 << (unsigned long long) id_map[ID_name]));
	}
	else{
		print_id_not_available(ID_name);
		return false;
	}
}

bool mor::IDChecker::id_passed(std::string ID_name, std::vector<bool> lepton_id)
{
	if(id_map.find(ID_name) != id_map.end()){
		return lepton_id[id_map[ID_name]];
	}
	else{
		print_id_not_available(ID_name);
		return false;
	}
}

void mor::IDChecker::print_id_not_available(std::string ID_name)
{
	if(error_counter == 0 || error_counter % 100000 == 0){
		std::cerr << "WARNING: mor::IDChecker::id_passed(): id not available: " << ID_name << " (message encountered " << error_counter << " times) " << std::endl;
		if(error_counter == 0){
			std::cerr << "WARNING: mor::IDChecker::id_passed(): id not available: " << ID_name << std::endl;
			if(error_counter == 0){
				std::cout << "Available ids: " <<std::endl;
				for(std::map<std::string,  unsigned long long>::iterator id = id_map.begin();id != id_map.end();
				    id++){
					std::cout << id->first << std::endl;
				}		
			}
		}
	}
	++error_counter;
}

bool mor::IDChecker::id_exists(std::string ID_name)
{
	if(id_map.find(ID_name) != id_map.end())
		return true;
	else
		return false;
}

template <class TYPE>
TYPE mor::IDChecker::id_value(std::string ID_name, std::vector<TYPE> *id_values)
{
	if(id_map.find(ID_name) != id_map.end()){
		return (*id_values)[id_map[ID_name]];
	}
	else{
		std::cerr << "WARNING: mor::IDChecker::id_value(): id value not available: " << ID_name << std::endl;
		exit(1);
		return false;
	}
}

template <class TYPE>
std::vector<TYPE> mor::IDChecker::id_value(std::string ID_name, std::vector<std::vector<TYPE> > *id_values)
{
	if(id_map.find(ID_name) != id_map.end()){
		return (*id_values)[id_map[ID_name]];
	}
	else{
		std::cerr << "WARNING: mor::IDChecker::id_value(): id value not available: " << ID_name << std::endl;
		exit(1);
		return std::vector<TYPE>();
	}
}

int mor::IDChecker::get_index(std::string ID_name)
{
	if(id_map.find(ID_name) != id_map.end()){
		return id_map[ID_name];
	}else{
		print_id_not_available(ID_name);
		return -1;
	}
}

void mor::IDChecker::set_id_vector(std::vector<std::string> *names_vec)
{
	this->names_vec = names_vec;
	id_map.clear();
	unsigned long long nid = 0;
	if(verbose) std::cout << "setting IDs..." << std::endl;
	if(verbose) std::cout << "----------------" << std::endl;
	for(std::vector<std::string>::iterator name = names_vec->begin();
		name != names_vec->end();
		++name){
		id_map[*name] = nid;
		if(verbose) std::cout << "adding ID: " << *name << " and assigning: " << nid << std::endl;
		++nid;
	}
	if(verbose) std::cout << "----------------" << std::endl;
}
