/**
    @file RootFourVectorHitFitTranslator

    @brief Translator for four-vector classes in ROOT,
    Contains specific implementation for the following classes:
    - LeptonTranslatorBase<math::XYZTLorentzVector>
    - JetTranslatorBase<math::XYZTLorenzVector>
    - METTranslatorBase<math::XYZTLorenzVector>

    @author Haryo Sumowidagdo <Suharyo.Sumowidagdo@cern.ch>

    @date Wed Aug 24 17:30:34 CEST 2011

    @version $Id: RootFourVectorHitFitTranslator.cpp,v 1.1 2011/08/24 21:31:11 haryo Exp $
 */

#include <cstdlib>
#include <DataFormats/Math/interface/LorentzVectorFwd.h>

#include <TopQuarkAnalysis/HitFit/interface/LeptonTranslatorBase.hpp>
#include <TopQuarkAnalysis/HitFit/interface/JetTranslatorBase.hpp>
#include <TopQuarkAnalysis/HitFit/interface/METTranslatorBase.hpp>

namespace hitfit {

template<>
LeptonTranslatorBase<math::XYZTLorentzVector>::LeptonTranslatorBase()
{

    std::string CMSSW_BASE(getenv("CMSSW_BASE"));
    std::string resolution_filename = CMSSW_BASE +
        std::string("/src/TopQuarkAnalysis/HitFit/data/exampleElectronResolution.txt");
    resolution_ = EtaDepResolution(resolution_filename);

} // LeptonTranslatorBase<math::XYZTLorentzVector>::LeptonTranslatorBase()


template<>
LeptonTranslatorBase<math::XYZTLorentzVector>::LeptonTranslatorBase(const std::string& ifile)
{

    std::string CMSSW_BASE(getenv("CMSSW_BASE"));
    std::string resolution_filename;

    if (ifile.empty()) {
        resolution_filename = CMSSW_BASE +
        std::string("/src/TopQuarkAnalysis/HitFit/data/exampleElectronResolution.txt");
    } else {
        resolution_filename = ifile ;
    }

    resolution_ = EtaDepResolution(resolution_filename);

} // LeptonTranslatorBase<math::XYZTLorentzVector>::LeptonTranslatorBase(const std::string& ifile)


template<>
LeptonTranslatorBase<math::XYZTLorentzVector>::~LeptonTranslatorBase()
{
}


template<>
Lepjets_Event_Lep
LeptonTranslatorBase<math::XYZTLorentzVector>::operator()(const math::XYZTLorentzVector& lepton,
                                                          int type /* = hitfit::lepton_label */,
                                                          bool useObjEmbRes /* = false */)
{

    Fourvec p(lepton.px(),lepton.py(),lepton.pz(),lepton.e());

    double lepton_eta(lepton.eta());
    Vector_Resolution lepton_resolution = resolution_.GetResolution(lepton_eta);
    Lepjets_Event_Lep retlep(p,
                             type,
                             lepton_resolution);
    return retlep;

} // Lepjets_Event_Lep LeptonTranslatorBase<math::XYZTLorentzVector>::operator()(const math::XYZTLorentzVector& lepton)


template<>
const EtaDepResolution&
LeptonTranslatorBase<math::XYZTLorentzVector>::resolution() const
{
    return resolution_;
}


template<>
bool
LeptonTranslatorBase<math::XYZTLorentzVector>::CheckEta(const math::XYZTLorentzVector& lepton) const
{
    return resolution_.CheckEta(lepton.eta());
}


template<>
JetTranslatorBase<math::XYZTLorentzVector>::JetTranslatorBase()
{

    std::string CMSSW_BASE(getenv("CMSSW_BASE"));
    std::string udsc_resolution_filename = CMSSW_BASE +
        std::string("/src/TopQuarkAnalysis/HitFit/data/exampleJetResolution.txt");
    std::string b_resolution_filename = udsc_resolution_filename;

    udscResolution_ = EtaDepResolution(udsc_resolution_filename);
    bResolution_    = EtaDepResolution(b_resolution_filename);

} // JetTranslatorBase<math::XYZTLorentzVector>::JetTranslatorBase()


template<>
JetTranslatorBase<math::XYZTLorentzVector>::JetTranslatorBase(const std::string& udscFile,
                                                              const std::string& bFile)
{

    std::string CMSSW_BASE(getenv("CMSSW_BASE"));
    std::string udsc_resolution_filename;
    std::string b_resolution_filename;

    if (udscFile.empty()) {
        udsc_resolution_filename = CMSSW_BASE +
            std::string("/src/TopQuarkAnalysis/HitFit/data/exampleJetResolution.txt");
    } else {
        udsc_resolution_filename = udscFile;
    }

    if (bFile.empty()) {
        b_resolution_filename = CMSSW_BASE +
            std::string("/src/TopQuarkAnalysis/HitFit/data/exampleJetResolution.txt");
    } else {
        b_resolution_filename = bFile;
    }

    udscResolution_ = EtaDepResolution(udsc_resolution_filename);
    bResolution_    = EtaDepResolution(b_resolution_filename);

} // JetTranslatorBase<math::XYZTLorentzVector>::JetTranslatorBase(const std::string& udscFile,const std::string& bFile)


template<>
JetTranslatorBase<math::XYZTLorentzVector>::~JetTranslatorBase()
{
} // JetTranslatorBase<math::XYZTLorentzVector>::~JetTranslatorBase()


template<>
Lepjets_Event_Jet
JetTranslatorBase<math::XYZTLorentzVector>::operator()(const math::XYZTLorentzVector& jet,
                                                       int type /* = hitfit::unknown_label */,
                                                       bool useObjEmbRes /* = false */)
{

    Fourvec p(jet.px(),jet.py(),jet.pz(),jet.e());

    double            jet_eta        = jet.eta();
    Vector_Resolution jet_resolution;

    if (type == hitfit::lepb_label||type == hitfit::hadb_label||type== hitfit::higgs_label) {
        jet_resolution = bResolution_.GetResolution(jet_eta);
    } else {
        jet_resolution = udscResolution_.GetResolution(jet_eta);
    }

    Lepjets_Event_Jet retjet(p,
                             type,
                             jet_resolution);
    return retjet;

} // Lepjets_Event_Jet JetTranslatorBase<math::XYZTLorentzVector>::operator()(const math::XYZTLorentzVector& j,int type)


template<>
const EtaDepResolution&
JetTranslatorBase<math::XYZTLorentzVector>::udscResolution() const
{
    return udscResolution_;
}


template<>
const EtaDepResolution&
JetTranslatorBase<math::XYZTLorentzVector>::bResolution() const
{
    return bResolution_;
}


template<>
bool
JetTranslatorBase<math::XYZTLorentzVector>::CheckEta(const math::XYZTLorentzVector& jet) const
{
    return udscResolution_.CheckEta(jet.eta()) && bResolution_.CheckEta(jet.eta());
}


template<>
METTranslatorBase<math::XYZTLorentzVector>::METTranslatorBase()
{
    resolution_ = Resolution(std::string("0,0,12"));
} // METTranslatorBase<math::XYZTLorentzVector>::METTranslatorBase()


template<>
METTranslatorBase<math::XYZTLorentzVector>::METTranslatorBase(const std::string& ifile)
{
    const Defaults_Text defs(ifile);
    std::string resolution_string(defs.get_string("met_resolution"));
    resolution_ = Resolution(resolution_string);

} // METTranslatorBase<math::XYZTLorentzVector>::METTranslatorBase(const std::string& ifile)


template<>
METTranslatorBase<math::XYZTLorentzVector>::~METTranslatorBase()
{
} // METTranslatorBase<math::XYZTLorentzVector>::~METTranslatorBase()


template<>
Fourvec
METTranslatorBase<math::XYZTLorentzVector>::operator()(const math::XYZTLorentzVector& m,
                                                       bool useObjEmbRes /* = false */)
{

    double e = sqrt(m.px()*m.px() + m.py()*m.py());
    return Fourvec (m.px(),m.py(),0.0,e);

} // Fourvec METTranslatorBase<math::XYZTLorentzVector>::operator()(const math::XYZTLorentzVector& m)



template<>
Resolution
METTranslatorBase<math::XYZTLorentzVector>::KtResolution(const math::XYZTLorentzVector& m,
                                                         bool useObjEmbRes /* = false */) const
{
    return resolution_;
} // Resolution METTranslatorBase<math::XYZTLorentzVector>::KtResolution(const math::XYZTLorentzVector& m)



template<>
Resolution
METTranslatorBase<math::XYZTLorentzVector>::METResolution(const math::XYZTLorentzVector& m,
                                                          bool useObjEmbRes /* = false */) const
{
    return KtResolution(m,useObjEmbRes);
} // Resolution METTranslatorBase<math::XYZTLorentzVector>::METResolution(const math::XYZTLorentzVector& m)

}
