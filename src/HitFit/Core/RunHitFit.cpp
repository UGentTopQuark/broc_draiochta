//
//     $Id: RunHitFit.cpp,v 1.10 2010/07/30 22:06:01 haryo Exp $
//
// Package: TopQuarkAnalysis/HitFit
// File   : src/RunHitFit.cpp
// Author : Haryo Sumowidagdo <Suharyo.Sumowidagdo@cern.ch>
// Purpose: Template class of experiment-independent wrapper class to HitFit.
//          This file is a dummy source file to enforce the build system
//          to compile RunHitFit.hpp

/**
    @file RunHitFit.cpp

    @brief Dummy source file to enforce compilation of RunHitFit class.

    @author Haryo Sumowidagdo <Suharyo.Sumowidagdo@cern.ch>

    @par Creation date:
    May 2009.

    @par Modification History:
    Nov 2009: Haryo Sumowidagdo <Suharyo.Sumowidagdo@cern.ch>:
    Add doxygen tags for automatic generation of documentation.
 */

#include "../../../interface/HitFit/Core/RunHitFit.hpp"
