/**
    @file MorJetHitFitTranslator.cc

    @brief Specialization of template class JetTranslatorBase in the
    package HitFit

    @author Haryo Sumowidagdo <Suharyo.Sumowidagdo@cern.ch>

    @par Created
    Sat Jun 27 17:49:21 2009 UTC

    @version $Id: MorJetHitFitTranslator.cc,v 1.11 2011/01/26 16:07:59 haryo Exp $
 */


#include "../../../interface/HitFit/Core/JetTranslatorBase.hpp"
#include "../../../interface/MorObjects/MJet.h"

namespace hitfit {


template<>
JetTranslatorBase<mor::Jet>::JetTranslatorBase()
{

}


template<>
JetTranslatorBase<mor::Jet>::JetTranslatorBase(const std::string& udscFile,
                                               const std::string& bFile)
{
    std::string udscResolution_filename;
    std::string bResolution_filename;
    
    udscResolution_filename = udscFile;
    bResolution_filename = bFile;
    
    
    udscResolution_ = EtaDepResolution(udscResolution_filename);
    bResolution_    = EtaDepResolution(bResolution_filename);

} // JetTranslatorBase<mor::Jet>::JetTranslatorBase(const std::string& ifile)

template<>
JetTranslatorBase<mor::Jet>::JetTranslatorBase(const std::string& udscFile,
					       const std::string& bFile,
					       const std::string& jetCorrectionLevel,
					       double jes,
					       double jesB)
{

    std::string udscResolution_filename;
    std::string bResolution_filename;

    udscResolution_filename = udscFile;
    bResolution_filename = bFile;
    

    udscResolution_ = EtaDepResolution(udscResolution_filename);
    bResolution_    = EtaDepResolution(bResolution_filename);
    jetCorrectionLevel_ = jetCorrectionLevel;
    jes_            = jes;
    jesB_           = jesB;

} 


template<>
JetTranslatorBase<mor::Jet>::~JetTranslatorBase()
{
} // JetTranslatorBase<mor::Jet>::~JetTranslatorBase()


template<>
Lepjets_Event_Jet
JetTranslatorBase<mor::Jet>::operator()(const mor::Jet& jet,
                                        int type /*= hitfit::unknown_label */,
                                        bool useObjEmbRes /* = false */)
{

    Fourvec p;

    double            jet_eta        = jet.eta();

    //if (jet.isCaloJet()) {
    //    jet_eta = ((reco::CaloJet*) jet.originalObject())->detectorP4().eta();
    //}
    //if (jet.isPFJet()) {
        // do nothing at the moment!
    //}

    Vector_Resolution jet_resolution;

    if (type == hitfit::hadb_label || type == hitfit::lepb_label || type == hitfit::higgs_label) {
        jet_resolution = bResolution_.GetResolution(jet_eta);
        //mor::Jet bPartonCorrJet((*(const_cast<mor::Jet*>(&jet))).corrected_jets("L7Parton","BOTTOM"));
        p = Fourvec(jet.px(),jet.py(),jet.pz(),jet.energy());

    } else {
        jet_resolution = udscResolution_.GetResolution(jet_eta);
        //mor::Jet udsPartonCorrJet(jet.corrected_jets("L7Parton","UDS"));
        p = Fourvec(jet.px(),jet.py(),jet.pz(),jet.energy());
    }



    Lepjets_Event_Jet retjet(p,
                             type,
                             jet_resolution);
    return retjet;

} // Lepjets_Event_Jet JetTranslatorBase<mor::Jet>::operator()(const mor::Jet& j,int type)


template<>
const EtaDepResolution&
JetTranslatorBase<mor::Jet>::udscResolution() const
{
    return udscResolution_;
}


template<>
const EtaDepResolution&
JetTranslatorBase<mor::Jet>::bResolution() const
{
    return bResolution_;
}


template<>
bool
JetTranslatorBase<mor::Jet>::CheckEta(const mor::Jet& jet) const
{
    double            jet_eta        = jet.eta();

    //if (jet.isCaloJet()) {
    //    jet_eta = ((reco::CaloJet*) jet.originalObject())->detectorP4().eta();
    //}
    //if (jet.isPFJet()) {
        // do nothing at the moment!
    //}
    return bResolution_.CheckEta(jet_eta) && udscResolution_.CheckEta(jet_eta);
}


    //

} // namespace hitfit
