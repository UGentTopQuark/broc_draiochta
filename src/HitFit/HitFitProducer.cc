#include "../../interface/HitFit/HitFitProducer.h"

using namespace hitfit;
const Int_t HitFitProducer::maxNCombStatic;
HitFitProducer::HitFitProducer(eire::HandleHolder *handle_holder)
{
	eire::ConfigReader *config_reader = handle_holder->get_config_reader();
	eire::ConfigReader *hitfit_config_reader = new eire::ConfigReader();
	std::string hitfit_config_file = config_reader->get_var("hitfit_config_file", "hitfit", false);
	//std::cout<<"hitfit_config_file "<<hitfit_config_file<<std::endl;
	use_mc_truth_seed = config_reader->get_bool_var("use_mc_truth_seed", "hitfit", false);
	hitfit_config_reader->read_config_from_file(hitfit_config_file, true);

	this->gen_evt = handle_holder->get_ttbar_gen_evt();
	this->handle_holder = handle_holder;
	this->event_information = handle_holder->get_event_information();

	chisq_cut = 0.0;
	bool verbose = false;

	// Configuration parameters taken from specified hitfit_config_file
	// This is taken from the default config of the broc

	maxNJets_             =  atoi(hitfit_config_reader->get_var("maxNJets", "mass_studies", false).c_str()); // int   //4
	maxNComb_             =  atoi(hitfit_config_reader->get_var("maxNComb", "mass_studies",false).c_str()); // int   //1
	bTagAlgo_             =  hitfit_config_reader->get_var("bTagAlgo","mass_studies", false);  // string
	minBTagValueBJet_     =  atof(hitfit_config_reader->get_var("minBDiscBJets","mass_studies", false).c_str()); // double
	maxBTagValueNonBJet_  =  atof(hitfit_config_reader->get_var("maxBDiscLightJets","mass_studies", false).c_str()); //double
	useBTag_              =  hitfit_config_reader->get_bool_var("useBTagging","mass_studies", false); // bool // false
	mW_                   =  atof(hitfit_config_reader->get_var("mW","mass_studies", false).c_str()); // double // 80.4
	mTop_                 =  atof(hitfit_config_reader->get_var("mTop","mass_studies", false).c_str()); // double // 172
	jetCorrectionLevel_   =  hitfit_config_reader->get_var("jetCorrectionLevel","mass_studies", false); // string  // L3
	jes_                  =  atof(hitfit_config_reader->get_var("jes","mass_studies", false).c_str()); // double  // 1? //
	jesB_                 =  atof(hitfit_config_reader->get_var("jesB","mass_studies", false).c_str()); // double // 1? //
                                                                                                                                                                                        
	hitfitDefault_ =  hitfit_config_reader->get_var("hitfitDefault","mass_studies", false);
	//	std::cout<<"hitfitDefault_"<<hitfitDefault_<<std::endl;
	hitfitElectronResolution_ = hitfit_config_reader->get_var("hitfitElectronResolution","mass_studies", false);
	hitfitMuonResolution_ = hitfit_config_reader->get_var("hitfitMuonResolution","mass_studies", false);
	hitfitUdscJetResolution_ = hitfit_config_reader->get_var("hitfitUdscResolution","mass_studies", false);
	hitfitBJetResolution_ = hitfit_config_reader->get_var("hitfitBJetResolution","mass_studies", false);
	hitfitMETResolution_ = hitfit_config_reader->get_var("hitfitMETResolution","mass_studies", false);

	// The following four initializers instantiate the translator between PAT objects                                                                                               
	// and HitFit objects using the ASCII text files which contains the resolutions.                                                                                                
	electronTranslator_ = hitfit::LeptonTranslatorBase<mor::Electron>(hitfitElectronResolution_);
	muonTranslator_     = hitfit::LeptonTranslatorBase<mor::Muon>(hitfitMuonResolution_);
	jetTranslator_      = hitfit::JetTranslatorBase<mor::Jet>(hitfitUdscJetResolution_, hitfitBJetResolution_, jetCorrectionLevel_, jes_, jesB_);
	metTranslator_      = hitfit::METTranslatorBase<mor::MET>(hitfitMETResolution_);

	// Define the object collections

	jets = handle_holder->get_tight_jets();
	original_jets = handle_holder->get_tight_jets();
	muons = handle_holder->get_tight_muons();
	electrons = handle_holder->get_tight_electrons();
	mets = handle_holder->get_mets();

	hitfit_jets = new std::vector<mor::Particle>;
	hitfit_electrons = new std::vector<mor::Particle>;
	hitfit_muons = new std::vector<mor::Particle>;
	hitfit_mets = new std::vector<mor::Particle>;
	//gen_evt = handle_holder->get_ttbar_gen_evt();

	// Create an instance of RunHitFit and initialize it.
                
	HitFit = new BrocHitFit(electronTranslator_,
				       muonTranslator_,
				       jetTranslator_,
				       metTranslator_,
				       hitfitDefault_,
				       mW_,
				       mW_,
				       mTop_);

	// Initial Hitfit cuts (in addition to broc selection) on detector objects, to ensure added collections have kinematics
	// within the areas of the transfer functions.
	
	maxEtaMu_  = 2.1;
	maxEtaEle_ = 2.5;
	maxEtaJet_ = 2.5;

	if(verbose){
		std::cout << std::endl;
		std::cout << "+++++++++++++     HitFitProducer    ++++++++++++" << std::endl;
		std::cout << " the following additional cuts are applied on   " << std::endl; 
		std::cout << " selected leptons and jets by the HitFitProducer" << std::endl; 
		std::cout << " |eta(muons    )| <= " << maxEtaMu_  << "       " << std::endl;
		std::cout << " |eta(electrons)| <= " << maxEtaEle_ << "       " << std::endl;
		std::cout << " |eta(jets     )| <= " << maxEtaJet_ << "       " << std::endl;
		std::cout << " Fit chisq        <= " << chisq_cut  << "       " << std::endl;
		std::cout << "++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
	}
}


HitFitProducer::~HitFitProducer()
{
	if(hitfit_jets){delete hitfit_jets; hitfit_jets = NULL;}
	if(hitfit_electrons){delete hitfit_electrons; hitfit_electrons = NULL;}
	if(hitfit_muons){delete hitfit_muons; hitfit_muons = NULL;}
	if(hitfit_mets){delete hitfit_mets; hitfit_mets = NULL;}
}

void HitFitProducer::book_branches()
{
	// Book the branches inside the tree (defined in the HitFitManager)

	tree->Branch("nSol",&fitResult.nSol, "nSol/i");
	tree->Branch("maxSol",&maxNComb_, "maxNComb_/i");

	tree->Branch("HadP_pt",fitResult.HadP_pt, "HadP_pt[maxNComb_]/D");
	tree->Branch("HadP_eta",fitResult.HadP_eta, "HadP_eta[maxNComb_]/D");
	tree->Branch("HadP_phi",fitResult.HadP_phi, "HadP_phi[maxNComb_]/D");
	tree->Branch("HadP_m",fitResult.HadP_m, "HadP_m[maxNComb_]/D");

	tree->Branch("HadB_pt",fitResult.HadB_pt, "HadB_pt[maxNComb_]/D");
	tree->Branch("HadB_eta",fitResult.HadB_eta, "HadB_eta[maxNComb_]/D");
	tree->Branch("HadB_phi",fitResult.HadB_phi, "HadB_phi[maxNComb_]/D");
	tree->Branch("HadB_m",fitResult.HadB_m, "HadB_m[maxNComb_]/D");

	tree->Branch("HadQ_pt",fitResult.HadQ_pt, "HadQ_pt[maxNComb_]/D");
	tree->Branch("HadQ_eta",fitResult.HadQ_eta, "HadQ_eta[maxNComb_]/D");
	tree->Branch("HadQ_phi",fitResult.HadQ_phi, "HadQ_phi[maxNComb_]/D");
	tree->Branch("HadQ_m",fitResult.HadQ_m, "HadQ_m[maxNComb_]/D");

	tree->Branch("LepB_pt",fitResult.LepB_pt, "LepB_pt[maxNComb_]/D");
	tree->Branch("LepB_eta",fitResult.LepB_eta, "LepB_eta[maxNComb_]/D");
	tree->Branch("LepB_phi",fitResult.LepB_phi, "LepB_phi[maxNComb_]/D");
	tree->Branch("LepB_m",fitResult.LepB_m, "LepB_m[maxNComb_]/D");

	tree->Branch("LepL_pt",fitResult.LepL_pt, "LepL_pt[maxNComb_]/D");
	tree->Branch("LepL_eta",fitResult.LepL_eta, "LepL_eta[maxNComb_]/D");
	tree->Branch("LepL_phi",fitResult.LepL_phi, "LepL_phi[maxNComb_]/D");
	tree->Branch("LepL_m",fitResult.LepL_m, "LepL_m[maxNComb_]/D");

	tree->Branch("LepN_pt",fitResult.LepN_pt, "LepN_pt[maxNComb_]/D");
	tree->Branch("LepN_eta",fitResult.LepN_eta, "LepN_eta[maxNComb_]/D");
	tree->Branch("LepN_phi",fitResult.LepN_phi, "LepN_phi[maxNComb_]/D");
	tree->Branch("LepN_m",fitResult.LepN_m, "LepN_m[maxNComb_]/D");

	tree->Branch("Combi_LightQ",fitResult.Combi_LightQ, "Combi_LightQ[maxNComb_]/I");
	tree->Branch("Combi_LightQBar",fitResult.Combi_LightQBar, "Combi_LightQBar[maxNComb_]/I");
	tree->Branch("Combi_HadB",fitResult.Combi_HadB, "Combi_HadB[maxNComb_]/I");
	tree->Branch("Combi_LepB",fitResult.Combi_LepB, "Combi_LepB[maxNComb_]/I");

	tree->Branch("Chi2",fitResult.Chi2, "Chi2[maxNComb_]/D");
	tree->Branch("Prob",fitResult.Prob, "Prob[maxNComb_]/D");
	tree->Branch("MT",fitResult.MT, "MT[maxNComb_]/D");
	tree->Branch("SigMT",fitResult.SigMT, "SigMT[maxNComb_]/D");
	tree->Branch("Status",fitResult.Status, "Status[maxNComb_]/I");

	tree->Branch("mc_Combi",mc_Combi, "mc_Combi[4]/I");
	tree->Branch("hitfit_state",&hitfit_state, "hitfit_state/I");
	tree->Branch("mc_JetPermutation_match",fitResult.matched, "matched[maxNComb_]/I");
	
	tree->Branch("mc_HadB_pt",&mc_HadB_pt, "mc_HadB_pt/D");
	tree->Branch("mc_HadB_eta",&mc_HadB_eta, "mc_HadB_eta/D");
	tree->Branch("mc_HadB_phi",&mc_HadB_phi, "mc_HadB_phi/D");

	tree->Branch("mc_LepB_pt",&mc_LepB_pt, "mc_LepB_pt/D");
	tree->Branch("mc_LepB_eta",&mc_LepB_eta, "mc_LepB_eta/D");
	tree->Branch("mc_LepB_phi",&mc_LepB_phi, "mc_LepB_phi/D");

	tree->Branch("mc_Q_pt",&mc_Q_pt, "mc_Q_pt/D");
	tree->Branch("mc_Q_eta",&mc_Q_eta, "mc_Q_eta/D");
	tree->Branch("mc_Q_phi",&mc_Q_phi, "mc_Q_phi/D");

	tree->Branch("mc_QBar_pt",&mc_QBar_pt, "mc_QBar_pt/D");
	tree->Branch("mc_QBar_eta",&mc_QBar_eta, "mc_QBar_eta/D");
	tree->Branch("mc_QBar_phi",&mc_QBar_phi, "mc_QBar_phi/D");

	tree->Branch("mc_LepL_charge",&mc_LepL_charge, "mc_LepL_charge/I");
	tree->Branch("mc_LepL_pt",&mc_LepL_pt, "mc_LepL_pt/D");
	tree->Branch("mc_LepL_eta",&mc_LepL_eta, "mc_LepL_eta/D");
	tree->Branch("mc_LepL_phi",&mc_LepL_phi, "mc_LepL_phi/D");

	tree->Branch("mc_LepN_pt",&mc_LepN_pt, "mc_LepN_pt/D");
	tree->Branch("mc_LepN_eta",&mc_LepN_eta, "mc_LepN_eta/D");
	tree->Branch("mc_LepN_phi",&mc_LepN_phi, "mc_LepN_phi/D");
	
	tree->Branch("event_number",&event_number, "event_number/D");
	tree->Branch("lumi_block",&lumi_block, "lumi_block/D");
	tree->Branch("run",&run, "run/D");
	tree->Branch("event_weight",&event_weight,"event_weight/D");

	tree->Branch("ExtraJet_pt",fitResult.ExtraJet_pt,"ExtraJet_pt[maxNComb_]/D");
	tree->Branch("ExtraJet_eta",fitResult.ExtraJet_eta,"ExtraJet_eta[maxNComb_]/D");
	tree->Branch("ExtraJet_phi",fitResult.ExtraJet_phi,"ExtraJet_phi[maxNComb_]/D");

	tree->Branch("HadP_pt_orig",fitResult.HadP_pt_orig, "HadP_pt_orig[maxNComb_]/D");
	tree->Branch("HadP_eta_orig",fitResult.HadP_eta_orig, "HadP_eta_orig[maxNComb_]/D");
	tree->Branch("HadP_phi_orig",fitResult.HadP_phi_orig, "HadP_phi_orig[maxNComb_]/D");
	tree->Branch("HadP_m_orig",fitResult.HadP_m_orig, "HadP_m_orig[maxNComb_]/D");

	tree->Branch("HadB_pt_orig",fitResult.HadB_pt_orig, "HadB_pt_orig[maxNComb_]/D");
	tree->Branch("HadB_eta_orig",fitResult.HadB_eta_orig, "HadB_eta_orig[maxNComb_]/D");
	tree->Branch("HadB_phi_orig",fitResult.HadB_phi_orig, "HadB_phi_orig[maxNComb_]/D");
	tree->Branch("HadB_m_orig",fitResult.HadB_m_orig, "HadB_m_orig[maxNComb_]/D");

	tree->Branch("HadQ_pt_orig",fitResult.HadQ_pt_orig, "HadQ_pt_orig[maxNComb_]/D");
	tree->Branch("HadQ_eta_orig",fitResult.HadQ_eta_orig, "HadQ_eta_orig[maxNComb_]/D");
	tree->Branch("HadQ_phi_orig",fitResult.HadQ_phi_orig, "HadQ_phi_orig[maxNComb_]/D");
	tree->Branch("HadQ_m_orig",fitResult.HadQ_m_orig, "HadQ_m_orig[maxNComb_]/D");

	tree->Branch("LepB_pt_orig",fitResult.LepB_pt_orig, "LepB_pt_orig[maxNComb_]/D");
	tree->Branch("LepB_eta_orig",fitResult.LepB_eta_orig, "LepB_eta_orig[maxNComb_]/D");
	tree->Branch("LepB_phi_orig",fitResult.LepB_phi_orig, "LepB_phi_orig[maxNComb_]/D");
	tree->Branch("LepB_m_orig",fitResult.LepB_m_orig, "LepB_m_orig[maxNComb_]/D");

	tree->Branch("LepL_pt_orig",fitResult.LepL_pt_orig, "LepL_pt_orig[maxNComb_]/D");
	tree->Branch("LepL_eta_orig",fitResult.LepL_eta_orig, "LepL_eta_orig[maxNComb_]/D");
	tree->Branch("LepL_phi_orig",fitResult.LepL_phi_orig, "LepL_phi_orig[maxNComb_]/D");
	tree->Branch("LepL_m_orig",fitResult.LepL_m_orig, "LepL_m_orig[maxNComb_]/D");

	tree->Branch("LepN_pt_orig",fitResult.LepN_pt_orig, "LepN_pt_orig[maxNComb_]/D");
	tree->Branch("LepN_eta_orig",fitResult.LepN_eta_orig, "LepN_eta_orig[maxNComb_]/D");
	tree->Branch("LepN_phi_orig",fitResult.LepN_phi_orig, "LepN_phi_orig[maxNComb_]/D");
	tree->Branch("LepN_m_orig",fitResult.LepN_m_orig, "LepN_m_orig[maxNComb_]/D");
}


bool HitFitProducer::fill_branches()
{

  // Fill the event information to the tree

  event_number = event_information->event_number();
  lumi_block = event_information->lumi_block();
  run = event_information->run();
  event_weight = this->handle_holder->get_event_weight();

  // Initial selection: Skip events with no appropriate 
  // lepton candidate or empty MET or less jets than partons.
  // Partons must be within the eta range of the transfer function.
 
  const unsigned int nPartons = 4;

  // Clear the internal state
  HitFit->clear();
  hitfit_state = -1;

  // Add lepton into HitFit
  bool foundLepton = false;
  // Add leptons into HitFit

  mor::Lepton orig_lepton;
  if(!muons->empty()){
	  for(unsigned iLep=0; iLep<(*muons).size() && !foundLepton; ++iLep) {
		  if(std::abs((*muons)[iLep].eta()) <= maxEtaMu_) {
			  mor::Muon muon(*(dynamic_cast<const mor::Muon*>(&((*muons)[0]))));
			  HitFit->AddLepton(muon);
			  orig_lepton = muon;
			  foundLepton = true;
		  }
	  }
  }
  else if(!electrons->empty()){
	  for(unsigned iLep=0; iLep<(*electrons).size() && !foundLepton; ++iLep) {
		  if(std::abs((*electrons)[iLep].eta()) <= maxEtaEle_) {
			  mor::Electron electron(*(dynamic_cast<const mor::Electron*>(&((*electrons)[0]))));
			  HitFit->AddLepton(electron);
			  orig_lepton = electron;
			  foundLepton = true;
		  }
	  }
  }

  // Add jets into HitFit
  int nJetsFound = 0;
  for(unsigned iJet=0; iJet<(*jets).size() && nJetsFound!=maxNJets_; ++iJet) {
	  if(std::abs((*jets)[iJet].eta()) <= maxEtaJet_) {
		  HitFit->AddJet((*jets)[iJet]);
		  nJetsFound++;
	  }
  }

  //uncomment to add missing transverse energy into HitFit
  //warning: adding the missing transverse energy will drastically reduce the fit efficiency
  //if(!mets->empty()){
  //	  HitFit->SetMet((*mets)[0]);
  //}

  hitfit_combi_seed.clear();
  hitfit_combi_seed.push_back(-1);
  hitfit_combi_seed.push_back(-1);
  hitfit_combi_seed.push_back(-1);
  hitfit_combi_seed.push_back(-1);

  hitfit_chisq = -1;

  // Initialisation of all fit results

  fitResult.nSol = 0;

  for(Int_t i=0; i < maxNComb_; ++i) {
	  fitResult.HadB_pt[i] = -1;
	  fitResult.HadB_eta[i] = 0;
	  fitResult.HadB_phi[i] = 0;
	  fitResult.HadB_m[i] = -1;
	  
	  fitResult.HadP_pt[i] = -1;
	  fitResult.HadP_eta[i] = 0;
	  fitResult.HadP_phi[i] = 0;
	  fitResult.HadP_m[i] = -1;
	  
	  fitResult.HadQ_pt[i] = -1;
	  fitResult.HadQ_eta[i] = 0;
	  fitResult.HadQ_phi[i] = 0;
	  fitResult.HadQ_m[i] = -1;
	  
	  fitResult.LepB_pt[i] = -1;
	  fitResult.LepB_eta[i] = 0;
	  fitResult.LepB_phi[i] = 0;
	  fitResult.LepB_m[i] = -1;
	  
	  fitResult.LepL_pt[i] = -1;
	  fitResult.LepL_eta[i] = 0;
	  fitResult.LepL_phi[i] = 0;
	  fitResult.LepL_m[i] = -1;
	  
	  fitResult.LepN_pt[i] = -1;
	  fitResult.LepN_eta[i] = 0;
	  fitResult.LepN_phi[i] = 0;
	  fitResult.LepN_m[i] = -1;
	  
	  fitResult.MT[i] = -1;
	  fitResult.SigMT[i] = -1;
	  fitResult.Prob[i] = -1;
	  fitResult.Chi2[i] = -1;
	  fitResult.Status[i] = -1;
	  
	  fitResult.matched[i] = -1;
	  
	  fitResult.Combi_LightQ[i] = -1;
	  fitResult.Combi_LightQBar[i] = -1;
	  fitResult.Combi_HadB[i] = -1;
	  fitResult.Combi_LepB[i] = -1;

	  fitResult.ExtraJet_pt[i] = -1;
	  fitResult.ExtraJet_eta[i] = 0;
	  fitResult.ExtraJet_phi[i] = 0;

	  fitResult.HadB_pt_orig[i] = -1;
	  fitResult.HadB_eta_orig[i] = 0;
	  fitResult.HadB_phi_orig[i] = 0;
	  fitResult.HadB_m_orig[i] = -1;
	  
	  fitResult.HadP_pt_orig[i] = -1;
	  fitResult.HadP_eta_orig[i] = 0;
	  fitResult.HadP_phi_orig[i] = 0;
	  fitResult.HadP_m_orig[i] = -1;
	  
	  fitResult.HadQ_pt_orig[i] = -1;
	  fitResult.HadQ_eta_orig[i] = 0;
	  fitResult.HadQ_phi_orig[i] = 0;
	  fitResult.HadQ_m_orig[i] = -1;
	  
	  fitResult.LepB_pt_orig[i] = -1;
	  fitResult.LepB_eta_orig[i] = 0;
	  fitResult.LepB_phi_orig[i] = 0;
	  fitResult.LepB_m_orig[i] = -1;
	  
	  fitResult.LepL_pt_orig[i] = -1;
	  fitResult.LepL_eta_orig[i] = 0;
	  fitResult.LepL_phi_orig[i] = 0;
	  fitResult.LepL_m_orig[i] = -1;
	  
	  fitResult.LepN_pt_orig[i] = -1;
	  fitResult.LepN_eta_orig[i] = 0;
	  fitResult.LepN_phi_orig[i] = 0;
	  fitResult.LepN_m_orig[i] = -1;
  }

  for (unsigned int i=0; i<4; ++i) {
	  mc_Combi[i] = -1;
  }

  if (gen_evt != NULL){
	  // Fill the quark information from the TTBarGenEvent MC class

	  mc_HadB_pt = ((*gen_evt).hadB()->p4().pt());
	  mc_HadB_eta = ((*gen_evt).hadB()->p4().eta());
	  mc_HadB_phi = ((*gen_evt).hadB()->p4().phi());

	  mc_LepB_pt = ((*gen_evt).lepB()->p4().pt());
	  mc_LepB_eta = ((*gen_evt).lepB()->p4().eta());
	  mc_LepB_phi = ((*gen_evt).lepB()->p4().phi());

	  mc_Q_pt = ((*gen_evt).q()->p4().pt());	  
	  mc_Q_eta = ((*gen_evt).q()->p4().eta());
	  mc_Q_phi = ((*gen_evt).q()->p4().phi());

	  mc_QBar_pt = ((*gen_evt).qbar()->p4().pt());
	  mc_QBar_eta = ((*gen_evt).qbar()->p4().eta());
	  mc_QBar_phi = ((*gen_evt).qbar()->p4().phi());

	  mc_LepL_charge = ((*gen_evt).lepton()->charge());

	  mc_LepL_pt = ((*gen_evt).lepton()->p4().pt());
	  mc_LepL_eta = ((*gen_evt).lepton()->p4().eta());
	  mc_LepL_phi = ((*gen_evt).lepton()->p4().phi());

	  mc_LepN_pt = ((*gen_evt).neutrino()->p4().pt());
	  mc_LepN_eta = ((*gen_evt).neutrino()->p4().eta());
	  mc_LepN_phi = ((*gen_evt).neutrino()->p4().phi());

	  // Fill the trueCombi array with the MC information inside the jet class
	  for (unsigned int i=0; i<(*jets).size(); ++i) {
		  int jetRole((*jets).at(i).ttbar_decay_product());
		  switch (jetRole) {
		  case 1:
			  mc_Combi[LightQ] = (i);
			  break;
		  case 2:
			  mc_Combi[LightQBar] = (i);
			  break;
		  case 3:
			  mc_Combi[HadB] = (i);
			  break;
		  case 4:
			  mc_Combi[LepB] = (i);
			  break;
		  default:
			  break;
		  }
	  }
  }else{
	  mc_HadB_pt = -1;
	  mc_LepB_pt = -1;
	  mc_Q_pt    = -1; 
	  mc_QBar_pt = -1;
	  
	  mc_HadB_eta = -1;
	  mc_LepB_eta = -1;
	  mc_Q_eta    = -1; 
	  mc_QBar_eta = -1;
	  
	  mc_HadB_phi = -1;
	  mc_LepB_phi = -1;
	  mc_Q_phi    = -1;
	  mc_QBar_phi = -1;

	  mc_LepL_charge = 0;

	  mc_LepL_pt = -1;
	  mc_LepL_eta = 0;
	  mc_LepL_phi = 0;

	  mc_LepN_pt = -1;
	  mc_LepN_eta = 0;
	  mc_LepN_phi = 0;
  }

  // Only run HitFit if all objects are within resolution function eta ranges.
  //warning: add check on the presence of missing transverse energy when you request HitFit to use it
  if( foundLepton && (unsigned)nJetsFound>=nPartons ) {   
	  std::list<FitResult> FitResultList;
 
	  //
	  // BEGIN DECLARATION OF VARIABLES FROM KINEMATIC FIT
	  //

	  // In this part are variables from the
	  // kinematic fit procedure

	  // Number of all permutations of the event
	  size_t nHitFit    = 0 ;

	  // Number of jets in the event
	  size_t nHitFitJet = 0 ;

	  // Results of the fit for all jet permutations of the event
	  std::vector<Fit_Result> hitFitResult;

	  //
	  // R U N   H I T F I T
	  //
	  // Run the kinematic fit and get how many permutations are possible
	  // in the fit


	  nHitFit         = HitFit->FitAllPermutation();

	  //
	  // BEGIN PART WHICH EXTRACTS INFORMATION FROM HITFIT
	  //

	  // Get the number of jets
	  nHitFitJet = HitFit->GetUnfittedEvent()[0].njets();

	  // Get the fit results for all permutations
	  hitFitResult = HitFit->GetFitAllPermutation();

	  // Loop over all permutations and extract the information
	  for (size_t fit = 0 ; fit != nHitFit ; ++fit) {

		  // Get the event after the fit
		  Lepjets_Event fittedEvent = hitFitResult[fit].ev();
      
		  /*
		    Get jet permutation according to TQAF convention
		    11 : leptonic b
		    12 : hadronic b
		    13 : hadronic W
		    14 : hadronic W
		  */
		  std::vector<int> hitCombi(4);
		  for (size_t jet = 0 ; jet != nHitFitJet ; ++jet) {
			  int jet_type = fittedEvent.jet(jet).type();
			  switch(jet_type) {
			  case 11: {hitCombi[LepB     ] = jet;
			    fitResult.LepB_pt_orig[fit] = (*jets)[jet].pt();
			    fitResult.LepB_eta_orig[fit] = (*jets)[jet].eta();
			    fitResult.LepB_phi_orig[fit] = (*jets)[jet].phi();
			    fitResult.LepB_m_orig[fit] = (*jets)[jet].p4().M();
			    break;}
			  case 12: {hitCombi[HadB     ] = jet;
			    fitResult.HadB_pt_orig[fit] = (*jets)[jet].pt();
			    fitResult.HadB_eta_orig[fit] = (*jets)[jet].eta();
			    fitResult.HadB_phi_orig[fit] = (*jets)[jet].phi();
			    fitResult.HadB_m_orig[fit] = (*jets)[jet].p4().M();
			    break;}
			  case 13: {hitCombi[LightQ   ] = jet;
			    fitResult.HadP_pt_orig[fit] = (*jets)[jet].pt();
			    fitResult.HadP_eta_orig[fit] = (*jets)[jet].eta();
			    fitResult.HadP_phi_orig[fit] = (*jets)[jet].phi();
			    fitResult.HadP_m_orig[fit] = (*jets)[jet].p4().M();
			    break;}
			  case 14: {hitCombi[LightQBar] = jet;
			    fitResult.HadQ_pt_orig[fit] = (*jets)[jet].pt();
			    fitResult.HadQ_eta_orig[fit] = (*jets)[jet].eta();
			    fitResult.HadQ_phi_orig[fit] = (*jets)[jet].phi();
			    fitResult.HadQ_m_orig[fit] = (*jets)[jet].p4().M();
			    break;}
			  }
			  if(jet_type == 20){
			    fitResult.ExtraJet_pt[fit] = (*jets)[jet].pt();
			    fitResult.ExtraJet_eta[fit] = (*jets)[jet].eta();
			    fitResult.ExtraJet_phi[fit] = (*jets)[jet].phi();
			  }
		  }


		  fitResult.LepL_pt_orig[fit] = orig_lepton.p4().Pt();
		  fitResult.LepL_eta_orig[fit] = orig_lepton.p4().Eta();
		  fitResult.LepL_phi_orig[fit] = orig_lepton.p4().Phi();
		  fitResult.LepL_m_orig[fit] = orig_lepton.p4().M();

		  fitResult.LepN_pt_orig[fit] = (*mets)[0].p4().Pt();
		  fitResult.LepN_eta_orig[fit] = (*mets)[0].p4().Eta();
		  fitResult.LepN_phi_orig[fit] = (*mets)[0].p4().Phi();
		  fitResult.LepN_m_orig[fit] = (*mets)[0].p4().M();
		  // Store the kinematic quantities in the corresponding containers.
     
		  Lepjets_Event_Jet hadP_ = fittedEvent.jet(hitCombi[hitfit::LightQ   ]);
		  Lepjets_Event_Jet hadQ_ = fittedEvent.jet(hitCombi[hitfit::LightQBar]);
		  Lepjets_Event_Jet hadB_ = fittedEvent.jet(hitCombi[hitfit::HadB     ]);
		  Lepjets_Event_Jet lepB_ = fittedEvent.jet(hitCombi[hitfit::LepB     ]);
		  Lepjets_Event_Lep lepL_ = fittedEvent.lep(0);

		  // Selects the fits that pass parton specific cuts on the b-tagging algo's bDiscriminator
		  if (   hitFitResult[fit].chisq() > 0    // only take into account converged fits + fit that pass the chisq cut
		         //&& (exp(-1.0*(hitFitResult[fit].chisq())/2.0) > 0.4) // only take into account fits with probability > 0.0
				 && hitFitResult[fit].chisq() < 5 // Based on Kelly's studies
		         && (!useBTag_ || (   useBTag_       // use btag information if chosen
					      && jets->at(hitCombi[hitfit::LightQ   ]).bDiscriminator(bTagAlgo_) < maxBTagValueNonBJet_
					      && jets->at(hitCombi[hitfit::LightQBar]).bDiscriminator(bTagAlgo_) < maxBTagValueNonBJet_
					      && jets->at(hitCombi[hitfit::HadB     ]).bDiscriminator(bTagAlgo_) > minBTagValueBJet_
					      && jets->at(hitCombi[hitfit::LepB     ]).bDiscriminator(bTagAlgo_) > minBTagValueBJet_
					      ) 
					      // Allow tight and medium cuts for the b-tags on the heavy jets:_
					      // && (jets->at(hitCombi[hitfit::HadB     ]).bDiscriminator(bTagAlgo_) > minBTagValueBJet_1
					      // 	  && jets->at(hitCombi[hitfit::LepB     ]).bDiscriminator(bTagAlgo_) > minBTagValueBJet_2)
					      // || (jets->at(hitCombi[hitfit::HadB     ]).bDiscriminator(bTagAlgo_) > minBTagValueBJet_2
					      // 	  && jets->at(hitCombi[hitfit::LepB     ]).bDiscriminator(bTagAlgo_) > minBTagValueBJet_1)
					      // )
			     )
			 ) { 

			  FitResult result;
			  result.Status = 0;
			  result.Chi2 = hitFitResult[fit].chisq();
			  result.Prob = exp(-1.0*(hitFitResult[fit].chisq())/2.0);
			  result.MT   = hitFitResult[fit].mt();
			  result.SigMT= hitFitResult[fit].sigmt();
			  result.HadB = mor::Particle(ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> >(hadB_.p().perp(),hadB_.p().pseudoRapidity(),hadB_.p().phi(),hadB_.p().invariantMass()));
			  result.HadP = mor::Particle(ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> >(hadP_.p().perp(),hadP_.p().pseudoRapidity(),hadP_.p().phi(),hadP_.p().invariantMass()));
			  result.HadQ = mor::Particle(ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> >(hadQ_.p().perp(),hadQ_.p().pseudoRapidity(),hadQ_.p().phi(),hadQ_.p().invariantMass()));
			  result.LepB = mor::Particle(ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> >(lepB_.p().perp(),lepB_.p().pseudoRapidity(),lepB_.p().phi(),lepB_.p().invariantMass()));
			  result.LepL = mor::Particle(ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> >(lepL_.p().perp(),lepL_.p().pseudoRapidity(),lepL_.p().phi(),lepL_.p().invariantMass()));
			  result.LepN = mor::Particle(ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> >(fittedEvent.met().perp(), fittedEvent.met().pseudoRapidity(),
														  fittedEvent.met().phi(), fittedEvent.met().invariantMass()));
			  result.JetCombi = hitCombi;
	      
			  FitResultList.push_back(result);
		  }

	  }

	  // sort results w.r.t. chi2 values
	  FitResultList.sort();
  
	  // -----------------------------------------------------
	  // feed out result
	  // starting with the JetComb having the smallest chi2
	  // -----------------------------------------------------

	  //}else{
	  if( ((unsigned)FitResultList.size())>0 ){
		  unsigned int i = 0;
		  for(std::list<FitResult>::const_iterator result = FitResultList.begin(); result != FitResultList.end(); ++result) {
			  if(fitResult.nSol >=  maxNComb_) break;
			  fitResult.nSol++;
		  
			  fitResult.HadB_pt[i] = result->HadB.pt();
			  fitResult.HadB_eta[i] = result->HadB.eta();
			  fitResult.HadB_phi[i] = result->HadB.phi();
			  fitResult.HadB_m[i] = result->HadB.mass();
			  
			  fitResult.HadP_pt[i] = result->HadP.pt();
			  fitResult.HadP_eta[i] = result->HadP.eta();
			  fitResult.HadP_phi[i] = result->HadP.phi();
			  fitResult.HadP_m[i] = result->HadP.mass();
			  
			  fitResult.HadQ_pt[i] = result->HadQ.pt();
			  fitResult.HadQ_eta[i] = result->HadQ.eta();
			  fitResult.HadQ_phi[i] = result->HadQ.phi();
			  fitResult.HadQ_m[i] = result->HadQ.mass();
			  
			  fitResult.LepB_pt[i] = result->LepB.pt();
			  fitResult.LepB_eta[i] = result->LepB.eta();
			  fitResult.LepB_phi[i] = result->LepB.phi();
			  fitResult.LepB_m[i] = result->LepB.mass();
			  
			  fitResult.LepL_pt[i] = result->LepL.pt();
			  fitResult.LepL_eta[i] = result->LepL.eta();
			  fitResult.LepL_phi[i] = result->LepL.phi();
			  fitResult.LepL_m[i] = result->LepL.mass();
			  
			  fitResult.LepN_pt[i] = result->LepN.pt();
			  fitResult.LepN_eta[i] = result->LepN.eta();
			  fitResult.LepN_phi[i] = result->LepN.phi();
			  fitResult.LepN_m[i] = result->LepN.mass();
			  
			  fitResult.MT[i] = result->MT;
			  fitResult.SigMT[i] = result->SigMT;
			  fitResult.Prob[i] = result->Prob;
			  fitResult.Chi2[i] = result->Chi2;
			  fitResult.Status[i] = result->Status;
			
			  fitResult.Combi_LightQ[i] = (Int_t)(result->JetCombi.at(0));
			  fitResult.Combi_LightQBar[i] = (Int_t)(result->JetCombi.at(1));
			  fitResult.Combi_HadB[i] = (Int_t)(result->JetCombi.at(2));
			  fitResult.Combi_LepB[i] = (Int_t)(result->JetCombi.at(3));

			  fitResult.matched[i] = 0;

			  // Push back the fitted objects to the hitfit_<object> collections as mor::Particles
			  
			  // Take the first (and best) fit
			  if (i == 0){
				  hitfit_jets->clear();
				  hitfit_jets->push_back(result->HadB);
				  hitfit_jets->push_back(result->LepB);
				  hitfit_jets->push_back(result->HadP);
				  hitfit_jets->push_back(result->HadQ);
				  
				  if (!muons->empty()){
					  hitfit_muons->clear();
					  hitfit_muons->push_back(result->LepL);
				  }else if (!electrons->empty()){
					  hitfit_electrons->clear();
					  hitfit_electrons->push_back(result->LepL);
				  }
				  hitfit_mets->clear();
				  hitfit_mets->push_back(result->LepN);
				  hitfit_chisq = result->Chi2;
			  }

			  // Check to see if parton matching was successful for the event
			  if((mc_Combi[0] == fitResult.Combi_LightQ[i] || mc_Combi[0] == fitResult.Combi_LightQBar[i]) && (mc_Combi[1] == fitResult.Combi_LightQ[i] || mc_Combi[1] == fitResult.Combi_LightQBar[i] ) && mc_Combi[2] == fitResult.Combi_HadB[i] && mc_Combi[3] == fitResult.Combi_LepB[i] ){
				  fitResult.matched[i] = 1;
			  } ++i;


		  }// for(std::list<FitResult>::const_iterator result = FitResultList.begin(); result != FitResultList.end(); ++result) 
	  
		  hitfit_combi_seed.clear();
		  if (use_mc_truth_seed){
			  hitfit_combi_seed.push_back(mc_Combi[LightQ]); // LightQ
			  hitfit_combi_seed.push_back(mc_Combi[LightQBar]); // LightQBar
			  hitfit_combi_seed.push_back(mc_Combi[HadB]); // HadB
			  hitfit_combi_seed.push_back(mc_Combi[LepB]); // LepB
		  }else{
			  hitfit_combi_seed.push_back(fitResult.Combi_LightQ[0]);
			  hitfit_combi_seed.push_back(fitResult.Combi_LightQBar[0]);
			  hitfit_combi_seed.push_back(fitResult.Combi_HadB[0]);
			  hitfit_combi_seed.push_back(fitResult.Combi_LepB[0]);
		  }
		  
		  hitfit_state = 0;

	  } // ( ((unsigned)FitResultList.size())>0 ){

  } //  if( foundLepton && !mets->empty() && (unsigned)nJetsFound>=nPartons ) {   
  else{
	  hitfit_combi_seed.clear();
	  hitfit_combi_seed.push_back(-2);
	  hitfit_combi_seed.push_back(-2);
	  hitfit_combi_seed.push_back(-2);
	  hitfit_combi_seed.push_back(-2);
	  hitfit_state = -2;
  }
	  
  // Set the handle holder vectors for kinematic 4-vectors and permutation estimate and fill the tree


  handle_holder->set_hitfit_jets(hitfit_jets);
  handle_holder->set_hitfit_electrons(hitfit_electrons);
  handle_holder->set_hitfit_muons(hitfit_muons);
  handle_holder->set_hitfit_mets(hitfit_mets);
  handle_holder->set_hitfit_jet_combination(hitfit_combi_seed);
  handle_holder->set_hitfit_chisq(hitfit_chisq);

  tree->Fill();

  // Return false if the event didn't pass the fit
  if (hitfit_combi_seed[0] == -1 || hitfit_combi_seed[0] == -2)
	  return false;
  else
	  return true;

}
 
// Return a vector of the estimated correct jet permutation to the Handle Holder, for use in other classes 
std::vector<int> HitFitProducer::get_hitfit_combination()
{
	return hitfit_combi_seed;
}

// Return the chisq for the chosen fit to the Handle Holder (for writing in the LHCOWriter)
double HitFitProducer::get_hitfit_chisq()
{
        return hitfit_chisq;
}

// Return the hitfit mor::Particle collections to the Handle Holder
std::vector<mor::Particle>* HitFitProducer::get_hitfit_jets()
{
	return hitfit_jets;
}

std::vector<mor::Particle>* HitFitProducer::get_hitfit_electrons()
{
	return hitfit_electrons;
}

std::vector<mor::Particle>* HitFitProducer::get_hitfit_muons()
{
	return hitfit_muons;
}

std::vector<mor::Particle>* HitFitProducer::get_hitfit_mets()
{
	return hitfit_mets;
}

void HitFitProducer::set_hitfit_combination(std::vector<int> combo)
{
	this->hitfit_combi_seed = combo;
}

void HitFitProducer::set_hitfit_chisq(double chi2)
{
        this->hitfit_chisq = chi2;
}

// Return the hitfit mor::Particle collections to the Handle Holder
void HitFitProducer::set_hitfit_jets(std::vector<mor::Particle> *jet_coll)
{
	this->hitfit_jets = jet_coll;
}

void HitFitProducer::set_hitfit_electrons(std::vector<mor::Particle> *electron_coll)
{
	this->hitfit_electrons = electron_coll;
}

void HitFitProducer::set_hitfit_muons(std::vector<mor::Particle> *muon_coll)
{
	this->hitfit_muons = muon_coll;
}

void HitFitProducer::set_hitfit_mets(std::vector<mor::Particle> *met_coll)
{
	this->hitfit_mets = met_coll;
}


void HitFitProducer::combine_jet(int index_jet_tobe_combined, int index)
{
  //combine the spare jet with one jet of the combination
  std::vector<mor::Jet> *jetcol = new std::vector<mor::Jet>();
  for(int j = 0; j < (int) original_jets->size(); j++){
    if(j != index_jet_tobe_combined){
      if(j == index){
        mor::Jet j = (*original_jets)[index];
        j.set_p4((*original_jets)[index].p4() + (*original_jets)[index_jet_tobe_combined].p4());
        jetcol->push_back(j);
      }
      else{
        jetcol->push_back((*original_jets)[j]);
      }
    }
  }
  
  this->jets = jetcol;

  //  for(int i = 0; i < (int) original_jets->size(); i++){
  // std::cout<<"orig jet "<<i<<" pt = "<<(*original_jets)[i].p4().Pt()<<std::endl;
  // }

  //  for(int i = 0; i < (int) jets->size(); i++){
  //std::cout<<"jet "<<i<<" pt = "<<(*jets)[i].p4().Pt()<<std::endl;
  //}
}
