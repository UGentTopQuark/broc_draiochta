#include "../../interface/TagAndProbe/TagAndProbe.h"

clibisfiosraigh::TagAndProbe::TagAndProbe(broc::CutsSet *cuts_set, eire::HandleHolder *handle_holder)
{
	eire::ConfigReader *config_reader = handle_holder->get_config_reader();
	std::string output_dir = config_reader->get_var("output_directory");
	std::string suffix = config_reader->get_var("outfile_suffix");
	std::string filename = output_dir+"/"+"trigger_tag_and_probe_"+handle_holder->get_ident()+suffix+".root";
	while(filename.find("|") != std::string::npos)
		filename.replace(filename.find("|"),1,"_");

	dilepton_reconstructor = handle_holder->services()->dilepton_reco();
	e_trigger_tree_producer = NULL;
	mu_trigger_tree_producer = NULL;

	std::string directory_name = "lepEffs";

	ntuple_file = new TFile(filename.c_str(), "RECREATE");
	if(directory_name != ""){
		ntuple_file->mkdir(directory_name.c_str());
		ntuple_file->Cd(directory_name.c_str());
	}

	do_electron_trigger_efficiency = config_reader->get_bool_var("fill_electron_trigger_trees","tag_and_probe",false);
	do_muon_trigger_efficiency = config_reader->get_bool_var("fill_muon_trigger_trees","tag_and_probe",false);

	if(do_muon_trigger_efficiency){	// muon tag and probe
		mu_trigger_tree_producer = new clibisfiosraigh::LepTriggerTreeProducer<mor::Muon>(handle_holder);
		mu_trigger_tree_producer->set_outfile(ntuple_file, directory_name);
		mu_trigger_tree_producer->create_tree("fitter_tree");
		mu_trigger_tree_producer->book_branches();
		mu_trigger_tree_producer->set_handles(handle_holder);
		mu_trigger_tree_producer->set_primary_vertices();
		mu_trigger_tree_producer->set_dilepton_reconstructor(dilepton_reconstructor);
		//		mu_trigger_tree_producer->set_TriggerObjectSelector(handle_holder);
	}
	if(do_electron_trigger_efficiency){	// electron tag and probe
		e_trigger_tree_producer = new clibisfiosraigh::LepTriggerTreeProducer<mor::Electron>(handle_holder);
		e_trigger_tree_producer->set_outfile(ntuple_file, directory_name);
		e_trigger_tree_producer->create_tree("fitter_tree");
		e_trigger_tree_producer->book_branches();
		e_trigger_tree_producer->set_handles(handle_holder);
		e_trigger_tree_producer->set_primary_vertices();
		e_trigger_tree_producer->set_dilepton_reconstructor(dilepton_reconstructor);
		//	e_trigger_tree_producer->set_TriggerObjectSelector(handle_holder);
	}
}

clibisfiosraigh::TagAndProbe::~TagAndProbe()
{
	// TreeProducers must be deleted before ntuple file
	if(do_muon_trigger_efficiency && mu_trigger_tree_producer){
		delete mu_trigger_tree_producer;
		mu_trigger_tree_producer = NULL;
	}
	
	if(do_electron_trigger_efficiency && e_trigger_tree_producer){
		delete e_trigger_tree_producer;
		e_trigger_tree_producer = NULL;
	}
	
	if(ntuple_file){
		ntuple_file->Write();
		ntuple_file->Close();
		delete ntuple_file;
		ntuple_file = NULL;
	}
}

void clibisfiosraigh::TagAndProbe::fill_trees()
{
	if(do_muon_trigger_efficiency) mu_trigger_tree_producer->fill_branches();
	if(do_electron_trigger_efficiency) e_trigger_tree_producer->fill_branches();
}
