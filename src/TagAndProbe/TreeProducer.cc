#include "../../interface/TagAndProbe/TreeProducer.h"

clibisfiosraigh::TreeProducer::TreeProducer()
{
	outfile = NULL;
	cuts_set = NULL;
	handle_holder = NULL;
}

clibisfiosraigh::TreeProducer::~TreeProducer()
{
	if(outfile){
		outfile->cd();
		if(directory_name != "") outfile->Cd(directory_name.c_str());
		tree->Write();
	}
	if(tree){
		delete tree;
		tree = NULL;
	}
}

void clibisfiosraigh::TreeProducer::set_outfile(TFile *ntuple_file, std::string directory_name)
{
        this->outfile = ntuple_file;
	this->directory_name = directory_name;
}

void clibisfiosraigh::TreeProducer::set_handles(eire::HandleHolder *handle_holder)
{
	this->handle_holder = handle_holder;

	this->muons = handle_holder->get_tight_muons();
	this->loose_muons = handle_holder->get_loose_muons();
	this->jets = handle_holder->get_tight_jets();
	this->electrons = handle_holder->get_tight_electrons();
	this->loose_electrons = handle_holder->get_loose_electrons();
	this->mets = handle_holder->get_corrected_mets();
	this->trigger = handle_holder->get_trigger();
	this->trigger_objects = handle_holder->get_trigger_objects();
	this->gen_particles = handle_holder->get_gen_particles();
	this->cuts_set = handle_holder->get_cuts_set();
}

void clibisfiosraigh::TreeProducer::create_tree(std::string tree_name)
{
        if(outfile){
		outfile->cd();
		// FIXME: mkdir?
		if(directory_name != "") outfile->Cd(directory_name.c_str());
		tree = new TTree(tree_name.c_str(),tree_name.c_str());
	}else{
		std::cerr << "ERROR: clibisfiosraigh::TreeProducer::create_tree(): no outfile set" << std::endl;
	}
}
