#include "../../interface/LHCOWriter/LHCOWriter.h"


broc::LHCOWriter::LHCOWriter(eire::HandleHolder *handle_holder)
{
    set_handles(handle_holder);
  id = "_"+handle_holder->get_ident();
  while(id.find("|") != std::string::npos){
    id.replace(id.find("|"),1,"_");
  }

  eire::ConfigReader *config_reader = handle_holder->get_config_reader();
  std::string output_dir = config_reader->get_var("output_directory");  
  std::string suffix = config_reader->get_var("outfile_suffix");

  weight_manager = new broc::EventWeightManager(cuts_set,handle_holder);
  //std::cout<<"Weight manager initialised\n";

  btag_algo = "NULL";
  btag_working_point = 0;
  trueMass = atof(config_reader->get_var("trueMass","lhco_writer",false).c_str());
  trueJES = atof(config_reader->get_var("JES_variation","lhco_writer",false).c_str());
  //std::cout<<trueMass<<" "<<trueJES<<std::endl;

  btag_algo = config_reader->get_var("btag_algo", "lhco_writer", false);
  btag_working_point = atof(config_reader->get_var("btag_working_point", "lhco_writer", false).c_str());      

  split_lepton_charges = config_reader->get_bool_var("split_lepton_charges", "lhco_writer", false);
  use_hitfit = config_reader->get_bool_var("enable_kinematic_fit_module", "global", false);
  use_hitfit_combi_seed = config_reader->get_bool_var("use_hitfit_combi_seed", "lhco_writer", false);
  use_hitfit_kinematics = config_reader->get_bool_var("use_hitfit_kinematics", "lhco_writer", false);
  write_ntuple =  config_reader->get_bool_var("write_ntuple","lhco_writer",false);


  if(!use_hitfit && use_hitfit_combi_seed)
	  std::cerr << "WARNING: You have selected to use the HitFit combination, but HitFit is not enabled!" << std::endl;
  
  if(use_hitfit_kinematics && !use_hitfit_combi_seed){
	  std::cerr << "WARNING: You have selected to use the HitFit kinematics, but not the HitFit Jet Combination Seed!  This is not yet supported.  Disabling use.." << std::endl;
	  use_hitfit_kinematics = false;
  }

  if((btag_algo == "NULL") || (btag_working_point == -1))
	  std::cerr << "WARNING: No btag algo or working point defined in runtime config!" << std::endl;

  if(!split_lepton_charges){
      lhcofile_tot_MC = fopen((output_dir + "/input_"+config_reader->get_var("trueMass","lhco_writer",false)+"_"+config_reader->get_var("JES_variation","lhco_writer",false)+id+suffix+".lhco_gen_level").c_str(),"w");
      if(use_hitfit && use_hitfit_kinematics)
	  lhcofile_tot_hitfit = fopen((output_dir + "/input_"+config_reader->get_var("trueMass","lhco_writer",false)+"_"+config_reader->get_var("JES_variation","lhco_writer",false)+id+suffix+ ".lhco_hitfit").c_str(), "w");
      lhcofile_tot = fopen((output_dir + "/input_" +config_reader->get_var("trueMass","lhco_writer",false)+"_"+config_reader->get_var("JES_variation","lhco_writer",false)+id+suffix+ ".lhco").c_str(), "w");
  }
  if(split_lepton_charges){
      lhcofile_pos_MC = fopen((output_dir + "/input_"+config_reader->get_var("trueMass","lhco_writer",false)+"_"+config_reader->get_var("JES_variation","lhco_writer",false)+id+suffix+"_pos.lhco_gen_level").c_str(),"w");
      lhcofile_neg_MC = fopen((output_dir + "/input_"+config_reader->get_var("trueMass","lhco_writer",false)+"_"+config_reader->get_var("JES_variation","lhco_writer",false)+id+suffix+"_neg.lhco_gen_level").c_str(),"w");
      if(use_hitfit && use_hitfit_kinematics){
	  lhcofile_pos_hitfit = fopen((output_dir + "/input_" +config_reader->get_var("trueMass","lhco_writer",false)+"_"+config_reader->get_var("JES_variation","lhco_writer",false)+id+suffix+ "_pos.lhco_hitfit").c_str(), "w");
	  lhcofile_neg_hitfit = fopen((output_dir + "/input_" +config_reader->get_var("trueMass","lhco_writer",false)+"_"+config_reader->get_var("JES_variation","lhco_writer",false)+id+suffix+ "_neg.lhco_hitfit").c_str(), "w");
      }
      lhcofile_pos = fopen((output_dir + "/input_" +config_reader->get_var("trueMass","lhco_writer",false)+"_"+config_reader->get_var("JES_variation","lhco_writer",false)+id+suffix+ "_pos.lhco").c_str(), "w");
      lhcofile_neg = fopen((output_dir + "/input_" +config_reader->get_var("trueMass","lhco_writer",false)+"_"+config_reader->get_var("JES_variation","lhco_writer",false)+id+suffix+ "_neg.lhco").c_str(), "w");
      
  }

  data_only = true;
  gen_filled = 0;
  reco_filled = 0;
  jets_matched = false;
  if(write_ntuple){
      jet1=NULL;
      jet2=NULL;
      jet3=NULL;
      jet4=NULL;
      lepton=NULL;
      met=NULL;
      tjet1=NULL;
      tjet2=NULL;
      tjet3=NULL;
      tjet4=NULL;
      neutrino=NULL;
      tlepton=NULL;
  }
  first = true;
  phadB = gen_event->hadB(); //particle 2                                                                                                                          
  plepB = gen_event->lepB(); //particle 3                                                                                                                          
  pq = gen_event->q(); //particle 4                                                                                                                                
  pqbar = gen_event->qbar(); //particle 5   
}

broc::LHCOWriter::~LHCOWriter()
{
    if(write_ntuple){
	delete jet1;        
	delete jet2;
        delete jet3;
        delete jet4;
	delete lepton;
	delete met;
	delete tjet1;
	delete tjet2;
	delete tjet3;
	delete tjet4;
	delete neutrino;
	delete tlepton;}
    if(split_lepton_charges){
	fclose(lhcofile_neg_MC);
	fclose(lhcofile_pos_MC);
	if(use_hitfit && use_hitfit_kinematics){
	    fclose(lhcofile_neg_hitfit);
	    fclose(lhcofile_pos_hitfit);
	}
	fclose(lhcofile_neg);
	fclose(lhcofile_pos);
    }else{
	fclose(lhcofile_tot_MC);
	if(use_hitfit && use_hitfit_kinematics)
	    fclose(lhcofile_tot_hitfit);
	fclose(lhcofile_tot);
    }
    first = true;
}

void broc::LHCOWriter::set_handles(eire::HandleHolder *handle_holder)
{
  this->handle_holder = handle_holder;
  this->cuts_set = handle_holder->get_cuts_set();
  this->event_information = handle_holder->get_event_information();
  this->electrons = handle_holder->get_tight_electrons();
  this->muons = handle_holder->get_tight_muons();
  this->mets = handle_holder->get_corrected_mets();
  this->jets = handle_holder->get_tight_jets();
  this->gen_event = handle_holder->get_ttbar_gen_evt();
}

void broc::LHCOWriter::book_branches()
{
    if(write_ntuple){
	tree->Branch("tjet1",&tjet1);
	tree->Branch("tjet2",&tjet2);
	tree->Branch("tjet3",&tjet3);
	tree->Branch("tjet4",&tjet4);
	tree->Branch("tlepton",&tlepton);
	tree->Branch("neutrino",&neutrino);
	tree->Branch("lepID",&lepID);
	tree->Branch("jet1",&jet1);
	tree->Branch("jet2",&jet2);
	tree->Branch("jet3",&jet3);
	tree->Branch("jet4",&jet4);
	tree->Branch("lepton",&lepton);
	tree->Branch("met",&met);
	tree->Branch("event_number",&tevent_number);
	tree->Branch("run_number",&trun_number);
	tree->Branch("event_weight",&tevent_weight);
	tree->Branch("trueMass",&trueMass);
	tree->Branch("trueJES",&trueJES);
	tree->Branch("jet1_typ",&jet1_typ);
	tree->Branch("jet1_jmass",&jet1_jmass);
	tree->Branch("jet1_ntrack",&jet1_ntrack);
	tree->Branch("jet1_btag",&jet1_btag);
	tree->Branch("jet1_mc_id",&jet1_mc_id);
	tree->Branch("jet1_match",&jet1_match);
	tree->Branch("jet2_typ",&jet2_typ);
	tree->Branch("jet2_jmass",&jet2_jmass);
	tree->Branch("jet2_ntrack",&jet2_ntrack);
	tree->Branch("jet2_btag",&jet2_btag);
	tree->Branch("jet2_mc_id",&jet2_mc_id);
	tree->Branch("jet2_match",&jet2_match);
	tree->Branch("jet3_typ",&jet3_typ);
	tree->Branch("jet3_jmass",&jet3_jmass);
	tree->Branch("jet3_ntrack",&jet3_ntrack);
	tree->Branch("jet3_btag",&jet3_btag);
	tree->Branch("jet3_mc_id",&jet3_mc_id);
	tree->Branch("jet3_match",&jet3_match);
	tree->Branch("jet4_typ",&jet4_typ);
	tree->Branch("jet4_jmass",&jet4_jmass);
	tree->Branch("jet4_ntrack",&jet4_ntrack);
	tree->Branch("jet4_btag",&jet4_btag);
	tree->Branch("jet4_mc_id",&jet4_mc_id);
	tree->Branch("jet4_match",&jet4_match);
	tree->Branch("lepton_typ",&lepton_typ);
	tree->Branch("lepton_jmass",&lepton_jmass);
	tree->Branch("lepton_ntrack",&lepton_ntrack);
	tree->Branch("lepton_btag",&lepton_btag);
	tree->Branch("lepton_mc_id",&lepton_mc_id);
	tree->Branch("pdf_weight",&pdf_weight);
	tree->Branch("pu_weight",&pu_weight);
	tree->Branch("pu_weight_up",&pu_weight_up);
	tree->Branch("pu_weight_down",&pu_weight_down);
	tree->Branch("trigger_weight_up",&trigger_weight_up);
	tree->Branch("trigger_weight_down",&trigger_weight_down);
	tree->Branch("trigger_weight",&trigger_weight);
	tree->Branch("lepID_weight_up",&lepID_weight_up);
	tree->Branch("lepID_weight_down",&lepID_weight_down);
	tree->Branch("lepID_weight",&lepID_weight);
	tree->Branch("btag_weight",&btag_weight);
	tree->Branch("btag_weight_up",&btag_weight_up);
	tree->Branch("btag_weight_down",&btag_weight_down);
	tree->Branch("mistag_weight_up",&mistag_weight_up);
	tree->Branch("mistag_weight_down",&mistag_weight_down);
	tree->Branch("topPt_weight",&topPt_weight);
	tree->Branch("bfrag_weight",&bfrag_weight);
	tree->Branch("jets_matched",&jets_matched);
	tree->Branch("hitfit_combination_seed",&hitfit_combi_seed);
	
	//std::cout<<"NTuple branches initialised...........\n";
    }
}

void broc::LHCOWriter::set_header(bool gen, FILE * file)
{
  fprintf(file,"##############################LHCO event file######################################\n");
   std::string dataset = handle_holder->get_ident();
  fprintf(file,("#Dataset processed: " + dataset + " and four jets"+id+"\n").c_str());
}

int broc::LHCOWriter::is_matched_event()
{
  int matched = 0;
  bool lepmatched = false;
  double dR = 5.0;
  if(electrons->size() > 0){dR = ROOT::Math::VectorUtil::DeltaR(gen_event->lepton()->p4(), (*electrons)[0].p4());}
  else if(muons->size() > 0){dR = ROOT::Math::VectorUtil::DeltaR(gen_event->lepton()->p4(), (*muons)[0].p4());}
  if(dR < 0.3){lepmatched = true;}
  if(gen_event->hadB_matched() && gen_event->lepB_matched() && gen_event->q_matched() && gen_event->qbar_matched() && lepmatched){matched = 1;}
  if(gen_event->lepton()->charge() == 0){//std::cout<<"no gen event!!"<<std::endl; 
      matched = -1;}
  return matched;
}

void broc::LHCOWriter::generator_file()
{

    //check the event information to get the event number 
    data_only = false;                                               
    double event_number = event_information->event_number();
    double run_number = event_information->run();
    double event_weight = handle_holder->get_event_weight();

    mor::Particle *plep = gen_event->lepton();
    mor::Particle *pneutrino = gen_event->neutrino();
    float jmass = 0.;
    double phi = 0.;
    int index = -1;
    
    //Filling leaves of lhco_gen_level
    FILE *lhcofile_all_MC = lhcofile_tot_MC;

    if(split_lepton_charges){
      if((-1)*plep->mc_match_id()/fabs(plep->mc_match_id())<0)
        lhcofile_all_MC = lhcofile_neg_MC;
      else if((-1)*plep->mc_match_id()/fabs(plep->mc_match_id())>0)
        lhcofile_all_MC = lhcofile_pos_MC;
      else return;
    }else{
	lhcofile_all_MC = lhcofile_tot_MC;
    }

    fprintf(lhcofile_all_MC,"#\ttype\teta\tphi\tpt\tjmass\tntrk\tbtag\thad/em\tev_weight\thel\n");
    fprintf(lhcofile_all_MC,"0\t%.0f.%.0f\t0\n",run_number,event_number);

    jmass = phadB->p4().M();
    if(jmass < 0.){jmass = 0.0;}
    phi = phadB->phi();
    if(phi < 0){phi = 2*TMath::Pi() + phi;}
    fill_leaves(1, lhcofile_all_MC,event_number, run_number, 4, phadB->eta(), phi, phadB->pt(), jmass, 1, 2, 0, event_weight, 0,1,phadB->p4().E(),"gen");
    jmass = pq->p4().M();
    if(jmass < 0.){jmass = 0.0;}
    phi = pq->phi();
    if(phi < 0){phi = 2*TMath::Pi() + phi;}
    fill_leaves(1, lhcofile_all_MC,event_number, run_number, 4, pq->eta(), phi, pq->pt(), jmass, 1, 0, 0, event_weight, 0,2,pq->p4().E(),"gen");
    jmass = pqbar->p4().M();
    if(jmass < 0.){jmass = 0.0;}
    phi = pqbar->phi();
    if(phi < 0){phi = 2*TMath::Pi() + phi;}
    fill_leaves(1, lhcofile_all_MC,event_number, run_number, 4, pqbar->eta(), phi, pqbar->pt(), jmass, 1, 0, 0, event_weight, 0,3,pqbar->p4().E(),"gen");
    jmass = plepB->p4().M();
    if(jmass < 0.){jmass = 0.0;}
    phi = plepB->phi();
    if(phi < 0){phi = 2*TMath::Pi() + phi;}
    fill_leaves(1, lhcofile_all_MC,event_number, run_number, 4, plepB->eta(), phi, plepB->pt(), jmass, 1, 2, 0, event_weight, 0,4,plepB->p4().E(),"gen");
    if(muons->size() >= 1){
	    jmass = plep->p4().M();
	    if(jmass < 0.){jmass = 0.0;}
	    double mindR = 5.;
	    int index = -1;
	    double dR = ROOT::Math::VectorUtil::DeltaR(phadB->p4(), plep->p4());
	    if(dR < mindR){mindR = dR; index = 1;}
	    dR = ROOT::Math::VectorUtil::DeltaR(plepB->p4(), plep->p4());
	    if(dR < mindR){mindR = dR; index = 2;}
	    dR = ROOT::Math::VectorUtil::DeltaR(pq->p4(), plep->p4());
	    if(dR < mindR){mindR = dR; index = 3;}
	    dR = ROOT::Math::VectorUtil::DeltaR(pqbar->p4(), plep->p4());
	    if(dR < mindR){mindR = dR; index = 4;}
	    phi = plep->phi();
	    if(phi < 0){phi = 2*TMath::Pi() + phi;}
	    //    std::cout<<"generated muon charge= "<<(-1)*plep->mc_match_id()<<std::endl;
	    fill_leaves(1, lhcofile_all_MC,event_number, run_number, 2, plep->eta(), phi, plep->pt(), jmass, (-1)*plep->mc_match_id()/fabs(plep->mc_match_id()), index, 0, event_weight, 0,5,plep->p4().E(),"gen");
    }
    
    if(electrons->size() >= 1){
	    jmass = plep->p4().M();
	    if(jmass < 0.){jmass = 0.0;}
	    phi = plep->phi();
	    if(phi < 0){phi = 2*TMath::Pi() + phi;}
	    //std::cout<<"generated electron charge= "<<(-1)*plep->mc_match_id()<<" pt = "<<plep->pt()<<" phi= "<<phi<<" eta= "<<plep->eta()<<std::endl;
	    fill_leaves(1, lhcofile_all_MC,event_number, run_number, 1, plep->eta(), phi, plep->pt(), jmass, (-1)*plep->mc_match_id()/fabs(plep->mc_match_id()), index, 0, event_weight, 0,5,plep->p4().E(),"gen");
    }
    jmass = pneutrino->p4().M();
    if(jmass < 0.){jmass = 0.0;}
    phi = pneutrino->phi();
    if(phi < 0){phi = 2*TMath::Pi() + phi;}
    fill_leaves(1, lhcofile_all_MC,event_number, run_number, 6, pneutrino->eta(), phi, pneutrino->pt(), jmass, 0, 0, 0, event_weight, 0,6,pneutrino->p4().E(),"gen");
    
    
}

// Fills the lhco file in the event loop
bool broc::LHCOWriter::fill_branches()
{

	int hitfit_state = cuts_set->get_cut_value("hitfit_state");

  // Set the headers

  if(first){
      first=false;
      if((!event_information->is_real_data())){
	  if(split_lepton_charges){
	      set_header(true,lhcofile_pos_MC);
	      set_header(true,lhcofile_neg_MC);
	  }else
	      set_header(true,lhcofile_tot_MC);
      }
      if(split_lepton_charges){
	  if(use_hitfit && use_hitfit_kinematics){
	      set_header(false,lhcofile_pos_hitfit);
	      set_header(false,lhcofile_neg_hitfit);
	  }
	  set_header(false,lhcofile_pos);
	  set_header(false,lhcofile_neg);
      }else{
	  if(use_hitfit && use_hitfit_kinematics)
	      set_header(false,lhcofile_tot_hitfit);
	  set_header(false,lhcofile_tot);
      }	    
  }
  
  // Only do the matching cut if HitFit is NOT enabled
  if(!use_hitfit){
	  if((!event_information->is_real_data()) && is_matched_event() == 0){return 0;}
  }

  // Obtain the hitfit collections inside the event loop (where they are set)

  if (hitfit_state == 1 && use_hitfit_kinematics){
	  this->hitfit_jets = handle_holder->get_hitfit_jets();
	  this->hitfit_electrons = handle_holder->get_hitfit_electrons();
	  this->hitfit_muons = handle_holder->get_hitfit_muons();
	  this->hitfit_mets = handle_holder->get_hitfit_mets();
  }

  this->hitfit_combi_seed = handle_holder->get_hitfit_combination();
  
  // Get the event information

  double event_number = event_information->event_number();
  double event_weight = handle_holder->get_event_weight();
  double run_number = event_information->run();

  FILE *lhcofile_all;

  int partnum = 0;

  // Fill the MC lhco file with the generator information

  if((!event_information->is_real_data())){generator_file();}

  int lep_charge = 0;

  if(muons->size() >= 1){
	  lep_charge = (*muons)[0].charge();
  }if(electrons->size() >= 1){
	  lep_charge = (*electrons)[0].charge();
  }

  if(split_lepton_charges){
      if(lep_charge>0){
	  if(use_hitfit_kinematics)
	      lhcofile_all = lhcofile_pos_hitfit;
	  else 
	      lhcofile_all = lhcofile_pos;
      }
      else if(lep_charge<0){
	  if(hitfit_state == 1 && use_hitfit_kinematics)
	      lhcofile_all = lhcofile_neg_hitfit;
	  else 
	      lhcofile_all = lhcofile_neg;
      }
      else return 0;
  }else{
      if (hitfit_state == 1 && use_hitfit_kinematics)
		  lhcofile_all = lhcofile_tot_hitfit;
      else 
		  lhcofile_all = lhcofile_tot;
  }
  fprintf(lhcofile_all,"#\ttype\teta\tphi\tpt\tjmass\tntrk\tbtag\thad/em\tev_weight\thel\n");
  fprintf(lhcofile_all,"0\t%.0f.%.0f\t0\n",run_number,event_number);  

  partnum++;

  // If seeding with HitFit, push back the jets to a new jet collection in the order specified by HitFit.

  std::vector<mor::Jet> sorted_jets;
  std::vector<mor::Particle> sorted_hitfit_jets;

  // Set the order of the jets corresponding to one of two processes based on the charge of the lepton,
  // in the case where MadWeight is used to run over a jet combination without permtutation

  if(use_hitfit && hitfit_state == 1 && use_hitfit_combi_seed){
	  if (lep_charge == -1){
		  sorted_jets.push_back((*jets)[hitfit_combi_seed[2]]); // HadB
		  sorted_jets.push_back((*jets)[hitfit_combi_seed[0]]); // LightQ
		  sorted_jets.push_back((*jets)[hitfit_combi_seed[1]]); // LightQBar
		  sorted_jets.push_back((*jets)[hitfit_combi_seed[3]]); // LepB
		  if (use_hitfit_kinematics){
			  sorted_hitfit_jets.push_back((*hitfit_jets)[0]); // HadB
			  sorted_hitfit_jets.push_back((*hitfit_jets)[2]); // LightQ
			  sorted_hitfit_jets.push_back((*hitfit_jets)[3]); // LightQBar
			  sorted_hitfit_jets.push_back((*hitfit_jets)[1]); // LepB
		  }
	  }else if (lep_charge == 1){
		  sorted_jets.push_back((*jets)[hitfit_combi_seed[2]]); // HadB
		  sorted_jets.push_back((*jets)[hitfit_combi_seed[0]]); // LightQ
		  sorted_jets.push_back((*jets)[hitfit_combi_seed[1]]); // LightQBar
		  sorted_jets.push_back((*jets)[hitfit_combi_seed[3]]); // LepB
		  if (use_hitfit_kinematics){
			  sorted_hitfit_jets.push_back((*hitfit_jets)[0]); // HadB
			  sorted_hitfit_jets.push_back((*hitfit_jets)[2]); // LightQ
			  sorted_hitfit_jets.push_back((*hitfit_jets)[3]); // LightQBar
			  sorted_hitfit_jets.push_back((*hitfit_jets)[1]); // LepB
		  }
	  }
  }

  // If multiple leptons exist in the event, take the first object in the collection with the highest pt

  if(jets->size() >= 4){

	  // If seeding with HitFit, write out the jets in the order that will be recognised by MadWeight when permutation is turned off.

	  if(use_hitfit && hitfit_state == 1 && use_hitfit_combi_seed){

		  int njets = jets->size();
	      if(njets>5)
			  njets = 5;
	      if(!event_information->is_real_data())
			  unambiguous_matching(jets);
	      
		  for(int i=0; i < 4; i++){

			  mor::Jet jet_i = (sorted_jets)[i];
			  mor::Particle hf_jet_i;

			  if (use_hitfit_kinematics)
				  hf_jet_i = (sorted_hitfit_jets)[i];

			  // Declare kinematic variables without filling them
			 
			  double pt = 0;
			  double eta = 0;
			  double phi = 0;
			  float jmass = 0;
			  float energy = 0;
			  int pbtag = 0;

			  // Fill the kinematic variables with either the fitted values from hitfit, or the default ones

			  if (use_hitfit_kinematics){
				  pt = hf_jet_i.pt();
				  eta = hf_jet_i.eta();
				  phi = hf_jet_i.phi();
				  jmass = hf_jet_i.p4().M();
				  energy = hf_jet_i.p4().E();
			  }else{
				  pt = jet_i.pt();
				  eta = jet_i.eta();
				  phi = jet_i.phi();
				  jmass = jet_i.p4().M();
				  energy = jet_i.p4().E();
			  }

			  if(jet_i.bDiscriminator(btag_algo) >= btag_working_point){pbtag = 2;}
			  if(phi < 0){phi = 2*TMath::Pi() + phi;}

			  fill_leaves(0, lhcofile_all,event_number, run_number, 4, eta, phi, pt, jmass, jet_i.cmulti(), pbtag, 0, event_weight, (int)fabs(jet_i.mc_match_id()),partnum, energy,"reco");
		      partnum++;
			  //fill_leaves(0, lhcofile_all,event_number, run_number, 4, eta, phi, pt, jmass, jet_i.cmulti(), pbtag, 0, event_weight, 0,partnum, energy,"reco");

		  }
	  }else{
		  // Default option.  Loop over the jets and write the information in decreasing order of jet pt.  MadWeight will need to permutate over the jet combination.

	      //std::cout<<"WARNING!  ordering in LHCO file is not optimised, run with permutations on!"<<std::endl;

	      int njets = jets->size();
	      if(njets>5)
		  njets = 5;
	      if(!event_information->is_real_data()){
			  unambiguous_matching(jets);
		  }
	      for(int i=0; i < njets; i++){
		  mor::Jet jet_i = (*jets)[i];
		  double pt = jet_i.pt();
		  double eta = jet_i.eta();
		  double phi = jet_i.phi();
		  float jmass = jet_i.p4().M();
		  float energy = jet_i.p4().E();
		  int pbtag = 0;
		  if(jet_i.bDiscriminator(btag_algo) >= btag_working_point){pbtag = 2;}
		  if(phi < 0){phi = 2*TMath::Pi() + phi;}
		  fill_leaves(0, lhcofile_all,event_number, run_number, 4, eta, phi, pt, jmass, jet_i.cmulti(), pbtag, 0, event_weight, (int)fabs(jet_i.mc_match_id()),partnum, energy,"reco");
		      partnum++;
	      }
	      
	  }
  }
  if (electrons->size() + muons->size() > 1)
      std::cerr << "ERROR!  Both electrons and muons present in the event!  Please restrict to one lepton only in selection.  Code will seg fault (until solution can be found)." << std::endl;
  
  // Fill the electrons
  
  if(electrons->size() >= 1){
      mor::Electron electron = (*electrons)[0];    
      
      double pt;
      double eta;
      double phi;
      float jmass;
      float energy;
      
      // Fill the kinematic variables with either the fitted values from hitfit, or the default ones
      
      if (use_hitfit_kinematics){
		  pt = (*hitfit_electrons)[0].pt();
		  eta = (*hitfit_electrons)[0].eta();
		  phi = (*hitfit_electrons)[0].phi();
		  //jmass = (*hitfit_electrons)[0].p4().M();
		  energy = (*hitfit_electrons)[0].p4().E();
      }else{
		  pt = electron.pt();
		  eta = electron.eta();
		  phi = electron.phi();
		  //jmass = electron.p4().M();
		  energy = electron.p4().E();
      }
      
      jmass = electron.p4().M(); // HitFit sets the jmass for the leptons to be zero.  Get the jmass to the selected object here instead
      double mindR = 5.;
      int index = -1;
      
      // Set the lepton index as the value pointing to the closest jet in dR.
      
      if(use_hitfit_combi_seed){
		  if (electron.charge() == 1)
			  index = 0;
		  else if (electron.charge() == -1)
			  index = 3;
      }else{
		  for(unsigned int j=0; j < jets->size(); j++){
			  double dR = ROOT::Math::VectorUtil::DeltaR((*jets)[j], electron);
			  if(dR <= mindR){mindR = dR; index = j;}
		  }
      }	    
      
      if(phi < 0){phi = 2*TMath::Pi() + phi;}
      
      // Fill the electron leaves, 'index+1' assumes jet-jet-jet-jet-lepton-met ordering (with the jet indicies starting at 0)
      
      fill_leaves(0, lhcofile_all, event_number, run_number, 1, eta, phi, pt, jmass, (electron.charge())*1, index+1, 0, event_weight, (int)electron.mc_match_id(), 5, energy,"reco");
      partnum++;
  }
  
  // Fill the muons
  
  if(muons->size() >= 1){
	  mor::Muon muon = (*muons)[0];

	  double pt;
	  double eta;
	  double phi;
	  float jmass;
	  float energy;

	  // Fill the kinematic variables with either the fitted values from hitfit, or the default ones
	  
	  if (hitfit_state == 1 && use_hitfit_kinematics){
		  pt = (*hitfit_muons)[0].pt();
		  eta = (*hitfit_muons)[0].eta();
		  phi = (*hitfit_muons)[0].phi();
		  //jmass = (*hitfit_muons)[0].p4().M();
		  energy = (*hitfit_muons)[0].p4().E();
	  }else{
		  pt = muon.pt();
		  eta = muon.eta();
		  phi = muon.phi();
		  //jmass = muon.p4().M();
		  energy = muon.p4().E();
	  }

	  jmass = muon.p4().M(); // HitFit sets the jmass for the leptons to be zero.  Get the jmass to the selected object here instead
	  double mindR = 5.;
	  int index = -1;
	  
	  // Set the lepton index as the value pointing to the closest jet in dR.
	  
	  if(use_hitfit_combi_seed){
		  if (muon.charge() == 1)
			  index = 0;
		  else if (muon.charge() == -1)
			  index = 3;
	  }else{
		  for(unsigned int j=0; j < jets->size(); j++){
			  double dR = ROOT::Math::VectorUtil::DeltaR((*jets)[j], muon);
			  if(dR <= mindR){mindR = dR; index = j;}
		  }
	  }	    

	  if(phi < 0){phi = 2*TMath::Pi() + phi;}
	  
	  // Fill the muon leaves, 'index+1' assumes jet-jet-jet-jet-lepton-met ordering (with the jet indicies starting at 0)
	  
	  fill_leaves(0, lhcofile_all,event_number, run_number, 2, eta, phi, pt, jmass, (muon.charge())*1, index+1, 0, event_weight, muon.mc_match_id(),partnum,energy,"reco");
	  partnum++;
  }

  // Fill the mets

  if(mets->size() >= 1){
	  mor::MET met = (*mets)[0];

	  double pt;
	  double eta;
	  double phi;
	  float jmass;
	  float energy;
	  
	  // Fill the kinematic variables with either the fitted values from hitfit, or the default ones

	  if (hitfit_state == 1 && use_hitfit_kinematics){
		  pt = (*hitfit_mets)[0].pt();
		  eta = (*hitfit_mets)[0].eta();
		  phi = (*hitfit_mets)[0].phi();
		  jmass = (*hitfit_mets)[0].p4().M();
		  energy = (*hitfit_mets)[0].p4().E();
	  }else{
		  pt = met.pt();
		  eta = met.eta();
		  phi = met.phi();
		  jmass = met.p4().M();
		  energy = met.p4().E();
	  }

	
	  if(phi < 0){phi = 2*TMath::Pi() + phi;}
	  
	  fill_leaves(0, lhcofile_all,event_number, run_number, 6, eta, phi, pt, jmass, 1, 0, 0, event_weight, 0, partnum, energy,"reco"); // met.charge() (after jmass) has been hard-coded to 1 here for the time being due to the charge information for mets not being fully propagated through the broc
	  partnum++;

  }

  // Return 0 just to satisify the template function, it doesn't actually do anything
  return 0;

}

void broc::LHCOWriter::match_jets(std::vector<mor::Jet> *jets){
    
    jet1_match=-1;
    jet2_match=-1;
    jet3_match=-1;
    jet4_match=-1;
    int hadBmatched=0;
    int lepBmatched=0;
    int qmatched = 0;
    int qbarmatched = 0;
    jets_matched = false;
    std::vector<int> match_v;
    std::vector<mor::Jet> sorted_jets = *jets;
    for(int i=0;i<jets->size();i++){
	match_v.push_back(-1);
	mor::Jet jet_i = (*jets)[i];
	double dR_min = 999;
	double dR_hadB = ROOT::Math::VectorUtil::DeltaR(phadB->p4(), jet_i.p4());
	if(dR_hadB < dR_min){dR_min = dR_hadB;}
	double dR_lepB = ROOT::Math::VectorUtil::DeltaR(plepB->p4(), jet_i.p4());
	if(dR_lepB < dR_min){dR_min = dR_lepB;}
	double dR_q = ROOT::Math::VectorUtil::DeltaR(pq->p4(), jet_i.p4());
	if(dR_q < dR_min){dR_min = dR_q;}
	double dR_qbar = ROOT::Math::VectorUtil::DeltaR(pqbar->p4(), jet_i.p4());
	if(dR_qbar < dR_min){dR_min = dR_qbar;}
	if(dR_min < 0.3){
	    if(dR_min == dR_hadB){
		match_v[i]=1;
		hadBmatched++;}
	    if(dR_min == dR_lepB){
		match_v[i]=4;
		lepBmatched++;}
	    if(dR_min == dR_q){
		match_v[i]=2;
		qmatched++;}
	    if(dR_min == dR_qbar){
		match_v[i]=3;
		qbarmatched++;}
	}
    }
    for(int i = 0;i<match_v.size();i++){
	switch(i){
	case 0:
	    jet1_match=match_v[i];
	    break;
	case 1:
	    jet2_match=match_v[i];
	    break;
	case 2:
	    jet3_match=match_v[i];
	    break;
	case 3:
	    jet4_match=match_v[i];
	    break;
	}
    }
    if(hadBmatched==1&&lepBmatched==1&&qmatched==1&&qbarmatched==1)
	jets_matched = true;

	return;
}

void broc::LHCOWriter::unambiguous_matching(std::vector<mor::Jet> *jets)
{
    int n_unamb_match = 0;
    std::vector<std::pair<int,int> > matched_pairs;
    //std::cout<<"event number :"<<event_information->event_number()<<std::endl;
    jet1_match=-1;
    jet2_match=-1;
    jet3_match=-1;
    jet4_match=-1;
    jets_matched = false;
    for(int i=0;i<jets->size();i++){
		mor::Jet jet_i = (*jets)[i];
		int j = 0;
        int nmatch = 0;
        double dR_min = 999;
        int32_t type =0;
        double dR_hadB = ROOT::Math::VectorUtil::DeltaR(phadB->p4(), jet_i.p4());
        if(dR_hadB < 0.3){
            dR_min = dR_hadB;type =2;nmatch++;j=1;}
        double dR_q = ROOT::Math::VectorUtil::DeltaR(pq->p4(), jet_i.p4());
        if(dR_q < 0.3){
            dR_min = dR_q; type=1;nmatch++;j=2;}
        double dR_qbar = ROOT::Math::VectorUtil::DeltaR(pqbar->p4(), jet_i.p4());
        if(dR_qbar < 0.3){
            dR_min = dR_qbar;type=1;nmatch++;j=3;}
        double dR_lepB = ROOT::Math::VectorUtil::DeltaR(plepB->p4(), jet_i.p4());
        if(dR_lepB < 0.3){
            dR_min = dR_lepB;type=2;nmatch++;j=4;}
		if(nmatch == 1){
            n_unamb_match++;
            matched_pairs.push_back(std::make_pair(i,j));
		}else{
			matched_pairs.push_back(std::make_pair(i,-1));
		}
    }
    if(n_unamb_match == jets->size()){
		jets_matched = true;
	}
	for(int i=0;i<matched_pairs.size();i++){
		switch(matched_pairs[i].first){
		case 0:
			jet1_match = matched_pairs[i].second;
			break;
		case 1:
			jet2_match = matched_pairs[i].second;
			break;
		case 2:
			jet3_match = matched_pairs[i].second;
			break;
		case 3:
			jet4_match = matched_pairs[i].second;
			break;
		}
    }
    matched_pairs.clear();
}



void broc::LHCOWriter::fill_leaves(int is_generated, FILE * file, double pevent_number, double prun_number, int32_t ptyp, float peta, float pphi, float ppt, float pjmass, float pntrk, int pbtag, float phadOverem, double pevent_weight, int pmc_id, int ppartnum, float pE, std::string event_type)
{  
    //Method called to fill in the lhco text file as well as the output root file containing reco and eventually both gen_level and smeared gen_level information
	  
    //Fill the lhco text file
    fprintf(file,"%i\t%i\t%.3f\t%.3f\t%.2f\t%.2f\t%.1f\t%i\t%.2f\t%.2f\t%d\t\n",ppartnum,ptyp,peta,pphi,ppt,fabs(pjmass),pntrk,pbtag,phadOverem,pevent_weight,pmc_id);
    
    //Eventually write the peNtuple file
    if(write_ntuple){
	TLorentzVector *tmp=new TLorentzVector();                                                                                 
        tmp->SetPtEtaPhiM(ppt,peta,pphi,pjmass); 
	if(event_type == "gen"){
	    if(ppartnum == 1){
		tjet1 = tmp;
		gen_filled++;}
	    if(ppartnum == 2){
		tjet2 = tmp;
                gen_filled++;}
	    if(ppartnum == 3){
		tjet3 = tmp;                
		gen_filled++;}
	    if(ppartnum == 4){
		tjet4 = tmp;
                gen_filled++;}
	    if(ppartnum == 5){
		tlepton = tmp;
		lepID = pmc_id;
                gen_filled++;}
	    if(ppartnum == 6){
		neutrino = tmp;
		gen_filled++;}
	}
	
	if(event_type == "reco"){
	    if(ppartnum == 1){
		if(event_information->is_real_data()){
		    trueMass = -1;
		    trueJES = -1;
		    tjet1=NULL;
		    tjet2=NULL;
		    tjet3=NULL;
		    tjet4=NULL;
		    tlepton=NULL;
		    neutrino=NULL;
		    lepID =-1;}
		tevent_number = pevent_number;
		trun_number = prun_number;
		tevent_weight = pevent_weight;
		weight_manager->get_individual_weights();
		pdf_weight = weight_manager->get_pdf_weight();
		pu_weight = weight_manager->get_pu_weight();
		pu_weight_up = weight_manager->get_pu_weight_up();
		pu_weight_down = weight_manager->get_pu_weight_down();
		trigger_weight_up = weight_manager->get_trigger_weight_up();
		trigger_weight_down = weight_manager->get_trigger_weight_down();
		trigger_weight = weight_manager->get_trigger_weight();
		lepID_weight_up = weight_manager->get_lepID_weight_up();
		lepID_weight_down = weight_manager->get_lepID_weight_down();
		lepID_weight = weight_manager->get_lepID_weight();
		btag_weight = weight_manager->get_btag_weight();
		btag_weight_up = weight_manager->get_btag_weight_up();
		btag_weight_down = weight_manager->get_btag_weight_down();
		mistag_weight_up = weight_manager->get_mistag_weight_up();
		mistag_weight_down = weight_manager->get_mistag_weight_down();
		topPt_weight = weight_manager->get_topPt_weight();
		bfrag_weight = weight_manager->get_bfrag_weight();
		//std::cout<<"LHCO : bfrag weight = "<<bfrag_weight<<std::endl;
		jet1 = tmp;
		jet1_typ = ptyp;
		jet1_jmass = pjmass;
		jet1_ntrack = pntrk;
		jet1_btag = pbtag;
		jet1_mc_id = pmc_id;
		reco_filled++;}
	    if(ppartnum == 2){
		jet2 = tmp;
		jet2_typ = ptyp;
                jet2_jmass = pjmass;
                jet2_ntrack = pntrk;
                jet2_btag = pbtag;
                jet2_mc_id = pmc_id;
		reco_filled++;}
	    if(ppartnum == 3){
		jet3 = tmp;
		jet3_typ = ptyp;
                jet3_jmass = pjmass;
                jet3_ntrack = pntrk;
                jet3_btag = pbtag;
                jet3_mc_id = pmc_id;
		reco_filled++;}
	    if(ppartnum == 4){
		jet4 = tmp;
		jet4_typ = ptyp;
                jet4_jmass = pjmass;
                jet4_ntrack = pntrk;
                jet4_btag = pbtag;
                jet4_mc_id = pmc_id;
		reco_filled++;}
	    if(ppartnum == 5){
		lepton = tmp;
		lepton_typ = ptyp;
                lepton_jmass = pjmass;
                lepton_ntrack = pntrk;
                lepton_btag = pbtag;
                lepton_mc_id = pmc_id;
		reco_filled++;}
	    if(ppartnum == 6){
		neutrino = tmp;
		reco_filled++;}
	}
	if(reco_filled == 6 && (gen_filled == 6 || data_only)){
	    //Check that all the leaves are filled in, then fill the tree. Each LHCOEvent gives a branch in the final tree
	    tree->Fill();
	    reco_filled = 0;
	    gen_filled = 0;}
    }
}
