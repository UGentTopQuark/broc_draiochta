#include "../../interface/LHCOWriter/JetEnergywriter.h"

broc::JetEnergywriter::JetEnergywriter(eire::HandleHolder *handle_holder)
{
    this->handle_holder = handle_holder;
    eire::ConfigReader *config_reader = handle_holder->get_config_reader();
    enable_smearing = config_reader->get_bool_var("enable_smearing","lhco_writer",false);  
    set_handles(handle_holder);
    id = "_"+handle_holder->get_ident();
    Ep = 0;
    Ej = -99;
    reco_eta = 0;
    reco_phi = 0;
    reco_pt = 0;
    btag = 0;
    event_number = 0;
    run_number = 0;
    event_weight = 0;
    phadB = gen_event->hadB(); //particle 2                                                                                                                          
    plepB = gen_event->lepB(); //particle 3                                                                                                                          
    pq = gen_event->q(); //particle 4                                                                                                                                
    pqbar = gen_event->qbar(); //particle 5   
    n_jets=0;
    hadBmatch=0;
    lepBmatch=0;
    qmatch=0;
    qbarmatch=0;

}

broc::JetEnergywriter::~JetEnergywriter()
{
}

void broc::JetEnergywriter::set_handles(eire::HandleHolder *handle_holder)
{
    this->handle_holder = handle_holder;
    this->event_information = handle_holder->get_event_information();
    this->electrons = handle_holder->get_tight_electrons();
    this->muons = handle_holder->get_tight_muons();
    this->mets = handle_holder->get_corrected_mets();
    this->jets = handle_holder->get_tight_jets();
    this->gen_event = handle_holder->get_ttbar_gen_evt();
    this->histo_writer = handle_holder->get_histo_writer();
    book_histos();
}

void broc::JetEnergywriter::book_histos()
{
    histos1d[("mindR"+id).c_str()]=histo_writer->create_1d(("mindR"+id).c_str(),"mindR",100,0,1,"mindR");
    histos1d[("n_jets_matched"+id).c_str()]=histo_writer->create_1d(("n_jets_matched"+id).c_str(),"n_jets_matched",5,-0.5,4.5,"n_jets_matched");
    histos1d[("n_times_partonmatch"+id).c_str()]=histo_writer->create_1d(("n_times_partonmatch"+id).c_str(),"n_times_partonmatch",5,-0.5,4.5,"n_times_partonmatch");
}

void broc::JetEnergywriter::book_branches()
{
    tree->Branch("Ep",&Ep,"Ep/F");
    tree->Branch("Ej",&Ej,"Ej/F");
    tree->Branch("reco_eta",&reco_eta,"reco_eta/F");
    tree->Branch("reco_phi",&reco_phi,"reco_phi/F");
    tree->Branch("reco_pt",&reco_pt,"reco_pt/F");
    tree->Branch("btag",&btag,"btag/I");
    tree->Branch("event_weight",&event_weight,"event_weight/F");
    tree->Branch("event_number",&event_number,"event_number/I");
    tree->Branch("run_number",&run_number,"run_number/I");
}

void broc::JetEnergywriter::matching_jets(mor::Jet jet)
{

    double dR_min = 999;
    int32_t type =0;
    mor::Particle *matched_parton = NULL;
    double dR_hadB = ROOT::Math::VectorUtil::DeltaR(phadB->p4(), jet.p4());
    if(dR_hadB < dR_min){dR_min = dR_hadB; matched_parton = phadB;type =2;}
    double dR_lepB = ROOT::Math::VectorUtil::DeltaR(plepB->p4(), jet.p4());
    if(dR_lepB < dR_min){dR_min = dR_lepB; matched_parton = plepB;type=2;}
    double dR_q = ROOT::Math::VectorUtil::DeltaR(pq->p4(), jet.p4());
    if(dR_q < dR_min){dR_min = dR_q; matched_parton = pq;type=1;}
    double dR_qbar = ROOT::Math::VectorUtil::DeltaR(pqbar->p4(), jet.p4());
    if(dR_qbar < dR_min){dR_min = dR_qbar; matched_parton = pqbar;type=1;}
    histos1d[("mindR"+id).c_str()]->Fill(dR_min);
    if(dR_min < 0.3){
	if(dR_min == dR_hadB){hadBmatch++;}
	if(dR_min == dR_lepB){lepBmatch++;}
	if(dR_min == dR_q){qmatch++;}
	if(dR_min == dR_qbar){qbarmatch++;}
	n_jets++;
	Ej = jet.p4().E();
	reco_eta = jet.eta();
	reco_phi = jet.phi();
	reco_pt = jet.pt();
	event_weight = handle_holder->get_event_weight();
	event_number = event_information->event_number();
	run_number = event_information->run();
	int b = 0;
	if(jet.bDiscriminator("combinedSecondaryVertexBJetTags") >= 0.679){b = 1;}  
	btag = b;
    }
    tree->Fill();
}

bool broc::JetEnergywriter::fill_branches()
{
    if(jets->size() >= 4){
	for(int i=0; i < 4; i++){
	    mor::Jet jet_i = (*jets)[i];
	    matching_jets(jet_i);
	}
    }

    histos1d[("n_jets_matched"+id).c_str()]->Fill(n_jets);
    histos1d[("n_times_partonmatch"+id).c_str()]->Fill(hadBmatch);
    histos1d[("n_times_partonmatch"+id).c_str()]->Fill(lepBmatch);
    histos1d[("n_times_partonmatch"+id).c_str()]->Fill(qmatch);
    histos1d[("n_times_partonmatch"+id).c_str()]->Fill(qbarmatch);

    n_jets=0; hadBmatch=0; lepBmatch = 0; qmatch = 0; qbarmatch=0;

	return 0;
}

