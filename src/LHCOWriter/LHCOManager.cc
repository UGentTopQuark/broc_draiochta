#include "../../interface/LHCOWriter/LHCOManager.h"


broc::LHCOManager::LHCOManager(eire::HandleHolder *handle_holder){
    
  eire::ConfigReader *config_reader = handle_holder->get_config_reader();
  std::string output_dir = config_reader->get_var("output_directory");
  std::string suffix = config_reader->get_var("outfile_suffix");

  lhco_writer = NULL;
  jetenergy_writer = NULL;
  enable_jet_energy = config_reader->get_bool_var("enable_jet_energy","lhco_writer",false);
  use_hitfit_kinematics = config_reader->get_bool_var("use_hitfit_kinematics", "lhco_writer", false);
  use_hitfit = config_reader->get_bool_var("enable_kinematic_fit_module", "global", false);
  use_hitfit_combi_seed = config_reader->get_bool_var("use_hitfit_combi_seed", "lhco_writer", false);
  write_ntuple = config_reader->get_bool_var("write_ntuple", "lhco_writer", false);

  std::string filename = output_dir+"/"+"LHCOTree_"+config_reader->get_var("trueMass","lhco_writer",false)+"_"+config_reader->get_var("JES_variation","lhco_writer",false)+"_"+handle_holder->get_ident()+suffix+".root";
  while(filename.find("|") != std::string::npos)
	  filename.replace(filename.find("|"),1,"_");
  
  std::string directory_name = "LHCO";
  
  ntuple_file = new TFile(filename.c_str(), "RECREATE");
  if(directory_name != ""){
	  ntuple_file->mkdir(directory_name.c_str());
	  ntuple_file->Cd(directory_name.c_str());
  }
  
  set_handles(handle_holder);

  lhco_writer = new broc::LHCOWriter(handle_holder);
  lhco_writer->set_outfile(ntuple_file, directory_name);
  lhco_writer->create_tree("Particle_tree");
  lhco_writer->book_branches();
  lhco_writer->set_handles(handle_holder);

  if(enable_jet_energy){
	  jetenergy_writer = new broc::JetEnergywriter(handle_holder);
	  jetenergy_writer->set_outfile(ntuple_file, directory_name);
	  jetenergy_writer->create_tree("JetPartonMatched_tree");
	  jetenergy_writer->set_handles(handle_holder);
	  jetenergy_writer->book_branches();
  }
}

broc::LHCOManager::~LHCOManager()
{
  if(lhco_writer){delete lhco_writer; lhco_writer = NULL;}
  if(jetenergy_writer){delete jetenergy_writer; jetenergy_writer = NULL;}
  if(ntuple_file){
    ntuple_file->Write();
    if(!(write_ntuple||enable_jet_energy))
	ntuple_file->Delete("T*;*");
    ntuple_file->Close();
    delete ntuple_file;
    ntuple_file = NULL;
  }
}

void broc::LHCOManager::set_handles(eire::HandleHolder *handle_holder)
{
  this->gen_event = handle_holder->get_ttbar_gen_evt();
}

void broc::LHCOManager::next_event()
{
	// Fill the main lhco file with the normal event kinematics

	lhco_writer->fill_branches();

	// If hitfit is enabled, fill an additional lhco file with the fitted kinematics

	//if(hitfit_state &&use_hitfit_kinematics && use_hitfit && use_hitfit_combi_seed)
		//lhco_writer->fill_branches(use_hitfit_kinematics);

	if(enable_jet_energy)
		jetenergy_writer->fill_branches();
	else{return;}
}
