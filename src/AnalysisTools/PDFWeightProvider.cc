#include "../../interface/AnalysisTools/PDFWeightProvider.h"

double eire::PDFWeightProvider::get_event_weight(std::string pdf_name, unsigned int npdf)
{
	std::vector<double> pdf_weights = pdf_set_provider->id_value<double>(pdf_name, pdf_evt_weights);
	if(pdf_weights.size() > npdf){
		return pdf_weights[npdf];
	}else{
		std::cerr << "WARNING: eire::PDFWeightProvider::get_event_weight(): invalid pdf index: " << npdf << std::endl;
		return -1;
	}
}

std::vector<double> eire::PDFWeightProvider::get_event_weight(std::string pdf_name)
{
	if(pdf_set_provider == NULL)
		std::cout << "ERROR: PDFWeightProvider. pdf_set_provider = NULL" << std::endl;

	return pdf_set_provider->id_value(pdf_name, pdf_evt_weights);
}

void eire::PDFWeightProvider::set_pdf_set_provider(mor::IDChecker *pdf_set_provider)
{
	this->pdf_set_provider = pdf_set_provider;
}

void eire::PDFWeightProvider::set_pdf_evt_weights(std::vector<std::vector<double> > *pdf_evt_weights)
{
	this->pdf_evt_weights = pdf_evt_weights;
}

unsigned int eire::PDFWeightProvider::get_nevt_weights(std::string pdf_name)
{
	std::vector<double> pdf_weights = pdf_set_provider->id_value<double>(pdf_name, pdf_evt_weights);
	return pdf_weights.size();
}
