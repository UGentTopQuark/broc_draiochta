#include "../../interface/AnalysisTools/RunSelector.h"

RunSelector::RunSelector()
{
	accept_all_events = false;
}

RunSelector::~RunSelector()
{
}

void RunSelector::set_event_information(mor::EventInformation *evt_info)
{
	this->evt_info = evt_info;
}

bool RunSelector::is_good()
{
	if(accept_all_events) return true;
	// JSON AUTO FILL BEGIN -- do not edit this comment
	double cur_run_number = evt_info->run();
	double cur_lumi_block = evt_info->lumi_block();
	if(	( cur_run_number == 190645 && (
	  (cur_lumi_block >= 10 && cur_lumi_block <= 110) ) ) ||
	( cur_run_number == 190646 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 111) ) ) ||
	( cur_run_number == 190659 && (
	  (cur_lumi_block >= 33 && cur_lumi_block <= 167) ) ) ||
	( cur_run_number == 190679 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 55) ) ) ||
	( cur_run_number == 190688 && (
	  (cur_lumi_block >= 69 && cur_lumi_block <= 249) ) ) ||
	( cur_run_number == 190702 && (
	  (cur_lumi_block >= 124 && cur_lumi_block <= 169) ||
	  (cur_lumi_block >= 51 && cur_lumi_block <= 53) ||
	  (cur_lumi_block >= 55 && cur_lumi_block <= 122) ) ) ||
	( cur_run_number == 190703 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 252) ) ) ||
	( cur_run_number == 190704 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 3) ) ) ||
	( cur_run_number == 190705 && (
	  (cur_lumi_block >= 81 && cur_lumi_block <= 336) ||
	  (cur_lumi_block >= 353 && cur_lumi_block <= 383) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 5) ||
	  (cur_lumi_block >= 338 && cur_lumi_block <= 350) ||
	  (cur_lumi_block >= 7 && cur_lumi_block <= 65) ) ) ||
	( cur_run_number == 190706 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 126) ) ) ||
	( cur_run_number == 190707 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 237) ||
	  (cur_lumi_block >= 239 && cur_lumi_block <= 257) ) ) ||
	( cur_run_number == 190708 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 189) ) ) ||
	( cur_run_number == 190733 && (
	  (cur_lumi_block >= 392 && cur_lumi_block <= 460) ||
	  (cur_lumi_block >= 99 && cur_lumi_block <= 389) ||
	  (cur_lumi_block >= 71 && cur_lumi_block <= 96) ) ) ||
	( cur_run_number == 190736 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 80) ||
	  (cur_lumi_block >= 83 && cur_lumi_block <= 185) ) ) ||
	( cur_run_number == 190738 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 130) ||
	  (cur_lumi_block >= 229 && cur_lumi_block <= 349) ||
	  (cur_lumi_block >= 133 && cur_lumi_block <= 226) ) ) ||
	( cur_run_number == 190782 && (
	  (cur_lumi_block >= 401 && cur_lumi_block <= 409) ||
	  (cur_lumi_block >= 236 && cur_lumi_block <= 399) ||
	  (cur_lumi_block >= 184 && cur_lumi_block <= 233) ||
	  (cur_lumi_block >= 55 && cur_lumi_block <= 181) ) ) ||
	( cur_run_number == 190895 && (
	  (cur_lumi_block >= 210 && cur_lumi_block <= 302) ||
	  (cur_lumi_block >= 64 && cur_lumi_block <= 202) ||
	  (cur_lumi_block >= 305 && cur_lumi_block <= 584) ||
	  (cur_lumi_block >= 587 && cur_lumi_block <= 948) ) ) ||
	( cur_run_number == 190906 && (
	  (cur_lumi_block >= 356 && cur_lumi_block <= 496) ||
	  (cur_lumi_block >= 259 && cur_lumi_block <= 354) ||
	  (cur_lumi_block >= 73 && cur_lumi_block <= 256) ) ) ||
	( cur_run_number == 190945 && (
	  (cur_lumi_block >= 124 && cur_lumi_block <= 207) ) ) ||
	( cur_run_number == 190949 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 81) ) ) ||
	( cur_run_number == 191043 && (
	  (cur_lumi_block >= 45 && cur_lumi_block <= 46) ) ) ||
	( cur_run_number == 191046 && (
	  (cur_lumi_block >= 119 && cur_lumi_block <= 180) ||
	  (cur_lumi_block >= 92 && cur_lumi_block <= 116) ||
	  (cur_lumi_block >= 84 && cur_lumi_block <= 88) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 21) ||
	  (cur_lumi_block >= 185 && cur_lumi_block <= 239) ||
	  (cur_lumi_block >= 183 && cur_lumi_block <= 183) ||
	  (cur_lumi_block >= 24 && cur_lumi_block <= 82) ) ) ||
	( cur_run_number == 191056 && (
	  (cur_lumi_block >= 4 && cur_lumi_block <= 9) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 1) ||
	  (cur_lumi_block >= 19 && cur_lumi_block <= 19) ||
	  (cur_lumi_block >= 16 && cur_lumi_block <= 17) ) ) ||
	( cur_run_number == 191057 && (
	  (cur_lumi_block >= 4 && cur_lumi_block <= 40) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 1) ) ) ||
	( cur_run_number == 191062 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 1) ||
	  (cur_lumi_block >= 3 && cur_lumi_block <= 3) ||
	  (cur_lumi_block >= 216 && cur_lumi_block <= 541) ||
	  (cur_lumi_block >= 5 && cur_lumi_block <= 214) ) ) ||
	( cur_run_number == 191090 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 55) ) ) ||
	( cur_run_number == 191201 && (
	  (cur_lumi_block >= 52 && cur_lumi_block <= 79) ||
	  (cur_lumi_block >= 38 && cur_lumi_block <= 49) ) ) ||
	( cur_run_number == 191202 && (
	  (cur_lumi_block >= 87 && cur_lumi_block <= 105) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 64) ||
	  (cur_lumi_block >= 66 && cur_lumi_block <= 68) ||
	  (cur_lumi_block >= 108 && cur_lumi_block <= 118) ) ) ||
	( cur_run_number == 191226 && (
	  (cur_lumi_block >= 833 && cur_lumi_block <= 1454) ||
	  (cur_lumi_block >= 81 && cur_lumi_block <= 831) ||
	  (cur_lumi_block >= 77 && cur_lumi_block <= 78) ||
	  (cur_lumi_block >= 1469 && cur_lumi_block <= 1507) ||
	  (cur_lumi_block >= 1510 && cur_lumi_block <= 1686) ||
	  (cur_lumi_block >= 1456 && cur_lumi_block <= 1466) ) ) ||
	( cur_run_number == 191247 && (
	  (cur_lumi_block >= 1049 && cur_lumi_block <= 1140) ||
	  (cur_lumi_block >= 1034 && cur_lumi_block <= 1046) ||
	  (cur_lumi_block >= 1190 && cur_lumi_block <= 1214) ||
	  (cur_lumi_block >= 283 && cur_lumi_block <= 606) ||
	  (cur_lumi_block >= 156 && cur_lumi_block <= 280) ||
	  (cur_lumi_block >= 622 && cur_lumi_block <= 818) ||
	  (cur_lumi_block >= 1217 && cur_lumi_block <= 1224) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 153) ||
	  (cur_lumi_block >= 608 && cur_lumi_block <= 620) ||
	  (cur_lumi_block >= 821 && cur_lumi_block <= 834) ||
	  (cur_lumi_block >= 837 && cur_lumi_block <= 1031) ||
	  (cur_lumi_block >= 1143 && cur_lumi_block <= 1187) ) ) ||
	( cur_run_number == 191248 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 103) ) ) ||
	( cur_run_number == 191264 && (
	  (cur_lumi_block >= 155 && cur_lumi_block <= 189) ||
	  (cur_lumi_block >= 59 && cur_lumi_block <= 79) ||
	  (cur_lumi_block >= 82 && cur_lumi_block <= 152) ) ) ||
	( cur_run_number == 191271 && (
	  (cur_lumi_block >= 225 && cur_lumi_block <= 363) ||
	  (cur_lumi_block >= 56 && cur_lumi_block <= 223) ) ) ||
	( cur_run_number == 191276 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 16) ) ) ||
	( cur_run_number == 191277 && (
	  (cur_lumi_block >= 778 && cur_lumi_block <= 811) ||
	  (cur_lumi_block >= 579 && cur_lumi_block <= 775) ||
	  (cur_lumi_block >= 167 && cur_lumi_block <= 253) ||
	  (cur_lumi_block >= 813 && cur_lumi_block <= 849) ||
	  (cur_lumi_block >= 537 && cur_lumi_block <= 576) ||
	  (cur_lumi_block >= 460 && cur_lumi_block <= 535) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 28) ||
	  (cur_lumi_block >= 30 && cur_lumi_block <= 164) ||
	  (cur_lumi_block >= 255 && cur_lumi_block <= 457) ) ) ||
	( cur_run_number == 191367 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 2) ) ) ||
	( cur_run_number == 191411 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 23) ) ) ||
	( cur_run_number == 191695 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 1) ) ) ||
	( cur_run_number == 191718 && (
	  (cur_lumi_block >= 98 && cur_lumi_block <= 207) ||
	  (cur_lumi_block >= 43 && cur_lumi_block <= 95) ) ) ||
	( cur_run_number == 191720 && (
	  (cur_lumi_block >= 3 && cur_lumi_block <= 15) ||
	  (cur_lumi_block >= 17 && cur_lumi_block <= 181) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 1) ) ) ||
	( cur_run_number == 191721 && (
	  (cur_lumi_block >= 36 && cur_lumi_block <= 183) ||
	  (cur_lumi_block >= 3 && cur_lumi_block <= 34) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 1) ||
	  (cur_lumi_block >= 186 && cur_lumi_block <= 189) ) ) ||
	( cur_run_number == 191726 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 13) ) ) ||
	( cur_run_number == 191810 && (
	  (cur_lumi_block >= 15 && cur_lumi_block <= 15) ||
	  (cur_lumi_block >= 52 && cur_lumi_block <= 92) ||
	  (cur_lumi_block >= 22 && cur_lumi_block <= 49) ) ) ||
	( cur_run_number == 191830 && (
	  (cur_lumi_block >= 304 && cur_lumi_block <= 393) ||
	  (cur_lumi_block >= 245 && cur_lumi_block <= 301) ||
	  (cur_lumi_block >= 54 && cur_lumi_block <= 242) ) ) ||
	( cur_run_number == 191833 && (
	  (cur_lumi_block >= 3 && cur_lumi_block <= 103) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 1) ) ) ||
	( cur_run_number == 191834 && (
	  (cur_lumi_block >= 33 && cur_lumi_block <= 74) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 30) ||
	  (cur_lumi_block >= 302 && cur_lumi_block <= 352) ||
	  (cur_lumi_block >= 77 && cur_lumi_block <= 299) ) ) ||
	( cur_run_number == 191837 && (
	  (cur_lumi_block >= 47 && cur_lumi_block <= 53) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 44) ||
	  (cur_lumi_block >= 56 && cur_lumi_block <= 65) ) ) ||
	( cur_run_number == 191856 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 133) ) ) ||
	( cur_run_number == 191859 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 28) ||
	  (cur_lumi_block >= 31 && cur_lumi_block <= 126) ) ) ||
	( cur_run_number == 193093 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 33) ) ) ||
	( cur_run_number == 193123 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 27) ) ) ||
	( cur_run_number == 193124 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 52) ) ) ||
	( cur_run_number == 193192 && (
	  (cur_lumi_block >= 58 && cur_lumi_block <= 86) ) ) ||
	( cur_run_number == 193193 && (
	  (cur_lumi_block >= 11 && cur_lumi_block <= 83) ||
	  (cur_lumi_block >= 276 && cur_lumi_block <= 495) ||
	  (cur_lumi_block >= 497 && cur_lumi_block <= 506) ||
	  (cur_lumi_block >= 122 && cur_lumi_block <= 160) ||
	  (cur_lumi_block >= 8 && cur_lumi_block <= 8) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 6) ||
	  (cur_lumi_block >= 162 && cur_lumi_block <= 274) ||
	  (cur_lumi_block >= 86 && cur_lumi_block <= 120) ) ) ||
	( cur_run_number == 193207 && (
	  (cur_lumi_block >= 54 && cur_lumi_block <= 182) ) ) ||
	( cur_run_number == 193334 && (
	  (cur_lumi_block >= 29 && cur_lumi_block <= 172) ) ) ||
	( cur_run_number == 193336 && (
	  (cur_lumi_block >= 267 && cur_lumi_block <= 492) ||
	  (cur_lumi_block >= 495 && cur_lumi_block <= 684) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 264) ||
	  (cur_lumi_block >= 687 && cur_lumi_block <= 729) ||
	  (cur_lumi_block >= 732 && cur_lumi_block <= 951) ) ) ||
	( cur_run_number == 193541 && (
	  (cur_lumi_block >= 578 && cur_lumi_block <= 619) ||
	  (cur_lumi_block >= 103 && cur_lumi_block <= 413) ||
	  (cur_lumi_block >= 77 && cur_lumi_block <= 101) ||
	  (cur_lumi_block >= 416 && cur_lumi_block <= 575) ) ) ||
	( cur_run_number == 193556 && (
	  (cur_lumi_block >= 41 && cur_lumi_block <= 83) ) ) ||
	( cur_run_number == 193557 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 84) ) ) ||
	( cur_run_number == 193575 && (
	  (cur_lumi_block >= 660 && cur_lumi_block <= 752) ||
	  (cur_lumi_block >= 417 && cur_lumi_block <= 658) ||
	  (cur_lumi_block >= 176 && cur_lumi_block <= 349) ||
	  (cur_lumi_block >= 48 && cur_lumi_block <= 173) ||
	  (cur_lumi_block >= 397 && cur_lumi_block <= 415) ||
	  (cur_lumi_block >= 351 && cur_lumi_block <= 394) ) ) ||
	( cur_run_number == 193621 && (
	  (cur_lumi_block >= 1139 && cur_lumi_block <= 1193) ||
	  (cur_lumi_block >= 772 && cur_lumi_block <= 976) ||
	  (cur_lumi_block >= 1373 && cur_lumi_block <= 1654) ||
	  (cur_lumi_block >= 1195 && cur_lumi_block <= 1371) ||
	  (cur_lumi_block >= 1056 && cur_lumi_block <= 1137) ||
	  (cur_lumi_block >= 60 && cur_lumi_block <= 570) ||
	  (cur_lumi_block >= 573 && cur_lumi_block <= 769) ||
	  (cur_lumi_block >= 979 && cur_lumi_block <= 1053) ) ) ||
	( cur_run_number == 193834 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 35) ) ) ||
	( cur_run_number == 193835 && (
	  (cur_lumi_block >= 22 && cur_lumi_block <= 26) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 20) ) ) ||
	( cur_run_number == 193836 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 2) ) ) ||
	( cur_run_number == 193998 && (
	  (cur_lumi_block >= 115 && cur_lumi_block <= 278) ||
	  (cur_lumi_block >= 66 && cur_lumi_block <= 113) ) ) ||
	( cur_run_number == 193999 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 45) ) ) ||
	( cur_run_number == 194027 && (
	  (cur_lumi_block >= 57 && cur_lumi_block <= 113) ) ) ||
	( cur_run_number == 194050 && (
	  (cur_lumi_block >= 53 && cur_lumi_block <= 113) ||
	  (cur_lumi_block >= 492 && cur_lumi_block <= 814) ||
	  (cur_lumi_block >= 816 && cur_lumi_block <= 1435) ||
	  (cur_lumi_block >= 394 && cur_lumi_block <= 490) ||
	  (cur_lumi_block >= 116 && cur_lumi_block <= 273) ||
	  (cur_lumi_block >= 1437 && cur_lumi_block <= 1735) ||
	  (cur_lumi_block >= 357 && cur_lumi_block <= 369) ||
	  (cur_lumi_block >= 372 && cur_lumi_block <= 391) ||
	  (cur_lumi_block >= 275 && cur_lumi_block <= 355) ||
	  (cur_lumi_block >= 1760 && cur_lumi_block <= 1888) ) ) ||
	( cur_run_number == 194051 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 12) ) ) ||
	( cur_run_number == 194052 && (
	  (cur_lumi_block >= 102 && cur_lumi_block <= 166) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 99) ) ) ||
	( cur_run_number == 194075 && (
	  (cur_lumi_block >= 111 && cur_lumi_block <= 111) ||
	  (cur_lumi_block >= 48 && cur_lumi_block <= 101) ||
	  (cur_lumi_block >= 109 && cur_lumi_block <= 109) ||
	  (cur_lumi_block >= 103 && cur_lumi_block <= 103) ||
	  (cur_lumi_block >= 105 && cur_lumi_block <= 107) ) ) ||
	( cur_run_number == 194076 && (
	  (cur_lumi_block >= 11 && cur_lumi_block <= 55) ||
	  (cur_lumi_block >= 165 && cur_lumi_block <= 228) ||
	  (cur_lumi_block >= 565 && cur_lumi_block <= 748) ||
	  (cur_lumi_block >= 509 && cur_lumi_block <= 527) ||
	  (cur_lumi_block >= 267 && cur_lumi_block <= 507) ||
	  (cur_lumi_block >= 230 && cur_lumi_block <= 264) ||
	  (cur_lumi_block >= 58 && cur_lumi_block <= 163) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 9) ||
	  (cur_lumi_block >= 530 && cur_lumi_block <= 538) ||
	  (cur_lumi_block >= 541 && cur_lumi_block <= 562) ) ) ||
	( cur_run_number == 194108 && (
	  (cur_lumi_block >= 398 && cur_lumi_block <= 433) ||
	  (cur_lumi_block >= 436 && cur_lumi_block <= 452) ||
	  (cur_lumi_block >= 593 && cur_lumi_block <= 668) ||
	  (cur_lumi_block >= 579 && cur_lumi_block <= 590) ||
	  (cur_lumi_block >= 266 && cur_lumi_block <= 373) ||
	  (cur_lumi_block >= 454 && cur_lumi_block <= 577) ||
	  (cur_lumi_block >= 81 && cur_lumi_block <= 161) ||
	  (cur_lumi_block >= 376 && cur_lumi_block <= 396) ||
	  (cur_lumi_block >= 164 && cur_lumi_block <= 264) ||
	  (cur_lumi_block >= 671 && cur_lumi_block <= 872) ) ) ||
	( cur_run_number == 194115 && (
	  (cur_lumi_block >= 819 && cur_lumi_block <= 857) ||
	  (cur_lumi_block >= 348 && cur_lumi_block <= 493) ||
	  (cur_lumi_block >= 186 && cur_lumi_block <= 338) ||
	  (cur_lumi_block >= 66 && cur_lumi_block <= 184) ||
	  (cur_lumi_block >= 496 && cur_lumi_block <= 731) ||
	  (cur_lumi_block >= 340 && cur_lumi_block <= 346) ) ) ||
	( cur_run_number == 194117 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 38) ) ) ||
	( cur_run_number == 194119 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 229) ||
	  (cur_lumi_block >= 232 && cur_lumi_block <= 261) ) ) ||
	( cur_run_number == 194120 && (
	  (cur_lumi_block >= 165 && cur_lumi_block <= 406) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 162) ) ) ||
	( cur_run_number == 194150 && (
	  (cur_lumi_block >= 129 && cur_lumi_block <= 261) ||
	  (cur_lumi_block >= 42 && cur_lumi_block <= 127) ||
	  (cur_lumi_block >= 264 && cur_lumi_block <= 311) ) ) ||
	( cur_run_number == 194151 && (
	  (cur_lumi_block >= 240 && cur_lumi_block <= 617) ||
	  (cur_lumi_block >= 621 && cur_lumi_block <= 621) ||
	  (cur_lumi_block >= 75 && cur_lumi_block <= 191) ||
	  (cur_lumi_block >= 193 && cur_lumi_block <= 238) ||
	  (cur_lumi_block >= 47 && cur_lumi_block <= 72) ||
	  (cur_lumi_block >= 623 && cur_lumi_block <= 623) ||
	  (cur_lumi_block >= 619 && cur_lumi_block <= 619) ) ) ||
	( cur_run_number == 194153 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 115) ) ) ||
	( cur_run_number == 194199 && (
	  (cur_lumi_block >= 339 && cur_lumi_block <= 402) ||
	  (cur_lumi_block >= 229 && cur_lumi_block <= 336) ||
	  (cur_lumi_block >= 96 && cur_lumi_block <= 227) ) ) ||
	( cur_run_number == 194210 && (
	  (cur_lumi_block >= 3 && cur_lumi_block <= 195) ||
	  (cur_lumi_block >= 198 && cur_lumi_block <= 217) ||
	  (cur_lumi_block >= 361 && cur_lumi_block <= 555) ||
	  (cur_lumi_block >= 220 && cur_lumi_block <= 359) ) ) ||
	( cur_run_number == 194223 && (
	  (cur_lumi_block >= 61 && cur_lumi_block <= 112) ) ) ||
	( cur_run_number == 194224 && (
	  (cur_lumi_block >= 253 && cur_lumi_block <= 309) ||
	  (cur_lumi_block >= 389 && cur_lumi_block <= 412) ||
	  (cur_lumi_block >= 208 && cur_lumi_block <= 250) ||
	  (cur_lumi_block >= 129 && cur_lumi_block <= 206) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 126) ||
	  (cur_lumi_block >= 312 && cur_lumi_block <= 386) ) ) ||
	( cur_run_number == 194225 && (
	  (cur_lumi_block >= 26 && cur_lumi_block <= 47) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 23) ||
	  (cur_lumi_block >= 88 && cur_lumi_block <= 149) ||
	  (cur_lumi_block >= 49 && cur_lumi_block <= 85) ) ) ||
	( cur_run_number == 194270 && (
	  (cur_lumi_block >= 56 && cur_lumi_block <= 68) ) ) ||
	( cur_run_number == 194303 && (
	  (cur_lumi_block >= 69 && cur_lumi_block <= 102) ||
	  (cur_lumi_block >= 56 && cur_lumi_block <= 66) ) ) ||
	( cur_run_number == 194304 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 43) ||
	  (cur_lumi_block >= 46 && cur_lumi_block <= 46) ) ) ||
	( cur_run_number == 194305 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 84) ) ) ||
	( cur_run_number == 194314 && (
	  (cur_lumi_block >= 52 && cur_lumi_block <= 130) ||
	  (cur_lumi_block >= 133 && cur_lumi_block <= 300) ) ) ||
	( cur_run_number == 194315 && (
	  (cur_lumi_block >= 455 && cur_lumi_block <= 467) ||
	  (cur_lumi_block >= 431 && cur_lumi_block <= 452) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 10) ||
	  (cur_lumi_block >= 317 && cur_lumi_block <= 428) ||
	  (cur_lumi_block >= 13 && cur_lumi_block <= 314) ) ) ||
	( cur_run_number == 194317 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 20) ) ) ||
	( cur_run_number == 194424 && (
	  (cur_lumi_block >= 63 && cur_lumi_block <= 141) ||
	  (cur_lumi_block >= 424 && cur_lumi_block <= 478) ||
	  (cur_lumi_block >= 198 && cur_lumi_block <= 266) ||
	  (cur_lumi_block >= 534 && cur_lumi_block <= 553) ||
	  (cur_lumi_block >= 268 && cur_lumi_block <= 421) ||
	  (cur_lumi_block >= 556 && cur_lumi_block <= 706) ||
	  (cur_lumi_block >= 481 && cur_lumi_block <= 531) ||
	  (cur_lumi_block >= 144 && cur_lumi_block <= 195) ||
	  (cur_lumi_block >= 708 && cur_lumi_block <= 708) ) ) ||
	( cur_run_number == 194428 && (
	  (cur_lumi_block >= 87 && cur_lumi_block <= 122) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 85) ||
	  (cur_lumi_block >= 296 && cur_lumi_block <= 465) ||
	  (cur_lumi_block >= 125 && cur_lumi_block <= 294) ) ) ||
	( cur_run_number == 194429 && (
	  (cur_lumi_block >= 57 && cur_lumi_block <= 147) ||
	  (cur_lumi_block >= 7 && cur_lumi_block <= 54) ||
	  (cur_lumi_block >= 413 && cur_lumi_block <= 742) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 4) ||
	  (cur_lumi_block >= 745 && cur_lumi_block <= 986) ||
	  (cur_lumi_block >= 988 && cur_lumi_block <= 1019) ||
	  (cur_lumi_block >= 150 && cur_lumi_block <= 411) ) ) ||
	( cur_run_number == 194439 && (
	  (cur_lumi_block >= 79 && cur_lumi_block <= 106) ||
	  (cur_lumi_block >= 46 && cur_lumi_block <= 77) ) ) ||
	( cur_run_number == 194455 && (
	  (cur_lumi_block >= 67 && cur_lumi_block <= 140) ||
	  (cur_lumi_block >= 142 && cur_lumi_block <= 255) ||
	  (cur_lumi_block >= 45 && cur_lumi_block <= 64) ||
	  (cur_lumi_block >= 293 && cur_lumi_block <= 303) ) ) ||
	( cur_run_number == 194464 && (
	  (cur_lumi_block >= 130 && cur_lumi_block <= 142) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 127) ||
	  (cur_lumi_block >= 145 && cur_lumi_block <= 210) ) ) ||
	( cur_run_number == 194479 && (
	  (cur_lumi_block >= 492 && cur_lumi_block <= 529) ||
	  (cur_lumi_block >= 165 && cur_lumi_block <= 232) ||
	  (cur_lumi_block >= 531 && cur_lumi_block <= 566) ||
	  (cur_lumi_block >= 377 && cur_lumi_block <= 431) ||
	  (cur_lumi_block >= 434 && cur_lumi_block <= 489) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 44) ||
	  (cur_lumi_block >= 265 && cur_lumi_block <= 374) ||
	  (cur_lumi_block >= 235 && cur_lumi_block <= 262) ) ) ||
	( cur_run_number == 194480 && (
	  (cur_lumi_block >= 207 && cur_lumi_block <= 375) ||
	  (cur_lumi_block >= 959 && cur_lumi_block <= 1402) ||
	  (cur_lumi_block >= 762 && cur_lumi_block <= 956) ||
	  (cur_lumi_block >= 389 && cur_lumi_block <= 759) ||
	  (cur_lumi_block >= 377 && cur_lumi_block <= 387) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 32) ||
	  (cur_lumi_block >= 34 && cur_lumi_block <= 205) ) ) ||
	( cur_run_number == 194533 && (
	  (cur_lumi_block >= 620 && cur_lumi_block <= 872) ||
	  (cur_lumi_block >= 417 && cur_lumi_block <= 618) ||
	  (cur_lumi_block >= 382 && cur_lumi_block <= 415) ||
	  (cur_lumi_block >= 46 && cur_lumi_block <= 379) ) ) ||
	( cur_run_number == 194619 && (
	  (cur_lumi_block >= 31 && cur_lumi_block <= 110) ) ) ||
	( cur_run_number == 194631 && (
	  (cur_lumi_block >= 102 && cur_lumi_block <= 169) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 42) ||
	  (cur_lumi_block >= 171 && cur_lumi_block <= 222) ||
	  (cur_lumi_block >= 44 && cur_lumi_block <= 100) ) ) ||
	( cur_run_number == 194643 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 287) ) ) ||
	( cur_run_number == 194644 && (
	  (cur_lumi_block >= 184 && cur_lumi_block <= 185) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 168) ||
	  (cur_lumi_block >= 321 && cur_lumi_block <= 421) ||
	  (cur_lumi_block >= 187 && cur_lumi_block <= 319) ||
	  (cur_lumi_block >= 171 && cur_lumi_block <= 181) ) ) ||
	( cur_run_number == 194691 && (
	  (cur_lumi_block >= 271 && cur_lumi_block <= 272) ||
	  (cur_lumi_block >= 61 && cur_lumi_block <= 104) ||
	  (cur_lumi_block >= 107 && cur_lumi_block <= 155) ||
	  (cur_lumi_block >= 158 && cur_lumi_block <= 251) ||
	  (cur_lumi_block >= 254 && cur_lumi_block <= 268) ||
	  (cur_lumi_block >= 292 && cur_lumi_block <= 313) ||
	  (cur_lumi_block >= 275 && cur_lumi_block <= 289) ) ) ||
	( cur_run_number == 194699 && (
	  (cur_lumi_block >= 67 && cur_lumi_block <= 71) ||
	  (cur_lumi_block >= 32 && cur_lumi_block <= 52) ||
	  (cur_lumi_block >= 241 && cur_lumi_block <= 259) ||
	  (cur_lumi_block >= 218 && cur_lumi_block <= 238) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 30) ||
	  (cur_lumi_block >= 73 && cur_lumi_block <= 154) ||
	  (cur_lumi_block >= 157 && cur_lumi_block <= 215) ||
	  (cur_lumi_block >= 55 && cur_lumi_block <= 64) ) ) ||
	( cur_run_number == 194702 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 138) ||
	  (cur_lumi_block >= 141 && cur_lumi_block <= 191) ) ) ||
	( cur_run_number == 194704 && (
	  (cur_lumi_block >= 548 && cur_lumi_block <= 592) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 41) ||
	  (cur_lumi_block >= 44 && cur_lumi_block <= 545) ) ) ||
	( cur_run_number == 194711 && (
	  (cur_lumi_block >= 9 && cur_lumi_block <= 619) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 7) ) ) ||
	( cur_run_number == 194712 && (
	  (cur_lumi_block >= 627 && cur_lumi_block <= 759) ||
	  (cur_lumi_block >= 61 && cur_lumi_block <= 418) ||
	  (cur_lumi_block >= 420 && cur_lumi_block <= 625) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 56) ) ) ||
	( cur_run_number == 194735 && (
	  (cur_lumi_block >= 104 && cur_lumi_block <= 130) ||
	  (cur_lumi_block >= 74 && cur_lumi_block <= 101) ||
	  (cur_lumi_block >= 44 && cur_lumi_block <= 71) ) ) ||
	( cur_run_number == 194778 && (
	  (cur_lumi_block >= 120 && cur_lumi_block <= 219) ||
	  (cur_lumi_block >= 60 && cur_lumi_block <= 118) ) ) ||
	( cur_run_number == 194789 && (
	  (cur_lumi_block >= 272 && cur_lumi_block <= 405) ||
	  (cur_lumi_block >= 21 && cur_lumi_block <= 32) ||
	  (cur_lumi_block >= 417 && cur_lumi_block <= 427) ||
	  (cur_lumi_block >= 168 && cur_lumi_block <= 269) ||
	  (cur_lumi_block >= 430 && cur_lumi_block <= 566) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 18) ||
	  (cur_lumi_block >= 34 && cur_lumi_block <= 80) ||
	  (cur_lumi_block >= 409 && cur_lumi_block <= 414) ||
	  (cur_lumi_block >= 82 && cur_lumi_block <= 166) ) ) ||
	( cur_run_number == 194790 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 45) ) ) ||
	( cur_run_number == 194825 && (
	  (cur_lumi_block >= 120 && cur_lumi_block <= 221) ||
	  (cur_lumi_block >= 72 && cur_lumi_block <= 117) ) ) ||
	( cur_run_number == 194896 && (
	  (cur_lumi_block >= 58 && cur_lumi_block <= 79) ||
	  (cur_lumi_block >= 34 && cur_lumi_block <= 55) ||
	  (cur_lumi_block >= 82 && cur_lumi_block <= 103) ) ) ||
	( cur_run_number == 194897 && (
	  (cur_lumi_block >= 80 && cur_lumi_block <= 96) ||
	  (cur_lumi_block >= 8 && cur_lumi_block <= 78) ||
	  (cur_lumi_block >= 98 && cur_lumi_block <= 102) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 6) ) ) ||
	( cur_run_number == 194912 && (
	  (cur_lumi_block >= 53 && cur_lumi_block <= 70) ||
	  (cur_lumi_block >= 1447 && cur_lumi_block <= 1487) ||
	  (cur_lumi_block >= 470 && cur_lumi_block <= 561) ||
	  (cur_lumi_block >= 446 && cur_lumi_block <= 450) ||
	  (cur_lumi_block >= 453 && cur_lumi_block <= 467) ||
	  (cur_lumi_block >= 1140 && cur_lumi_block <= 1166) ||
	  (cur_lumi_block >= 72 && cur_lumi_block <= 96) ||
	  (cur_lumi_block >= 663 && cur_lumi_block <= 813) ||
	  (cur_lumi_block >= 564 && cur_lumi_block <= 660) ||
	  (cur_lumi_block >= 1007 && cur_lumi_block <= 1025) ||
	  (cur_lumi_block >= 1069 && cur_lumi_block <= 1137) ||
	  (cur_lumi_block >= 1168 && cur_lumi_block <= 1249) ||
	  (cur_lumi_block >= 1251 && cur_lumi_block <= 1304) ||
	  (cur_lumi_block >= 1027 && cur_lumi_block <= 1067) ||
	  (cur_lumi_block >= 1489 && cur_lumi_block <= 1503) ||
	  (cur_lumi_block >= 98 && cur_lumi_block <= 444) ||
	  (cur_lumi_block >= 1307 && cur_lumi_block <= 1444) ||
	  (cur_lumi_block >= 1506 && cur_lumi_block <= 1662) ||
	  (cur_lumi_block >= 815 && cur_lumi_block <= 840) ||
	  (cur_lumi_block >= 843 && cur_lumi_block <= 864) ||
	  (cur_lumi_block >= 866 && cur_lumi_block <= 1004) ) ) ||
	( cur_run_number == 194914 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 38) ) ) ||
	( cur_run_number == 194915 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 74) ) ) ||
	( cur_run_number == 195013 && (
	  (cur_lumi_block >= 208 && cur_lumi_block <= 299) ||
	  (cur_lumi_block >= 369 && cur_lumi_block <= 447) ||
	  (cur_lumi_block >= 326 && cur_lumi_block <= 366) ||
	  (cur_lumi_block >= 94 && cur_lumi_block <= 144) ||
	  (cur_lumi_block >= 146 && cur_lumi_block <= 185) ||
	  (cur_lumi_block >= 450 && cur_lumi_block <= 526) ||
	  (cur_lumi_block >= 302 && cur_lumi_block <= 324) ||
	  (cur_lumi_block >= 528 && cur_lumi_block <= 541) ||
	  (cur_lumi_block >= 187 && cur_lumi_block <= 206) ) ) ||
	( cur_run_number == 195014 && (
	  (cur_lumi_block >= 121 && cur_lumi_block <= 148) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 6) ||
	  (cur_lumi_block >= 9 && cur_lumi_block <= 119) ) ) ||
	( cur_run_number == 195015 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 13) ) ) ||
	( cur_run_number == 195016 && (
	  (cur_lumi_block >= 370 && cur_lumi_block <= 422) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 21) ||
	  (cur_lumi_block >= 186 && cur_lumi_block <= 241) ||
	  (cur_lumi_block >= 23 && cur_lumi_block <= 55) ||
	  (cur_lumi_block >= 65 && cur_lumi_block <= 174) ||
	  (cur_lumi_block >= 563 && cur_lumi_block <= 569) ||
	  (cur_lumi_block >= 248 && cur_lumi_block <= 251) ||
	  (cur_lumi_block >= 425 && cur_lumi_block <= 560) ||
	  (cur_lumi_block >= 58 && cur_lumi_block <= 63) ||
	  (cur_lumi_block >= 243 && cur_lumi_block <= 246) ||
	  (cur_lumi_block >= 254 && cur_lumi_block <= 367) ||
	  (cur_lumi_block >= 177 && cur_lumi_block <= 184) ) ) ||
	( cur_run_number == 195099 && (
	  (cur_lumi_block >= 189 && cur_lumi_block <= 208) ||
	  (cur_lumi_block >= 70 && cur_lumi_block <= 144) ||
	  (cur_lumi_block >= 227 && cur_lumi_block <= 248) ||
	  (cur_lumi_block >= 147 && cur_lumi_block <= 186) ||
	  (cur_lumi_block >= 211 && cur_lumi_block <= 224) ) ) ||
	( cur_run_number == 195109 && (
	  (cur_lumi_block >= 98 && cur_lumi_block <= 241) ) ) ||
	( cur_run_number == 195112 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 12) ||
	  (cur_lumi_block >= 15 && cur_lumi_block <= 26) ) ) ||
	( cur_run_number == 195113 && (
	  (cur_lumi_block >= 391 && cur_lumi_block <= 403) ||
	  (cur_lumi_block >= 495 && cur_lumi_block <= 579) ||
	  (cur_lumi_block >= 212 && cur_lumi_block <= 388) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 209) ||
	  (cur_lumi_block >= 422 && cur_lumi_block <= 492) ||
	  (cur_lumi_block >= 406 && cur_lumi_block <= 419) ) ) ||
	( cur_run_number == 195114 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 69) ||
	  (cur_lumi_block >= 72 && cur_lumi_block <= 103) ) ) ||
	( cur_run_number == 195115 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 7) ||
	  (cur_lumi_block >= 10 && cur_lumi_block <= 22) ) ) ||
	( cur_run_number == 195147 && (
	  (cur_lumi_block >= 445 && cur_lumi_block <= 536) ||
	  (cur_lumi_block >= 366 && cur_lumi_block <= 442) ||
	  (cur_lumi_block >= 297 && cur_lumi_block <= 331) ||
	  (cur_lumi_block >= 334 && cur_lumi_block <= 363) ||
	  (cur_lumi_block >= 132 && cur_lumi_block <= 282) ||
	  (cur_lumi_block >= 539 && cur_lumi_block <= 559) ||
	  (cur_lumi_block >= 285 && cur_lumi_block <= 294) ) ) ||
	( cur_run_number == 195163 && (
	  (cur_lumi_block >= 227 && cur_lumi_block <= 240) ||
	  (cur_lumi_block >= 72 && cur_lumi_block <= 138) ||
	  (cur_lumi_block >= 246 && cur_lumi_block <= 347) ||
	  (cur_lumi_block >= 243 && cur_lumi_block <= 243) ||
	  (cur_lumi_block >= 140 && cur_lumi_block <= 224) ) ) ||
	( cur_run_number == 195164 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 64) ) ) ||
	( cur_run_number == 195165 && (
	  (cur_lumi_block >= 7 && cur_lumi_block <= 41) ||
	  (cur_lumi_block >= 263 && cur_lumi_block <= 266) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 4) ||
	  (cur_lumi_block >= 44 && cur_lumi_block <= 54) ||
	  (cur_lumi_block >= 156 && cur_lumi_block <= 260) ||
	  (cur_lumi_block >= 56 && cur_lumi_block <= 153) ) ) ||
	( cur_run_number == 195251 && (
	  (cur_lumi_block >= 167 && cur_lumi_block <= 242) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 131) ||
	  (cur_lumi_block >= 154 && cur_lumi_block <= 165) ||
	  (cur_lumi_block >= 134 && cur_lumi_block <= 137) ||
	  (cur_lumi_block >= 140 && cur_lumi_block <= 152) ) ) ||
	( cur_run_number == 195303 && (
	  (cur_lumi_block >= 318 && cur_lumi_block <= 409) ||
	  (cur_lumi_block >= 280 && cur_lumi_block <= 310) ||
	  (cur_lumi_block >= 194 && cur_lumi_block <= 277) ||
	  (cur_lumi_block >= 109 && cur_lumi_block <= 191) ||
	  (cur_lumi_block >= 312 && cur_lumi_block <= 316) ) ) ||
	( cur_run_number == 195304 && (
	  (cur_lumi_block >= 729 && cur_lumi_block <= 1003) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 3) ||
	  (cur_lumi_block >= 344 && cur_lumi_block <= 588) ||
	  (cur_lumi_block >= 1083 && cur_lumi_block <= 1140) ||
	  (cur_lumi_block >= 1006 && cur_lumi_block <= 1079) ||
	  (cur_lumi_block >= 157 && cur_lumi_block <= 341) ||
	  (cur_lumi_block >= 27 && cur_lumi_block <= 80) ||
	  (cur_lumi_block >= 6 && cur_lumi_block <= 22) ||
	  (cur_lumi_block >= 83 && cur_lumi_block <= 100) ||
	  (cur_lumi_block >= 590 && cur_lumi_block <= 727) ||
	  (cur_lumi_block >= 103 && cur_lumi_block <= 154) ||
	  (cur_lumi_block >= 1143 && cur_lumi_block <= 1229) ) ) ||
	( cur_run_number == 195378 && (
	  (cur_lumi_block >= 130 && cur_lumi_block <= 185) ||
	  (cur_lumi_block >= 90 && cur_lumi_block <= 117) ||
	  (cur_lumi_block >= 206 && cur_lumi_block <= 302) ||
	  (cur_lumi_block >= 703 && cur_lumi_block <= 734) ||
	  (cur_lumi_block >= 567 && cur_lumi_block <= 645) ||
	  (cur_lumi_block >= 737 && cur_lumi_block <= 1120) ||
	  (cur_lumi_block >= 647 && cur_lumi_block <= 701) ||
	  (cur_lumi_block >= 120 && cur_lumi_block <= 127) ||
	  (cur_lumi_block >= 305 && cur_lumi_block <= 542) ||
	  (cur_lumi_block >= 1122 && cur_lumi_block <= 1133) ||
	  (cur_lumi_block >= 544 && cur_lumi_block <= 565) ||
	  (cur_lumi_block >= 187 && cur_lumi_block <= 204) ) ) ||
	( cur_run_number == 195390 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 1) ||
	  (cur_lumi_block >= 186 && cur_lumi_block <= 187) ||
	  (cur_lumi_block >= 30 && cur_lumi_block <= 145) ||
	  (cur_lumi_block >= 147 && cur_lumi_block <= 183) ||
	  (cur_lumi_block >= 190 && cur_lumi_block <= 208) ||
	  (cur_lumi_block >= 210 && cur_lumi_block <= 213) ||
	  (cur_lumi_block >= 215 && cur_lumi_block <= 400) ||
	  (cur_lumi_block >= 4 && cur_lumi_block <= 27) ) ) ||
	( cur_run_number == 195396 && (
	  (cur_lumi_block >= 58 && cur_lumi_block <= 63) ||
	  (cur_lumi_block >= 49 && cur_lumi_block <= 55) ||
	  (cur_lumi_block >= 66 && cur_lumi_block <= 131) ) ) ||
	( cur_run_number == 195397 && (
	  (cur_lumi_block >= 143 && cur_lumi_block <= 251) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 10) ||
	  (cur_lumi_block >= 1184 && cur_lumi_block <= 1198) ||
	  (cur_lumi_block >= 972 && cur_lumi_block <= 1121) ||
	  (cur_lumi_block >= 1200 && cur_lumi_block <= 1209) ||
	  (cur_lumi_block >= 1123 && cur_lumi_block <= 1181) ||
	  (cur_lumi_block >= 123 && cur_lumi_block <= 141) ||
	  (cur_lumi_block >= 253 && cur_lumi_block <= 253) ||
	  (cur_lumi_block >= 12 && cur_lumi_block <= 89) ||
	  (cur_lumi_block >= 92 && cur_lumi_block <= 120) ||
	  (cur_lumi_block >= 527 && cur_lumi_block <= 608) ||
	  (cur_lumi_block >= 779 && cur_lumi_block <= 970) ||
	  (cur_lumi_block >= 256 && cur_lumi_block <= 475) ||
	  (cur_lumi_block >= 611 && cur_lumi_block <= 776) ||
	  (cur_lumi_block >= 478 && cur_lumi_block <= 525) ) ) ||
	( cur_run_number == 195398 && (
	  (cur_lumi_block >= 827 && cur_lumi_block <= 1225) ||
	  (cur_lumi_block >= 139 && cur_lumi_block <= 494) ||
	  (cur_lumi_block >= 820 && cur_lumi_block <= 824) ||
	  (cur_lumi_block >= 1309 && cur_lumi_block <= 1712) ||
	  (cur_lumi_block >= 1228 && cur_lumi_block <= 1307) ||
	  (cur_lumi_block >= 587 && cur_lumi_block <= 817) ||
	  (cur_lumi_block >= 1721 && cur_lumi_block <= 1736) ||
	  (cur_lumi_block >= 3 && cur_lumi_block <= 137) ||
	  (cur_lumi_block >= 497 && cur_lumi_block <= 585) ||
	  (cur_lumi_block >= 1741 && cur_lumi_block <= 1752) ||
	  (cur_lumi_block >= 1767 && cur_lumi_block <= 1795) ) ) ||
	( cur_run_number == 195399 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 192) ||
	  (cur_lumi_block >= 194 && cur_lumi_block <= 382) ) ) ||
	( cur_run_number == 195530 && (
	  (cur_lumi_block >= 107 && cur_lumi_block <= 156) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 80) ||
	  (cur_lumi_block >= 82 && cur_lumi_block <= 104) ||
	  (cur_lumi_block >= 159 && cur_lumi_block <= 300) ||
	  (cur_lumi_block >= 302 && cur_lumi_block <= 405) ) ) ||
	( cur_run_number == 195540 && (
	  (cur_lumi_block >= 68 && cur_lumi_block <= 123) ||
	  (cur_lumi_block >= 126 && cur_lumi_block <= 137) ||
	  (cur_lumi_block >= 286 && cur_lumi_block <= 319) ||
	  (cur_lumi_block >= 140 && cur_lumi_block <= 283) ) ) ||
	( cur_run_number == 195551 && (
	  (cur_lumi_block >= 91 && cur_lumi_block <= 106) ) ) ||
	( cur_run_number == 195552 && (
	  (cur_lumi_block >= 825 && cur_lumi_block <= 883) ||
	  (cur_lumi_block >= 158 && cur_lumi_block <= 182) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 21) ||
	  (cur_lumi_block >= 352 && cur_lumi_block <= 469) ||
	  (cur_lumi_block >= 30 && cur_lumi_block <= 147) ||
	  (cur_lumi_block >= 23 && cur_lumi_block <= 27) ||
	  (cur_lumi_block >= 1303 && cur_lumi_block <= 1789) ||
	  (cur_lumi_block >= 885 && cur_lumi_block <= 1152) ||
	  (cur_lumi_block >= 290 && cur_lumi_block <= 349) ||
	  (cur_lumi_block >= 149 && cur_lumi_block <= 155) ||
	  (cur_lumi_block >= 1154 && cur_lumi_block <= 1300) ||
	  (cur_lumi_block >= 185 && cur_lumi_block <= 287) ||
	  (cur_lumi_block >= 472 && cur_lumi_block <= 815) ||
	  (cur_lumi_block >= 818 && cur_lumi_block <= 823) ) ) ||
	( cur_run_number == 195633 && (
	  (cur_lumi_block >= 40 && cur_lumi_block <= 42) ) ) ||
	( cur_run_number == 195647 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 41) ) ) ||
	( cur_run_number == 195649 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 69) ||
	  (cur_lumi_block >= 154 && cur_lumi_block <= 181) ||
	  (cur_lumi_block >= 72 && cur_lumi_block <= 151) ||
	  (cur_lumi_block >= 183 && cur_lumi_block <= 247) ) ) ||
	( cur_run_number == 195655 && (
	  (cur_lumi_block >= 131 && cur_lumi_block <= 184) ||
	  (cur_lumi_block >= 263 && cur_lumi_block <= 350) ||
	  (cur_lumi_block >= 353 && cur_lumi_block <= 446) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 129) ||
	  (cur_lumi_block >= 186 && cur_lumi_block <= 260) ||
	  (cur_lumi_block >= 448 && cur_lumi_block <= 483) ||
	  (cur_lumi_block >= 485 && cur_lumi_block <= 498) ) ) ||
	( cur_run_number == 195656 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 362) ) ) ||
	( cur_run_number == 195658 && (
	  (cur_lumi_block >= 384 && cur_lumi_block <= 386) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 37) ||
	  (cur_lumi_block >= 364 && cur_lumi_block <= 382) ||
	  (cur_lumi_block >= 40 && cur_lumi_block <= 362) ) ) ||
	( cur_run_number == 195749 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 8) ||
	  (cur_lumi_block >= 36 && cur_lumi_block <= 131) ||
	  (cur_lumi_block >= 10 && cur_lumi_block <= 33) ) ) ||
	( cur_run_number == 195757 && (
	  (cur_lumi_block >= 118 && cur_lumi_block <= 161) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 82) ||
	  (cur_lumi_block >= 85 && cur_lumi_block <= 115) ||
	  (cur_lumi_block >= 163 && cur_lumi_block <= 206) ) ) ||
	( cur_run_number == 195758 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 18) ) ) ||
	( cur_run_number == 195774 && (
	  (cur_lumi_block >= 365 && cur_lumi_block <= 466) ||
	  (cur_lumi_block >= 620 && cur_lumi_block <= 649) ||
	  (cur_lumi_block >= 139 && cur_lumi_block <= 151) ||
	  (cur_lumi_block >= 469 && cur_lumi_block <= 618) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 13) ||
	  (cur_lumi_block >= 154 && cur_lumi_block <= 162) ||
	  (cur_lumi_block >= 16 && cur_lumi_block <= 137) ||
	  (cur_lumi_block >= 651 && cur_lumi_block <= 830) ||
	  (cur_lumi_block >= 279 && cur_lumi_block <= 362) ||
	  (cur_lumi_block >= 258 && cur_lumi_block <= 276) ||
	  (cur_lumi_block >= 164 && cur_lumi_block <= 256) ) ) ||
	( cur_run_number == 195775 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 57) ||
	  (cur_lumi_block >= 103 && cur_lumi_block <= 170) ||
	  (cur_lumi_block >= 60 && cur_lumi_block <= 100) ) ) ||
	( cur_run_number == 195776 && (
	  (cur_lumi_block >= 401 && cur_lumi_block <= 409) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 63) ||
	  (cur_lumi_block >= 286 && cur_lumi_block <= 337) ||
	  (cur_lumi_block >= 411 && cur_lumi_block <= 477) ||
	  (cur_lumi_block >= 66 && cur_lumi_block <= 283) ||
	  (cur_lumi_block >= 340 && cur_lumi_block <= 399) ) ) ||
	( cur_run_number == 195841 && (
	  (cur_lumi_block >= 74 && cur_lumi_block <= 85) ) ) ||
	( cur_run_number == 195868 && (
	  (cur_lumi_block >= 90 && cur_lumi_block <= 107) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 88) ||
	  (cur_lumi_block >= 110 && cur_lumi_block <= 205) ) ) ||
	( cur_run_number == 195915 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 109) ||
	  (cur_lumi_block >= 830 && cur_lumi_block <= 850) ||
	  (cur_lumi_block >= 419 && cur_lumi_block <= 429) ||
	  (cur_lumi_block >= 787 && cur_lumi_block <= 828) ||
	  (cur_lumi_block >= 749 && cur_lumi_block <= 785) ||
	  (cur_lumi_block >= 111 && cur_lumi_block <= 275) ||
	  (cur_lumi_block >= 393 && cur_lumi_block <= 417) ||
	  (cur_lumi_block >= 278 && cur_lumi_block <= 390) ||
	  (cur_lumi_block >= 432 && cur_lumi_block <= 505) ||
	  (cur_lumi_block >= 507 && cur_lumi_block <= 747) ) ) ||
	( cur_run_number == 195916 && (
	  (cur_lumi_block >= 71 && cur_lumi_block <= 212) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 16) ||
	  (cur_lumi_block >= 19 && cur_lumi_block <= 68) ) ) ||
	( cur_run_number == 195917 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 4) ) ) ||
	( cur_run_number == 195918 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 44) ||
	  (cur_lumi_block >= 46 && cur_lumi_block <= 46) ||
	  (cur_lumi_block >= 49 && cur_lumi_block <= 64) ) ) ||
	( cur_run_number == 195919 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 15) ) ) ||
	( cur_run_number == 195923 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 14) ) ) ||
	( cur_run_number == 195925 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 12) ) ) ||
	( cur_run_number == 195926 && (
	  (cur_lumi_block >= 21 && cur_lumi_block <= 34) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 1) ||
	  (cur_lumi_block >= 3 && cur_lumi_block <= 19) ) ) ||
	( cur_run_number == 195929 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 29) ) ) ||
	( cur_run_number == 195930 && (
	  (cur_lumi_block >= 80 && cur_lumi_block <= 176) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 77) ||
	  (cur_lumi_block >= 179 && cur_lumi_block <= 526) ||
	  (cur_lumi_block >= 529 && cur_lumi_block <= 596) ) ) ||
	( cur_run_number == 195937 && (
	  (cur_lumi_block >= 188 && cur_lumi_block <= 396) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 28) ||
	  (cur_lumi_block >= 31 && cur_lumi_block <= 186) ) ) ||
	( cur_run_number == 195947 && (
	  (cur_lumi_block >= 23 && cur_lumi_block <= 62) ||
	  (cur_lumi_block >= 64 && cur_lumi_block <= 88) ) ) ||
	( cur_run_number == 195948 && (
	  (cur_lumi_block >= 605 && cur_lumi_block <= 615) ||
	  (cur_lumi_block >= 503 && cur_lumi_block <= 540) ||
	  (cur_lumi_block >= 543 && cur_lumi_block <= 565) ||
	  (cur_lumi_block >= 119 && cur_lumi_block <= 144) ||
	  (cur_lumi_block >= 567 && cur_lumi_block <= 602) ||
	  (cur_lumi_block >= 355 && cur_lumi_block <= 369) ||
	  (cur_lumi_block >= 147 && cur_lumi_block <= 147) ||
	  (cur_lumi_block >= 51 && cur_lumi_block <= 116) ||
	  (cur_lumi_block >= 372 && cur_lumi_block <= 402) ||
	  (cur_lumi_block >= 404 && cur_lumi_block <= 500) ||
	  (cur_lumi_block >= 150 && cur_lumi_block <= 352) ) ) ||
	( cur_run_number == 195950 && (
	  (cur_lumi_block >= 833 && cur_lumi_block <= 1587) ||
	  (cur_lumi_block >= 718 && cur_lumi_block <= 787) ||
	  (cur_lumi_block >= 385 && cur_lumi_block <= 421) ||
	  (cur_lumi_block >= 831 && cur_lumi_block <= 831) ||
	  (cur_lumi_block >= 453 && cur_lumi_block <= 483) ||
	  (cur_lumi_block >= 353 && cur_lumi_block <= 382) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 71) ||
	  (cur_lumi_block >= 141 && cur_lumi_block <= 169) ||
	  (cur_lumi_block >= 803 && cur_lumi_block <= 829) ||
	  (cur_lumi_block >= 424 && cur_lumi_block <= 450) ||
	  (cur_lumi_block >= 172 && cur_lumi_block <= 332) ||
	  (cur_lumi_block >= 789 && cur_lumi_block <= 800) ||
	  (cur_lumi_block >= 619 && cur_lumi_block <= 715) ||
	  (cur_lumi_block >= 335 && cur_lumi_block <= 350) ||
	  (cur_lumi_block >= 73 && cur_lumi_block <= 138) ||
	  (cur_lumi_block >= 485 && cur_lumi_block <= 616) ) ) ||
	( cur_run_number == 195963 && (
	  (cur_lumi_block >= 54 && cur_lumi_block <= 58) ) ) ||
	( cur_run_number == 195970 && (
	  (cur_lumi_block >= 44 && cur_lumi_block <= 49) ||
	  (cur_lumi_block >= 51 && cur_lumi_block <= 85) ) ) ||
	( cur_run_number == 196019 && (
	  (cur_lumi_block >= 54 && cur_lumi_block <= 68) ) ) ||
	( cur_run_number == 196027 && (
	  (cur_lumi_block >= 121 && cur_lumi_block <= 155) ||
	  (cur_lumi_block >= 158 && cur_lumi_block <= 186) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 55) ||
	  (cur_lumi_block >= 58 && cur_lumi_block <= 119) ) ) ||
	( cur_run_number == 196046 && (
	  (cur_lumi_block >= 12 && cur_lumi_block <= 40) ) ) ||
	( cur_run_number == 196047 && (
	  (cur_lumi_block >= 70 && cur_lumi_block <= 75) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 64) ) ) ||
	( cur_run_number == 196048 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 44) ||
	  (cur_lumi_block >= 46 && cur_lumi_block <= 48) ) ) ||
	( cur_run_number == 196197 && (
	  (cur_lumi_block >= 519 && cur_lumi_block <= 562) ||
	  (cur_lumi_block >= 181 && cur_lumi_block <= 311) ||
	  (cur_lumi_block >= 125 && cur_lumi_block <= 179) ||
	  (cur_lumi_block >= 58 && cur_lumi_block <= 122) ||
	  (cur_lumi_block >= 313 && cur_lumi_block <= 516) ) ) ||
	( cur_run_number == 196199 && (
	  (cur_lumi_block >= 239 && cur_lumi_block <= 285) ||
	  (cur_lumi_block >= 287 && cur_lumi_block <= 534) ||
	  (cur_lumi_block >= 121 && cur_lumi_block <= 147) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 33) ||
	  (cur_lumi_block >= 36 && cur_lumi_block <= 83) ||
	  (cur_lumi_block >= 86 && cur_lumi_block <= 118) ||
	  (cur_lumi_block >= 150 && cur_lumi_block <= 237) ) ) ||
	( cur_run_number == 196200 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 68) ) ) ||
	( cur_run_number == 196202 && (
	  (cur_lumi_block >= 64 && cur_lumi_block <= 108) ||
	  (cur_lumi_block >= 3 && cur_lumi_block <= 61) ) ) ||
	( cur_run_number == 196203 && (
	  (cur_lumi_block >= 107 && cur_lumi_block <= 117) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 102) ) ) ||
	( cur_run_number == 196218 && (
	  (cur_lumi_block >= 759 && cur_lumi_block <= 820) ||
	  (cur_lumi_block >= 754 && cur_lumi_block <= 757) ||
	  (cur_lumi_block >= 55 && cur_lumi_block <= 199) ||
	  (cur_lumi_block >= 226 && cur_lumi_block <= 393) ||
	  (cur_lumi_block >= 744 && cur_lumi_block <= 752) ||
	  (cur_lumi_block >= 396 && cur_lumi_block <= 494) ||
	  (cur_lumi_block >= 201 && cur_lumi_block <= 224) ||
	  (cur_lumi_block >= 496 && cur_lumi_block <= 741) ) ) ||
	( cur_run_number == 196239 && (
	  (cur_lumi_block >= 274 && cur_lumi_block <= 373) ||
	  (cur_lumi_block >= 468 && cur_lumi_block <= 647) ||
	  (cur_lumi_block >= 435 && cur_lumi_block <= 465) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 59) ||
	  (cur_lumi_block >= 375 && cur_lumi_block <= 432) ||
	  (cur_lumi_block >= 157 && cur_lumi_block <= 272) ||
	  (cur_lumi_block >= 709 && cur_lumi_block <= 1025) ||
	  (cur_lumi_block >= 650 && cur_lumi_block <= 706) ||
	  (cur_lumi_block >= 62 && cur_lumi_block <= 154) ) ) ||
	( cur_run_number == 196249 && (
	  (cur_lumi_block >= 63 && cur_lumi_block <= 77) ||
	  (cur_lumi_block >= 80 && cur_lumi_block <= 99) ) ) ||
	( cur_run_number == 196250 && (
	  (cur_lumi_block >= 267 && cur_lumi_block <= 426) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 2) ||
	  (cur_lumi_block >= 5 && cur_lumi_block <= 265) ) ) ||
	( cur_run_number == 196252 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 35) ) ) ||
	( cur_run_number == 196334 && (
	  (cur_lumi_block >= 170 && cur_lumi_block <= 193) ||
	  (cur_lumi_block >= 292 && cur_lumi_block <= 342) ||
	  (cur_lumi_block >= 126 && cur_lumi_block <= 132) ||
	  (cur_lumi_block >= 259 && cur_lumi_block <= 267) ||
	  (cur_lumi_block >= 135 && cur_lumi_block <= 167) ||
	  (cur_lumi_block >= 59 && cur_lumi_block <= 111) ||
	  (cur_lumi_block >= 196 && cur_lumi_block <= 257) ||
	  (cur_lumi_block >= 270 && cur_lumi_block <= 289) ||
	  (cur_lumi_block >= 113 && cur_lumi_block <= 123) ) ) ||
	( cur_run_number == 196349 && (
	  (cur_lumi_block >= 157 && cur_lumi_block <= 244) ||
	  (cur_lumi_block >= 65 && cur_lumi_block <= 84) ||
	  (cur_lumi_block >= 246 && cur_lumi_block <= 258) ||
	  (cur_lumi_block >= 86 && cur_lumi_block <= 154) ) ) ||
	( cur_run_number == 196357 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 4) ) ) ||
	( cur_run_number == 196359 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 2) ) ) ||
	( cur_run_number == 196362 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 88) ) ) ||
	( cur_run_number == 196363 && (
	  (cur_lumi_block >= 11 && cur_lumi_block <= 34) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 8) ) ) ||
	( cur_run_number == 196364 && (
	  (cur_lumi_block >= 139 && cur_lumi_block <= 365) ||
	  (cur_lumi_block >= 887 && cur_lumi_block <= 1196) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 93) ||
	  (cur_lumi_block >= 1203 && cur_lumi_block <= 1299) ||
	  (cur_lumi_block >= 798 && cur_lumi_block <= 884) ||
	  (cur_lumi_block >= 96 && cur_lumi_block <= 136) ||
	  (cur_lumi_block >= 368 && cur_lumi_block <= 380) ||
	  (cur_lumi_block >= 382 && cur_lumi_block <= 601) ||
	  (cur_lumi_block >= 1199 && cur_lumi_block <= 1200) ||
	  (cur_lumi_block >= 603 && cur_lumi_block <= 795) ) ) ||
	( cur_run_number == 196437 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 1) ||
	  (cur_lumi_block >= 77 && cur_lumi_block <= 169) ||
	  (cur_lumi_block >= 3 && cur_lumi_block <= 74) ) ) ||
	( cur_run_number == 196438 && (
	  (cur_lumi_block >= 701 && cur_lumi_block <= 1269) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 181) ||
	  (cur_lumi_block >= 184 && cur_lumi_block <= 699) ) ) ||
	( cur_run_number == 196452 && (
	  (cur_lumi_block >= 718 && cur_lumi_block <= 726) ||
	  (cur_lumi_block >= 728 && cur_lumi_block <= 956) ||
	  (cur_lumi_block >= 958 && cur_lumi_block <= 1004) ||
	  (cur_lumi_block >= 82 && cur_lumi_block <= 112) ||
	  (cur_lumi_block >= 1007 && cur_lumi_block <= 1091) ||
	  (cur_lumi_block >= 589 && cur_lumi_block <= 618) ||
	  (cur_lumi_block >= 114 && cur_lumi_block <= 490) ||
	  (cur_lumi_block >= 622 && cur_lumi_block <= 668) ||
	  (cur_lumi_block >= 493 && cur_lumi_block <= 586) ||
	  (cur_lumi_block >= 671 && cur_lumi_block <= 716) ) ) ||
	( cur_run_number == 196453 && (
	  (cur_lumi_block >= 1182 && cur_lumi_block <= 1248) ||
	  (cur_lumi_block >= 673 && cur_lumi_block <= 714) ||
	  (cur_lumi_block >= 991 && cur_lumi_block <= 1178) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 74) ||
	  (cur_lumi_block >= 77 && cur_lumi_block <= 145) ||
	  (cur_lumi_block >= 147 && cur_lumi_block <= 669) ||
	  (cur_lumi_block >= 1180 && cur_lumi_block <= 1180) ||
	  (cur_lumi_block >= 802 && cur_lumi_block <= 988) ||
	  (cur_lumi_block >= 1250 && cur_lumi_block <= 1528) ||
	  (cur_lumi_block >= 717 && cur_lumi_block <= 799) ||
	  (cur_lumi_block >= 1531 && cur_lumi_block <= 1647) ) ) ||
	( cur_run_number == 196495 && (
	  (cur_lumi_block >= 182 && cur_lumi_block <= 272) ||
	  (cur_lumi_block >= 114 && cur_lumi_block <= 180) ) ) ||
	( cur_run_number == 196509 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 68) ) ) ||
	( cur_run_number == 196531 && (
	  (cur_lumi_block >= 152 && cur_lumi_block <= 253) ||
	  (cur_lumi_block >= 288 && cur_lumi_block <= 302) ||
	  (cur_lumi_block >= 305 && cur_lumi_block <= 422) ||
	  (cur_lumi_block >= 425 && cur_lumi_block <= 440) ||
	  (cur_lumi_block >= 256 && cur_lumi_block <= 285) ||
	  (cur_lumi_block >= 62 && cur_lumi_block <= 150) ) ) ||
	( cur_run_number == 198049 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 11) ||
	  (cur_lumi_block >= 14 && cur_lumi_block <= 57) ) ) ||
	( cur_run_number == 198050 && (
	  (cur_lumi_block >= 2 && cur_lumi_block <= 155) ) ) ||
	( cur_run_number == 198063 && (
	  (cur_lumi_block >= 127 && cur_lumi_block <= 294) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 37) ||
	  (cur_lumi_block >= 74 && cur_lumi_block <= 124) ||
	  (cur_lumi_block >= 40 && cur_lumi_block <= 72) ) ) ||
	( cur_run_number == 198116 && (
	  (cur_lumi_block >= 36 && cur_lumi_block <= 52) ||
	  (cur_lumi_block >= 58 && cur_lumi_block <= 96) ||
	  (cur_lumi_block >= 98 && cur_lumi_block <= 112) ||
	  (cur_lumi_block >= 54 && cur_lumi_block <= 55) ) ) ||
	( cur_run_number == 198207 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 97) ) ) ||
	( cur_run_number == 198208 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 92) ||
	  (cur_lumi_block >= 94 && cur_lumi_block <= 134) ||
	  (cur_lumi_block >= 137 && cur_lumi_block <= 147) ||
	  (cur_lumi_block >= 150 && cur_lumi_block <= 209) ) ) ||
	( cur_run_number == 198210 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 221) ) ) ||
	( cur_run_number == 198212 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 574) ) ) ||
	( cur_run_number == 198213 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 107) ) ) ||
	( cur_run_number == 198215 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 12) ) ) ||
	( cur_run_number == 198230 && (
	  (cur_lumi_block >= 237 && cur_lumi_block <= 324) ||
	  (cur_lumi_block >= 627 && cur_lumi_block <= 651) ||
	  (cur_lumi_block >= 814 && cur_lumi_block <= 948) ||
	  (cur_lumi_block >= 808 && cur_lumi_block <= 811) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 33) ||
	  (cur_lumi_block >= 653 && cur_lumi_block <= 805) ||
	  (cur_lumi_block >= 950 && cur_lumi_block <= 1090) ||
	  (cur_lumi_block >= 36 && cur_lumi_block <= 57) ||
	  (cur_lumi_block >= 390 && cur_lumi_block <= 459) ||
	  (cur_lumi_block >= 326 && cur_lumi_block <= 388) ||
	  (cur_lumi_block >= 1093 && cur_lumi_block <= 1103) ||
	  (cur_lumi_block >= 1335 && cur_lumi_block <= 1380) ||
	  (cur_lumi_block >= 1106 && cur_lumi_block <= 1332) ||
	  (cur_lumi_block >= 60 && cur_lumi_block <= 235) ||
	  (cur_lumi_block >= 462 && cur_lumi_block <= 625) ) ) ||
	( cur_run_number == 198249 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 7) ) ) ||
	( cur_run_number == 198269 && (
	  (cur_lumi_block >= 3 && cur_lumi_block <= 198) ) ) ||
	( cur_run_number == 198271 && (
	  (cur_lumi_block >= 516 && cur_lumi_block <= 616) ||
	  (cur_lumi_block >= 793 && cur_lumi_block <= 797) ||
	  (cur_lumi_block >= 453 && cur_lumi_block <= 513) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 91) ||
	  (cur_lumi_block >= 93 && cur_lumi_block <= 170) ||
	  (cur_lumi_block >= 301 && cur_lumi_block <= 450) ||
	  (cur_lumi_block >= 631 && cur_lumi_block <= 791) ||
	  (cur_lumi_block >= 173 && cur_lumi_block <= 299) ||
	  (cur_lumi_block >= 619 && cur_lumi_block <= 628) ) ) ||
	( cur_run_number == 198272 && (
	  (cur_lumi_block >= 188 && cur_lumi_block <= 245) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 185) ||
	  (cur_lumi_block >= 436 && cur_lumi_block <= 444) ||
	  (cur_lumi_block >= 248 && cur_lumi_block <= 314) ||
	  (cur_lumi_block >= 454 && cur_lumi_block <= 620) ||
	  (cur_lumi_block >= 317 && cur_lumi_block <= 433) ) ) ||
	( cur_run_number == 198346 && (
	  (cur_lumi_block >= 44 && cur_lumi_block <= 47) ) ) ||
	( cur_run_number == 198372 && (
	  (cur_lumi_block >= 57 && cur_lumi_block <= 110) ) ) ||
	( cur_run_number == 198485 && (
	  (cur_lumi_block >= 68 && cur_lumi_block <= 109) ||
	  (cur_lumi_block >= 136 && cur_lumi_block <= 181) ||
	  (cur_lumi_block >= 184 && cur_lumi_block <= 239) ||
	  (cur_lumi_block >= 112 && cur_lumi_block <= 134) ) ) ||
	( cur_run_number == 198487 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 145) ||
	  (cur_lumi_block >= 1437 && cur_lumi_block <= 1610) ||
	  (cur_lumi_block >= 147 && cur_lumi_block <= 514) ||
	  (cur_lumi_block >= 997 && cur_lumi_block <= 1434) ||
	  (cur_lumi_block >= 854 && cur_lumi_block <= 994) ||
	  (cur_lumi_block >= 760 && cur_lumi_block <= 852) ||
	  (cur_lumi_block >= 517 && cur_lumi_block <= 668) ||
	  (cur_lumi_block >= 736 && cur_lumi_block <= 757) ||
	  (cur_lumi_block >= 671 && cur_lumi_block <= 733) ) ) ||
	( cur_run_number == 198522 && (
	  (cur_lumi_block >= 65 && cur_lumi_block <= 144) ||
	  (cur_lumi_block >= 147 && cur_lumi_block <= 208) ) ) ||
	( cur_run_number == 198941 && (
	  (cur_lumi_block >= 102 && cur_lumi_block <= 189) ||
	  (cur_lumi_block >= 222 && cur_lumi_block <= 241) ||
	  (cur_lumi_block >= 252 && cur_lumi_block <= 284) ||
	  (cur_lumi_block >= 243 && cur_lumi_block <= 249) ||
	  (cur_lumi_block >= 191 && cur_lumi_block <= 220) ) ) ||
	( cur_run_number == 198954 && (
	  (cur_lumi_block >= 159 && cur_lumi_block <= 277) ||
	  (cur_lumi_block >= 108 && cur_lumi_block <= 156) ) ) ||
	( cur_run_number == 198955 && (
	  (cur_lumi_block >= 271 && cur_lumi_block <= 284) ||
	  (cur_lumi_block >= 53 && cur_lumi_block <= 220) ||
	  (cur_lumi_block >= 1190 && cur_lumi_block <= 1246) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 45) ||
	  (cur_lumi_block >= 583 && cur_lumi_block <= 742) ||
	  (cur_lumi_block >= 1249 && cur_lumi_block <= 1304) ||
	  (cur_lumi_block >= 913 && cur_lumi_block <= 946) ||
	  (cur_lumi_block >= 286 && cur_lumi_block <= 338) ||
	  (cur_lumi_block >= 1165 && cur_lumi_block <= 1169) ||
	  (cur_lumi_block >= 1306 && cur_lumi_block <= 1467) ||
	  (cur_lumi_block >= 1185 && cur_lumi_block <= 1188) ||
	  (cur_lumi_block >= 1172 && cur_lumi_block <= 1182) ||
	  (cur_lumi_block >= 744 && cur_lumi_block <= 910) ||
	  (cur_lumi_block >= 47 && cur_lumi_block <= 50) ||
	  (cur_lumi_block >= 1470 && cur_lumi_block <= 1485) ||
	  (cur_lumi_block >= 949 && cur_lumi_block <= 1162) ||
	  (cur_lumi_block >= 1487 && cur_lumi_block <= 1552) ||
	  (cur_lumi_block >= 340 && cur_lumi_block <= 580) ||
	  (cur_lumi_block >= 223 && cur_lumi_block <= 269) ) ) ||
	( cur_run_number == 198969 && (
	  (cur_lumi_block >= 646 && cur_lumi_block <= 918) ||
	  (cur_lumi_block >= 1239 && cur_lumi_block <= 1253) ||
	  (cur_lumi_block >= 920 && cur_lumi_block <= 1011) ||
	  (cur_lumi_block >= 468 && cur_lumi_block <= 643) ||
	  (cur_lumi_block >= 1178 && cur_lumi_block <= 1236) ||
	  (cur_lumi_block >= 416 && cur_lumi_block <= 466) ||
	  (cur_lumi_block >= 325 && cur_lumi_block <= 365) ||
	  (cur_lumi_block >= 84 && cur_lumi_block <= 247) ||
	  (cur_lumi_block >= 1013 && cur_lumi_block <= 1175) ||
	  (cur_lumi_block >= 249 && cur_lumi_block <= 323) ||
	  (cur_lumi_block >= 58 && cur_lumi_block <= 81) ||
	  (cur_lumi_block >= 367 && cur_lumi_block <= 413) ) ) ||
	( cur_run_number == 199008 && (
	  (cur_lumi_block >= 124 && cur_lumi_block <= 208) ||
	  (cur_lumi_block >= 376 && cur_lumi_block <= 482) ||
	  (cur_lumi_block >= 333 && cur_lumi_block <= 373) ||
	  (cur_lumi_block >= 95 && cur_lumi_block <= 121) ||
	  (cur_lumi_block >= 75 && cur_lumi_block <= 93) ||
	  (cur_lumi_block >= 211 && cur_lumi_block <= 331) ||
	  (cur_lumi_block >= 608 && cur_lumi_block <= 644) ||
	  (cur_lumi_block >= 485 && cur_lumi_block <= 605) ) ) ||
	( cur_run_number == 199011 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 11) ||
	  (cur_lumi_block >= 13 && cur_lumi_block <= 24) ) ) ||
	( cur_run_number == 199021 && (
	  (cur_lumi_block >= 1226 && cur_lumi_block <= 1479) ||
	  (cur_lumi_block >= 130 && cur_lumi_block <= 133) ||
	  (cur_lumi_block >= 565 && cur_lumi_block <= 1223) ||
	  (cur_lumi_block >= 1481 && cur_lumi_block <= 1494) ||
	  (cur_lumi_block >= 91 && cur_lumi_block <= 128) ||
	  (cur_lumi_block >= 535 && cur_lumi_block <= 563) ||
	  (cur_lumi_block >= 136 && cur_lumi_block <= 309) ||
	  (cur_lumi_block >= 471 && cur_lumi_block <= 533) ||
	  (cur_lumi_block >= 59 && cur_lumi_block <= 88) ||
	  (cur_lumi_block >= 414 && cur_lumi_block <= 469) ||
	  (cur_lumi_block >= 335 && cur_lumi_block <= 410) ||
	  (cur_lumi_block >= 311 && cur_lumi_block <= 333) ) ) ||
	( cur_run_number == 199318 && (
	  (cur_lumi_block >= 65 && cur_lumi_block <= 138) ) ) ||
	( cur_run_number == 199319 && (
	  (cur_lumi_block >= 492 && cur_lumi_block <= 493) ||
	  (cur_lumi_block >= 645 && cur_lumi_block <= 720) ||
	  (cur_lumi_block >= 734 && cur_lumi_block <= 741) ||
	  (cur_lumi_block >= 723 && cur_lumi_block <= 728) ||
	  (cur_lumi_block >= 360 && cur_lumi_block <= 422) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 7) ||
	  (cur_lumi_block >= 754 && cur_lumi_block <= 943) ||
	  (cur_lumi_block >= 730 && cur_lumi_block <= 731) ||
	  (cur_lumi_block >= 351 && cur_lumi_block <= 358) ||
	  (cur_lumi_block >= 615 && cur_lumi_block <= 642) ||
	  (cur_lumi_block >= 280 && cur_lumi_block <= 348) ||
	  (cur_lumi_block >= 424 && cur_lumi_block <= 490) ||
	  (cur_lumi_block >= 9 && cur_lumi_block <= 223) ||
	  (cur_lumi_block >= 226 && cur_lumi_block <= 277) ||
	  (cur_lumi_block >= 744 && cur_lumi_block <= 752) ||
	  (cur_lumi_block >= 496 && cur_lumi_block <= 612) ||
	  (cur_lumi_block >= 945 && cur_lumi_block <= 997) ) ) ||
	( cur_run_number == 199336 && (
	  (cur_lumi_block >= 234 && cur_lumi_block <= 614) ||
	  (cur_lumi_block >= 617 && cur_lumi_block <= 789) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 33) ||
	  (cur_lumi_block >= 125 && cur_lumi_block <= 231) ||
	  (cur_lumi_block >= 36 && cur_lumi_block <= 122) ||
	  (cur_lumi_block >= 791 && cur_lumi_block <= 977) ) ) ||
	( cur_run_number == 199356 && (
	  (cur_lumi_block >= 208 && cur_lumi_block <= 231) ||
	  (cur_lumi_block >= 123 && cur_lumi_block <= 168) ||
	  (cur_lumi_block >= 95 && cur_lumi_block <= 121) ||
	  (cur_lumi_block >= 171 && cur_lumi_block <= 205) ) ) ||
	( cur_run_number == 199409 && (
	  (cur_lumi_block >= 206 && cur_lumi_block <= 290) ||
	  (cur_lumi_block >= 91 && cur_lumi_block <= 204) ||
	  (cur_lumi_block >= 586 && cur_lumi_block <= 602) ||
	  (cur_lumi_block >= 1016 && cur_lumi_block <= 1300) ||
	  (cur_lumi_block >= 25 && cur_lumi_block <= 54) ||
	  (cur_lumi_block >= 604 && cur_lumi_block <= 1014) ||
	  (cur_lumi_block >= 56 && cur_lumi_block <= 89) ||
	  (cur_lumi_block >= 293 && cur_lumi_block <= 583) ) ) ||
	( cur_run_number == 199428 && (
	  (cur_lumi_block >= 387 && cur_lumi_block <= 414) ||
	  (cur_lumi_block >= 212 && cur_lumi_block <= 382) ||
	  (cur_lumi_block >= 200 && cur_lumi_block <= 210) ||
	  (cur_lumi_block >= 533 && cur_lumi_block <= 648) ||
	  (cur_lumi_block >= 61 && cur_lumi_block <= 197) ||
	  (cur_lumi_block >= 417 && cur_lumi_block <= 436) ||
	  (cur_lumi_block >= 439 && cur_lumi_block <= 530) ) ) ||
	( cur_run_number == 199429 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 28) ||
	  (cur_lumi_block >= 30 && cur_lumi_block <= 36) ||
	  (cur_lumi_block >= 39 && cur_lumi_block <= 55) ||
	  (cur_lumi_block >= 58 && cur_lumi_block <= 101) ||
	  (cur_lumi_block >= 103 && cur_lumi_block <= 148) ||
	  (cur_lumi_block >= 151 && cur_lumi_block <= 154) ) ) ||
	( cur_run_number == 199435 && (
	  (cur_lumi_block >= 656 && cur_lumi_block <= 696) ||
	  (cur_lumi_block >= 63 && cur_lumi_block <= 106) ||
	  (cur_lumi_block >= 1444 && cur_lumi_block <= 1487) ||
	  (cur_lumi_block >= 1330 && cur_lumi_block <= 1411) ||
	  (cur_lumi_block >= 263 && cur_lumi_block <= 579) ||
	  (cur_lumi_block >= 1434 && cur_lumi_block <= 1441) ||
	  (cur_lumi_block >= 1147 && cur_lumi_block <= 1327) ||
	  (cur_lumi_block >= 582 && cur_lumi_block <= 654) ||
	  (cur_lumi_block >= 1037 && cur_lumi_block <= 1144) ||
	  (cur_lumi_block >= 109 && cur_lumi_block <= 261) ||
	  (cur_lumi_block >= 1489 && cur_lumi_block <= 1610) ||
	  (cur_lumi_block >= 1414 && cur_lumi_block <= 1431) ||
	  (cur_lumi_block >= 699 && cur_lumi_block <= 1034) ) ) ||
	( cur_run_number == 199436 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 113) ||
	  (cur_lumi_block >= 116 && cur_lumi_block <= 254) ||
	  (cur_lumi_block >= 257 && cur_lumi_block <= 675) ||
	  (cur_lumi_block >= 678 && cur_lumi_block <= 748) ) ) ||
	( cur_run_number == 199564 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 3) ) ) ||
	( cur_run_number == 199569 && (
	  (cur_lumi_block >= 139 && cur_lumi_block <= 367) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 2) ||
	  (cur_lumi_block >= 5 && cur_lumi_block <= 136) ) ) ||
	( cur_run_number == 199570 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 17) ) ) ||
	( cur_run_number == 199571 && (
	  (cur_lumi_block >= 363 && cur_lumi_block <= 561) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 184) ||
	  (cur_lumi_block >= 186 && cur_lumi_block <= 360) ) ) ||
	( cur_run_number == 199572 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 317) ) ) ||
	( cur_run_number == 199573 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 22) ) ) ||
	( cur_run_number == 199574 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 53) ||
	  (cur_lumi_block >= 156 && cur_lumi_block <= 246) ||
	  (cur_lumi_block >= 56 && cur_lumi_block <= 153) ) ) ||
	( cur_run_number == 199608 && (
	  (cur_lumi_block >= 2255 && cur_lumi_block <= 2422) ||
	  (cur_lumi_block >= 464 && cur_lumi_block <= 800) ||
	  (cur_lumi_block >= 344 && cur_lumi_block <= 390) ||
	  (cur_lumi_block >= 159 && cur_lumi_block <= 209) ||
	  (cur_lumi_block >= 1907 && cur_lumi_block <= 1962) ||
	  (cur_lumi_block >= 1395 && cur_lumi_block <= 1630) ||
	  (cur_lumi_block >= 1633 && cur_lumi_block <= 1904) ||
	  (cur_lumi_block >= 211 && cur_lumi_block <= 341) ||
	  (cur_lumi_block >= 1965 && cur_lumi_block <= 2252) ||
	  (cur_lumi_block >= 802 && cur_lumi_block <= 1064) ||
	  (cur_lumi_block >= 392 && cur_lumi_block <= 461) ||
	  (cur_lumi_block >= 60 && cur_lumi_block <= 157) ||
	  (cur_lumi_block >= 1067 && cur_lumi_block <= 1392) ) ) ||
	( cur_run_number == 199698 && (
	  (cur_lumi_block >= 72 && cur_lumi_block <= 94) ||
	  (cur_lumi_block >= 96 && cur_lumi_block <= 127) ) ) ||
	( cur_run_number == 199699 && (
	  (cur_lumi_block >= 412 && cur_lumi_block <= 756) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 154) ||
	  (cur_lumi_block >= 157 && cur_lumi_block <= 169) ||
	  (cur_lumi_block >= 172 && cur_lumi_block <= 410) ) ) ||
	( cur_run_number == 199703 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 94) ||
	  (cur_lumi_block >= 97 && cur_lumi_block <= 482) ||
	  (cur_lumi_block >= 485 && cur_lumi_block <= 529) ) ) ||
	( cur_run_number == 199739 && (
	  (cur_lumi_block >= 66 && cur_lumi_block <= 133) ) ) ||
	( cur_run_number == 199751 && (
	  (cur_lumi_block >= 121 && cur_lumi_block <= 127) ||
	  (cur_lumi_block >= 103 && cur_lumi_block <= 119) ) ) ||
	( cur_run_number == 199752 && (
	  (cur_lumi_block >= 188 && cur_lumi_block <= 211) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 141) ||
	  (cur_lumi_block >= 214 && cur_lumi_block <= 322) ||
	  (cur_lumi_block >= 144 && cur_lumi_block <= 180) ||
	  (cur_lumi_block >= 182 && cur_lumi_block <= 186) ) ) ||
	( cur_run_number == 199753 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 59) ) ) ||
	( cur_run_number == 199754 && (
	  (cur_lumi_block >= 328 && cur_lumi_block <= 457) ||
	  (cur_lumi_block >= 808 && cur_lumi_block <= 998) ||
	  (cur_lumi_block >= 205 && cur_lumi_block <= 325) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 203) ||
	  (cur_lumi_block >= 610 && cur_lumi_block <= 613) ||
	  (cur_lumi_block >= 615 && cur_lumi_block <= 806) ||
	  (cur_lumi_block >= 459 && cur_lumi_block <= 607) ) ) ||
	( cur_run_number == 199804 && (
	  (cur_lumi_block >= 90 && cur_lumi_block <= 181) ||
	  (cur_lumi_block >= 78 && cur_lumi_block <= 88) ||
	  (cur_lumi_block >= 281 && cur_lumi_block <= 290) ||
	  (cur_lumi_block >= 522 && cur_lumi_block <= 575) ||
	  (cur_lumi_block >= 292 && cur_lumi_block <= 519) ||
	  (cur_lumi_block >= 238 && cur_lumi_block <= 278) ||
	  (cur_lumi_block >= 631 && cur_lumi_block <= 632) ||
	  (cur_lumi_block >= 577 && cur_lumi_block <= 628) ||
	  (cur_lumi_block >= 183 && cur_lumi_block <= 235) ) ) ||
	( cur_run_number == 199812 && (
	  (cur_lumi_block >= 70 && cur_lumi_block <= 141) ||
	  (cur_lumi_block >= 626 && cur_lumi_block <= 751) ||
	  (cur_lumi_block >= 560 && cur_lumi_block <= 571) ||
	  (cur_lumi_block >= 474 && cur_lumi_block <= 505) ||
	  (cur_lumi_block >= 754 && cur_lumi_block <= 796) ||
	  (cur_lumi_block >= 214 && cur_lumi_block <= 471) ||
	  (cur_lumi_block >= 144 && cur_lumi_block <= 163) ||
	  (cur_lumi_block >= 508 && cur_lumi_block <= 557) ||
	  (cur_lumi_block >= 182 && cur_lumi_block <= 211) ||
	  (cur_lumi_block >= 574 && cur_lumi_block <= 623) ) ) ||
	( cur_run_number == 199832 && (
	  (cur_lumi_block >= 142 && cur_lumi_block <= 286) ||
	  (cur_lumi_block >= 121 && cur_lumi_block <= 139) ||
	  (cur_lumi_block >= 65 && cur_lumi_block <= 118) ||
	  (cur_lumi_block >= 58 && cur_lumi_block <= 62) ) ) ||
	( cur_run_number == 199833 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 13) ||
	  (cur_lumi_block >= 16 && cur_lumi_block <= 103) ||
	  (cur_lumi_block >= 105 && cur_lumi_block <= 250) ||
	  (cur_lumi_block >= 1034 && cur_lumi_block <= 1185) ||
	  (cur_lumi_block >= 1188 && cur_lumi_block <= 1239) ||
	  (cur_lumi_block >= 253 && cur_lumi_block <= 493) ||
	  (cur_lumi_block >= 797 && cur_lumi_block <= 1032) ||
	  (cur_lumi_block >= 496 && cur_lumi_block <= 794) ) ) ||
	( cur_run_number == 199834 && (
	  (cur_lumi_block >= 11 && cur_lumi_block <= 11) ||
	  (cur_lumi_block >= 21 && cur_lumi_block <= 54) ||
	  (cur_lumi_block >= 505 && cur_lumi_block <= 942) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 9) ||
	  (cur_lumi_block >= 286 && cur_lumi_block <= 503) ||
	  (cur_lumi_block >= 14 && cur_lumi_block <= 18) ||
	  (cur_lumi_block >= 69 && cur_lumi_block <= 284) ||
	  (cur_lumi_block >= 56 && cur_lumi_block <= 57) ||
	  (cur_lumi_block >= 62 && cur_lumi_block <= 65) ) ) ||
	( cur_run_number == 199862 && (
	  (cur_lumi_block >= 59 && cur_lumi_block <= 141) ) ) ||
	( cur_run_number == 199864 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 87) ||
	  (cur_lumi_block >= 106 && cur_lumi_block <= 372) ||
	  (cur_lumi_block >= 388 && cur_lumi_block <= 486) ||
	  (cur_lumi_block >= 374 && cur_lumi_block <= 385) ||
	  (cur_lumi_block >= 92 && cur_lumi_block <= 103) ||
	  (cur_lumi_block >= 89 && cur_lumi_block <= 89) ) ) ||
	( cur_run_number == 199867 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 134) ||
	  (cur_lumi_block >= 174 && cur_lumi_block <= 218) ||
	  (cur_lumi_block >= 136 && cur_lumi_block <= 172) ||
	  (cur_lumi_block >= 221 && cur_lumi_block <= 320) ) ) ||
	( cur_run_number == 199868 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 21) ) ) ||
	( cur_run_number == 199875 && (
	  (cur_lumi_block >= 152 && cur_lumi_block <= 334) ||
	  (cur_lumi_block >= 70 && cur_lumi_block <= 150) ) ) ||
	( cur_run_number == 199876 && (
	  (cur_lumi_block >= 365 && cur_lumi_block <= 376) ||
	  (cur_lumi_block >= 274 && cur_lumi_block <= 340) ||
	  (cur_lumi_block >= 22 && cur_lumi_block <= 95) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 19) ||
	  (cur_lumi_block >= 252 && cur_lumi_block <= 272) ||
	  (cur_lumi_block >= 97 && cur_lumi_block <= 249) ||
	  (cur_lumi_block >= 343 && cur_lumi_block <= 362) ) ) ||
	( cur_run_number == 199877 && (
	  (cur_lumi_block >= 703 && cur_lumi_block <= 871) ||
	  (cur_lumi_block >= 607 && cur_lumi_block <= 701) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 173) ||
	  (cur_lumi_block >= 175 && cur_lumi_block <= 605) ) ) ||
	( cur_run_number == 199960 && (
	  (cur_lumi_block >= 365 && cur_lumi_block <= 367) ||
	  (cur_lumi_block >= 370 && cur_lumi_block <= 380) ||
	  (cur_lumi_block >= 204 && cur_lumi_block <= 232) ||
	  (cur_lumi_block >= 469 && cur_lumi_block <= 485) ||
	  (cur_lumi_block >= 461 && cur_lumi_block <= 466) ||
	  (cur_lumi_block >= 72 && cur_lumi_block <= 139) ||
	  (cur_lumi_block >= 383 && cur_lumi_block <= 459) ||
	  (cur_lumi_block >= 141 && cur_lumi_block <= 197) ||
	  (cur_lumi_block >= 235 && cur_lumi_block <= 363) ) ) ||
	( cur_run_number == 199961 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 211) ||
	  (cur_lumi_block >= 213 && cur_lumi_block <= 287) ) ) ||
	( cur_run_number == 199967 && (
	  (cur_lumi_block >= 122 && cur_lumi_block <= 170) ||
	  (cur_lumi_block >= 172 && cur_lumi_block <= 198) ||
	  (cur_lumi_block >= 60 && cur_lumi_block <= 120) ) ) ||
	( cur_run_number == 199973 && (
	  (cur_lumi_block >= 73 && cur_lumi_block <= 89) ) ) ||
	( cur_run_number == 200041 && (
	  (cur_lumi_block >= 571 && cur_lumi_block <= 593) ||
	  (cur_lumi_block >= 862 && cur_lumi_block <= 930) ||
	  (cur_lumi_block >= 402 && cur_lumi_block <= 568) ||
	  (cur_lumi_block >= 932 && cur_lumi_block <= 1096) ||
	  (cur_lumi_block >= 162 && cur_lumi_block <= 274) ||
	  (cur_lumi_block >= 388 && cur_lumi_block <= 389) ||
	  (cur_lumi_block >= 595 && cur_lumi_block <= 646) ||
	  (cur_lumi_block >= 85 && cur_lumi_block <= 157) ||
	  (cur_lumi_block >= 731 && cur_lumi_block <= 860) ||
	  (cur_lumi_block >= 649 && cur_lumi_block <= 728) ||
	  (cur_lumi_block >= 277 && cur_lumi_block <= 318) ||
	  (cur_lumi_block >= 392 && cur_lumi_block <= 400) ||
	  (cur_lumi_block >= 337 && cur_lumi_block <= 386) ||
	  (cur_lumi_block >= 321 && cur_lumi_block <= 335) ||
	  (cur_lumi_block >= 62 && cur_lumi_block <= 83) ) ) ||
	( cur_run_number == 200042 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 110) ||
	  (cur_lumi_block >= 112 && cur_lumi_block <= 536) ) ) ||
	( cur_run_number == 200049 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 177) ) ) ||
	( cur_run_number == 200075 && (
	  (cur_lumi_block >= 142 && cur_lumi_block <= 232) ||
	  (cur_lumi_block >= 502 && cur_lumi_block <= 605) ||
	  (cur_lumi_block >= 434 && cur_lumi_block <= 500) ||
	  (cur_lumi_block >= 329 && cur_lumi_block <= 422) ||
	  (cur_lumi_block >= 425 && cur_lumi_block <= 431) ||
	  (cur_lumi_block >= 256 && cur_lumi_block <= 326) ||
	  (cur_lumi_block >= 76 && cur_lumi_block <= 139) ) ) ||
	( cur_run_number == 200091 && (
	  (cur_lumi_block >= 1341 && cur_lumi_block <= 1433) ||
	  (cur_lumi_block >= 67 && cur_lumi_block <= 67) ||
	  (cur_lumi_block >= 204 && cur_lumi_block <= 425) ||
	  (cur_lumi_block >= 428 && cur_lumi_block <= 535) ||
	  (cur_lumi_block >= 70 && cur_lumi_block <= 151) ||
	  (cur_lumi_block >= 1683 && cur_lumi_block <= 1710) ||
	  (cur_lumi_block >= 537 && cur_lumi_block <= 607) ||
	  (cur_lumi_block >= 1135 && cur_lumi_block <= 1339) ||
	  (cur_lumi_block >= 610 && cur_lumi_block <= 879) ||
	  (cur_lumi_block >= 174 && cur_lumi_block <= 187) ||
	  (cur_lumi_block >= 154 && cur_lumi_block <= 172) ||
	  (cur_lumi_block >= 946 && cur_lumi_block <= 999) ||
	  (cur_lumi_block >= 1667 && cur_lumi_block <= 1680) ||
	  (cur_lumi_block >= 190 && cur_lumi_block <= 196) ||
	  (cur_lumi_block >= 1435 && cur_lumi_block <= 1450) ||
	  (cur_lumi_block >= 199 && cur_lumi_block <= 201) ||
	  (cur_lumi_block >= 1027 && cur_lumi_block <= 1132) ||
	  (cur_lumi_block >= 1001 && cur_lumi_block <= 1025) ||
	  (cur_lumi_block >= 881 && cur_lumi_block <= 943) ||
	  (cur_lumi_block >= 1453 && cur_lumi_block <= 1523) ||
	  (cur_lumi_block >= 1526 && cur_lumi_block <= 1664) ) ) ||
	( cur_run_number == 200152 && (
	  (cur_lumi_block >= 74 && cur_lumi_block <= 116) ) ) ||
	( cur_run_number == 200160 && (
	  (cur_lumi_block >= 52 && cur_lumi_block <= 68) ) ) ||
	( cur_run_number == 200161 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 97) ||
	  (cur_lumi_block >= 100 && cur_lumi_block <= 112) ) ) ||
	( cur_run_number == 200174 && (
	  (cur_lumi_block >= 81 && cur_lumi_block <= 84) ) ) ||
	( cur_run_number == 200177 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 56) ) ) ||
	( cur_run_number == 200178 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 38) ) ) ||
	( cur_run_number == 200180 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 18) ) ) ||
	( cur_run_number == 200186 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 3) ||
	  (cur_lumi_block >= 6 && cur_lumi_block <= 24) ) ) ||
	( cur_run_number == 200188 && (
	  (cur_lumi_block >= 274 && cur_lumi_block <= 352) ||
	  (cur_lumi_block >= 79 && cur_lumi_block <= 271) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 24) ||
	  (cur_lumi_block >= 27 && cur_lumi_block <= 28) ||
	  (cur_lumi_block >= 31 && cur_lumi_block <= 76) ) ) ||
	( cur_run_number == 200190 && (
	  (cur_lumi_block >= 593 && cur_lumi_block <= 595) ||
	  (cur_lumi_block >= 460 && cur_lumi_block <= 565) ||
	  (cur_lumi_block >= 79 && cur_lumi_block <= 143) ||
	  (cur_lumi_block >= 567 && cur_lumi_block <= 588) ||
	  (cur_lumi_block >= 324 && cur_lumi_block <= 401) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 4) ||
	  (cur_lumi_block >= 162 && cur_lumi_block <= 256) ||
	  (cur_lumi_block >= 403 && cur_lumi_block <= 453) ||
	  (cur_lumi_block >= 6 && cur_lumi_block <= 76) ||
	  (cur_lumi_block >= 597 && cur_lumi_block <= 646) ||
	  (cur_lumi_block >= 146 && cur_lumi_block <= 159) ||
	  (cur_lumi_block >= 649 && cur_lumi_block <= 878) ||
	  (cur_lumi_block >= 258 && cur_lumi_block <= 321) ||
	  (cur_lumi_block >= 456 && cur_lumi_block <= 457) ||
	  (cur_lumi_block >= 591 && cur_lumi_block <= 591) ) ) ||
	( cur_run_number == 200229 && (
	  (cur_lumi_block >= 627 && cur_lumi_block <= 629) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 33) ||
	  (cur_lumi_block >= 222 && cur_lumi_block <= 244) ||
	  (cur_lumi_block >= 41 && cur_lumi_block <= 219) ||
	  (cur_lumi_block >= 293 && cur_lumi_block <= 624) ||
	  (cur_lumi_block >= 247 && cur_lumi_block <= 290) ) ) ||
	( cur_run_number == 200243 && (
	  (cur_lumi_block >= 106 && cur_lumi_block <= 139) ||
	  (cur_lumi_block >= 69 && cur_lumi_block <= 103) ) ) ||
	( cur_run_number == 200244 && (
	  (cur_lumi_block >= 307 && cur_lumi_block <= 442) ||
	  (cur_lumi_block >= 445 && cur_lumi_block <= 507) ||
	  (cur_lumi_block >= 3 && cur_lumi_block <= 304) ||
	  (cur_lumi_block >= 510 && cur_lumi_block <= 619) ) ) ||
	( cur_run_number == 200245 && (
	  (cur_lumi_block >= 131 && cur_lumi_block <= 248) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 103) ||
	  (cur_lumi_block >= 105 && cur_lumi_block <= 128) ||
	  (cur_lumi_block >= 251 && cur_lumi_block <= 357) ) ) ||
	( cur_run_number == 200368 && (
	  (cur_lumi_block >= 72 && cur_lumi_block <= 180) ) ) ||
	( cur_run_number == 200369 && (
	  (cur_lumi_block >= 441 && cur_lumi_block <= 578) ||
	  (cur_lumi_block >= 363 && cur_lumi_block <= 439) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 5) ||
	  (cur_lumi_block >= 580 && cur_lumi_block <= 603) ||
	  (cur_lumi_block >= 64 && cur_lumi_block <= 360) ||
	  (cur_lumi_block >= 606 && cur_lumi_block <= 684) ||
	  (cur_lumi_block >= 8 && cur_lumi_block <= 61) ||
	  (cur_lumi_block >= 686 && cur_lumi_block <= 686) ) ) ||
	( cur_run_number == 200381 && (
	  (cur_lumi_block >= 91 && cur_lumi_block <= 195) ||
	  (cur_lumi_block >= 18 && cur_lumi_block <= 36) ||
	  (cur_lumi_block >= 38 && cur_lumi_block <= 89) ||
	  (cur_lumi_block >= 8 && cur_lumi_block <= 15) ) ) ||
	( cur_run_number == 200466 && (
	  (cur_lumi_block >= 134 && cur_lumi_block <= 274) ) ) ||
	( cur_run_number == 200473 && (
	  (cur_lumi_block >= 306 && cur_lumi_block <= 469) ||
	  (cur_lumi_block >= 96 && cur_lumi_block <= 157) ||
	  (cur_lumi_block >= 733 && cur_lumi_block <= 738) ||
	  (cur_lumi_block >= 159 && cur_lumi_block <= 224) ||
	  (cur_lumi_block >= 740 && cur_lumi_block <= 1324) ||
	  (cur_lumi_block >= 622 && cur_lumi_block <= 688) ||
	  (cur_lumi_block >= 226 && cur_lumi_block <= 304) ||
	  (cur_lumi_block >= 472 && cur_lumi_block <= 524) ||
	  (cur_lumi_block >= 545 && cur_lumi_block <= 619) ||
	  (cur_lumi_block >= 527 && cur_lumi_block <= 542) ||
	  (cur_lumi_block >= 691 && cur_lumi_block <= 730) ) ) ||
	( cur_run_number == 200491 && (
	  (cur_lumi_block >= 152 && cur_lumi_block <= 157) ||
	  (cur_lumi_block >= 276 && cur_lumi_block <= 334) ||
	  (cur_lumi_block >= 336 && cur_lumi_block <= 360) ||
	  (cur_lumi_block >= 363 && cur_lumi_block <= 419) ||
	  (cur_lumi_block >= 87 && cur_lumi_block <= 107) ||
	  (cur_lumi_block >= 110 && cur_lumi_block <= 149) ||
	  (cur_lumi_block >= 240 && cur_lumi_block <= 270) ||
	  (cur_lumi_block >= 273 && cur_lumi_block <= 273) ||
	  (cur_lumi_block >= 199 && cur_lumi_block <= 237) ||
	  (cur_lumi_block >= 160 && cur_lumi_block <= 197) ) ) ||
	( cur_run_number == 200515 && (
	  (cur_lumi_block >= 97 && cur_lumi_block <= 183) ) ) ||
	( cur_run_number == 200519 && (
	  (cur_lumi_block >= 613 && cur_lumi_block <= 747) ||
	  (cur_lumi_block >= 227 && cur_lumi_block <= 258) ||
	  (cur_lumi_block >= 129 && cur_lumi_block <= 136) ||
	  (cur_lumi_block >= 353 && cur_lumi_block <= 611) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 111) ||
	  (cur_lumi_block >= 261 && cur_lumi_block <= 350) ||
	  (cur_lumi_block >= 138 && cur_lumi_block <= 224) ||
	  (cur_lumi_block >= 114 && cur_lumi_block <= 126) ) ) ||
	( cur_run_number == 200525 && (
	  (cur_lumi_block >= 314 && cur_lumi_block <= 464) ||
	  (cur_lumi_block >= 193 && cur_lumi_block <= 276) ||
	  (cur_lumi_block >= 491 && cur_lumi_block <= 674) ||
	  (cur_lumi_block >= 467 && cur_lumi_block <= 488) ||
	  (cur_lumi_block >= 166 && cur_lumi_block <= 190) ||
	  (cur_lumi_block >= 77 && cur_lumi_block <= 149) ||
	  (cur_lumi_block >= 939 && cur_lumi_block <= 990) ||
	  (cur_lumi_block >= 707 && cur_lumi_block <= 755) ||
	  (cur_lumi_block >= 898 && cur_lumi_block <= 937) ||
	  (cur_lumi_block >= 757 && cur_lumi_block <= 895) ||
	  (cur_lumi_block >= 676 && cur_lumi_block <= 704) ||
	  (cur_lumi_block >= 278 && cur_lumi_block <= 311) ||
	  (cur_lumi_block >= 151 && cur_lumi_block <= 164) ) ) ||
	( cur_run_number == 200532 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 37) ) ) ||
	( cur_run_number == 200599 && (
	  (cur_lumi_block >= 75 && cur_lumi_block <= 129) ||
	  (cur_lumi_block >= 132 && cur_lumi_block <= 137) ) ) ||
	( cur_run_number == 200600 && (
	  (cur_lumi_block >= 902 && cur_lumi_block <= 943) ||
	  (cur_lumi_block >= 594 && cur_lumi_block <= 596) ||
	  (cur_lumi_block >= 420 && cur_lumi_block <= 526) ||
	  (cur_lumi_block >= 336 && cur_lumi_block <= 397) ||
	  (cur_lumi_block >= 327 && cur_lumi_block <= 334) ||
	  (cur_lumi_block >= 826 && cur_lumi_block <= 900) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 183) ||
	  (cur_lumi_block >= 598 && cur_lumi_block <= 609) ||
	  (cur_lumi_block >= 399 && cur_lumi_block <= 417) ||
	  (cur_lumi_block >= 186 && cur_lumi_block <= 299) ||
	  (cur_lumi_block >= 663 && cur_lumi_block <= 823) ||
	  (cur_lumi_block >= 316 && cur_lumi_block <= 324) ||
	  (cur_lumi_block >= 302 && cur_lumi_block <= 313) ||
	  (cur_lumi_block >= 529 && cur_lumi_block <= 591) ||
	  (cur_lumi_block >= 611 && cur_lumi_block <= 660) ||
	  (cur_lumi_block >= 945 && cur_lumi_block <= 1139) ) ) ||
	( cur_run_number == 200961 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 115) ) ) ||
	( cur_run_number == 200976 && (
	  (cur_lumi_block >= 94 && cur_lumi_block <= 164) ) ) ||
	( cur_run_number == 200990 && (
	  (cur_lumi_block >= 75 && cur_lumi_block <= 143) ) ) ||
	( cur_run_number == 200991 && (
	  (cur_lumi_block >= 1019 && cur_lumi_block <= 1048) ||
	  (cur_lumi_block >= 635 && cur_lumi_block <= 916) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 42) ||
	  (cur_lumi_block >= 255 && cur_lumi_block <= 632) ||
	  (cur_lumi_block >= 44 && cur_lumi_block <= 44) ||
	  (cur_lumi_block >= 918 && cur_lumi_block <= 1017) ||
	  (cur_lumi_block >= 83 && cur_lumi_block <= 175) ||
	  (cur_lumi_block >= 184 && cur_lumi_block <= 252) ||
	  (cur_lumi_block >= 47 && cur_lumi_block <= 80) ||
	  (cur_lumi_block >= 178 && cur_lumi_block <= 181) ) ) ||
	( cur_run_number == 200992 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 405) ||
	  (cur_lumi_block >= 436 && cur_lumi_block <= 581) ||
	  (cur_lumi_block >= 408 && cur_lumi_block <= 434) ) ) ||
	( cur_run_number == 201062 && (
	  (cur_lumi_block >= 78 && cur_lumi_block <= 268) ) ) ||
	( cur_run_number == 201097 && (
	  (cur_lumi_block >= 83 && cur_lumi_block <= 136) ||
	  (cur_lumi_block >= 303 && cur_lumi_block <= 370) ||
	  (cur_lumi_block >= 248 && cur_lumi_block <= 300) ||
	  (cur_lumi_block >= 138 && cur_lumi_block <= 245) ||
	  (cur_lumi_block >= 432 && cur_lumi_block <= 497) ||
	  (cur_lumi_block >= 372 && cur_lumi_block <= 429) ) ) ||
	( cur_run_number == 201114 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 14) ) ) ||
	( cur_run_number == 201115 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 73) ) ) ||
	( cur_run_number == 201159 && (
	  (cur_lumi_block >= 70 && cur_lumi_block <= 211) ) ) ||
	( cur_run_number == 201164 && (
	  (cur_lumi_block >= 274 && cur_lumi_block <= 416) ||
	  (cur_lumi_block >= 200 && cur_lumi_block <= 271) ||
	  (cur_lumi_block >= 180 && cur_lumi_block <= 198) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 8) ||
	  (cur_lumi_block >= 96 && cur_lumi_block <= 125) ||
	  (cur_lumi_block >= 128 && cur_lumi_block <= 178) ||
	  (cur_lumi_block >= 418 && cur_lumi_block <= 418) ||
	  (cur_lumi_block >= 10 && cur_lumi_block <= 94) ) ) ||
	( cur_run_number == 201168 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 37) ||
	  (cur_lumi_block >= 560 && cur_lumi_block <= 730) ||
	  (cur_lumi_block >= 39 && cur_lumi_block <= 275) ||
	  (cur_lumi_block >= 483 && cur_lumi_block <= 558) ||
	  (cur_lumi_block >= 278 && cur_lumi_block <= 481) ) ) ||
	( cur_run_number == 201173 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 194) ||
	  (cur_lumi_block >= 197 && cur_lumi_block <= 586) ) ) ||
	( cur_run_number == 201174 && (
	  (cur_lumi_block >= 342 && cur_lumi_block <= 451) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 214) ||
	  (cur_lumi_block >= 265 && cur_lumi_block <= 339) ||
	  (cur_lumi_block >= 216 && cur_lumi_block <= 263) ) ) ||
	( cur_run_number == 201191 && (
	  (cur_lumi_block >= 977 && cur_lumi_block <= 1105) ||
	  (cur_lumi_block >= 796 && cur_lumi_block <= 838) ||
	  (cur_lumi_block >= 1120 && cur_lumi_block <= 1382) ||
	  (cur_lumi_block >= 494 && cur_lumi_block <= 506) ||
	  (cur_lumi_block >= 100 && cur_lumi_block <= 216) ||
	  (cur_lumi_block >= 609 && cur_lumi_block <= 794) ||
	  (cur_lumi_block >= 587 && cur_lumi_block <= 594) ||
	  (cur_lumi_block >= 75 && cur_lumi_block <= 98) ||
	  (cur_lumi_block >= 597 && cur_lumi_block <= 607) ||
	  (cur_lumi_block >= 509 && cur_lumi_block <= 585) ||
	  (cur_lumi_block >= 218 && cur_lumi_block <= 389) ||
	  (cur_lumi_block >= 1108 && cur_lumi_block <= 1117) ||
	  (cur_lumi_block >= 392 && cur_lumi_block <= 492) ||
	  (cur_lumi_block >= 1385 && cur_lumi_block <= 1386) ||
	  (cur_lumi_block >= 841 && cur_lumi_block <= 974) ) ) ||
	( cur_run_number == 201193 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 19) ) ) ||
	( cur_run_number == 201196 && (
	  (cur_lumi_block >= 241 && cur_lumi_block <= 278) ||
	  (cur_lumi_block >= 341 && cur_lumi_block <= 515) ||
	  (cur_lumi_block >= 723 && cur_lumi_block <= 789) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 238) ||
	  (cur_lumi_block >= 803 && cur_lumi_block <= 841) ||
	  (cur_lumi_block >= 286 && cur_lumi_block <= 299) ||
	  (cur_lumi_block >= 518 && cur_lumi_block <= 720) ||
	  (cur_lumi_block >= 302 && cur_lumi_block <= 338) ) ) ||
	( cur_run_number == 201197 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 23) ) ) ||
	( cur_run_number == 201202 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 437) ) ) ||
	( cur_run_number == 201229 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 5) ||
	  (cur_lumi_block >= 29 && cur_lumi_block <= 73) ||
	  (cur_lumi_block >= 8 && cur_lumi_block <= 26) ) ) ||
	( cur_run_number == 201278 && (
	  (cur_lumi_block >= 942 && cur_lumi_block <= 974) ||
	  (cur_lumi_block >= 1796 && cur_lumi_block <= 1802) ||
	  (cur_lumi_block >= 166 && cur_lumi_block <= 229) ||
	  (cur_lumi_block >= 598 && cur_lumi_block <= 938) ||
	  (cur_lumi_block >= 318 && cur_lumi_block <= 595) ||
	  (cur_lumi_block >= 1909 && cur_lumi_block <= 1929) ||
	  (cur_lumi_block >= 1932 && cur_lumi_block <= 2174) ||
	  (cur_lumi_block >= 259 && cur_lumi_block <= 316) ||
	  (cur_lumi_block >= 1306 && cur_lumi_block <= 1793) ||
	  (cur_lumi_block >= 1163 && cur_lumi_block <= 1304) ||
	  (cur_lumi_block >= 976 && cur_lumi_block <= 1160) ||
	  (cur_lumi_block >= 232 && cur_lumi_block <= 256) ||
	  (cur_lumi_block >= 1805 && cur_lumi_block <= 1906) ||
	  (cur_lumi_block >= 62 && cur_lumi_block <= 163) ) ) ||
	( cur_run_number == 201554 && (
	  (cur_lumi_block >= 70 && cur_lumi_block <= 86) ||
	  (cur_lumi_block >= 116 && cur_lumi_block <= 126) ||
	  (cur_lumi_block >= 88 && cur_lumi_block <= 114) ) ) ||
	( cur_run_number == 201602 && (
	  (cur_lumi_block >= 617 && cur_lumi_block <= 635) ||
	  (cur_lumi_block >= 83 && cur_lumi_block <= 194) ||
	  (cur_lumi_block >= 496 && cur_lumi_block <= 614) ||
	  (cur_lumi_block >= 196 && cur_lumi_block <= 494) ||
	  (cur_lumi_block >= 76 && cur_lumi_block <= 81) ) ) ||
	( cur_run_number == 201611 && (
	  (cur_lumi_block >= 87 && cur_lumi_block <= 145) ||
	  (cur_lumi_block >= 149 && cur_lumi_block <= 182) ||
	  (cur_lumi_block >= 184 && cur_lumi_block <= 186) ) ) ||
	( cur_run_number == 201613 && (
	  (cur_lumi_block >= 53 && cur_lumi_block <= 210) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 42) ||
	  (cur_lumi_block >= 44 && cur_lumi_block <= 49) ||
	  (cur_lumi_block >= 228 && cur_lumi_block <= 646) ||
	  (cur_lumi_block >= 213 && cur_lumi_block <= 215) ||
	  (cur_lumi_block >= 218 && cur_lumi_block <= 225) ) ) ||
	( cur_run_number == 201624 && (
	  (cur_lumi_block >= 95 && cur_lumi_block <= 240) ||
	  (cur_lumi_block >= 83 && cur_lumi_block <= 92) ||
	  (cur_lumi_block >= 270 && cur_lumi_block <= 270) ) ) ||
	( cur_run_number == 201625 && (
	  (cur_lumi_block >= 673 && cur_lumi_block <= 758) ||
	  (cur_lumi_block >= 793 && cur_lumi_block <= 944) ||
	  (cur_lumi_block >= 315 && cur_lumi_block <= 348) ||
	  (cur_lumi_block >= 351 && cur_lumi_block <= 416) ||
	  (cur_lumi_block >= 760 && cur_lumi_block <= 791) ||
	  (cur_lumi_block >= 418 && cur_lumi_block <= 588) ||
	  (cur_lumi_block >= 211 && cur_lumi_block <= 312) ||
	  (cur_lumi_block >= 591 && cur_lumi_block <= 671) ) ) ||
	( cur_run_number == 201657 && (
	  (cur_lumi_block >= 77 && cur_lumi_block <= 93) ||
	  (cur_lumi_block >= 110 && cur_lumi_block <= 118) ||
	  (cur_lumi_block >= 95 && cur_lumi_block <= 108) ) ) ||
	( cur_run_number == 201658 && (
	  (cur_lumi_block >= 21 && cur_lumi_block <= 118) ||
	  (cur_lumi_block >= 139 && cur_lumi_block <= 288) ||
	  (cur_lumi_block >= 121 && cur_lumi_block <= 136) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 19) ) ) ||
	( cur_run_number == 201668 && (
	  (cur_lumi_block >= 78 && cur_lumi_block <= 157) ) ) ||
	( cur_run_number == 201669 && (
	  (cur_lumi_block >= 139 && cur_lumi_block <= 141) ||
	  (cur_lumi_block >= 143 && cur_lumi_block <= 165) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 9) ||
	  (cur_lumi_block >= 12 && cur_lumi_block <= 136) ) ) ||
	( cur_run_number == 201671 && (
	  (cur_lumi_block >= 122 && cur_lumi_block <= 174) ||
	  (cur_lumi_block >= 617 && cur_lumi_block <= 766) ||
	  (cur_lumi_block >= 464 && cur_lumi_block <= 482) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 120) ||
	  (cur_lumi_block >= 899 && cur_lumi_block <= 911) ||
	  (cur_lumi_block >= 914 && cur_lumi_block <= 1007) ||
	  (cur_lumi_block >= 547 && cur_lumi_block <= 571) ||
	  (cur_lumi_block >= 501 && cur_lumi_block <= 545) ||
	  (cur_lumi_block >= 177 && cur_lumi_block <= 462) ||
	  (cur_lumi_block >= 768 && cur_lumi_block <= 896) ||
	  (cur_lumi_block >= 574 && cur_lumi_block <= 614) ||
	  (cur_lumi_block >= 485 && cur_lumi_block <= 499) ) ) ||
	( cur_run_number == 201678 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 120) ) ) ||
	( cur_run_number == 201679 && (
	  (cur_lumi_block >= 324 && cur_lumi_block <= 461) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 110) ||
	  (cur_lumi_block >= 244 && cur_lumi_block <= 298) ||
	  (cur_lumi_block >= 463 && cur_lumi_block <= 483) ||
	  (cur_lumi_block >= 112 && cur_lumi_block <= 241) ||
	  (cur_lumi_block >= 302 && cur_lumi_block <= 321) ) ) ||
	( cur_run_number == 201692 && (
	  (cur_lumi_block >= 78 && cur_lumi_block <= 81) ||
	  (cur_lumi_block >= 83 && cur_lumi_block <= 179) ) ) ||
	( cur_run_number == 201705 && (
	  (cur_lumi_block >= 65 && cur_lumi_block <= 73) ||
	  (cur_lumi_block >= 75 && cur_lumi_block <= 109) ||
	  (cur_lumi_block >= 111 && cur_lumi_block <= 187) ) ) ||
	( cur_run_number == 201706 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 62) ) ) ||
	( cur_run_number == 201707 && (
	  (cur_lumi_block >= 118 && cur_lumi_block <= 130) ||
	  (cur_lumi_block >= 26 && cur_lumi_block <= 42) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 23) ||
	  (cur_lumi_block >= 514 && cur_lumi_block <= 545) ||
	  (cur_lumi_block >= 133 && cur_lumi_block <= 160) ||
	  (cur_lumi_block >= 473 && cur_lumi_block <= 511) ||
	  (cur_lumi_block >= 625 && cur_lumi_block <= 735) ||
	  (cur_lumi_block >= 572 && cur_lumi_block <= 622) ||
	  (cur_lumi_block >= 738 && cur_lumi_block <= 806) ||
	  (cur_lumi_block >= 809 && cur_lumi_block <= 876) ||
	  (cur_lumi_block >= 547 && cur_lumi_block <= 570) ||
	  (cur_lumi_block >= 279 && cur_lumi_block <= 471) ||
	  (cur_lumi_block >= 163 && cur_lumi_block <= 276) ||
	  (cur_lumi_block >= 879 && cur_lumi_block <= 964) ||
	  (cur_lumi_block >= 45 && cur_lumi_block <= 115) ) ) ||
	( cur_run_number == 201708 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 79) ) ) ||
	( cur_run_number == 201718 && (
	  (cur_lumi_block >= 58 && cur_lumi_block <= 108) ) ) ||
	( cur_run_number == 201727 && (
	  (cur_lumi_block >= 67 && cur_lumi_block <= 185) ) ) ||
	( cur_run_number == 201729 && (
	  (cur_lumi_block >= 129 && cur_lumi_block <= 154) ||
	  (cur_lumi_block >= 22 && cur_lumi_block <= 75) ||
	  (cur_lumi_block >= 77 && cur_lumi_block <= 126) ||
	  (cur_lumi_block >= 219 && cur_lumi_block <= 244) ||
	  (cur_lumi_block >= 6 && cur_lumi_block <= 20) ||
	  (cur_lumi_block >= 156 && cur_lumi_block <= 216) ) ) ||
	( cur_run_number == 201794 && (
	  (cur_lumi_block >= 58 && cur_lumi_block <= 94) ) ) ||
	( cur_run_number == 201802 && (
	  (cur_lumi_block >= 68 && cur_lumi_block <= 209) ||
	  (cur_lumi_block >= 290 && cur_lumi_block <= 296) ||
	  (cur_lumi_block >= 211 && cur_lumi_block <= 214) ||
	  (cur_lumi_block >= 216 && cur_lumi_block <= 220) ||
	  (cur_lumi_block >= 223 && cur_lumi_block <= 288) ) ) ||
	( cur_run_number == 201816 && (
	  (cur_lumi_block >= 107 && cur_lumi_block <= 157) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 72) ||
	  (cur_lumi_block >= 74 && cur_lumi_block <= 105) ) ) ||
	( cur_run_number == 201817 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 274) ) ) ||
	( cur_run_number == 201818 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 1) ) ) ||
	( cur_run_number == 201819 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 94) ||
	  (cur_lumi_block >= 96 && cur_lumi_block <= 241) ) ) ||
	( cur_run_number == 201824 && (
	  (cur_lumi_block >= 289 && cur_lumi_block <= 492) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 139) ||
	  (cur_lumi_block >= 179 && cur_lumi_block <= 286) ||
	  (cur_lumi_block >= 141 && cur_lumi_block <= 176) ) ) ||
	( cur_run_number == 202012 && (
	  (cur_lumi_block >= 126 && cur_lumi_block <= 131) ||
	  (cur_lumi_block >= 98 && cur_lumi_block <= 121) ) ) ||
	( cur_run_number == 202013 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 2) ||
	  (cur_lumi_block >= 38 && cur_lumi_block <= 57) ||
	  (cur_lumi_block >= 5 && cur_lumi_block <= 35) ) ) ||
	( cur_run_number == 202014 && (
	  (cur_lumi_block >= 104 && cur_lumi_block <= 174) ||
	  (cur_lumi_block >= 79 && cur_lumi_block <= 102) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 5) ||
	  (cur_lumi_block >= 16 && cur_lumi_block <= 18) ||
	  (cur_lumi_block >= 192 && cur_lumi_block <= 196) ||
	  (cur_lumi_block >= 20 && cur_lumi_block <= 77) ||
	  (cur_lumi_block >= 8 && cur_lumi_block <= 14) ||
	  (cur_lumi_block >= 177 && cur_lumi_block <= 190) ) ) ||
	( cur_run_number == 202016 && (
	  (cur_lumi_block >= 934 && cur_lumi_block <= 1010) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 48) ||
	  (cur_lumi_block >= 179 && cur_lumi_block <= 743) ||
	  (cur_lumi_block >= 893 && cur_lumi_block <= 896) ||
	  (cur_lumi_block >= 898 && cur_lumi_block <= 932) ||
	  (cur_lumi_block >= 51 && cur_lumi_block <= 134) ||
	  (cur_lumi_block >= 137 && cur_lumi_block <= 177) ||
	  (cur_lumi_block >= 745 && cur_lumi_block <= 831) ||
	  (cur_lumi_block >= 834 && cur_lumi_block <= 890) ) ) ||
	( cur_run_number == 202044 && (
	  (cur_lumi_block >= 104 && cur_lumi_block <= 266) ||
	  (cur_lumi_block >= 84 && cur_lumi_block <= 101) ||
	  (cur_lumi_block >= 463 && cur_lumi_block <= 466) ||
	  (cur_lumi_block >= 268 && cur_lumi_block <= 461) ) ) ||
	( cur_run_number == 202045 && (
	  (cur_lumi_block >= 33 && cur_lumi_block <= 72) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 30) ||
	  (cur_lumi_block >= 788 && cur_lumi_block <= 809) ||
	  (cur_lumi_block >= 75 && cur_lumi_block <= 528) ||
	  (cur_lumi_block >= 531 && cur_lumi_block <= 601) ||
	  (cur_lumi_block >= 822 && cur_lumi_block <= 823) ||
	  (cur_lumi_block >= 603 && cur_lumi_block <= 785) ) ) ||
	( cur_run_number == 202054 && (
	  (cur_lumi_block >= 492 && cur_lumi_block <= 605) ||
	  (cur_lumi_block >= 6 && cur_lumi_block <= 266) ||
	  (cur_lumi_block >= 268 && cur_lumi_block <= 489) ||
	  (cur_lumi_block >= 608 && cur_lumi_block <= 631) ) ) ||
	( cur_run_number == 202060 && (
	  (cur_lumi_block >= 644 && cur_lumi_block <= 682) ||
	  (cur_lumi_block >= 144 && cur_lumi_block <= 154) ||
	  (cur_lumi_block >= 246 && cur_lumi_block <= 497) ||
	  (cur_lumi_block >= 156 && cur_lumi_block <= 244) ||
	  (cur_lumi_block >= 746 && cur_lumi_block <= 936) ||
	  (cur_lumi_block >= 684 && cur_lumi_block <= 743) ||
	  (cur_lumi_block >= 499 && cur_lumi_block <= 642) ||
	  (cur_lumi_block >= 76 && cur_lumi_block <= 142) ) ) ||
	( cur_run_number == 202074 && (
	  (cur_lumi_block >= 66 && cur_lumi_block <= 174) ) ) ||
	( cur_run_number == 202075 && (
	  (cur_lumi_block >= 189 && cur_lumi_block <= 214) ||
	  (cur_lumi_block >= 217 && cur_lumi_block <= 247) ||
	  (cur_lumi_block >= 21 && cur_lumi_block <= 187) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 18) ||
	  (cur_lumi_block >= 500 && cur_lumi_block <= 537) ||
	  (cur_lumi_block >= 562 && cur_lumi_block <= 615) ||
	  (cur_lumi_block >= 542 && cur_lumi_block <= 560) ||
	  (cur_lumi_block >= 618 && cur_lumi_block <= 628) ||
	  (cur_lumi_block >= 250 && cur_lumi_block <= 342) ||
	  (cur_lumi_block >= 345 && cur_lumi_block <= 406) ||
	  (cur_lumi_block >= 409 && cur_lumi_block <= 497) ||
	  (cur_lumi_block >= 539 && cur_lumi_block <= 539) ) ) ||
	( cur_run_number == 202084 && (
	  (cur_lumi_block >= 179 && cur_lumi_block <= 180) ||
	  (cur_lumi_block >= 159 && cur_lumi_block <= 177) ||
	  (cur_lumi_block >= 182 && cur_lumi_block <= 239) ||
	  (cur_lumi_block >= 83 && cur_lumi_block <= 156) ) ) ||
	( cur_run_number == 202087 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 25) ||
	  (cur_lumi_block >= 359 && cur_lumi_block <= 652) ||
	  (cur_lumi_block >= 28 && cur_lumi_block <= 208) ||
	  (cur_lumi_block >= 210 && cur_lumi_block <= 357) ||
	  (cur_lumi_block >= 655 && cur_lumi_block <= 853) ||
	  (cur_lumi_block >= 856 && cur_lumi_block <= 1093) ) ) ||
	( cur_run_number == 202088 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 286) ) ) ||
	( cur_run_number == 202093 && (
	  (cur_lumi_block >= 322 && cur_lumi_block <= 360) ||
	  (cur_lumi_block >= 107 && cur_lumi_block <= 320) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 104) ) ) ||
	( cur_run_number == 202116 && (
	  (cur_lumi_block >= 59 && cur_lumi_block <= 60) ) ) ||
	( cur_run_number == 202178 && (
	  (cur_lumi_block >= 67 && cur_lumi_block <= 78) ||
	  (cur_lumi_block >= 1447 && cur_lumi_block <= 1505) ||
	  (cur_lumi_block >= 428 && cur_lumi_block <= 475) ||
	  (cur_lumi_block >= 967 && cur_lumi_block <= 1444) ||
	  (cur_lumi_block >= 1508 && cur_lumi_block <= 1519) ||
	  (cur_lumi_block >= 720 && cur_lumi_block <= 965) ||
	  (cur_lumi_block >= 188 && cur_lumi_block <= 337) ||
	  (cur_lumi_block >= 91 && cur_lumi_block <= 177) ||
	  (cur_lumi_block >= 80 && cur_lumi_block <= 88) ||
	  (cur_lumi_block >= 1522 && cur_lumi_block <= 1555) ||
	  (cur_lumi_block >= 180 && cur_lumi_block <= 186) ||
	  (cur_lumi_block >= 551 && cur_lumi_block <= 717) ||
	  (cur_lumi_block >= 379 && cur_lumi_block <= 425) ||
	  (cur_lumi_block >= 340 && cur_lumi_block <= 377) ||
	  (cur_lumi_block >= 478 && cur_lumi_block <= 548) ) ) ||
	( cur_run_number == 202205 && (
	  (cur_lumi_block >= 94 && cur_lumi_block <= 114) ) ) ||
	( cur_run_number == 202209 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 48) ||
	  (cur_lumi_block >= 51 && cur_lumi_block <= 142) ) ) ||
	( cur_run_number == 202237 && (
	  (cur_lumi_block >= 131 && cur_lumi_block <= 131) ||
	  (cur_lumi_block >= 952 && cur_lumi_block <= 976) ||
	  (cur_lumi_block >= 939 && cur_lumi_block <= 950) ||
	  (cur_lumi_block >= 979 && cur_lumi_block <= 1079) ||
	  (cur_lumi_block >= 422 && cur_lumi_block <= 538) ||
	  (cur_lumi_block >= 222 && cur_lumi_block <= 235) ||
	  (cur_lumi_block >= 238 && cur_lumi_block <= 275) ||
	  (cur_lumi_block >= 39 && cur_lumi_block <= 128) ||
	  (cur_lumi_block >= 291 && cur_lumi_block <= 316) ||
	  (cur_lumi_block >= 134 && cur_lumi_block <= 219) ||
	  (cur_lumi_block >= 540 && cur_lumi_block <= 936) ||
	  (cur_lumi_block >= 277 && cur_lumi_block <= 289) ||
	  (cur_lumi_block >= 319 && cur_lumi_block <= 419) ) ) ||
	( cur_run_number == 202272 && (
	  (cur_lumi_block >= 1299 && cur_lumi_block <= 1481) ||
	  (cur_lumi_block >= 579 && cur_lumi_block <= 683) ||
	  (cur_lumi_block >= 307 && cur_lumi_block <= 313) ||
	  (cur_lumi_block >= 208 && cur_lumi_block <= 305) ||
	  (cur_lumi_block >= 188 && cur_lumi_block <= 205) ||
	  (cur_lumi_block >= 315 && cur_lumi_block <= 371) ||
	  (cur_lumi_block >= 937 && cur_lumi_block <= 1295) ||
	  (cur_lumi_block >= 707 && cur_lumi_block <= 740) ||
	  (cur_lumi_block >= 742 && cur_lumi_block <= 890) ||
	  (cur_lumi_block >= 144 && cur_lumi_block <= 185) ||
	  (cur_lumi_block >= 436 && cur_lumi_block <= 480) ||
	  (cur_lumi_block >= 558 && cur_lumi_block <= 577) ||
	  (cur_lumi_block >= 115 && cur_lumi_block <= 141) ||
	  (cur_lumi_block >= 483 && cur_lumi_block <= 555) ||
	  (cur_lumi_block >= 76 && cur_lumi_block <= 112) ||
	  (cur_lumi_block >= 686 && cur_lumi_block <= 705) ) ) ||
	( cur_run_number == 202299 && (
	  (cur_lumi_block >= 455 && cur_lumi_block <= 555) ||
	  (cur_lumi_block >= 68 && cur_lumi_block <= 84) ||
	  (cur_lumi_block >= 143 && cur_lumi_block <= 193) ||
	  (cur_lumi_block >= 87 && cur_lumi_block <= 141) ||
	  (cur_lumi_block >= 416 && cur_lumi_block <= 452) ||
	  (cur_lumi_block >= 361 && cur_lumi_block <= 379) ||
	  (cur_lumi_block >= 382 && cur_lumi_block <= 414) ||
	  (cur_lumi_block >= 196 && cur_lumi_block <= 358) ) ) ||
	( cur_run_number == 202305 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 89) ||
	  (cur_lumi_block >= 133 && cur_lumi_block <= 323) ||
	  (cur_lumi_block >= 92 && cur_lumi_block <= 130) ) ) ||
	( cur_run_number == 202314 && (
	  (cur_lumi_block >= 67 && cur_lumi_block <= 104) ||
	  (cur_lumi_block >= 107 && cur_lumi_block <= 265) ||
	  (cur_lumi_block >= 268 && cur_lumi_block <= 278) ) ) ||
	( cur_run_number == 202328 && (
	  (cur_lumi_block >= 588 && cur_lumi_block <= 610) ||
	  (cur_lumi_block >= 612 && cur_lumi_block <= 614) ||
	  (cur_lumi_block >= 158 && cur_lumi_block <= 276) ||
	  (cur_lumi_block >= 294 && cur_lumi_block <= 434) ||
	  (cur_lumi_block >= 46 && cur_lumi_block <= 89) ||
	  (cur_lumi_block >= 437 && cur_lumi_block <= 460) ||
	  (cur_lumi_block >= 463 && cur_lumi_block <= 586) ||
	  (cur_lumi_block >= 92 && cur_lumi_block <= 156) ||
	  (cur_lumi_block >= 278 && cur_lumi_block <= 291) ) ) ||
	( cur_run_number == 202333 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 235) ) ) ||
	( cur_run_number == 202389 && (
	  (cur_lumi_block >= 185 && cur_lumi_block <= 190) ||
	  (cur_lumi_block >= 192 && cur_lumi_block <= 199) ||
	  (cur_lumi_block >= 81 && cur_lumi_block <= 182) ) ) ||
	( cur_run_number == 202469 && (
	  (cur_lumi_block >= 87 && cur_lumi_block <= 158) ||
	  (cur_lumi_block >= 160 && cur_lumi_block <= 174) ||
	  (cur_lumi_block >= 177 && cur_lumi_block <= 352) ) ) ||
	( cur_run_number == 202472 && (
	  (cur_lumi_block >= 99 && cur_lumi_block <= 112) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 96) ) ) ||
	( cur_run_number == 202477 && (
	  (cur_lumi_block >= 131 && cur_lumi_block <= 150) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 129) ) ) ||
	( cur_run_number == 202478 && (
	  (cur_lumi_block >= 720 && cur_lumi_block <= 927) ||
	  (cur_lumi_block >= 180 && cur_lumi_block <= 183) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 177) ||
	  (cur_lumi_block >= 929 && cur_lumi_block <= 973) ||
	  (cur_lumi_block >= 186 && cur_lumi_block <= 219) ||
	  (cur_lumi_block >= 1215 && cur_lumi_block <= 1248) ||
	  (cur_lumi_block >= 222 && cur_lumi_block <= 360) ||
	  (cur_lumi_block >= 1031 && cur_lumi_block <= 1186) ||
	  (cur_lumi_block >= 534 && cur_lumi_block <= 718) ||
	  (cur_lumi_block >= 509 && cur_lumi_block <= 531) ||
	  (cur_lumi_block >= 1189 && cur_lumi_block <= 1212) ||
	  (cur_lumi_block >= 362 && cur_lumi_block <= 506) ||
	  (cur_lumi_block >= 975 && cur_lumi_block <= 1029) ) ) ||
	( cur_run_number == 202504 && (
	  (cur_lumi_block >= 718 && cur_lumi_block <= 763) ||
	  (cur_lumi_block >= 620 && cur_lumi_block <= 715) ||
	  (cur_lumi_block >= 1474 && cur_lumi_block <= 1679) ||
	  (cur_lumi_block >= 99 && cur_lumi_block <= 133) ||
	  (cur_lumi_block >= 77 && cur_lumi_block <= 96) ||
	  (cur_lumi_block >= 1682 && cur_lumi_block <= 1704) ||
	  (cur_lumi_block >= 213 && cur_lumi_block <= 241) ||
	  (cur_lumi_block >= 184 && cur_lumi_block <= 211) ||
	  (cur_lumi_block >= 135 && cur_lumi_block <= 182) ||
	  (cur_lumi_block >= 1174 && cur_lumi_block <= 1247) ||
	  (cur_lumi_block >= 243 && cur_lumi_block <= 392) ||
	  (cur_lumi_block >= 1250 && cur_lumi_block <= 1471) ||
	  (cur_lumi_block >= 766 && cur_lumi_block <= 1172) ||
	  (cur_lumi_block >= 395 && cur_lumi_block <= 527) ||
	  (cur_lumi_block >= 529 && cur_lumi_block <= 617) ) ) ||
	( cur_run_number == 202972 && (
	  (cur_lumi_block >= 33 && cur_lumi_block <= 184) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 30) ||
	  (cur_lumi_block >= 292 && cur_lumi_block <= 295) ||
	  (cur_lumi_block >= 186 && cur_lumi_block <= 290) ||
	  (cur_lumi_block >= 298 && cur_lumi_block <= 371) ||
	  (cur_lumi_block >= 374 && cur_lumi_block <= 429) ||
	  (cur_lumi_block >= 431 && cur_lumi_block <= 544) ) ) ||
	( cur_run_number == 202973 && (
	  (cur_lumi_block >= 237 && cur_lumi_block <= 305) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 234) ||
	  (cur_lumi_block >= 532 && cur_lumi_block <= 541) ||
	  (cur_lumi_block >= 853 && cur_lumi_block <= 1408) ||
	  (cur_lumi_block >= 555 && cur_lumi_block <= 851) ||
	  (cur_lumi_block >= 308 && cur_lumi_block <= 437) ||
	  (cur_lumi_block >= 439 && cur_lumi_block <= 530) ||
	  (cur_lumi_block >= 544 && cur_lumi_block <= 552) ) ) ||
	( cur_run_number == 203002 && (
	  (cur_lumi_block >= 643 && cur_lumi_block <= 669) ||
	  (cur_lumi_block >= 130 && cur_lumi_block <= 141) ||
	  (cur_lumi_block >= 720 && cur_lumi_block <= 1034) ||
	  (cur_lumi_block >= 77 && cur_lumi_block <= 128) ||
	  (cur_lumi_block >= 144 && cur_lumi_block <= 207) ||
	  (cur_lumi_block >= 1073 && cur_lumi_block <= 1370) ||
	  (cur_lumi_block >= 674 && cur_lumi_block <= 717) ||
	  (cur_lumi_block >= 1395 && cur_lumi_block <= 1410) ||
	  (cur_lumi_block >= 1413 && cur_lumi_block <= 1596) ||
	  (cur_lumi_block >= 1037 && cur_lumi_block <= 1070) ||
	  (cur_lumi_block >= 362 && cur_lumi_block <= 501) ||
	  (cur_lumi_block >= 209 && cur_lumi_block <= 267) ||
	  (cur_lumi_block >= 1372 && cur_lumi_block <= 1392) ||
	  (cur_lumi_block >= 270 && cur_lumi_block <= 360) ||
	  (cur_lumi_block >= 671 && cur_lumi_block <= 671) ||
	  (cur_lumi_block >= 504 && cur_lumi_block <= 641) ) ) ||
	( cur_run_number == 203709 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 121) ) ) ||
	( cur_run_number == 203742 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 29) ) ) ||
	( cur_run_number == 203777 && (
	  (cur_lumi_block >= 103 && cur_lumi_block <= 113) ) ) ||
	( cur_run_number == 203830 && (
	  (cur_lumi_block >= 82 && cur_lumi_block <= 182) ) ) ||
	( cur_run_number == 203832 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 11) ) ) ||
	( cur_run_number == 203833 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 70) ||
	  (cur_lumi_block >= 73 && cur_lumi_block <= 128) ) ) ||
	( cur_run_number == 203834 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 40) ) ) ||
	( cur_run_number == 203835 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 70) ||
	  (cur_lumi_block >= 73 && cur_lumi_block <= 358) ) ) ||
	( cur_run_number == 203853 && (
	  (cur_lumi_block >= 122 && cur_lumi_block <= 222) ) ) ||
	( cur_run_number == 203894 && (
	  (cur_lumi_block >= 480 && cur_lumi_block <= 902) ||
	  (cur_lumi_block >= 275 && cur_lumi_block <= 477) ||
	  (cur_lumi_block >= 82 && cur_lumi_block <= 272) ||
	  (cur_lumi_block >= 905 && cur_lumi_block <= 1319) ) ) ||
	( cur_run_number == 203909 && (
	  (cur_lumi_block >= 143 && cur_lumi_block <= 382) ||
	  (cur_lumi_block >= 79 && cur_lumi_block <= 113) ||
	  (cur_lumi_block >= 116 && cur_lumi_block <= 117) ||
	  (cur_lumi_block >= 120 && cur_lumi_block <= 140) ) ) ||
	( cur_run_number == 203912 && (
	  (cur_lumi_block >= 1035 && cur_lumi_block <= 1321) ||
	  (cur_lumi_block >= 701 && cur_lumi_block <= 820) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 306) ||
	  (cur_lumi_block >= 867 && cur_lumi_block <= 1033) ||
	  (cur_lumi_block >= 569 && cur_lumi_block <= 609) ||
	  (cur_lumi_block >= 308 && cur_lumi_block <= 566) ||
	  (cur_lumi_block >= 611 && cur_lumi_block <= 698) ||
	  (cur_lumi_block >= 823 && cur_lumi_block <= 865) ) ) ||
	( cur_run_number == 203987 && (
	  (cur_lumi_block >= 342 && cur_lumi_block <= 781) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 9) ||
	  (cur_lumi_block >= 12 && cur_lumi_block <= 241) ||
	  (cur_lumi_block >= 243 && cur_lumi_block <= 339) ||
	  (cur_lumi_block >= 784 && cur_lumi_block <= 1014) ) ) ||
	( cur_run_number == 203992 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 15) ) ) ||
	( cur_run_number == 203994 && (
	  (cur_lumi_block >= 306 && cur_lumi_block <= 342) ||
	  (cur_lumi_block >= 139 && cur_lumi_block <= 304) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 56) ||
	  (cur_lumi_block >= 344 && cur_lumi_block <= 425) ||
	  (cur_lumi_block >= 59 && cur_lumi_block <= 136) ) ) ||
	( cur_run_number == 204100 && (
	  (cur_lumi_block >= 117 && cur_lumi_block <= 139) ) ) ||
	( cur_run_number == 204101 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 74) ) ) ||
	( cur_run_number == 204113 && (
	  (cur_lumi_block >= 129 && cur_lumi_block <= 191) ||
	  (cur_lumi_block >= 585 && cur_lumi_block <= 690) ||
	  (cur_lumi_block >= 402 && cur_lumi_block <= 583) ||
	  (cur_lumi_block >= 329 && cur_lumi_block <= 388) ||
	  (cur_lumi_block >= 105 && cur_lumi_block <= 127) ||
	  (cur_lumi_block >= 82 && cur_lumi_block <= 96) ||
	  (cur_lumi_block >= 261 && cur_lumi_block <= 327) ||
	  (cur_lumi_block >= 194 && cur_lumi_block <= 258) ||
	  (cur_lumi_block >= 390 && cur_lumi_block <= 400) ||
	  (cur_lumi_block >= 98 && cur_lumi_block <= 102) ) ) ||
	( cur_run_number == 204114 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 358) ) ) ||
	( cur_run_number == 204238 && (
	  (cur_lumi_block >= 23 && cur_lumi_block <= 52) ||
	  (cur_lumi_block >= 55 && cur_lumi_block <= 55) ) ) ||
	( cur_run_number == 204250 && (
	  (cur_lumi_block >= 524 && cur_lumi_block <= 543) ||
	  (cur_lumi_block >= 546 && cur_lumi_block <= 682) ||
	  (cur_lumi_block >= 287 && cur_lumi_block <= 336) ||
	  (cur_lumi_block >= 121 && cur_lumi_block <= 177) ||
	  (cur_lumi_block >= 179 && cur_lumi_block <= 285) ||
	  (cur_lumi_block >= 403 && cur_lumi_block <= 521) ||
	  (cur_lumi_block >= 339 && cur_lumi_block <= 400) ||
	  (cur_lumi_block >= 684 && cur_lumi_block <= 801) ||
	  (cur_lumi_block >= 92 && cur_lumi_block <= 118) ) ) ||
	( cur_run_number == 204511 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 56) ) ) ||
	( cur_run_number == 204541 && (
	  (cur_lumi_block >= 142 && cur_lumi_block <= 149) ||
	  (cur_lumi_block >= 42 && cur_lumi_block <= 42) ||
	  (cur_lumi_block >= 44 && cur_lumi_block <= 139) ||
	  (cur_lumi_block >= 151 && cur_lumi_block <= 204) ||
	  (cur_lumi_block >= 5 && cur_lumi_block <= 39) ) ) ||
	( cur_run_number == 204544 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 11) ||
	  (cur_lumi_block >= 13 && cur_lumi_block <= 93) ||
	  (cur_lumi_block >= 96 && cur_lumi_block <= 195) ||
	  (cur_lumi_block >= 197 && cur_lumi_block <= 224) ||
	  (cur_lumi_block >= 226 && cur_lumi_block <= 334) ||
	  (cur_lumi_block >= 337 && cur_lumi_block <= 426) ) ) ||
	( cur_run_number == 204552 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 9) ) ) ||
	( cur_run_number == 204553 && (
	  (cur_lumi_block >= 63 && cur_lumi_block <= 101) ||
	  (cur_lumi_block >= 53 && cur_lumi_block <= 60) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 51) ) ) ||
	( cur_run_number == 204554 && (
	  (cur_lumi_block >= 7 && cur_lumi_block <= 221) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 5) ||
	  (cur_lumi_block >= 458 && cur_lumi_block <= 470) ||
	  (cur_lumi_block >= 472 && cur_lumi_block <= 481) ||
	  (cur_lumi_block >= 483 && cur_lumi_block <= 514) ||
	  (cur_lumi_block >= 224 && cur_lumi_block <= 455) ) ) ||
	( cur_run_number == 204555 && (
	  (cur_lumi_block >= 331 && cur_lumi_block <= 334) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 329) ) ) ||
	( cur_run_number == 204563 && (
	  (cur_lumi_block >= 102 && cur_lumi_block <= 178) ||
	  (cur_lumi_block >= 91 && cur_lumi_block <= 99) ||
	  (cur_lumi_block >= 180 && cur_lumi_block <= 219) ||
	  (cur_lumi_block >= 366 && cur_lumi_block <= 366) ||
	  (cur_lumi_block >= 473 && cur_lumi_block <= 524) ||
	  (cur_lumi_block >= 222 && cur_lumi_block <= 229) ||
	  (cur_lumi_block >= 369 && cur_lumi_block <= 470) ||
	  (cur_lumi_block >= 231 && cur_lumi_block <= 364) ||
	  (cur_lumi_block >= 527 && cur_lumi_block <= 571) ) ) ||
	( cur_run_number == 204564 && (
	  (cur_lumi_block >= 1209 && cur_lumi_block <= 1248) ||
	  (cur_lumi_block >= 193 && cur_lumi_block <= 293) ||
	  (cur_lumi_block >= 87 && cur_lumi_block <= 89) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 84) ||
	  (cur_lumi_block >= 737 && cur_lumi_block <= 855) ||
	  (cur_lumi_block >= 190 && cur_lumi_block <= 191) ||
	  (cur_lumi_block >= 161 && cur_lumi_block <= 187) ||
	  (cur_lumi_block >= 437 && cur_lumi_block <= 735) ||
	  (cur_lumi_block >= 1251 && cur_lumi_block <= 1284) ||
	  (cur_lumi_block >= 429 && cur_lumi_block <= 434) ||
	  (cur_lumi_block >= 92 && cur_lumi_block <= 159) ||
	  (cur_lumi_block >= 317 && cur_lumi_block <= 340) ||
	  (cur_lumi_block >= 296 && cur_lumi_block <= 315) ||
	  (cur_lumi_block >= 858 && cur_lumi_block <= 1206) ||
	  (cur_lumi_block >= 343 && cur_lumi_block <= 427) ) ) ||
	( cur_run_number == 204565 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 48) ) ) ||
	( cur_run_number == 204566 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 12) ) ) ||
	( cur_run_number == 204567 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 38) ) ) ||
	( cur_run_number == 204576 && (
	  (cur_lumi_block >= 49 && cur_lumi_block <= 192) ||
	  (cur_lumi_block >= 195 && cur_lumi_block <= 301) ) ) ||
	( cur_run_number == 204577 && (
	  (cur_lumi_block >= 656 && cur_lumi_block <= 669) ||
	  (cur_lumi_block >= 67 && cur_lumi_block <= 105) ||
	  (cur_lumi_block >= 1059 && cur_lumi_block <= 1115) ||
	  (cur_lumi_block >= 107 && cur_lumi_block <= 170) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 46) ||
	  (cur_lumi_block >= 742 && cur_lumi_block <= 913) ||
	  (cur_lumi_block >= 183 && cur_lumi_block <= 193) ||
	  (cur_lumi_block >= 1117 && cur_lumi_block <= 1282) ||
	  (cur_lumi_block >= 915 && cur_lumi_block <= 1057) ||
	  (cur_lumi_block >= 173 && cur_lumi_block <= 181) ||
	  (cur_lumi_block >= 49 && cur_lumi_block <= 64) ||
	  (cur_lumi_block >= 196 && cur_lumi_block <= 653) ||
	  (cur_lumi_block >= 671 && cur_lumi_block <= 740) ) ) ||
	( cur_run_number == 204599 && (
	  (cur_lumi_block >= 124 && cur_lumi_block <= 125) ||
	  (cur_lumi_block >= 266 && cur_lumi_block <= 292) ||
	  (cur_lumi_block >= 294 && cur_lumi_block <= 334) ||
	  (cur_lumi_block >= 128 && cur_lumi_block <= 173) ||
	  (cur_lumi_block >= 85 && cur_lumi_block <= 94) ||
	  (cur_lumi_block >= 248 && cur_lumi_block <= 264) ||
	  (cur_lumi_block >= 97 && cur_lumi_block <= 121) ||
	  (cur_lumi_block >= 243 && cur_lumi_block <= 245) ||
	  (cur_lumi_block >= 73 && cur_lumi_block <= 83) ||
	  (cur_lumi_block >= 175 && cur_lumi_block <= 240) ) ) ||
	( cur_run_number == 204601 && (
	  (cur_lumi_block >= 593 && cur_lumi_block <= 652) ||
	  (cur_lumi_block >= 565 && cur_lumi_block <= 591) ||
	  (cur_lumi_block >= 814 && cur_lumi_block <= 892) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 25) ||
	  (cur_lumi_block >= 292 && cur_lumi_block <= 563) ||
	  (cur_lumi_block >= 1105 && cur_lumi_block <= 1161) ||
	  (cur_lumi_block >= 1006 && cur_lumi_block <= 1038) ||
	  (cur_lumi_block >= 65 && cur_lumi_block <= 80) ||
	  (cur_lumi_block >= 1040 && cur_lumi_block <= 1088) ||
	  (cur_lumi_block >= 986 && cur_lumi_block <= 1003) ||
	  (cur_lumi_block >= 28 && cur_lumi_block <= 62) ||
	  (cur_lumi_block >= 83 && cur_lumi_block <= 89) ||
	  (cur_lumi_block >= 783 && cur_lumi_block <= 812) ||
	  (cur_lumi_block >= 655 && cur_lumi_block <= 780) ||
	  (cur_lumi_block >= 92 && cur_lumi_block <= 290) ||
	  (cur_lumi_block >= 894 && cur_lumi_block <= 984) ||
	  (cur_lumi_block >= 1091 && cur_lumi_block <= 1102) ||
	  (cur_lumi_block >= 1164 && cur_lumi_block <= 1250) ) ) ||
	( cur_run_number == 205086 && (
	  (cur_lumi_block >= 95 && cur_lumi_block <= 149) ) ) ||
	( cur_run_number == 205111 && (
	  (cur_lumi_block >= 88 && cur_lumi_block <= 390) ||
	  (cur_lumi_block >= 444 && cur_lumi_block <= 446) ||
	  (cur_lumi_block >= 392 && cur_lumi_block <= 441) ) ) ||
	( cur_run_number == 205158 && (
	  (cur_lumi_block >= 594 && cur_lumi_block <= 595) ||
	  (cur_lumi_block >= 665 && cur_lumi_block <= 667) ||
	  (cur_lumi_block >= 315 && cur_lumi_block <= 473) ||
	  (cur_lumi_block >= 672 && cur_lumi_block <= 685) ||
	  (cur_lumi_block >= 292 && cur_lumi_block <= 313) ||
	  (cur_lumi_block >= 615 && cur_lumi_block <= 663) ||
	  (cur_lumi_block >= 476 && cur_lumi_block <= 591) ||
	  (cur_lumi_block >= 597 && cur_lumi_block <= 612) ||
	  (cur_lumi_block >= 81 && cur_lumi_block <= 289) ||
	  (cur_lumi_block >= 687 && cur_lumi_block <= 733) ) ) ||
	( cur_run_number == 205193 && (
	  (cur_lumi_block >= 80 && cur_lumi_block <= 109) ||
	  (cur_lumi_block >= 352 && cur_lumi_block <= 486) ||
	  (cur_lumi_block >= 488 && cur_lumi_block <= 650) ||
	  (cur_lumi_block >= 111 && cur_lumi_block <= 349) ||
	  (cur_lumi_block >= 714 && cur_lumi_block <= 902) ||
	  (cur_lumi_block >= 652 && cur_lumi_block <= 712) ) ) ||
	( cur_run_number == 205217 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 12) ||
	  (cur_lumi_block >= 174 && cur_lumi_block <= 250) ||
	  (cur_lumi_block >= 16 && cur_lumi_block <= 111) ||
	  (cur_lumi_block >= 253 && cur_lumi_block <= 318) ||
	  (cur_lumi_block >= 113 && cur_lumi_block <= 171) ) ) ||
	( cur_run_number == 205233 && (
	  (cur_lumi_block >= 94 && cur_lumi_block <= 153) ) ) ||
	( cur_run_number == 205236 && (
	  (cur_lumi_block >= 263 && cur_lumi_block <= 331) ||
	  (cur_lumi_block >= 193 && cur_lumi_block <= 207) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 190) ||
	  (cur_lumi_block >= 334 && cur_lumi_block <= 352) ||
	  (cur_lumi_block >= 209 && cur_lumi_block <= 260) ) ) ||
	( cur_run_number == 205238 && (
	  (cur_lumi_block >= 384 && cur_lumi_block <= 596) ||
	  (cur_lumi_block >= 306 && cur_lumi_block <= 355) ||
	  (cur_lumi_block >= 358 && cur_lumi_block <= 381) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 6) ||
	  (cur_lumi_block >= 598 && cur_lumi_block <= 617) ||
	  (cur_lumi_block >= 9 && cur_lumi_block <= 199) ||
	  (cur_lumi_block >= 202 && cur_lumi_block <= 254) ||
	  (cur_lumi_block >= 256 && cur_lumi_block <= 304) ) ) ||
	( cur_run_number == 205303 && (
	  (cur_lumi_block >= 35 && cur_lumi_block <= 54) ||
	  (cur_lumi_block >= 90 && cur_lumi_block <= 132) ||
	  (cur_lumi_block >= 135 && cur_lumi_block <= 144) ) ) ||
	( cur_run_number == 205310 && (
	  (cur_lumi_block >= 309 && cur_lumi_block <= 313) ||
	  (cur_lumi_block >= 460 && cur_lumi_block <= 559) ||
	  (cur_lumi_block >= 324 && cur_lumi_block <= 457) ||
	  (cur_lumi_block >= 316 && cur_lumi_block <= 316) ||
	  (cur_lumi_block >= 319 && cur_lumi_block <= 321) ||
	  (cur_lumi_block >= 76 && cur_lumi_block <= 306) ) ) ||
	( cur_run_number == 205311 && (
	  (cur_lumi_block >= 397 && cur_lumi_block <= 592) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 85) ||
	  (cur_lumi_block >= 88 && cur_lumi_block <= 92) ||
	  (cur_lumi_block >= 186 && cur_lumi_block <= 395) ||
	  (cur_lumi_block >= 913 && cur_lumi_block <= 1260) ||
	  (cur_lumi_block >= 595 && cur_lumi_block <= 910) ||
	  (cur_lumi_block >= 95 && cur_lumi_block <= 183) ) ) ||
	( cur_run_number == 205339 && (
	  (cur_lumi_block >= 71 && cur_lumi_block <= 175) ||
	  (cur_lumi_block >= 233 && cur_lumi_block <= 262) ||
	  (cur_lumi_block >= 265 && cur_lumi_block <= 404) ||
	  (cur_lumi_block >= 216 && cur_lumi_block <= 230) ||
	  (cur_lumi_block >= 178 && cur_lumi_block <= 213) ) ) ||
	( cur_run_number == 205344 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 83) ||
	  (cur_lumi_block >= 106 && cur_lumi_block <= 359) ||
	  (cur_lumi_block >= 433 && cur_lumi_block <= 949) ||
	  (cur_lumi_block >= 1348 && cur_lumi_block <= 1586) ||
	  (cur_lumi_block >= 951 && cur_lumi_block <= 967) ||
	  (cur_lumi_block >= 969 && cur_lumi_block <= 1127) ||
	  (cur_lumi_block >= 362 && cur_lumi_block <= 431) ||
	  (cur_lumi_block >= 1129 && cur_lumi_block <= 1346) ||
	  (cur_lumi_block >= 86 && cur_lumi_block <= 104) ) ) ||
	( cur_run_number == 205515 && (
	  (cur_lumi_block >= 82 && cur_lumi_block <= 201) ||
	  (cur_lumi_block >= 203 && cur_lumi_block <= 216) ) ) ||
	( cur_run_number == 205519 && (
	  (cur_lumi_block >= 370 && cur_lumi_block <= 386) ||
	  (cur_lumi_block >= 389 && cur_lumi_block <= 472) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 47) ||
	  (cur_lumi_block >= 50 && cur_lumi_block <= 172) ||
	  (cur_lumi_block >= 175 && cur_lumi_block <= 367) ) ) ||
	( cur_run_number == 205526 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 269) ||
	  (cur_lumi_block >= 272 && cur_lumi_block <= 277) ||
	  (cur_lumi_block >= 280 && cur_lumi_block <= 332) ) ) ||
	( cur_run_number == 205614 && (
	  (cur_lumi_block >= 7 && cur_lumi_block <= 40) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 4) ) ) ||
	( cur_run_number == 205617 && (
	  (cur_lumi_block >= 32 && cur_lumi_block <= 102) ||
	  (cur_lumi_block >= 266 && cur_lumi_block <= 448) ||
	  (cur_lumi_block >= 143 && cur_lumi_block <= 264) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 29) ||
	  (cur_lumi_block >= 125 && cur_lumi_block <= 140) ||
	  (cur_lumi_block >= 105 && cur_lumi_block <= 123) ||
	  (cur_lumi_block >= 534 && cur_lumi_block <= 547) ||
	  (cur_lumi_block >= 451 && cur_lumi_block <= 532) ) ) ||
	( cur_run_number == 205618 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 12) ) ) ||
	( cur_run_number == 205620 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 175) ) ) ||
	( cur_run_number == 205666 && (
	  (cur_lumi_block >= 122 && cur_lumi_block <= 165) ||
	  (cur_lumi_block >= 580 && cur_lumi_block <= 594) ||
	  (cur_lumi_block >= 325 && cur_lumi_block <= 578) ||
	  (cur_lumi_block >= 261 && cur_lumi_block <= 322) ||
	  (cur_lumi_block >= 597 && cur_lumi_block <= 721) ||
	  (cur_lumi_block >= 168 && cur_lumi_block <= 259) ||
	  (cur_lumi_block >= 60 && cur_lumi_block <= 119) ||
	  (cur_lumi_block >= 724 && cur_lumi_block <= 739) ) ) ||
	( cur_run_number == 205667 && (
	  (cur_lumi_block >= 777 && cur_lumi_block <= 1109) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 165) ||
	  (cur_lumi_block >= 692 && cur_lumi_block <= 751) ||
	  (cur_lumi_block >= 754 && cur_lumi_block <= 774) ||
	  (cur_lumi_block >= 168 && cur_lumi_block <= 282) ||
	  (cur_lumi_block >= 321 && cur_lumi_block <= 412) ||
	  (cur_lumi_block >= 415 && cur_lumi_block <= 689) ||
	  (cur_lumi_block >= 285 && cur_lumi_block <= 318) ) ) ||
	( cur_run_number == 205683 && (
	  (cur_lumi_block >= 181 && cur_lumi_block <= 198) ||
	  (cur_lumi_block >= 85 && cur_lumi_block <= 178) ||
	  (cur_lumi_block >= 201 && cur_lumi_block <= 305) ||
	  (cur_lumi_block >= 76 && cur_lumi_block <= 82) ) ) ||
	( cur_run_number == 205690 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 40) ) ) ||
	( cur_run_number == 205694 && (
	  (cur_lumi_block >= 455 && cur_lumi_block <= 593) ||
	  (cur_lumi_block >= 208 && cur_lumi_block <= 230) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 205) ||
	  (cur_lumi_block >= 595 && cur_lumi_block <= 890) ||
	  (cur_lumi_block >= 350 && cur_lumi_block <= 452) ||
	  (cur_lumi_block >= 233 && cur_lumi_block <= 347) ) ) ||
	( cur_run_number == 205718 && (
	  (cur_lumi_block >= 341 && cur_lumi_block <= 361) ||
	  (cur_lumi_block >= 78 && cur_lumi_block <= 97) ||
	  (cur_lumi_block >= 363 && cur_lumi_block <= 524) ||
	  (cur_lumi_block >= 105 && cur_lumi_block <= 176) ||
	  (cur_lumi_block >= 100 && cur_lumi_block <= 103) ||
	  (cur_lumi_block >= 534 && cur_lumi_block <= 589) ||
	  (cur_lumi_block >= 591 && cur_lumi_block <= 694) ||
	  (cur_lumi_block >= 527 && cur_lumi_block <= 531) ||
	  (cur_lumi_block >= 49 && cur_lumi_block <= 75) ||
	  (cur_lumi_block >= 178 && cur_lumi_block <= 338) ) ) ||
	( cur_run_number == 205774 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 80) ) ) ||
	( cur_run_number == 205777 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 8) ) ) ||
	( cur_run_number == 205781 && (
	  (cur_lumi_block >= 91 && cur_lumi_block <= 197) ||
	  (cur_lumi_block >= 200 && cur_lumi_block <= 502) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 89) ) ) ||
	( cur_run_number == 205826 && (
	  (cur_lumi_block >= 306 && cur_lumi_block <= 468) ||
	  (cur_lumi_block >= 80 && cur_lumi_block <= 232) ||
	  (cur_lumi_block >= 235 && cur_lumi_block <= 303) ) ) ||
	( cur_run_number == 205833 && (
	  (cur_lumi_block >= 167 && cur_lumi_block <= 173) ||
	  (cur_lumi_block >= 315 && cur_lumi_block <= 346) ||
	  (cur_lumi_block >= 360 && cur_lumi_block <= 366) ||
	  (cur_lumi_block >= 157 && cur_lumi_block <= 165) ||
	  (cur_lumi_block >= 350 && cur_lumi_block <= 355) ||
	  (cur_lumi_block >= 123 && cur_lumi_block <= 155) ||
	  (cur_lumi_block >= 84 && cur_lumi_block <= 86) ||
	  (cur_lumi_block >= 221 && cur_lumi_block <= 267) ||
	  (cur_lumi_block >= 176 && cur_lumi_block <= 219) ||
	  (cur_lumi_block >= 89 && cur_lumi_block <= 121) ||
	  (cur_lumi_block >= 270 && cur_lumi_block <= 312) ) ) ||
	( cur_run_number == 205834 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 12) ||
	  (cur_lumi_block >= 14 && cur_lumi_block <= 195) ) ) ||
	( cur_run_number == 205908 && (
	  (cur_lumi_block >= 68 && cur_lumi_block <= 200) ||
	  (cur_lumi_block >= 202 && cur_lumi_block <= 209) ) ) ||
	( cur_run_number == 205921 && (
	  (cur_lumi_block >= 271 && cur_lumi_block <= 394) ||
	  (cur_lumi_block >= 22 && cur_lumi_block <= 73) ||
	  (cur_lumi_block >= 397 && cur_lumi_block <= 401) ||
	  (cur_lumi_block >= 500 && cur_lumi_block <= 571) ||
	  (cur_lumi_block >= 410 && cur_lumi_block <= 428) ||
	  (cur_lumi_block >= 782 && cur_lumi_block <= 853) ||
	  (cur_lumi_block >= 431 && cur_lumi_block <= 498) ||
	  (cur_lumi_block >= 76 && cur_lumi_block <= 268) ||
	  (cur_lumi_block >= 574 && cur_lumi_block <= 779) ) ) ||
	( cur_run_number == 206066 && (
	  (cur_lumi_block >= 89 && cur_lumi_block <= 146) ) ) ||
	( cur_run_number == 206088 && (
	  (cur_lumi_block >= 181 && cur_lumi_block <= 199) ||
	  (cur_lumi_block >= 161 && cur_lumi_block <= 178) ||
	  (cur_lumi_block >= 202 && cur_lumi_block <= 286) ||
	  (cur_lumi_block >= 86 && cur_lumi_block <= 159) ) ) ||
	( cur_run_number == 206102 && (
	  (cur_lumi_block >= 281 && cur_lumi_block <= 349) ||
	  (cur_lumi_block >= 133 && cur_lumi_block <= 208) ||
	  (cur_lumi_block >= 238 && cur_lumi_block <= 246) ||
	  (cur_lumi_block >= 120 && cur_lumi_block <= 130) ||
	  (cur_lumi_block >= 83 && cur_lumi_block <= 116) ||
	  (cur_lumi_block >= 249 && cur_lumi_block <= 278) ||
	  (cur_lumi_block >= 211 && cur_lumi_block <= 235) ) ) ||
	( cur_run_number == 206187 && (
	  (cur_lumi_block >= 571 && cur_lumi_block <= 647) ||
	  (cur_lumi_block >= 245 && cur_lumi_block <= 288) ||
	  (cur_lumi_block >= 489 && cur_lumi_block <= 569) ||
	  (cur_lumi_block >= 107 && cur_lumi_block <= 169) ||
	  (cur_lumi_block >= 290 && cur_lumi_block <= 340) ||
	  (cur_lumi_block >= 437 && cur_lumi_block <= 486) ||
	  (cur_lumi_block >= 429 && cur_lumi_block <= 435) ||
	  (cur_lumi_block >= 649 && cur_lumi_block <= 662) ||
	  (cur_lumi_block >= 664 && cur_lumi_block <= 708) ||
	  (cur_lumi_block >= 172 && cur_lumi_block <= 242) ||
	  (cur_lumi_block >= 343 && cur_lumi_block <= 427) ) ) ||
	( cur_run_number == 206188 && (
	  (cur_lumi_block >= 42 && cur_lumi_block <= 55) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 40) ) ) ||
	( cur_run_number == 206199 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 75) ||
	  (cur_lumi_block >= 77 && cur_lumi_block <= 82) ||
	  (cur_lumi_block >= 85 && cur_lumi_block <= 114) ) ) ||
	( cur_run_number == 206207 && (
	  (cur_lumi_block >= 179 && cur_lumi_block <= 194) ||
	  (cur_lumi_block >= 422 && cur_lumi_block <= 447) ||
	  (cur_lumi_block >= 82 && cur_lumi_block <= 130) ||
	  (cur_lumi_block >= 572 && cur_lumi_block <= 690) ||
	  (cur_lumi_block >= 390 && cur_lumi_block <= 419) ||
	  (cur_lumi_block >= 450 && cur_lumi_block <= 569) ||
	  (cur_lumi_block >= 132 && cur_lumi_block <= 176) ||
	  (cur_lumi_block >= 196 && cur_lumi_block <= 388) ) ) ||
	( cur_run_number == 206208 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 470) ||
	  (cur_lumi_block >= 472 && cur_lumi_block <= 518) ) ) ||
	( cur_run_number == 206210 && (
	  (cur_lumi_block >= 11 && cur_lumi_block <= 25) ||
	  (cur_lumi_block >= 386 && cur_lumi_block <= 466) ||
	  (cur_lumi_block >= 300 && cur_lumi_block <= 383) ||
	  (cur_lumi_block >= 28 && cur_lumi_block <= 275) ||
	  (cur_lumi_block >= 277 && cur_lumi_block <= 298) ) ) ||
	( cur_run_number == 206243 && (
	  (cur_lumi_block >= 726 && cur_lumi_block <= 905) ||
	  (cur_lumi_block >= 435 && cur_lumi_block <= 448) ||
	  (cur_lumi_block >= 199 && cur_lumi_block <= 354) ||
	  (cur_lumi_block >= 451 && cur_lumi_block <= 533) ||
	  (cur_lumi_block >= 172 && cur_lumi_block <= 196) ||
	  (cur_lumi_block >= 536 && cur_lumi_block <= 554) ||
	  (cur_lumi_block >= 357 && cur_lumi_block <= 433) ||
	  (cur_lumi_block >= 62 && cur_lumi_block <= 169) ||
	  (cur_lumi_block >= 557 && cur_lumi_block <= 723) ) ) ||
	( cur_run_number == 206245 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 62) ) ) ||
	( cur_run_number == 206246 && (
	  (cur_lumi_block >= 412 && cur_lumi_block <= 676) ||
	  (cur_lumi_block >= 288 && cur_lumi_block <= 407) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 14) ||
	  (cur_lumi_block >= 16 && cur_lumi_block <= 237) ||
	  (cur_lumi_block >= 965 && cur_lumi_block <= 997) ||
	  (cur_lumi_block >= 787 && cur_lumi_block <= 962) ||
	  (cur_lumi_block >= 240 && cur_lumi_block <= 285) ||
	  (cur_lumi_block >= 706 && cur_lumi_block <= 785) ||
	  (cur_lumi_block >= 1201 && cur_lumi_block <= 1290) ||
	  (cur_lumi_block >= 1000 && cur_lumi_block <= 1198) ||
	  (cur_lumi_block >= 678 && cur_lumi_block <= 704) ) ) ||
	( cur_run_number == 206257 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 29) ) ) ||
	( cur_run_number == 206258 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 36) ||
	  (cur_lumi_block >= 39 && cur_lumi_block <= 223) ||
	  (cur_lumi_block >= 226 && cur_lumi_block <= 249) ) ) ||
	( cur_run_number == 206302 && (
	  (cur_lumi_block >= 189 && cur_lumi_block <= 229) ||
	  (cur_lumi_block >= 11 && cur_lumi_block <= 33) ||
	  (cur_lumi_block >= 234 && cur_lumi_block <= 241) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 8) ||
	  (cur_lumi_block >= 110 && cur_lumi_block <= 149) ||
	  (cur_lumi_block >= 84 && cur_lumi_block <= 108) ||
	  (cur_lumi_block >= 36 && cur_lumi_block <= 44) ||
	  (cur_lumi_block >= 47 && cur_lumi_block <= 82) ||
	  (cur_lumi_block >= 243 && cur_lumi_block <= 276) ||
	  (cur_lumi_block >= 231 && cur_lumi_block <= 232) ||
	  (cur_lumi_block >= 151 && cur_lumi_block <= 186) ) ) ||
	( cur_run_number == 206303 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 19) ||
	  (cur_lumi_block >= 23 && cur_lumi_block <= 286) ) ) ||
	( cur_run_number == 206304 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 4) ||
	  (cur_lumi_block >= 6 && cur_lumi_block <= 62) ) ) ||
	( cur_run_number == 206331 && (
	  (cur_lumi_block >= 225 && cur_lumi_block <= 312) ||
	  (cur_lumi_block >= 91 && cur_lumi_block <= 222) ) ) ||
	( cur_run_number == 206389 && (
	  (cur_lumi_block >= 88 && cur_lumi_block <= 185) ||
	  (cur_lumi_block >= 275 && cur_lumi_block <= 392) ||
	  (cur_lumi_block >= 252 && cur_lumi_block <= 272) ||
	  (cur_lumi_block >= 187 && cur_lumi_block <= 249) ) ) ||
	( cur_run_number == 206391 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 55) ||
	  (cur_lumi_block >= 57 && cur_lumi_block <= 91) ) ) ||
	( cur_run_number == 206401 && (
	  (cur_lumi_block >= 267 && cur_lumi_block <= 409) ||
	  (cur_lumi_block >= 212 && cur_lumi_block <= 249) ||
	  (cur_lumi_block >= 197 && cur_lumi_block <= 210) ||
	  (cur_lumi_block >= 251 && cur_lumi_block <= 265) ||
	  (cur_lumi_block >= 92 && cur_lumi_block <= 194) ||
	  (cur_lumi_block >= 69 && cur_lumi_block <= 90) ) ) ||
	( cur_run_number == 206446 && (
	  (cur_lumi_block >= 208 && cur_lumi_block <= 301) ||
	  (cur_lumi_block >= 143 && cur_lumi_block <= 159) ||
	  (cur_lumi_block >= 445 && cur_lumi_block <= 445) ||
	  (cur_lumi_block >= 912 && cur_lumi_block <= 948) ||
	  (cur_lumi_block >= 162 && cur_lumi_block <= 205) ||
	  (cur_lumi_block >= 950 && cur_lumi_block <= 989) ||
	  (cur_lumi_block >= 304 && cur_lumi_block <= 442) ||
	  (cur_lumi_block >= 992 && cur_lumi_block <= 1030) ||
	  (cur_lumi_block >= 476 && cur_lumi_block <= 616) ||
	  (cur_lumi_block >= 448 && cur_lumi_block <= 474) ||
	  (cur_lumi_block >= 92 && cur_lumi_block <= 141) ||
	  (cur_lumi_block >= 874 && cur_lumi_block <= 910) ||
	  (cur_lumi_block >= 619 && cur_lumi_block <= 872) ||
	  (cur_lumi_block >= 1109 && cur_lumi_block <= 1149) ||
	  (cur_lumi_block >= 1033 && cur_lumi_block <= 1075) ) ) ||
	( cur_run_number == 206448 && (
	  (cur_lumi_block >= 1173 && cur_lumi_block <= 1231) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 143) ||
	  (cur_lumi_block >= 1235 && cur_lumi_block <= 1237) ||
	  (cur_lumi_block >= 561 && cur_lumi_block <= 1170) ||
	  (cur_lumi_block >= 145 && cur_lumi_block <= 559) ) ) ||
	( cur_run_number == 206466 && (
	  (cur_lumi_block >= 306 && cur_lumi_block <= 405) ||
	  (cur_lumi_block >= 480 && cur_lumi_block <= 511) ||
	  (cur_lumi_block >= 407 && cur_lumi_block <= 419) ||
	  (cur_lumi_block >= 514 && cur_lumi_block <= 676) ||
	  (cur_lumi_block >= 422 && cur_lumi_block <= 477) ||
	  (cur_lumi_block >= 280 && cur_lumi_block <= 296) ||
	  (cur_lumi_block >= 299 && cur_lumi_block <= 303) ||
	  (cur_lumi_block >= 24 && cur_lumi_block <= 137) ||
	  (cur_lumi_block >= 140 && cur_lumi_block <= 277) ) ) ||
	( cur_run_number == 206476 && (
	  (cur_lumi_block >= 143 && cur_lumi_block <= 219) ||
	  (cur_lumi_block >= 133 && cur_lumi_block <= 137) ||
	  (cur_lumi_block >= 73 && cur_lumi_block <= 129) ||
	  (cur_lumi_block >= 140 && cur_lumi_block <= 141) ) ) ||
	( cur_run_number == 206477 && (
	  (cur_lumi_block >= 33 && cur_lumi_block <= 41) ||
	  (cur_lumi_block >= 53 && cur_lumi_block <= 70) ||
	  (cur_lumi_block >= 118 && cur_lumi_block <= 184) ||
	  (cur_lumi_block >= 91 && cur_lumi_block <= 94) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 14) ||
	  (cur_lumi_block >= 77 && cur_lumi_block <= 89) ||
	  (cur_lumi_block >= 16 && cur_lumi_block <= 31) ||
	  (cur_lumi_block >= 44 && cur_lumi_block <= 51) ||
	  (cur_lumi_block >= 97 && cur_lumi_block <= 115) ||
	  (cur_lumi_block >= 73 && cur_lumi_block <= 75) ) ) ||
	( cur_run_number == 206478 && (
	  (cur_lumi_block >= 139 && cur_lumi_block <= 144) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 27) ||
	  (cur_lumi_block >= 29 && cur_lumi_block <= 136) ) ) ||
	( cur_run_number == 206484 && (
	  (cur_lumi_block >= 189 && cur_lumi_block <= 384) ||
	  (cur_lumi_block >= 387 && cur_lumi_block <= 463) ||
	  (cur_lumi_block >= 554 && cur_lumi_block <= 554) ||
	  (cur_lumi_block >= 166 && cur_lumi_block <= 186) ||
	  (cur_lumi_block >= 136 && cur_lumi_block <= 163) ||
	  (cur_lumi_block >= 465 && cur_lumi_block <= 551) ||
	  (cur_lumi_block >= 98 && cur_lumi_block <= 133) ||
	  (cur_lumi_block >= 556 && cur_lumi_block <= 669) ||
	  (cur_lumi_block >= 73 && cur_lumi_block <= 95) ) ) ||
	( cur_run_number == 206512 && (
	  (cur_lumi_block >= 607 && cur_lumi_block <= 1005) ||
	  (cur_lumi_block >= 91 && cur_lumi_block <= 123) ||
	  (cur_lumi_block >= 193 && cur_lumi_block <= 201) ||
	  (cur_lumi_block >= 136 && cur_lumi_block <= 161) ||
	  (cur_lumi_block >= 214 && cur_lumi_block <= 332) ||
	  (cur_lumi_block >= 125 && cur_lumi_block <= 133) ||
	  (cur_lumi_block >= 587 && cur_lumi_block <= 604) ||
	  (cur_lumi_block >= 334 && cur_lumi_block <= 584) ||
	  (cur_lumi_block >= 203 && cur_lumi_block <= 212) ||
	  (cur_lumi_block >= 1165 && cur_lumi_block <= 1211) ||
	  (cur_lumi_block >= 1008 && cur_lumi_block <= 1123) ||
	  (cur_lumi_block >= 163 && cur_lumi_block <= 190) ||
	  (cur_lumi_block >= 1126 && cur_lumi_block <= 1163) ) ) ||
	( cur_run_number == 206513 && (
	  (cur_lumi_block >= 237 && cur_lumi_block <= 238) ||
	  (cur_lumi_block >= 241 && cur_lumi_block <= 323) ||
	  (cur_lumi_block >= 42 && cur_lumi_block <= 188) ||
	  (cur_lumi_block >= 3 && cur_lumi_block <= 39) ||
	  (cur_lumi_block >= 191 && cur_lumi_block <= 234) ) ) ||
	( cur_run_number == 206542 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 115) ||
	  (cur_lumi_block >= 514 && cur_lumi_block <= 547) ||
	  (cur_lumi_block >= 730 && cur_lumi_block <= 739) ||
	  (cur_lumi_block >= 741 && cur_lumi_block <= 833) ||
	  (cur_lumi_block >= 168 && cur_lumi_block <= 511) ||
	  (cur_lumi_block >= 606 && cur_lumi_block <= 668) ||
	  (cur_lumi_block >= 117 && cur_lumi_block <= 165) ||
	  (cur_lumi_block >= 550 && cur_lumi_block <= 603) ||
	  (cur_lumi_block >= 671 && cur_lumi_block <= 727) ) ) ||
	( cur_run_number == 206550 && (
	  (cur_lumi_block >= 77 && cur_lumi_block <= 132) ||
	  (cur_lumi_block >= 135 && cur_lumi_block <= 144) ) ) ||
	( cur_run_number == 206572 && (
	  (cur_lumi_block >= 37 && cur_lumi_block <= 47) ) ) ||
	( cur_run_number == 206573 && (
	  (cur_lumi_block >= 2 && cur_lumi_block <= 14) ) ) ||
	( cur_run_number == 206574 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 87) ) ) ||
	( cur_run_number == 206575 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 7) ||
	  (cur_lumi_block >= 12 && cur_lumi_block <= 69) ||
	  (cur_lumi_block >= 10 && cur_lumi_block <= 10) ) ) ||
	( cur_run_number == 206594 && (
	  (cur_lumi_block >= 72 && cur_lumi_block <= 107) ||
	  (cur_lumi_block >= 110 && cur_lumi_block <= 246) ||
	  (cur_lumi_block >= 249 && cur_lumi_block <= 281) ) ) ||
	( cur_run_number == 206595 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 34) ||
	  (cur_lumi_block >= 45 && cur_lumi_block <= 193) ||
	  (cur_lumi_block >= 37 && cur_lumi_block <= 42) ) ) ||
	( cur_run_number == 206596 && (
	  (cur_lumi_block >= 697 && cur_lumi_block <= 728) ||
	  (cur_lumi_block >= 239 && cur_lumi_block <= 292) ||
	  (cur_lumi_block >= 295 && cur_lumi_block <= 695) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 13) ||
	  (cur_lumi_block >= 730 && cur_lumi_block <= 810) ||
	  (cur_lumi_block >= 222 && cur_lumi_block <= 228) ||
	  (cur_lumi_block >= 15 && cur_lumi_block <= 220) ||
	  (cur_lumi_block >= 231 && cur_lumi_block <= 236) ) ) ||
	( cur_run_number == 206598 && (
	  (cur_lumi_block >= 659 && cur_lumi_block <= 719) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 81) ||
	  (cur_lumi_block >= 105 && cur_lumi_block <= 588) ||
	  (cur_lumi_block >= 83 && cur_lumi_block <= 103) ||
	  (cur_lumi_block >= 591 && cur_lumi_block <= 657) ) ) ||
	( cur_run_number == 206605 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 36) ||
	  (cur_lumi_block >= 39 && cur_lumi_block <= 78) ) ) ||
	( cur_run_number == 206744 && (
	  (cur_lumi_block >= 398 && cur_lumi_block <= 452) ||
	  (cur_lumi_block >= 160 && cur_lumi_block <= 192) ||
	  (cur_lumi_block >= 49 && cur_lumi_block <= 157) ||
	  (cur_lumi_block >= 195 && cur_lumi_block <= 395) ) ) ||
	( cur_run_number == 206745 && (
	  (cur_lumi_block >= 306 && cur_lumi_block <= 318) ||
	  (cur_lumi_block >= 1527 && cur_lumi_block <= 1862) ||
	  (cur_lumi_block >= 227 && cur_lumi_block <= 237) ||
	  (cur_lumi_block >= 723 && cur_lumi_block <= 796) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 81) ||
	  (cur_lumi_block >= 946 && cur_lumi_block <= 1106) ||
	  (cur_lumi_block >= 799 && cur_lumi_block <= 894) ||
	  (cur_lumi_block >= 240 && cur_lumi_block <= 304) ||
	  (cur_lumi_block >= 84 && cur_lumi_block <= 199) ||
	  (cur_lumi_block >= 1988 && cur_lumi_block <= 1996) ||
	  (cur_lumi_block >= 202 && cur_lumi_block <= 224) ||
	  (cur_lumi_block >= 1108 && cur_lumi_block <= 1524) ||
	  (cur_lumi_block >= 897 && cur_lumi_block <= 944) ||
	  (cur_lumi_block >= 321 && cur_lumi_block <= 720) ) ) ||
	( cur_run_number == 206859 && (
	  (cur_lumi_block >= 612 && cur_lumi_block <= 681) ||
	  (cur_lumi_block >= 771 && cur_lumi_block <= 808) ||
	  (cur_lumi_block >= 734 && cur_lumi_block <= 768) ||
	  (cur_lumi_block >= 79 && cur_lumi_block <= 210) ||
	  (cur_lumi_block >= 212 && cur_lumi_block <= 258) ||
	  (cur_lumi_block >= 830 && cur_lumi_block <= 848) ||
	  (cur_lumi_block >= 325 && cur_lumi_block <= 356) ||
	  (cur_lumi_block >= 811 && cur_lumi_block <= 827) ||
	  (cur_lumi_block >= 359 && cur_lumi_block <= 609) ||
	  (cur_lumi_block >= 684 && cur_lumi_block <= 732) ||
	  (cur_lumi_block >= 260 && cur_lumi_block <= 323) ) ) ||
	( cur_run_number == 206866 && (
	  (cur_lumi_block >= 33 && cur_lumi_block <= 113) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 30) ||
	  (cur_lumi_block >= 115 && cur_lumi_block <= 274) ) ) ||
	( cur_run_number == 206868 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 3) ||
	  (cur_lumi_block >= 10 && cur_lumi_block <= 16) ) ) ||
	( cur_run_number == 206869 && (
	  (cur_lumi_block >= 274 && cur_lumi_block <= 502) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 251) ||
	  (cur_lumi_block >= 522 && cur_lumi_block <= 566) ||
	  (cur_lumi_block >= 253 && cur_lumi_block <= 271) ||
	  (cur_lumi_block >= 568 && cur_lumi_block <= 752) ||
	  (cur_lumi_block >= 507 && cur_lumi_block <= 520) ) ) ||
	( cur_run_number == 206897 && (
	  (cur_lumi_block >= 63 && cur_lumi_block <= 102) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 34) ||
	  (cur_lumi_block >= 133 && cur_lumi_block <= 137) ||
	  (cur_lumi_block >= 114 && cur_lumi_block <= 131) ||
	  (cur_lumi_block >= 111 && cur_lumi_block <= 112) ||
	  (cur_lumi_block >= 109 && cur_lumi_block <= 109) ||
	  (cur_lumi_block >= 38 && cur_lumi_block <= 61) ) ) ||
	( cur_run_number == 206901 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 98) ) ) ||
	( cur_run_number == 206906 && (
	  (cur_lumi_block >= 142 && cur_lumi_block <= 149) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 31) ||
	  (cur_lumi_block >= 96 && cur_lumi_block <= 136) ||
	  (cur_lumi_block >= 138 && cur_lumi_block <= 139) ||
	  (cur_lumi_block >= 38 && cur_lumi_block <= 94) ||
	  (cur_lumi_block >= 177 && cur_lumi_block <= 206) ||
	  (cur_lumi_block >= 151 && cur_lumi_block <= 175) ) ) ||
	( cur_run_number == 206940 && (
	  (cur_lumi_block >= 384 && cur_lumi_block <= 712) ||
	  (cur_lumi_block >= 155 && cur_lumi_block <= 298) ||
	  (cur_lumi_block >= 715 && cur_lumi_block <= 803) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 151) ||
	  (cur_lumi_block >= 963 && cur_lumi_block <= 1027) ||
	  (cur_lumi_block >= 301 && cur_lumi_block <= 382) ||
	  (cur_lumi_block >= 805 && cur_lumi_block <= 960) ||
	  (cur_lumi_block >= 153 && cur_lumi_block <= 153) ) ) ||
	( cur_run_number == 207099 && (
	  (cur_lumi_block >= 605 && cur_lumi_block <= 755) ||
	  (cur_lumi_block >= 370 && cur_lumi_block <= 481) ||
	  (cur_lumi_block >= 757 && cur_lumi_block <= 1046) ||
	  (cur_lumi_block >= 333 && cur_lumi_block <= 367) ||
	  (cur_lumi_block >= 83 && cur_lumi_block <= 134) ||
	  (cur_lumi_block >= 484 && cur_lumi_block <= 602) ||
	  (cur_lumi_block >= 323 && cur_lumi_block <= 330) ||
	  (cur_lumi_block >= 316 && cur_lumi_block <= 320) ||
	  (cur_lumi_block >= 137 && cur_lumi_block <= 172) ||
	  (cur_lumi_block >= 1048 && cur_lumi_block <= 1171) ||
	  (cur_lumi_block >= 216 && cur_lumi_block <= 314) ||
	  (cur_lumi_block >= 175 && cur_lumi_block <= 213) ) ) ||
	( cur_run_number == 207100 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 91) ||
	  (cur_lumi_block >= 94 && cur_lumi_block <= 94) ) ) ||
	( cur_run_number == 207214 && (
	  (cur_lumi_block >= 718 && cur_lumi_block <= 757) ||
	  (cur_lumi_block >= 643 && cur_lumi_block <= 708) ||
	  (cur_lumi_block >= 759 && cur_lumi_block <= 808) ||
	  (cur_lumi_block >= 179 && cur_lumi_block <= 181) ||
	  (cur_lumi_block >= 811 && cur_lumi_block <= 829) ||
	  (cur_lumi_block >= 57 && cur_lumi_block <= 112) ||
	  (cur_lumi_block >= 199 && cur_lumi_block <= 220) ||
	  (cur_lumi_block >= 114 && cur_lumi_block <= 177) ||
	  (cur_lumi_block >= 184 && cur_lumi_block <= 196) ||
	  (cur_lumi_block >= 408 && cur_lumi_block <= 482) ||
	  (cur_lumi_block >= 265 && cur_lumi_block <= 405) ||
	  (cur_lumi_block >= 485 && cur_lumi_block <= 640) ||
	  (cur_lumi_block >= 223 && cur_lumi_block <= 262) ) ) ||
	( cur_run_number == 207217 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 32) ) ) ||
	( cur_run_number == 207219 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 112) ) ) ||
	( cur_run_number == 207220 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 160) ) ) ||
	( cur_run_number == 207221 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 102) ) ) ||
	( cur_run_number == 207222 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 17) ||
	  (cur_lumi_block >= 20 && cur_lumi_block <= 289) ) ) ||
	( cur_run_number == 207231 && (
	  (cur_lumi_block >= 693 && cur_lumi_block <= 875) ||
	  (cur_lumi_block >= 306 && cur_lumi_block <= 354) ||
	  (cur_lumi_block >= 1173 && cur_lumi_block <= 1187) ||
	  (cur_lumi_block >= 1447 && cur_lumi_block <= 1505) ||
	  (cur_lumi_block >= 70 && cur_lumi_block <= 84) ||
	  (cur_lumi_block >= 552 && cur_lumi_block <= 626) ||
	  (cur_lumi_block >= 878 && cur_lumi_block <= 1000) ||
	  (cur_lumi_block >= 628 && cur_lumi_block <= 690) ||
	  (cur_lumi_block >= 508 && cur_lumi_block <= 549) ||
	  (cur_lumi_block >= 123 && cur_lumi_block <= 184) ||
	  (cur_lumi_block >= 1418 && cur_lumi_block <= 1445) ||
	  (cur_lumi_block >= 1229 && cur_lumi_block <= 1415) ||
	  (cur_lumi_block >= 484 && cur_lumi_block <= 504) ||
	  (cur_lumi_block >= 192 && cur_lumi_block <= 303) ||
	  (cur_lumi_block >= 1003 && cur_lumi_block <= 1170) ||
	  (cur_lumi_block >= 1189 && cur_lumi_block <= 1227) ||
	  (cur_lumi_block >= 357 && cur_lumi_block <= 481) ||
	  (cur_lumi_block >= 86 && cur_lumi_block <= 121) ||
	  (cur_lumi_block >= 187 && cur_lumi_block <= 189) ) ) ||
	( cur_run_number == 207233 && (
	  (cur_lumi_block >= 121 && cur_lumi_block <= 148) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 119) ) ) ||
	( cur_run_number == 207269 && (
	  (cur_lumi_block >= 80 && cur_lumi_block <= 394) ||
	  (cur_lumi_block >= 397 && cur_lumi_block <= 436) ||
	  (cur_lumi_block >= 466 && cur_lumi_block <= 551) ||
	  (cur_lumi_block >= 568 && cur_lumi_block <= 577) ||
	  (cur_lumi_block >= 439 && cur_lumi_block <= 463) ) ) ||
	( cur_run_number == 207273 && (
	  (cur_lumi_block >= 3 && cur_lumi_block <= 877) ) ) ||
	( cur_run_number == 207279 && (
	  (cur_lumi_block >= 643 && cur_lumi_block <= 961) ||
	  (cur_lumi_block >= 554 && cur_lumi_block <= 640) ||
	  (cur_lumi_block >= 309 && cur_lumi_block <= 416) ||
	  (cur_lumi_block >= 68 && cur_lumi_block <= 138) ||
	  (cur_lumi_block >= 1098 && cur_lumi_block <= 1160) ||
	  (cur_lumi_block >= 269 && cur_lumi_block <= 307) ||
	  (cur_lumi_block >= 141 && cur_lumi_block <= 149) ||
	  (cur_lumi_block >= 963 && cur_lumi_block <= 1095) ||
	  (cur_lumi_block >= 240 && cur_lumi_block <= 266) ||
	  (cur_lumi_block >= 498 && cur_lumi_block <= 551) ||
	  (cur_lumi_block >= 151 && cur_lumi_block <= 237) ) ) ||
	( cur_run_number == 207320 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 110) ||
	  (cur_lumi_block >= 112 && cur_lumi_block <= 350) ) ) ||
	( cur_run_number == 207371 && (
	  (cur_lumi_block >= 72 && cur_lumi_block <= 117) ||
	  (cur_lumi_block >= 120 && cur_lumi_block <= 124) ) ) ||
	( cur_run_number == 207372 && (
	  (cur_lumi_block >= 480 && cur_lumi_block <= 496) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 27) ||
	  (cur_lumi_block >= 116 && cur_lumi_block <= 154) ||
	  (cur_lumi_block >= 30 && cur_lumi_block <= 113) ||
	  (cur_lumi_block >= 156 && cur_lumi_block <= 174) ||
	  (cur_lumi_block >= 176 && cur_lumi_block <= 478) ) ) ||
	( cur_run_number == 207397 && (
	  (cur_lumi_block >= 32 && cur_lumi_block <= 77) ||
	  (cur_lumi_block >= 80 && cur_lumi_block <= 140) ||
	  (cur_lumi_block >= 143 && cur_lumi_block <= 179) ) ) ||
	( cur_run_number == 207398 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 14) ||
	  (cur_lumi_block >= 16 && cur_lumi_block <= 33) ) ) ||
	( cur_run_number == 207454 && (
	  (cur_lumi_block >= 365 && cur_lumi_block <= 458) ||
	  (cur_lumi_block >= 612 && cur_lumi_block <= 632) ||
	  (cur_lumi_block >= 1081 && cur_lumi_block <= 1321) ||
	  (cur_lumi_block >= 1714 && cur_lumi_block <= 1988) ||
	  (cur_lumi_block >= 977 && cur_lumi_block <= 1064) ||
	  (cur_lumi_block >= 79 && cur_lumi_block <= 95) ||
	  (cur_lumi_block >= 635 && cur_lumi_block <= 781) ||
	  (cur_lumi_block >= 1467 && cur_lumi_block <= 1569) ||
	  (cur_lumi_block >= 461 && cur_lumi_block <= 498) ||
	  (cur_lumi_block >= 869 && cur_lumi_block <= 974) ||
	  (cur_lumi_block >= 1571 && cur_lumi_block <= 1604) ||
	  (cur_lumi_block >= 126 && cur_lumi_block <= 259) ||
	  (cur_lumi_block >= 261 && cur_lumi_block <= 363) ||
	  (cur_lumi_block >= 501 && cur_lumi_block <= 609) ||
	  (cur_lumi_block >= 98 && cur_lumi_block <= 123) ||
	  (cur_lumi_block >= 784 && cur_lumi_block <= 866) ||
	  (cur_lumi_block >= 1607 && cur_lumi_block <= 1712) ||
	  (cur_lumi_block >= 1067 && cur_lumi_block <= 1079) ||
	  (cur_lumi_block >= 1323 && cur_lumi_block <= 1464) ) ) ||
	( cur_run_number == 207469 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 31) ||
	  (cur_lumi_block >= 34 && cur_lumi_block <= 45) ) ) ||
	( cur_run_number == 207477 && (
	  (cur_lumi_block >= 565 && cur_lumi_block <= 570) ||
	  (cur_lumi_block >= 107 && cur_lumi_block <= 111) ||
	  (cur_lumi_block >= 486 && cur_lumi_block <= 494) ||
	  (cur_lumi_block >= 298 && cur_lumi_block <= 483) ||
	  (cur_lumi_block >= 497 && cur_lumi_block <= 527) ||
	  (cur_lumi_block >= 114 && cur_lumi_block <= 147) ||
	  (cur_lumi_block >= 530 && cur_lumi_block <= 563) ||
	  (cur_lumi_block >= 76 && cur_lumi_block <= 104) ||
	  (cur_lumi_block >= 150 && cur_lumi_block <= 295) ) ) ||
	( cur_run_number == 207487 && (
	  (cur_lumi_block >= 363 && cur_lumi_block <= 468) ||
	  (cur_lumi_block >= 50 && cur_lumi_block <= 98) ||
	  (cur_lumi_block >= 471 && cur_lumi_block <= 472) ||
	  (cur_lumi_block >= 313 && cur_lumi_block <= 359) ||
	  (cur_lumi_block >= 101 && cur_lumi_block <= 311) ) ) ||
	( cur_run_number == 207488 && (
	  (cur_lumi_block >= 200 && cur_lumi_block <= 250) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 63) ||
	  (cur_lumi_block >= 116 && cur_lumi_block <= 198) ||
	  (cur_lumi_block >= 252 && cur_lumi_block <= 288) ||
	  (cur_lumi_block >= 291 && cur_lumi_block <= 365) ||
	  (cur_lumi_block >= 95 && cur_lumi_block <= 113) ||
	  (cur_lumi_block >= 368 && cur_lumi_block <= 377) ||
	  (cur_lumi_block >= 379 && cur_lumi_block <= 440) ||
	  (cur_lumi_block >= 66 && cur_lumi_block <= 92) ) ) ||
	( cur_run_number == 207490 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 48) ||
	  (cur_lumi_block >= 51 && cur_lumi_block <= 111) ) ) ||
	( cur_run_number == 207491 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 176) ||
	  (cur_lumi_block >= 179 && cur_lumi_block <= 458) ) ) ||
	( cur_run_number == 207492 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 20) ||
	  (cur_lumi_block >= 23 && cur_lumi_block <= 298) ) ) ||
	( cur_run_number == 207515 && (
	  (cur_lumi_block >= 1055 && cur_lumi_block <= 1143) ||
	  (cur_lumi_block >= 322 && cur_lumi_block <= 381) ||
	  (cur_lumi_block >= 79 && cur_lumi_block <= 109) ||
	  (cur_lumi_block >= 851 && cur_lumi_block <= 954) ||
	  (cur_lumi_block >= 383 && cur_lumi_block <= 498) ||
	  (cur_lumi_block >= 500 && cur_lumi_block <= 730) ||
	  (cur_lumi_block >= 733 && cur_lumi_block <= 849) ||
	  (cur_lumi_block >= 997 && cur_lumi_block <= 1052) ||
	  (cur_lumi_block >= 957 && cur_lumi_block <= 994) ||
	  (cur_lumi_block >= 228 && cur_lumi_block <= 320) ||
	  (cur_lumi_block >= 134 && cur_lumi_block <= 208) ||
	  (cur_lumi_block >= 211 && cur_lumi_block <= 225) ||
	  (cur_lumi_block >= 112 && cur_lumi_block <= 132) ||
	  (cur_lumi_block >= 1145 && cur_lumi_block <= 1211) ) ) ||
	( cur_run_number == 207517 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 12) ||
	  (cur_lumi_block >= 15 && cur_lumi_block <= 57) ) ) ||
	( cur_run_number == 207518 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 59) ||
	  (cur_lumi_block >= 61 && cur_lumi_block <= 83) ) ) ||
	( cur_run_number == 207882 && (
	  (cur_lumi_block >= 22 && cur_lumi_block <= 45) ) ) ||
	( cur_run_number == 207883 && (
	  (cur_lumi_block >= 7 && cur_lumi_block <= 75) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 1) ||
	  (cur_lumi_block >= 3 && cur_lumi_block <= 4) ) ) ||
	( cur_run_number == 207884 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 106) ||
	  (cur_lumi_block >= 108 && cur_lumi_block <= 183) ) ) ||
	( cur_run_number == 207885 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 90) ) ) ||
	( cur_run_number == 207886 && (
	  (cur_lumi_block >= 32 && cur_lumi_block <= 90) ||
	  (cur_lumi_block >= 158 && cur_lumi_block <= 166) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 30) ||
	  (cur_lumi_block >= 168 && cur_lumi_block <= 171) ||
	  (cur_lumi_block >= 92 && cur_lumi_block <= 156) ) ) ||
	( cur_run_number == 207889 && (
	  (cur_lumi_block >= 910 && cur_lumi_block <= 945) ||
	  (cur_lumi_block >= 306 && cur_lumi_block <= 442) ||
	  (cur_lumi_block >= 445 && cur_lumi_block <= 445) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 43) ||
	  (cur_lumi_block >= 733 && cur_lumi_block <= 907) ||
	  (cur_lumi_block >= 553 && cur_lumi_block <= 731) ||
	  (cur_lumi_block >= 47 && cur_lumi_block <= 57) ||
	  (cur_lumi_block >= 447 && cur_lumi_block <= 551) ||
	  (cur_lumi_block >= 60 && cur_lumi_block <= 303) ) ) ||
	( cur_run_number == 207898 && (
	  (cur_lumi_block >= 239 && cur_lumi_block <= 257) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 33) ||
	  (cur_lumi_block >= 36 && cur_lumi_block <= 57) ||
	  (cur_lumi_block >= 60 && cur_lumi_block <= 235) ||
	  (cur_lumi_block >= 260 && cur_lumi_block <= 277) ) ) ||
	( cur_run_number == 207905 && (
	  (cur_lumi_block >= 1183 && cur_lumi_block <= 1283) ||
	  (cur_lumi_block >= 1285 && cur_lumi_block <= 1331) ||
	  (cur_lumi_block >= 198 && cur_lumi_block <= 281) ||
	  (cur_lumi_block >= 1333 && cur_lumi_block <= 1515) ||
	  (cur_lumi_block >= 331 && cur_lumi_block <= 402) ||
	  (cur_lumi_block >= 864 && cur_lumi_block <= 884) ||
	  (cur_lumi_block >= 675 && cur_lumi_block <= 805) ||
	  (cur_lumi_block >= 75 && cur_lumi_block <= 196) ||
	  (cur_lumi_block >= 568 && cur_lumi_block <= 672) ||
	  (cur_lumi_block >= 1518 && cur_lumi_block <= 1734) ||
	  (cur_lumi_block >= 886 && cur_lumi_block <= 1180) ||
	  (cur_lumi_block >= 807 && cur_lumi_block <= 850) ||
	  (cur_lumi_block >= 284 && cur_lumi_block <= 329) ||
	  (cur_lumi_block >= 1737 && cur_lumi_block <= 1796) ||
	  (cur_lumi_block >= 852 && cur_lumi_block <= 861) ||
	  (cur_lumi_block >= 404 && cur_lumi_block <= 565) ) ) ||
	( cur_run_number == 207920 && (
	  (cur_lumi_block >= 600 && cur_lumi_block <= 708) ||
	  (cur_lumi_block >= 489 && cur_lumi_block <= 518) ||
	  (cur_lumi_block >= 520 && cur_lumi_block <= 598) ||
	  (cur_lumi_block >= 294 && cur_lumi_block <= 486) ||
	  (cur_lumi_block >= 264 && cur_lumi_block <= 291) ||
	  (cur_lumi_block >= 149 && cur_lumi_block <= 241) ||
	  (cur_lumi_block >= 84 && cur_lumi_block <= 146) ||
	  (cur_lumi_block >= 710 && cur_lumi_block <= 826) ||
	  (cur_lumi_block >= 243 && cur_lumi_block <= 261) ) ) ||
	( cur_run_number == 207921 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 37) ||
	  (cur_lumi_block >= 40 && cur_lumi_block <= 58) ) ) ||
	( cur_run_number == 207922 && (
	  (cur_lumi_block >= 274 && cur_lumi_block <= 291) ||
	  (cur_lumi_block >= 71 && cur_lumi_block <= 100) ||
	  (cur_lumi_block >= 129 && cur_lumi_block <= 242) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 69) ||
	  (cur_lumi_block >= 103 && cur_lumi_block <= 126) ) ) ||
	( cur_run_number == 207924 && (
	  (cur_lumi_block >= 181 && cur_lumi_block <= 339) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 52) ||
	  (cur_lumi_block >= 173 && cur_lumi_block <= 178) ||
	  (cur_lumi_block >= 54 && cur_lumi_block <= 171) ) ) ||
	( cur_run_number == 208307 && (
	  (cur_lumi_block >= 1057 && cur_lumi_block <= 1205) ||
	  (cur_lumi_block >= 763 && cur_lumi_block <= 798) ||
	  (cur_lumi_block >= 262 && cur_lumi_block <= 275) ||
	  (cur_lumi_block >= 2 && cur_lumi_block <= 42) ||
	  (cur_lumi_block >= 670 && cur_lumi_block <= 761) ||
	  (cur_lumi_block >= 586 && cur_lumi_block <= 605) ||
	  (cur_lumi_block >= 453 && cur_lumi_block <= 527) ||
	  (cur_lumi_block >= 891 && cur_lumi_block <= 893) ||
	  (cur_lumi_block >= 72 && cur_lumi_block <= 147) ||
	  (cur_lumi_block >= 1208 && cur_lumi_block <= 1294) ||
	  (cur_lumi_block >= 618 && cur_lumi_block <= 667) ||
	  (cur_lumi_block >= 800 && cur_lumi_block <= 889) ||
	  (cur_lumi_block >= 896 && cur_lumi_block <= 1055) ||
	  (cur_lumi_block >= 47 && cur_lumi_block <= 70) ||
	  (cur_lumi_block >= 345 && cur_lumi_block <= 450) ||
	  (cur_lumi_block >= 1297 && cur_lumi_block <= 1328) ||
	  (cur_lumi_block >= 608 && cur_lumi_block <= 616) ||
	  (cur_lumi_block >= 530 && cur_lumi_block <= 583) ||
	  (cur_lumi_block >= 278 && cur_lumi_block <= 342) ||
	  (cur_lumi_block >= 256 && cur_lumi_block <= 259) ||
	  (cur_lumi_block >= 45 && cur_lumi_block <= 45) ||
	  (cur_lumi_block >= 150 && cur_lumi_block <= 252) ) ) ||
	( cur_run_number == 208339 && (
	  (cur_lumi_block >= 91 && cur_lumi_block <= 122) ||
	  (cur_lumi_block >= 349 && cur_lumi_block <= 363) ||
	  (cur_lumi_block >= 77 && cur_lumi_block <= 89) ||
	  (cur_lumi_block >= 125 && cur_lumi_block <= 208) ||
	  (cur_lumi_block >= 211 && cur_lumi_block <= 346) ) ) ||
	( cur_run_number == 208341 && (
	  (cur_lumi_block >= 777 && cur_lumi_block <= 824) ||
	  (cur_lumi_block >= 695 && cur_lumi_block <= 775) ||
	  (cur_lumi_block >= 87 && cur_lumi_block <= 117) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 84) ||
	  (cur_lumi_block >= 515 && cur_lumi_block <= 685) ||
	  (cur_lumi_block >= 120 && cur_lumi_block <= 513) ||
	  (cur_lumi_block >= 688 && cur_lumi_block <= 693) ) ) ||
	( cur_run_number == 208351 && (
	  (cur_lumi_block >= 100 && cur_lumi_block <= 356) ||
	  (cur_lumi_block >= 359 && cur_lumi_block <= 367) ||
	  (cur_lumi_block >= 369 && cur_lumi_block <= 369) ||
	  (cur_lumi_block >= 83 && cur_lumi_block <= 97) ) ) ||
	( cur_run_number == 208352 && (
	  (cur_lumi_block >= 17 && cur_lumi_block <= 17) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 15) ||
	  (cur_lumi_block >= 19 && cur_lumi_block <= 19) ) ) ||
	( cur_run_number == 208353 && (
	  (cur_lumi_block >= 271 && cur_lumi_block <= 348) ||
	  (cur_lumi_block >= 78 && cur_lumi_block <= 269) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 76) ) ) ||
	( cur_run_number == 208357 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 70) ||
	  (cur_lumi_block >= 73 && cur_lumi_block <= 507) ) ) ||
	( cur_run_number == 208390 && (
	  (cur_lumi_block >= 130 && cur_lumi_block <= 169) ||
	  (cur_lumi_block >= 72 && cur_lumi_block <= 128) ) ) ||
	( cur_run_number == 208391 && (
	  (cur_lumi_block >= 535 && cur_lumi_block <= 588) ||
	  (cur_lumi_block >= 495 && cur_lumi_block <= 498) ||
	  (cur_lumi_block >= 526 && cur_lumi_block <= 533) ||
	  (cur_lumi_block >= 219 && cur_lumi_block <= 493) ||
	  (cur_lumi_block >= 500 && cur_lumi_block <= 523) ||
	  (cur_lumi_block >= 663 && cur_lumi_block <= 869) ||
	  (cur_lumi_block >= 84 && cur_lumi_block <= 162) ||
	  (cur_lumi_block >= 52 && cur_lumi_block <= 82) ||
	  (cur_lumi_block >= 591 && cur_lumi_block <= 660) ||
	  (cur_lumi_block >= 164 && cur_lumi_block <= 216) ) ) ||
	( cur_run_number == 208427 && (
	  (cur_lumi_block >= 271 && cur_lumi_block <= 312) ||
	  (cur_lumi_block >= 315 && cur_lumi_block <= 315) ||
	  (cur_lumi_block >= 166 && cur_lumi_block <= 173) ||
	  (cur_lumi_block >= 580 && cur_lumi_block <= 647) ||
	  (cur_lumi_block >= 364 && cur_lumi_block <= 402) ||
	  (cur_lumi_block >= 425 && cur_lumi_block <= 577) ||
	  (cur_lumi_block >= 92 && cur_lumi_block <= 161) ||
	  (cur_lumi_block >= 317 && cur_lumi_block <= 335) ||
	  (cur_lumi_block >= 337 && cur_lumi_block <= 361) ||
	  (cur_lumi_block >= 49 && cur_lumi_block <= 89) ||
	  (cur_lumi_block >= 164 && cur_lumi_block <= 164) ||
	  (cur_lumi_block >= 175 && cur_lumi_block <= 268) ||
	  (cur_lumi_block >= 404 && cur_lumi_block <= 422) ) ) ||
	( cur_run_number == 208428 && (
	  (cur_lumi_block >= 70 && cur_lumi_block <= 156) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 58) ||
	  (cur_lumi_block >= 159 && cur_lumi_block <= 227) ||
	  (cur_lumi_block >= 61 && cur_lumi_block <= 68) ) ) ||
	( cur_run_number == 208429 && (
	  (cur_lumi_block >= 455 && cur_lumi_block <= 589) ||
	  (cur_lumi_block >= 715 && cur_lumi_block <= 922) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 56) ||
	  (cur_lumi_block >= 162 && cur_lumi_block <= 237) ||
	  (cur_lumi_block >= 141 && cur_lumi_block <= 159) ||
	  (cur_lumi_block >= 592 && cur_lumi_block <= 712) ||
	  (cur_lumi_block >= 240 && cur_lumi_block <= 440) ||
	  (cur_lumi_block >= 442 && cur_lumi_block <= 452) ||
	  (cur_lumi_block >= 59 && cur_lumi_block <= 139) ) ) ||
	( cur_run_number == 208487 && (
	  (cur_lumi_block >= 309 && cur_lumi_block <= 459) ||
	  (cur_lumi_block >= 2 && cur_lumi_block <= 26) ||
	  (cur_lumi_block >= 29 && cur_lumi_block <= 159) ||
	  (cur_lumi_block >= 479 && cur_lumi_block <= 621) ||
	  (cur_lumi_block >= 161 && cur_lumi_block <= 307) ||
	  (cur_lumi_block >= 462 && cur_lumi_block <= 476) ) ) ||
	( cur_run_number == 208509 && (
	  (cur_lumi_block >= 71 && cur_lumi_block <= 232) ) ) ||
	( cur_run_number == 208538 && (
	  (cur_lumi_block >= 2 && cur_lumi_block <= 43) ) ) ||
	( cur_run_number == 208540 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 26) ||
	  (cur_lumi_block >= 29 && cur_lumi_block <= 98) ) ) ||
	( cur_run_number == 208541 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 57) ||
	  (cur_lumi_block >= 378 && cur_lumi_block <= 413) ||
	  (cur_lumi_block >= 59 && cur_lumi_block <= 173) ||
	  (cur_lumi_block >= 175 && cur_lumi_block <= 376) ) ) ||
	( cur_run_number == 208551 && (
	  (cur_lumi_block >= 119 && cur_lumi_block <= 193) ||
	  (cur_lumi_block >= 303 && cur_lumi_block <= 354) ||
	  (cur_lumi_block >= 356 && cur_lumi_block <= 554) ||
	  (cur_lumi_block >= 215 && cur_lumi_block <= 300) ||
	  (cur_lumi_block >= 557 && cur_lumi_block <= 580) ||
	  (cur_lumi_block >= 195 && cur_lumi_block <= 212) ) ) ||
	( cur_run_number == 208686 && (
	  (cur_lumi_block >= 227 && cur_lumi_block <= 243) ||
	  (cur_lumi_block >= 82 && cur_lumi_block <= 181) ||
	  (cur_lumi_block >= 246 && cur_lumi_block <= 311) ||
	  (cur_lumi_block >= 183 && cur_lumi_block <= 224) ||
	  (cur_lumi_block >= 313 && cur_lumi_block <= 459) ||
	  (cur_lumi_block >= 73 && cur_lumi_block <= 79) ) ) )
		return true;
	else
		return false;
	// JSON AUTO FILL END -- do not edit this comment
	
	return true;
}

void RunSelector::accept_all(bool accept)
{
	this->accept_all_events = accept;
}
