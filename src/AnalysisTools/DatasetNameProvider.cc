#include "../../interface/AnalysisTools/DatasetNameProvider.h"

broc::DatasetNameProvider::DatasetNameProvider(eire::ConfigReader *config_reader)
{
	name_map_reader = new eire::ConfigReader();

	mapping_file_name = config_reader->get_var("dataset_mapping_file", "global", true);

	initialise();
}

broc::DatasetNameProvider::~DatasetNameProvider()
{
	if(name_map_reader){ delete name_map_reader; name_map_reader = NULL; }
}

std::string broc::DatasetNameProvider::get_name(std::string input_name)
{
	if(name_mapping.find(input_name) == name_mapping.end()){
		std::cerr << "broc::DatasetNameProvider::get_name(): cannot find mapping for dataset: " << input_name << std::endl;	
		exit(1);
	}

	return name_mapping[input_name];
}

void broc::DatasetNameProvider::initialise()
{
	name_map_reader->read_config_from_file(mapping_file_name);
	std::vector<std::string> names = name_map_reader->get_variables_for_section("global");
	
	for(std::vector<std::string>::iterator name = names.begin();
		name != names.end();
		++name){
		name_mapping[*name] = name_map_reader->get_var(*name,"global");
	}
}
