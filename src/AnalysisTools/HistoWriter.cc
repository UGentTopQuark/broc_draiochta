#include "../../interface/AnalysisTools/HistoWriter.h"

HistoWriter::HistoWriter()
{
}

HistoWriter::~HistoWriter()
{
	outfile->cd();
	outfile->mkdir("eventselection");
	outfile->Cd("eventselection");
	for(std::vector<eire::TH1D*>::iterator histo = histos1d.begin();histo != histos1d.end();++histo)
	{
		//jl 26.08.10: re-normalise QCD histograms
		//jl 30.08.10: dirty trick to take care of negative bins
		hname = (*histo)->GetName();
		if(hname.find("QCDMM") != std::string ::npos)
		{
			//printf("histo %s\n", hname.c_str());
			//for(int i = 1 ; i<=(*histo)->GetNbinsX() ; i++)
			//{
			//	//printf("n %d, content %f\n",i,(*histo)->GetBinContent(i));
			//	if((*histo)->GetBinContent(i) <0.) 
			//	{
			//		(*histo)->SetBinContent(i,0.);
			//		(*histo)->SetBinError(i,0.);
			//
			//	}
			//	//printf("AFTER n %d, content %f\n",i,(*histo)->GetBinContent(i));
			//	//printf("ERROR %f \n",(*histo)->GetBinError(i));
			//}
                        //printf("Re-normalising histo %s with SumOfWeights: %f\n",hname.c_str(),(*histo)->GetSumOfWeights());
                        //printf("Prior to renorma, nentries is %f\n",(*histo)->GetEntries());
                        (*histo)->SetEntries((*histo)->GetSumOfWeights());
                        //printf("After renorma, nentries is %f\n",(*histo)->GetEntries());

		}
		//jl end
		(*histo)->Write();
	}

	for(std::vector<eire::TH2D*>::iterator histo = histos2d.begin();
		histo != histos2d.end();
		++histo){
		(*histo)->Write();
	}
}

eire::TH1D* HistoWriter::create_1d(std::string identifier,
				std::string title,
				int nbins_x, double min_x, double max_x, std::string x_title, std::string y_title)
{
	outfile->cd();
	eire::TH1D* histo = new eire::TH1D(identifier.c_str(), title.c_str(), nbins_x, min_x, max_x);
	histo->SetXTitle(x_title.c_str());
	histo->SetYTitle(y_title.c_str());
	//jl 30.08.10: need sumW2, at least for QCD MM histos
	histo->Sumw2();
	histos1d.push_back(histo);

	return histo;
}

eire::TH1D* HistoWriter::create_1d(std::string identifier,
				std::string title,
				int nbins_x, float var_bins[100], std::string x_title, std::string y_title)
{
	outfile->cd();
	eire::TH1D* histo = new eire::TH1D(identifier.c_str(), title.c_str(), nbins_x, var_bins);
	histo->SetXTitle(x_title.c_str());
	histo->SetYTitle(y_title.c_str());
	histo->Sumw2();
	histos1d.push_back(histo);

	return histo;
}

eire::TH2D* HistoWriter::create_2d(std::string identifier,
				std::string title,
				int nbins_x, double min_x, double max_x,
				int nbins_y, double min_y, double max_y,
				std::string x_title, std::string y_title)
{
	outfile->cd();
	eire::TH2D* histo = new eire::TH2D(identifier.c_str(), title.c_str(), 
			       nbins_x, min_x, max_x,
			       nbins_y, min_y, max_y);
	histo->SetXTitle(x_title.c_str());
	histo->SetYTitle(y_title.c_str());
	histo->Sumw2();
	histos2d.push_back(histo);

	return histo;
}

void HistoWriter::set_outfile(TFile *outfile)
{
	this->outfile = outfile;
}
