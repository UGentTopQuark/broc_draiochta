#include "../../interface/CrossTriggerStudy/TotalCrossTrigEfficiencyEstimator.h"
//#include "../../interface/EventSelection/TopTriggerEfficiencyProvider.h"

template <class ParType1>
broc::TotalCrossTrigEfficiencyEstimator<ParType1>::TotalCrossTrigEfficiencyEstimator(eire::HandleHolder *handle_holder, bool el, bool mu, bool jet, bool tot, bool one, bool two, bool three)
{

  
  enable_el_leg = el;
  enable_mu_leg = mu;
  enable_jet_leg = jet;
  enable_total_leg = tot;
  jet_one = one;
  jet_two = two;
  jet_three = three;
  
  this->handle_holder = handle_holder;
  this->primary_vertices = handle_holder->get_tight_primary_vertices();
  this->event_information = handle_holder->get_event_information();
  this->histo_writer = handle_holder->get_histo_writer();
  
  jet_trigger_matcher = new truicear::TriggerMatcher<mor::Jet, mor::TriggerObject>();
  lepton_trigger_matcher = new truicear::TriggerMatcher<ParType1, mor::TriggerObject>();
  
  trig_name_prov = new eire::TriggerNameProvider();
  trigger_id = (int) handle_holder->get_cuts_set()->get_cut_value("hlt_cross_trigger");                                                                          
  ref_trigger = (int) handle_holder->get_cuts_set()->get_cut_value("hlt_reference_trigger");    
  
  if (ref_trigger == -1) ref_trigger = 999;


  obj_selector = new truicear::TriggerObjectSelector(handle_holder);


  lep_pt=0;
  lep_eta=0;
  lep_phi=0;
  jet1_pt=0;
  jet1_eta=0;
  jet1_abseta=0;
  jet1_phi=0;
  jet2_pt=0;
  jet2_eta=0;
  jet2_abseta=0;
  jet2_phi=0;
  jet3_pt=0;
  jet3_eta=0;
  jet3_abseta=0;
  jet3_phi=0;
  jet4_pt=0;
  jet4_eta=0;
  jet4_abseta=0;
  jet4_phi=0;

  e_mva_id = 0;
  dbeta_PFrelIso = 0;
  met = 0;
  passing_ref = -1;
  passing_cross_trigger = -1;
  passing_cross_trigger_with_matching = -1;
  njets=0;
  npvertices=0;
  run=0;
  lumi_section=0;
  event_number=0;
  event_weight=0;

  jets = NULL;
  mva_id_calc = NULL;  
  	 
  set_handles(handle_holder);
}

template <class ParType1>
broc::TotalCrossTrigEfficiencyEstimator<ParType1>::~TotalCrossTrigEfficiencyEstimator()
{
  if(trig_name_prov){ delete trig_name_prov; trig_name_prov = NULL; }
  if(jet_trigger_matcher){
    delete jet_trigger_matcher;
    jet_trigger_matcher = NULL;
  }
  if(lepton_trigger_matcher){
    delete lepton_trigger_matcher;
    lepton_trigger_matcher = NULL;
  }
}

template <class ParType1>
void broc::TotalCrossTrigEfficiencyEstimator<ParType1>::book_branches()
{
  tree->Branch("lep_pt",&lep_pt, "lep_pt/F");
  tree->Branch("lep_eta",&lep_eta, "lep_eta/F");
  tree->Branch("lep_phi",&lep_phi, "lep_phi/F");
  tree->Branch("jet1_pt",&jet1_pt, "jet1_pt/F");
  tree->Branch("jet1_eta",&jet1_eta, "jet1_eta/F");
  tree->Branch("jet1_abseta",&jet1_abseta, "jet1_abseta/F");
  tree->Branch("jet1_phi",&jet1_phi, "jet1_phi/F");
  tree->Branch("jet2_pt",&jet2_pt, "jet2_pt/F");
  tree->Branch("jet2_eta",&jet2_eta, "jet2_eta/F");
  tree->Branch("jet2_abseta",&jet2_abseta, "jet2_abseta/F");
  tree->Branch("jet2_phi",&jet2_phi, "jet2_phi/F");
  tree->Branch("jet3_pt",&jet3_pt, "jet3_pt/F");
  tree->Branch("jet3_eta",&jet3_eta, "jet3_eta/F");
  tree->Branch("jet3_abseta",&jet3_abseta, "jet3_abseta/F");
  tree->Branch("jet3_phi",&jet3_phi, "jet3_phi/F");
  tree->Branch("jet4_pt",&jet4_pt, "jet4_pt/F");
  tree->Branch("jet4_eta",&jet4_eta, "jet4_eta/F");
  tree->Branch("jet4_abseta",&jet4_abseta, "jet4_abseta/F");
  tree->Branch("jet4_phi",&jet4_phi, "jet4_phi/F");
  tree->Branch("dbeta_PFrelIso", &dbeta_PFrelIso, "dbeta_PFrelIso/F");
  tree->Branch("e_mva_id",&e_mva_id, "e_mva_id/F");
  tree->Branch("met",&met, "met/F");
  tree->Branch("run",&run, "run/F");
  tree->Branch("lumi_section",&lumi_section, "lumi_section/F");
  tree->Branch("event_number",&event_number, "event_number/I");
  tree->Branch("njets",&njets, "njets/F");
  tree->Branch("npvertices",&npvertices, "npvertices/F");
  tree->Branch("event_weight",&event_weight, "event_weight/F");
  tree->Branch("passing_ref",&passing_ref, "passing_ref/I");
  tree->Branch("passing_cross_trigger",&passing_cross_trigger, "passing_cross_trigger/I");
  tree->Branch("passing_cross_trigger_with_matching",&passing_cross_trigger_with_matching, "passing_cross_trigger_with_matching/I");
}

namespace broc{
template <class ParType1>
std::vector<ParType1>* broc::TotalCrossTrigEfficiencyEstimator<ParType1>::get_leptons()
{
  
}

template <>
std::vector<mor::Electron>* broc::TotalCrossTrigEfficiencyEstimator<mor::Electron>::get_leptons()
{
  return  handle_holder->get_tight_electrons();
}


template <>
std::vector<mor::Muon>* broc::TotalCrossTrigEfficiencyEstimator<mor::Muon>::get_leptons()
{
  return  handle_holder->get_tight_muons();
}
}

template <class ParType1>
bool broc::TotalCrossTrigEfficiencyEstimator<ParType1>::fill_branches()
{
  ParType1 probe_lepton;
  this->lepton =  get_leptons();
  probe_lepton = (*lepton)[0];

  unsigned int n_of_jet = 0;
  if(jet_one){n_of_jet = 1;}
  if(jet_two){n_of_jet = 2;}
  if(jet_three){n_of_jet = 3;}
  
  //set the method used for the trigger matching (matching in dR) and set the value of the dR cut
  double max_trig_dR = 0.1;
  jet_trigger_matcher->set_max_dR(max_trig_dR);
  lepton_trigger_matcher->set_max_dR(max_trig_dR);
  jet_trigger_matcher->set_matching_method(0);
  lepton_trigger_matcher->set_matching_method(0);

  //get the names of the triggers: trigger_id is the cross-trigger and ref_trigger is the reference trigger
  std::string trigger_name = trig_name_prov->hlt_name(trigger_id);                                                                                                        
  std::string ref_trigger_name = trig_name_prov->hlt_name(ref_trigger);                                                                                                   
  // check if event passed reference trigger and cross trigger                                                                                                                
  bool passed_ref = handle_holder->get_trigger()->triggered(ref_trigger_name);
  bool passed_cross = handle_holder->get_trigger()->triggered(trigger_name); 
 
  bool passed_cross_with_matching = false;

  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%REFERENCE TRIGGER%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  if(!passed_ref){return false;} //if the event doesn't pass the reference trigger, exit the method 

  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%CROSS-TRIGGER%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  //get the trigger object-selector for the cross-trigger objects: 2 for the jets and 1 for the leptons
  truicear::TriggerObjectSelector *trigObj_crosstrig = new truicear::TriggerObjectSelector(handle_holder);
  trigObj_crosstrig->set_trigger_name(trigger_name,2);
  truicear::TriggerObjectSelector *trigObj_crosstrig_el = new truicear::TriggerObjectSelector(handle_holder);
  trigObj_crosstrig_el->set_trigger_name(trigger_name,1);
  //truicear::TriggerObjectSelector *trigObj_crosstrig_total = new truicear::TriggerObjectSelector(handle_holder);
  //trigObj_crosstrig_el->set_trigger_name(trigger_name,trigger_name);

  //select the jet and lepton trigger objects of the cross-trigger
  std::vector<mor::TriggerObject> *trig_objects = trigObj_crosstrig->get_selected_objects(handle_holder->get_trigger_objects());
  std::vector<mor::TriggerObject> *trig_objects_el = trigObj_crosstrig_el->get_selected_objects(handle_holder->get_trigger_objects());
  //  std::vector<mor::TriggerObject> *trig_objects_total = trigObj_crosstrig_total->get_selected_objects(handle_holder->get_trigger_objects());


  //  for(unsigned int i=0; i < trig_objects_total->size(); i++){
  // std::cout<<"trigger total "<<i<<" pt= "<<(*trig_objects_total)[i].pt()<<" eta= "<<(*trig_objects_total)[i].eta()<<" phi= "<<(*trig_objects_total)[i].phi()<<std::endl;
  //}
  if(enable_el_leg && trig_objects_el->size() >= 1){passed_cross = true;}
  if(enable_mu_leg && trig_objects_el->size() >= 1){passed_cross = true;}
  if(enable_jet_leg){
    if(trig_objects_el->size() < 1){return false;}
    if(trig_objects->size() >= n_of_jet){
    passed_cross = true;}
  }
 if(enable_total_leg && (trig_objects->size() >= 3 && trig_objects_el->size() >= 1)){passed_cross=true;}

  if(verbose) std::cout<<"passed cross trig= "<<passed_cross<<" el trigger objects= "<<trig_objects_el->size()<<" jet trigger objects= "<<trig_objects->size()<<std::endl;//"trigger objects total"<<trig_objects_total->size()<<" n_of_jets= "<<n_of_jet<<std::endl;
  if(verbose) std::cout<<"enable jet leg"<<enable_jet_leg<<std::endl;
  for(unsigned int i=0; i < trig_objects_el->size(); i++){
    if(verbose) std::cout<<"trigger el "<<i<<" pt= "<<(*trig_objects_el)[i].pt()<<" eta= "<<(*trig_objects_el)[i].eta()<<" phi= "<<(*trig_objects_el)[i].phi()<<std::endl;
  }
  for(unsigned int i=0; i < jets->size(); i++){
    if(verbose) std::cout<<"jet "<<i<<" pt= "<<(*jets)[i].pt()<<" eta= "<<(*jets)[i].eta()<<" phi= "<<(*jets)[i].phi()<<std::endl;
  }
  for(unsigned int i=0; i < trig_objects->size(); i++){
    if(verbose) std::cout<<"trigger jet "<<i<<" pt= "<<(*trig_objects)[i].pt()<<" eta= "<<(*trig_objects)[i].eta()<<" phi= "<<(*trig_objects)[i].phi()<<std::endl;
  }
    
  //get the Lepton that is matched to a trigger object and get the trigger objects that are matched to that lepton
  std::vector<ParType1> *matched_leptons = 0;
  if(enable_el_leg || enable_mu_leg || enable_total_leg){
  matched_leptons = lepton_trigger_matcher->match(lepton, trig_objects_el);
  }
  //  std::vector<mor::TriggerObject> *matched_trigger_objects = lepton_trigger_matcher->get_type2_matched();
  //get the Jets that are matched to the trigger objects
  std::vector<mor::Jet> *matched_jets = jet_trigger_matcher->match(jets, trig_objects);
  
  //check that all the objects are matched, if so put bool to true
  if((enable_el_leg || enable_mu_leg) && passed_cross && matched_leptons->size() >= 1){passed_cross_with_matching = true;}
  if(enable_jet_leg && passed_cross && matched_jets->size() >= n_of_jet){passed_cross_with_matching = true;}
  if(enable_total_leg && passed_cross && matched_leptons->size() >= 1 && matched_jets->size() >= 3 ){passed_cross_with_matching = true;}
  
  //delete all the trigger object selectors per event
  delete trigObj_crosstrig;
  trigObj_crosstrig = NULL;
  delete trigObj_crosstrig_el;
  trigObj_crosstrig_el = NULL;
  
  //at this point the tree can be filled, we know now that the hadronic part of the cross-trigger is satisfied, but if the event passes the cross-trigger, but cannot match an lepton to the trigger object, we artificially set the event as failing the cross trigger
  fill_probe_branch(probe_lepton,passed_ref, passed_cross, passed_cross_with_matching);

  // Return 0 to satisfy template function
  return 0;
}

namespace broc{
template <class ParType1>
void broc::TotalCrossTrigEfficiencyEstimator<ParType1>::fill_probe_branch( ParType1 &probe_lepton, bool ref, bool cross, bool cross_with_matching)
{

}

template <>
void broc::TotalCrossTrigEfficiencyEstimator<mor::Electron>::fill_probe_branch(mor::Electron &probe_lepton, bool ref, bool cross, bool cross_with_matching)
{
  if(enable_el_leg || enable_mu_leg || enable_total_leg){
    lep_pt = probe_lepton.pt();
    lep_eta = probe_lepton.eta();
    lep_phi = probe_lepton.phi();
  }
  if(jets->size() >= 1){
    jet1_pt = (*jets)[0].pt();
    jet1_eta =(*jets)[0].eta();
    jet1_abseta = std::abs((*jets)[0].eta());
    jet1_phi =(*jets)[0].phi();
  }
  if(jets->size() >= 2){
    jet2_pt =(*jets)[1].pt();
    jet2_eta =(*jets)[1].eta();
    jet2_abseta = std::abs((*jets)[1].eta());
    jet2_phi =(*jets)[1].phi();
  }
  if(jets->size() >= 3){
    jet3_pt =(*jets)[2].pt();
    jet3_eta =(*jets)[2].eta();
    jet3_abseta = std::abs((*jets)[2].eta());
    jet3_phi =(*jets)[2].phi();
  }
  if(jets->size() >= 4){
    jet4_pt =(*jets)[3].pt();
    jet4_eta =(*jets)[3].eta();
    jet4_abseta = std::abs((*jets)[3].eta());
    jet4_phi =(*jets)[3].phi();
  }

  mva_id_calc = handle_holder->services()->mva_id_calc();
  e_mva_id = mva_id_calc->discriminator(probe_lepton);
  
  dbeta_PFrelIso = probe_lepton.dbeta_PFrelIso();
  met = handle_holder->get_mets()->begin()->pt();

  run = event_information->run();
  lumi_section = event_information->lumi_block();
  event_number = (int) event_information->event_number(); 

  njets = jets->size();
  npvertices   = primary_vertices->size();
  event_weight = handle_holder->get_event_weight();

  //  TopTriggerEfficiencyProvider *weight_provider = new TopTriggerEfficiencyProvider();
  //double weight = weight_provider->get_weight(lep_pt, lep_eta, jet4_pt, jet4_eta, npvertices, 199608, njets, 1);
  //std::cout<<"in TotalCrossTrigEfficiencyEstimator: weight = "<<weight<<std::endl;

  if(ref){
    passing_ref = 1;
    if(cross){
      passing_cross_trigger = 1;
    }
    else{
      passing_cross_trigger = 0;
    }
    if(cross_with_matching){
      passing_cross_trigger_with_matching = 1;
    }
    else{
      passing_cross_trigger_with_matching = 0;
    }
  }
  else{passing_ref = 0;
    passing_cross_trigger = -1;
    passing_cross_trigger_with_matching = -1;
  }
  tree->Fill();

}

template <>
void broc::TotalCrossTrigEfficiencyEstimator<mor::Muon>::fill_probe_branch(mor::Muon &probe_lepton, bool ref, bool cross, bool cross_with_matching)
{

  if(enable_el_leg || enable_mu_leg || enable_total_leg){
    lep_pt = probe_lepton.pt();
    lep_eta = probe_lepton.eta();
    lep_phi = probe_lepton.phi();
  }
  if(jets->size() >= 1){
    jet1_pt = (*jets)[0].pt();
    jet1_eta =(*jets)[0].eta();
    jet1_abseta = std::abs((*jets)[0].eta());
    jet1_phi =(*jets)[0].phi();
  }
  if(jets->size() >= 2){
    jet2_pt =(*jets)[1].pt();
    jet2_eta =(*jets)[1].eta();
    jet2_abseta = std::abs((*jets)[1].eta());
    jet2_phi =(*jets)[1].phi();
  }
  if(jets->size() >= 3){
    jet3_pt =(*jets)[2].pt();
    jet3_eta =(*jets)[2].eta();
    jet3_abseta = std::abs((*jets)[2].eta());
    jet3_phi =(*jets)[2].phi();
  }
  if(jets->size() >= 4){
    jet4_pt =(*jets)[3].pt();
    jet4_eta =(*jets)[3].eta();
    jet4_abseta = std::abs((*jets)[3].eta());
    jet4_phi =(*jets)[3].phi();
  }

  e_mva_id = -999;
  dbeta_PFrelIso = probe_lepton.dbeta_PFrelIso();
  met = handle_holder->get_mets()->begin()->pt();

  run = event_information->run();
  lumi_section = event_information->lumi_block();
  event_number = (int) event_information->event_number(); 

  njets = jets->size();
  npvertices   = primary_vertices->size();
  event_weight = handle_holder->get_event_weight();

  if(ref){
    passing_ref = 1;
    if(cross){
      passing_cross_trigger = 1;
    }
    else{
      passing_cross_trigger = 0;
    }
    if(cross_with_matching){
      passing_cross_trigger_with_matching = 1;
    }
    else{
      passing_cross_trigger_with_matching = 0;
    }
  }
  else{passing_ref = 0;
    passing_cross_trigger = -1;
    passing_cross_trigger_with_matching = -1;
  }
  tree->Fill();

}
}

template <class ParType1>
double broc::TotalCrossTrigEfficiencyEstimator<ParType1>::min_lep_jet_dR( ParType1 *lepton)
{
	double min_dR = -1;
	for(std::vector<mor::Jet>::iterator jet = jets->begin();
		jet != jets->end();
		++jet){
		double dR = ROOT::Math::VectorUtil::DeltaR(*jet, *lepton);
		if(min_dR == -1 || dR < min_dR){
			min_dR = dR;
		}
	}

	return min_dR;
}

template class broc::TotalCrossTrigEfficiencyEstimator<mor::Muon>;
template class broc::TotalCrossTrigEfficiencyEstimator<mor::Electron>;

