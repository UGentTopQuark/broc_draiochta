#include "../../interface/BTagStudies/BTagStudies.h"

bclib::BTagStudies::BTagStudies(broc::CutsSet *cuts_set, eire::HandleHolder *handle_holder)
{
	this->cuts_set = cuts_set;
	this->handle_holder = handle_holder;

	eire::ConfigReader *config_reader = handle_holder->get_config_reader();
	std::string output_dir = config_reader->get_var("output_directory");
	std::string suffix = config_reader->get_var("outfile_suffix");
	std::string filename = output_dir+"/"+"btag_system8_"+handle_holder->get_ident()+suffix+".root";
	while(filename.find("|") != std::string::npos)
		filename.replace(filename.find("|"),1,"_");

	std::string directory_name = "btagEffs";
	ntuple_file = new TFile(filename.c_str(), "RECREATE");
	if(directory_name != ""){
		ntuple_file->mkdir(directory_name.c_str());
		ntuple_file->Cd(directory_name.c_str());
	}

	btag_sys8_tree_producer = new bclib::System8TreeProducer(cuts_set,handle_holder);
	btag_sys8_tree_producer->set_handles();
	btag_sys8_tree_producer->set_outfile(ntuple_file, directory_name);
	btag_sys8_tree_producer->create_tree("fitter_tree");
	btag_sys8_tree_producer->book_branches();

}

bclib::BTagStudies::~BTagStudies()
{
	if(btag_sys8_tree_producer){
		delete btag_sys8_tree_producer;
		btag_sys8_tree_producer = NULL;
	}
	
	if(ntuple_file){
		ntuple_file->Write();
		ntuple_file->Close();
		delete ntuple_file;
		ntuple_file = NULL;
	}
}
void bclib::BTagStudies::fill_trees()
{
	btag_sys8_tree_producer->fill_branches();
}
