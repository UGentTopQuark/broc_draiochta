#include "../../interface/EventSelection/DiLeptonReconstructor.h"

DiLeptonReconstructor::DiLeptonReconstructor(eire::HandleHolder *handle_holder)
{
	candidates = new std::vector<mor::Particle>();

	min_mass = -1;
	max_mass = -1;

	loose_muons = NULL;
	tight_muons = NULL;
	loose_electrons = NULL;
	tight_electrons = NULL;

	selected_lepton_id = -1;
	allow_equal_charge = false;

	loose_muon_legs = new std::vector<mor::Muon>();
	loose_electron_legs = new std::vector<mor::Electron>();
	tight_muon_legs = new std::vector<mor::Muon>();
	tight_electron_legs = new std::vector<mor::Electron>();

	this->loose_muons = handle_holder->get_loose_muons();
	this->loose_electrons = handle_holder->get_loose_electrons();
	this->tight_muons = handle_holder->get_tight_muons();
	this->tight_electrons = handle_holder->get_tight_electrons();
}

DiLeptonReconstructor::~DiLeptonReconstructor()
{
	if(loose_electron_legs){
		delete loose_electron_legs;
		loose_electron_legs = NULL;
	}
	if(loose_muon_legs){
		delete loose_muon_legs;
		loose_muon_legs = NULL;
	}
	if(tight_muon_legs){
		delete tight_muon_legs;
		tight_muon_legs = NULL;
	}
	if(tight_electron_legs){
		delete tight_electron_legs;
		tight_electron_legs = NULL;
	}
	if(candidates){
		delete candidates;
		candidates = NULL;
	}
}

std::vector<mor::Particle>* DiLeptonReconstructor::get_candidates()
{
	reconstruct_all_candidates();
	return candidates;
}

void DiLeptonReconstructor::set_mass_window(double min, double max)
{
	this->min_mass = min;
	this->max_mass = max;
}

void DiLeptonReconstructor::set_allow_equal_charge(bool allow)
{
	allow_equal_charge = allow;
}


void DiLeptonReconstructor::reconstruct_all_candidates()
{
	candidates->clear();
	loose_electron_legs->clear();
	loose_muon_legs->clear();
	tight_electron_legs->clear();
	tight_muon_legs->clear();
	
	if(selected_lepton_id == -1 || selected_lepton_id == 11) reconstruct_candidates<mor::Electron>(loose_electrons, tight_electrons, loose_electron_legs, tight_electron_legs);
	if(selected_lepton_id == -1 || selected_lepton_id == 13) reconstruct_candidates<mor::Muon>(loose_muons, tight_muons, loose_muon_legs, tight_muon_legs);
}

void DiLeptonReconstructor::select_lepton(int lepton_id)
{
	this->selected_lepton_id = lepton_id;
}

template <class LeptonType>
void DiLeptonReconstructor::reconstruct_candidates(std::vector<LeptonType> *loose_leptons, std::vector<LeptonType> *tight_leptons, std::vector<LeptonType> *loose_lepton_legs, std::vector<LeptonType> *tight_lepton_legs)
{
	std::vector<bool> assigned_type1;
	std::vector<bool> assigned_type2;
	int nlep1 = 0;
	int nlep2 = 0;

	assigned_type1.assign(tight_leptons->size(), false);
	assigned_type2.assign(loose_leptons->size(), false);
	for(typename std::vector<LeptonType>::iterator lepton1 =
		tight_leptons->begin();
	    lepton1 != tight_leptons->end();
	    ++lepton1)
	{
		nlep2 = 0;
		for(typename std::vector<LeptonType>::iterator lepton2 =
			loose_leptons->begin();
		    lepton2 != loose_leptons->end();
		    ++lepton2)
		{
			// don't use the same lepton twice
			if(lepton1->Pt() == lepton2->Pt() &&
			   lepton1->Eta() == lepton2->Eta()){
				++nlep2;
				continue;
			}

			bool already_found_cand = false;
			mor::Particle candidate(lepton1->p4() + lepton2->p4());
			for(std::vector<mor::Particle>::iterator found_cand = candidates->begin();
			    found_cand != candidates->end(); found_cand++){
				if(found_cand->pt() == candidate.pt() && found_cand->eta() == candidate.eta()){
					already_found_cand = true;
					break;
				}
			}
			if(already_found_cand){
				++nlep2;
				continue;
			}

			//Check if candidate passes cuts set.
			if((allow_equal_charge || lepton1->charge() != lepton2->charge()) &&
			   ((min_mass == -1 || min_mass < candidate.mass()) &&
			    (max_mass == -1 || candidate.mass() < max_mass))&&
				!assigned_type1[nlep1] &&
			        !assigned_type2[nlep2]){
				candidates->push_back(candidate);
				tight_lepton_legs->push_back(*lepton1);
				loose_lepton_legs->push_back(*lepton2);
				assigned_type1[nlep1] = true;
				assigned_type2[nlep2] = true;
			}
			++nlep2;
		}
		++nlep1;
	}
}
