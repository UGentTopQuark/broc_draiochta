#include "../../interface/EventSelection/BJetIdentifier.h"

broc::BJetIdentifier::BJetIdentifier(eire::HandleHolder *handle_holder)
{
	min_btag = handle_holder->get_cuts_set()->get_vcut_value("min_btag");

	double num_name_btag = handle_holder->get_cuts_set()->get_cut_value("name_btag");

	broc::BTagAlgoNameProvider btag_name_prov;
	name_btag = (num_name_btag == -1) ? "-1" : btag_name_prov.get_name((int) num_name_btag);
}

broc::BJetIdentifier::~BJetIdentifier()
{
}

void broc::BJetIdentifier::mark_bjets(std::vector<mor::Jet> *jets)
{
	// no b-tag cut set
	if(min_btag->size() == 0 || (min_btag->size() == 1 && (*min_btag)[0] == -1)){
		// don't do anything... all jets are NOT b-tagged
	}else if(min_btag->size() == 1){ // only one cut value for all jets
		double min_btag_cut = (*min_btag)[0];
		// loop over all jets and mark if they pass the cut value
		for(std::vector<mor::Jet>::iterator jet = jets->begin(); jet != jets->end(); ++jet){
			if(jet->bDiscriminator(name_btag) > min_btag_cut){
				jet->set_is_bjet(true);
			}
		}
	}else{
		unsigned int tagged_jets = 0;
		std::map<int,bool> seen;	// bookkeeping to avoid double counting of jets
		// loop over all b-tag cuts
		for(unsigned int icut = 0; (icut < min_btag->size()) && (icut <= tagged_jets); ++icut){
			// if a jet passed the previous cut, tag also all jets that pass the next lower cut
			for(unsigned int ijet = 0; ijet < jets->size(); ++ijet){
				if((*jets)[ijet].bDiscriminator(name_btag) > (*min_btag)[icut] &&
					seen.find(ijet) == seen.end()){
					(*jets)[ijet].set_is_bjet(true);
					seen[ijet] = true;
					++tagged_jets;
				}
			}
		}
	}
}
