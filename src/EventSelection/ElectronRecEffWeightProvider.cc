#include "../../interface/EventSelection/ElectronRecEffWeightProvider.h"

broc::ElectronRecEffWeightProvider::ElectronRecEffWeightProvider(eire::HandleHolder *handle_holder)
{
	if(handle_holder->get_ident().find("Data") != std::string::npos)
		do_not_reweight = true;
	else
		do_not_reweight = false;

	electrons = handle_holder->get_tight_electrons();

	double study_unc = handle_holder->get_cuts_set()->get_cut_value("study_e_ID_ISO_unc");
	
	up = false;
	down = false;

	if(study_unc == 1){ down = true; }
	else if(study_unc == 2){ up = true; }
}

broc::ElectronRecEffWeightProvider::~ElectronRecEffWeightProvider()
{
}

double broc::ElectronRecEffWeightProvider::get_weight(int UpDown)
{
	double weight = 1.;

	if(do_not_reweight)
		weight = 1.;
	else{
	  if(electrons->size() >= 1){
	    std::vector<mor::Electron>::iterator e = electrons->begin();
	    double eta = e->supercluster_eta();
	    double abseta = std::abs(eta);
	    double pt = e->pt();
	    double ud = 0.; // up-down switch for systematics
	    if(up || UpDown == 1) ud = 1.;
	    else if(down || UpDown == -1) ud = -1.;
	    if(abseta < 0.8){
	      if(pt > 50.){ weight = 0.990+ud*0.005; }
	      else if(pt < 20.){ weight = 0.; }
	      else if(pt < 30.){ weight = 0.982+ud*0.015; }
	      else if(pt < 40.){ weight = 0.988+ud*0.009; }
	      else { weight = 0.990+ud*0.005; }
	    }else if(abseta < 1.478){
	      if(pt > 50.){ weight = 0.991+ud*0.005; }
	      else if(pt < 20.){ weight = 0.; }
	      else if(pt < 30.){ weight = 0.993+ud*0.014; }
	      else if(pt < 40.){ weight = 0.993+ud*0.009; }
	      else { weight = 0.993+ud*0.005; }
	    }else if(abseta < 2.0){
	      if(pt > 50.){ weight = 0.990+ud*0.007; }
	      else if(pt < 20.){ weight = 0.; }
	      else if(pt < 30.){ weight = 0.988+ud*0.015; }
	      else if(pt < 40.){ weight = 0.993+ud*0.010; }
	      else { weight = 0.992+ud*0.005; }
	    }else if(abseta < 2.5){
	      if(pt > 50.){ weight = 0.998+ud*0.008; }
	      else if(pt < 20.){ weight = 0.; }
	      else if(pt < 30.){ weight = 1.002+ud*0.016; }
	      else if(pt < 40.){ weight = 1.004+ud*0.010; }
	      else { weight = 1.005+ud*0.006; }
	    }
	    else{
	      weight = 0.;
	    }
	  }
	}
	
	return weight;
}
