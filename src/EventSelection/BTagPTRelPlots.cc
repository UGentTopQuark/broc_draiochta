#include "../../interface/EventSelection/BTagPTRelPlots.h"

BTagPTRelPlots::BTagPTRelPlots(std::string ident)
{
	pTrel = -1;
	unoff_pTrel = -1;
	id = ident;


}

BTagPTRelPlots::~BTagPTRelPlots()
{
}

void BTagPTRelPlots::plot_all()
{
	if(!select_muon_and_jet()){
		return;
	}
	calculate_pt_rel();
	plot_pt_rel();//Plot relative pt of muon in jet for b-tag efficiency measurement.
	for(std::vector<std::string>::iterator algo_id = algo_ids.begin();
	    algo_id != algo_ids.end();algo_id++){
		//Same value will be filled for each algo id. 
		//Just makes it easier to read in template fitter.
		plot_pt_rel("",*algo_id);
	}
	plot_pt_rel_btagged();

	plot_misc();
}

void BTagPTRelPlots::book_histos()
{
	//global var
	algo_ids.push_back("_TCHE");
	algo_ids.push_back("_TCHP");
	algo_ids.push_back("_JP");
	algo_ids.push_back("_JBP");
	algo_ids.push_back("_SSVHE");
	algo_ids.push_back("_SSVHP");
	algo_ids.push_back("_CSV");
	algo_ids.push_back("_CSVMVA");

	std::vector<std::string> flavours;
	flavours.push_back("");
	flavours.push_back("_b");
	flavours.push_back("_udsc");
	flavours.push_back("_c");
	flavours.push_back("_uds");

	std::vector<std::string> wps;//working points
	wps.push_back("");
	wps.push_back("_loosewp");
	wps.push_back("_mediumwp");
	wps.push_back("_tightwp");
   
	std::vector<std::string> tmp_algo_ids = algo_ids;
	tmp_algo_ids.insert(tmp_algo_ids.begin(),"");
	for(std::vector<std::string>::iterator algo_id = tmp_algo_ids.begin();
	    algo_id != tmp_algo_ids.end();algo_id++){
		//bin ranges when plotting btag value different for each algorithm
		int nbins = 35;
		double min_x = 0,max_x = 35;	
		if(*algo_id == "_TCHE")        {nbins = 60; min_x = 0;max_x = 60; }
		// if(btag.first == "newJetBProbabilityBJetTags")             {nbins = 50; min_x = 0;max_x = 10; }
		// if(btag.first == "newJetProbabilityBJetTags")              {nbins = 15; min_x = 0;max_x = 3; }
		// if(btag.first == "newTrackCountingHighPurBJetTags")        {nbins = 50; min_x = 0;max_x = 50; }
		// if(btag.first == "newSoftMuonByIP3dBJetTags")                {nbins = 40; min_x = -1;max_x = 1; }
		// if(btag.first == "newSoftMuonBJetTags")                    {nbins = 40; min_x = -1;max_x = 1; }
		// if(btag.first == "newSimpleSecondaryVertexHighEffBJetTags")       {nbins = 40; min_x = -2;max_x = 8; }
		// if(btag.first == "newSimpleSecondaryVertexHighPurBJetTags")       {nbins = 40; min_x = -2;max_x = 8; }
		// //	if(btag.first == "impactParameterMVABJetTags")          {nbins = 40; min_x = 0;max_x = 1; }
		// if(btag.first == "newCombinedSecondaryVertexMVABJetTags")  {nbins = 40; min_x = 0;max_x = 1; }
		// if(btag.first == "newCombinedSecondaryVertexBJetTags")     {nbins = 40; min_x = 0;max_x = 1; }
		// //if(btag.first == "softElectronBJetTags")                {nbins = 40; min_x = -1;max_x = 1; }
		
		
		for(std::vector<std::string>::iterator flavour = flavours.begin();
		    flavour != flavours.end();flavour++){
			if(id.find("Data") != std::string::npos && *flavour != ""){
				break;}//Dont book truth matched flavour histos if running over Data

			for(std::vector<std::string>::iterator wp = wps.begin();
			    wp != wps.end();wp++){
				
				histos1d[("pTrel_mu_in_jet"+*wp+*algo_id+*flavour+id).c_str()]=histo_writer->create_1d(("pTrel_mu_in_jet"+*wp+*algo_id+*flavour+id).c_str(),("pTrel for "+*algo_id+" algo. wp: "+*wp+" MC flavour: " +*flavour).c_str(),50,0,10);
				
				histos1d[("pTrel_unoff_mu_in_jet"+*wp+*algo_id+*flavour+id).c_str()]=histo_writer->create_1d(("pTrel_unoff_mu_in_jet"+*wp+*algo_id+*flavour+id).c_str(),("pTrel for "+*algo_id+" algo. wp: "+*wp+" MC flavour: " +*flavour).c_str(),100,0,20);
			}
			if(*algo_id != ""){
				histos1d[("bDiscrim_mu_in_jet"+*algo_id+*flavour+id).c_str()]=histo_writer->create_1d(("bDiscrim_mu_in_jet"+*algo_id+*flavour+id).c_str(),("bDiscrim value for "+*algo_id+" algo. MC flavour: " +*flavour).c_str(),nbins,min_x,max_x, "bTag Discriminator Value");			       
			}
		}
	}

	for(std::vector<std::string>::iterator flavour = flavours.begin();
	    flavour != flavours.end();flavour++){
		if(id.find("Data") != std::string::npos && *flavour != ""){
			break;}//Dont book truth matched flavour histos if running over Data

		histos1d[("mu_pt_mu_in_jet"+*flavour+id).c_str()]=histo_writer->create_1d(("mu_pt_mu_in_jet"+*flavour+id).c_str(),("Muon in jet, muon pt. jet flavour: " +*flavour).c_str(),20,0,100, "Muon p_{T} (GeV)");
		histos1d[("mu_eta_mu_in_jet"+*flavour+id).c_str()]=histo_writer->create_1d(("mu_eta_mu_in_jet"+*flavour+id).c_str(),("Muon in jet, muon eta. jet flavour: " +*flavour).c_str(),50,-2.5,2.5, "Muon #eta ");
		histos1d[("mu_phi_mu_in_jet"+*flavour+id).c_str()]=histo_writer->create_1d(("mu_phi_mu_in_jet"+*flavour+id).c_str(),("Muon in jet, muon phi. jet flavour: " +*flavour).c_str(),35,-3.5,3.5, "Muon #phi (rad)");
		histos1d[("jet_pt_mu_in_jet"+*flavour+id).c_str()]=histo_writer->create_1d(("jet_pt_mu_in_jet"+*flavour+id).c_str(),("Muon in jet, jet pt. jet flavour: " +*flavour).c_str(),40,0,200, "Jet p_{T} (GeV)");
		histos1d[("jet_eta_mu_in_jet"+*flavour+id).c_str()]=histo_writer->create_1d(("jet_eta_mu_in_jet"+*flavour+id).c_str(),("Muon in jet, jet eta. jet flavour: " +*flavour).c_str(),50,-2.5,2.5, "Jet #eta ");
		histos1d[("jet_phi_mu_in_jet"+*flavour+id).c_str()]=histo_writer->create_1d(("jet_phi_mu_in_jet"+*flavour+id).c_str(),("Muon in jet, jet phi. jet flavour: " +*flavour).c_str(),35,-3.5,3.5, "Jet #phi (rad)");
		histos1d[("dR_mu_in_jet"+*flavour+id).c_str()]=histo_writer->create_1d(("dR_mu_in_jet"+*flavour+id).c_str(),("Muon in jet, dR(jet,mu). jet flavour: " +*flavour).c_str(),30,-0.1,0.5, "#Delta R(jet,mu)");
		histos1d[("dR_jet_TPjet"+*flavour+id).c_str()]=histo_writer->create_1d(("dR_jet_TPjet"+*flavour+id).c_str(),("dR(jet, TPjet). jet flavour: " +*flavour).c_str(),30,-0.1,0.5, "#Delta R(jet,TPjet)");

		histos2d[("mu_in_jet_pt_vs_dPt_TPjet_jet"+*flavour+id).c_str()]=histo_writer->create_2d(("mu_in_jet_pt_vs_dPt_TPjet_jet"+*flavour+id).c_str(),"Muon in jet pt vs TPjet pt minus jet pt",25,0,100,250,0,100, "Muon p_{T} (GeV)", "TP Jet - Jet p_{T} (GeV)");
	}
				
}

bool BTagPTRelPlots::select_muon_and_jet()
{
	if(handle_holder == NULL){
		std::cerr << "ERROR: BTagPTRelPlots plot_mu_in_jet_relative_pt: handle_holder NULL" << std::endl;
		return false;
	}

	//Select muons in bjets
	std::vector<mor::Muon> *passing_mu = 0;
	if(handle_holder->get_selected_noniso_muons() && handle_holder->get_selected_nomu_jets()){
		passing_mu = handle_holder->get_selected_noniso_muons();
	}else{
		std::cout << "WARNING: process_noniso_muons is false. Not plotting BTagPTRel plots" << std::endl;
		return false;
	}

	//Find corresponding muon and jet
	for(std::vector<mor::Jet>::iterator jet = handle_holder->get_selected_nomu_jets()->begin();
	    jet != handle_holder->get_selected_nomu_jets()->end();++jet){
		for(std::vector<mor::Muon>::iterator muon = passing_mu->begin();
		    muon!=passing_mu->end();++muon){
			double dR = ROOT::Math::VectorUtil::DeltaR(*jet, *muon);

			if(dR < 0.4){
				found_mu = *muon;
				found_jet = *jet;
				double min_TPjet_dR = -1;
				//Find top projection jet corresponding to found jet
				for(std::vector<mor::Jet>::iterator TPjet = jets->begin();
				    TPjet != jets->end();++TPjet){
					double TPjet_dR = ROOT::Math::VectorUtil::DeltaR(*jet, *TPjet);
					if(min_TPjet_dR  == -1 || TPjet_dR < min_TPjet_dR){
						min_TPjet_dR = TPjet_dR;
						found_TPmu_jet = *TPjet;
					}
				}
				return true;
			}
		}
			
	}

	histos1d[("dR_mu_in_jet"+id).c_str()]->Fill(-0.11);
	return false;
}
void BTagPTRelPlots::calculate_pt_rel()
{
	//Caculate pTrel 
	const TVector3 mu_v3(found_mu.px(),found_mu.py(),found_mu.pz());  
	const TVector3 jet_v3(found_jet.px(),found_jet.py(),found_jet.pz());  

	//Interpretation of equation in AN-11-503
	TVector3 cross_mu_jet = mu_v3.Cross(jet_v3);
	unoff_pTrel = cross_mu_jet.Mag()/jet_v3.Mag();

	//Taken from RecoBTag/PerformanceMeasurements/plugins/PerformanceAnalyser.cc
	TVector3 jet_mu_v3 = jet_v3;
	jet_mu_v3 += mu_v3;
	pTrel = mu_v3.Perp(jet_mu_v3);


}

void BTagPTRelPlots::plot_pt_rel(std::string working_point,std::string algo)
{

	//If MC: flavour match jets and fill according histos
	if(!(handle_holder->get_event_information()->is_real_data())){
		if(fabs(found_jet.mc_match_id()) == 5){//bjets
			plot_pt_rel_flavour(working_point,"_b",algo);
			
		}else{//cjets && light jets
			plot_pt_rel_flavour(working_point,"_udsc",algo);
		}
		
		if(fabs(found_jet.mc_match_id()) == 4){//cjets
			plot_pt_rel_flavour(working_point,"_c",algo);
			
		}else if(!(fabs(found_jet.mc_match_id()) == 5)){//light jets
			plot_pt_rel_flavour(working_point,"_uds",algo);
			
		}
	}
	//If Data or MC: fill histogram, no flavour matching.
	plot_pt_rel_flavour(working_point,"",algo);

}

void BTagPTRelPlots::plot_pt_rel_btagged()
{
	std::vector<std::string> btag_algos;
	btag_algos.push_back("trackCountingHighEffBJetTags");
	btag_algos.push_back("trackCountingHighPurBJetTags");
	btag_algos.push_back("jetProbabilityBJetTags");
	btag_algos.push_back("jetBProbabilityBJetTags");
	btag_algos.push_back("simpleSecondaryVertexHighEffBJetTags");
	btag_algos.push_back("simpleSecondaryVertexHighPurBJetTags");
	btag_algos.push_back("combinedSecondaryVertexBJetTags");
	btag_algos.push_back("combinedSecondaryVertexMVABJetTags");
				      
	//Warning: algo_ids set in book_histos. Must have same nentries as these vectors.
	std::vector<double> loose_wps;
	loose_wps.push_back(10.2);
	loose_wps.push_back(3.41);
	loose_wps.push_back(0.790);
	loose_wps.push_back(3.74);
	loose_wps.push_back(3.05);
	loose_wps.push_back(2.0);
	loose_wps.push_back(0.898);
	loose_wps.push_back(1.0);//no rec

	std::vector<double> medium_wps;
	medium_wps.push_back(3.3);
	medium_wps.push_back(1.93);
	medium_wps.push_back(0.545);
	medium_wps.push_back(2.55);
	medium_wps.push_back(1.74);
	medium_wps.push_back(1.0);//no rec
	medium_wps.push_back(0.679);
	medium_wps.push_back(0.5);//no rec

	std::vector<double> tight_wps;
	tight_wps.push_back(1.7);
	tight_wps.push_back(1.19);
	tight_wps.push_back(0.275);
	tight_wps.push_back(1.33);
	tight_wps.push_back(1.0);//no rec
	tight_wps.push_back(0.5);//no rec
	tight_wps.push_back(0.244);
	tight_wps.push_back(0.2);//no rec

	if((btag_algos.size() != algo_ids.size()) ||
	   (algo_ids.size() != loose_wps.size()) ||
	   (loose_wps.size() != medium_wps.size()) || 
	   (medium_wps.size() != tight_wps.size())){
		std::cerr << "ERROR: BTagPTRelPlots plot_pt_rel_btagged. Vector sizes inconsistent " << std::endl;
		return;
	}

	for(int i = 0; i < (int) btag_algos.size();i++){

		double bDiscrim = found_TPmu_jet.bDiscriminator(btag_algos[i]);

		//fill bDiscrim plot. unnecessary but informative
		plot_bDiscrim(bDiscrim,algo_ids[i]);


		if(verbose) std::cout << "Algo: " << btag_algos[i] << " bDiscrim: " << bDiscrim << std::endl;
		if(verbose) std::cout << " id: " << algo_ids[i] << std::endl;
		if(verbose) std::cout << " loosewp: " << loose_wps[i];
		if(verbose) std::cout << " mediumwp: " << medium_wps[i];
		if(verbose) std::cout << " tightwp: " << tight_wps[i] << std::endl;

		if(bDiscrim > tight_wps[i]){
			plot_pt_rel("_tightwp",algo_ids[i]);
		}
		if(bDiscrim > medium_wps[i]){
			plot_pt_rel("_mediumwp",algo_ids[i]);
		}
		if(bDiscrim > loose_wps[i]){
			plot_pt_rel("_loosewp",algo_ids[i]);
		}

	}

}

void BTagPTRelPlots::plot_bDiscrim(double bDiscrim,std::string algo)
{
	//If MC: flavour match jets and fill according histos
	if(!(handle_holder->get_event_information()->is_real_data())){
		if(fabs(found_jet.mc_match_id()) == 5){//bjets
				plot_bDiscrim_flavour(bDiscrim,"_b",algo);

			}else{//cjets && light jets
				plot_bDiscrim_flavour(bDiscrim,"_udsc",algo);
			}

		if(fabs(found_jet.mc_match_id()) == 4){//cjets
				plot_bDiscrim_flavour(bDiscrim,"_c",algo);

		}else if(!(fabs(found_jet.mc_match_id()) == 5)){//light jets
				plot_bDiscrim_flavour(bDiscrim,"_uds",algo);

			}


	}
	//If Data or MC: fill histogram, no flavour matching.
	plot_bDiscrim_flavour(bDiscrim,"",algo);

}

void BTagPTRelPlots::plot_misc()
{
	//Fill unnecessary but informative histograms
	//If MC: flavour match jets and fill according histos
	if(!(handle_holder->get_event_information()->is_real_data())){
		if(fabs(found_jet.mc_match_id()) == 5){//bjets
				plot_jet_mu_vars_flavour("_b");

			}else{//cjets && light jets
				plot_jet_mu_vars_flavour("_udsc");
			}

	if(fabs(found_jet.mc_match_id()) == 4){//cjets
				plot_jet_mu_vars_flavour("_c");

	}else if(!(fabs(found_jet.mc_match_id()) == 5)){//light jets
				plot_jet_mu_vars_flavour("_uds");

			}

	}
	//If Data or MC: fill histogram, no flavour matching.
	plot_jet_mu_vars_flavour("");

}

void BTagPTRelPlots::plot_pt_rel_flavour(std::string wp,std::string flavour,std::string algo)
{
	histos1d[("pTrel_mu_in_jet"+wp+algo+flavour+id).c_str()]->Fill(pTrel);
	histos1d[("pTrel_unoff_mu_in_jet"+wp+algo+flavour+id).c_str()]->Fill(unoff_pTrel);
}

void BTagPTRelPlots::plot_bDiscrim_flavour(double bDiscrim,std::string flavour,std::string algo)
{
	histos1d[("bDiscrim_mu_in_jet"+algo+flavour+id).c_str()]->Fill(bDiscrim);
}

void BTagPTRelPlots::plot_jet_mu_vars_flavour(std::string flavour)
{
	histos1d[("mu_pt_mu_in_jet"+flavour+id).c_str()]->Fill(found_mu.Pt());
	histos1d[("mu_eta_mu_in_jet"+flavour+id).c_str()]->Fill(found_mu.Eta());
	histos1d[("mu_phi_mu_in_jet"+flavour+id).c_str()]->Fill(found_mu.Phi());
	histos1d[("jet_pt_mu_in_jet"+flavour+id).c_str()]->Fill(found_jet.Pt());
	histos1d[("jet_eta_mu_in_jet"+flavour+id).c_str()]->Fill(found_jet.Eta());
	histos1d[("jet_phi_mu_in_jet"+flavour+id).c_str()]->Fill(found_jet.Phi());

        double dR = ROOT::Math::VectorUtil::DeltaR(found_jet,found_mu);
        histos1d[("dR_mu_in_jet"+flavour+id).c_str()]->Fill(dR);
        double dR_TPjet = ROOT::Math::VectorUtil::DeltaR(found_jet,found_TPmu_jet);
        histos1d[("dR_jet_TPjet"+flavour+id).c_str()]->Fill(dR_TPjet);

	double dPt = found_jet.Pt() - found_TPmu_jet.Pt();
	histos2d[("mu_in_jet_pt_vs_dPt_TPjet_jet"+flavour+id).c_str()]->Fill(found_mu.Pt(),dPt);


}
