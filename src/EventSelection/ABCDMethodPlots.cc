#include "../../interface/EventSelection/ABCDMethodPlots.h"

ABCDMethodPlots::ABCDMethodPlots(std::string ident)
{
	id = ident;
}

ABCDMethodPlots::~ABCDMethodPlots()
{
}

void ABCDMethodPlots::plot_all()
{
	plot_leptIso_muon();
	plot_leptIso_electron();
	plot_d0sig_vs_relIso();
}

void ABCDMethodPlots::book_histos()
{
	// file service to write histos automatically to root files
	// ABCD method plots
	histos2d[("isolation_vs_met_muon"+id).c_str()]=histo_writer->create_2d(("isolation_vs_met_muon"+id).c_str(),"lepton isolation vs MET",50, 0, 50, 100,0,100);
	histos2d[("isolation_vs_met_electron"+id).c_str()]=histo_writer->create_2d(("isolation_vs_met_electron"+id).c_str(),"lepton isolation vs MET",50, 0, 50, 100,0,100);
	histos2d[("d0sig_vs_comb_rel_iso_electron"+id).c_str()]=histo_writer->create_2d(("d0sig_vs_comb_rel_iso_electron"+id).c_str(),"d0 significance over combined relative isolation",100, 0, 1, 100,0,10,"rel.Iso","d_{0}/#sigma_{d_{0}}");
	histos2d[("d0sig_vs_comb_rel_iso_muon"+id).c_str()]=histo_writer->create_2d(("d0sig_vs_comb_rel_iso_muon"+id).c_str(),"d0 significance over combined relative isolation",100, 0, 1, 100,0,10,"rel.Iso","d_{0}/#sigma_{d_{0}}");
	histos2d[("d0_vs_calo_iso_muon"+id).c_str()]=histo_writer->create_2d(("d0_vs_calo_iso_muon"+id).c_str(),"d0 vs calo iso of muons",60., 0, 60., 100,0,0.15,"calo Iso [GeV]","d_{0} [cm]");
	histos2d[("d0_vs_calo_iso_electron"+id).c_str()]=histo_writer->create_2d(("d0_vs_calo_iso_electron"+id).c_str(),"d0 vs calo iso of electrons",60., 0, 60., 100,0,0.15,"calo Iso [GeV]","d_{0} [cm]");
}

// For estimation of QCD usingABCD method, plot lepton isolation vs MET
void ABCDMethodPlots::plot_leptIso_muon()
{
	if(corrected_mets->size() < 1 || isolated_muons->size() < 1)
		return;

	double event_met = 0.0, lepton_isolation = 0.0;
	for(std::vector<mor::MET>::iterator met_iter = corrected_mets->begin();
	    met_iter!=corrected_mets->end();
	    ++met_iter){
		event_met=met_iter->Et(); 
	}
	
	std::vector<mor::Muon>::iterator muon_iter = isolated_muons->begin();
	lepton_isolation = muon_iter->trackIso();
	
	histos2d[("isolation_vs_met_muon"+id).c_str()]->Fill(lepton_isolation, event_met);
}

// For estimation of QCD usingABCD method, plot lepton isolation vs MET
void ABCDMethodPlots::plot_leptIso_electron()
{
	if(corrected_mets->size() < 1 || isolated_electrons->size() < 1)
		return;

	double event_met = 0.0, lepton_isolation = 0.0;
	for(std::vector<mor::MET>::iterator met_iter = corrected_mets->begin();
                met_iter!=corrected_mets->end();
                ++met_iter){
		event_met=met_iter->Et(); 
	}
	
	std::vector<mor::Electron>::iterator electron_iter = isolated_electrons->begin();
	lepton_isolation = electron_iter->trackIso();
	
	histos2d[("isolation_vs_met_electron"+id).c_str()]->Fill(lepton_isolation, event_met);
}

void ABCDMethodPlots::plot_d0sig_vs_relIso()
{
	for(std::vector<mor::Electron>::iterator electron_iter = isolated_electrons->begin();
		electron_iter != isolated_electrons->end();
		++electron_iter){
		histos2d[("d0sig_vs_comb_rel_iso_electron"+id).c_str()]->Fill(electron_iter->relIso(), electron_iter->d0()/electron_iter->d0_sigma());

		histos2d[("d0_vs_calo_iso_electron"+id).c_str()]->Fill(electron_iter->caloIso(), fabs(electron_iter->d0()));
	}
	for(std::vector<mor::Muon>::iterator muon_iter = isolated_muons->begin();
		muon_iter != isolated_muons->end();
		++muon_iter){
		histos2d[("d0sig_vs_comb_rel_iso_muon"+id).c_str()]->Fill(muon_iter->relIso(), muon_iter->d0()/muon_iter->d0_sigma());
		histos2d[("d0_vs_calo_iso_muon"+id).c_str()]->Fill(muon_iter->caloIso(), fabs(muon_iter->d0()));
	}
}
