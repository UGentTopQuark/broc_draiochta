#include "../../interface/EventSelection/BTagEfficiencyWeightProvider.h"

broc::BTagEfficiencyWeightProvider::BTagEfficiencyWeightProvider(eire::HandleHolder *handle_holder)
{
	if(handle_holder->get_ident().find("Data") != std::string::npos)
		do_not_reweight = true;
	else
		do_not_reweight = false;

	jets = handle_holder->get_tight_jets();
	gen_evt = handle_holder->get_ttbar_gen_evt();

	double study_unc = handle_holder->get_cuts_set()->get_cut_value("study_BTag_unc");
	eire::ConfigReader *config_reader = handle_holder->get_config_reader();
	working_point = config_reader->get_var("working_point","b_tagging",false);
	apply_sync_factor = config_reader->get_bool_var("apply_sync_factor","b_tagging",false);

	if(apply_sync_factor){
		std::cout << "Applying btag sync factor weights.  Warning - calibrated only for 2xCSVT and MEM Selection!" << std::endl;
	}

	sample = config_reader->get_var("sample","b_tagging",false);	
	if( !( (sample == "MadGraph" && (working_point=="CSVT"||sample=="CSVM") ) || (sample=="MC@NLO"&&(working_point=="CSVT"||sample=="CSVM")) || (sample=="MadSpin"&&(working_point=="CSVT" || working_point=="CSVM")) ))
	    std::cout<<"WARNING: Working point and/or sample not supported. Applying a weight of 1."<<std::endl;

	

	up = false;
	down = false;

	if(study_unc == 1){ down = true; }
	else if(study_unc == 2){ up = true; }
}

broc::BTagEfficiencyWeightProvider::~BTagEfficiencyWeightProvider()
{
}

bool broc::BTagEfficiencyWeightProvider::is_true_b(mor::Jet jet)
{
  bool is_bjet = false;
  mor::Jet *b_had = gen_evt->hadB_jet();
  mor::Jet *b_lep = gen_evt->lepB_jet();
  double dR_had = sqrt(acos(cos((jet.phi() - b_had->phi())*(jet.phi() - b_had->phi()))) + (jet.eta() - b_had->eta())*(jet.eta() - b_had->eta()));
  double dR_lep = sqrt(acos(cos((jet.phi() - b_lep->phi())*(jet.phi() - b_lep->phi()))) + (jet.eta() - b_lep->eta())*(jet.eta() - b_lep->eta()));
  if(dR_had < 0.3 || dR_lep < 0.3){is_bjet = true;}
  //std::cout<<"b_had pt = "<<b_had->pt()<<" phi = "<<b_had->phi()<<" eta = "<<b_had->eta()<<std::endl;
  //std::cout<<"b_lep pt = "<<b_lep->pt()<<" phi = "<<b_lep->phi()<<" eta = "<<b_lep->eta()<<std::endl;
  //std::cout<<"jet pt = "<<jet.pt()<<" phi = "<<jet.phi()<<" eta = "<<jet.eta()<<std::endl;
  //std::cout<<"gen jet pt = "<<jet.pt_genjet()<<" phi = "<<jet.phi_genjet()<<" eta = "<<jet.eta_genjet()<<std::endl;
  return is_bjet;
}

double broc::BTagEfficiencyWeightProvider::get_Data_Prob(int UpDown_b, int UpDown_light)
{
  double Prob = 1;
  for(unsigned int i = 0; i < jets->size(); i++){
      if((*jets)[i].is_bjet()){//tagged jet
		  if(fabs((*jets)[i].mc_match_id()) == 5){
			  Prob *= get_SF((*jets)[i].pt(), UpDown_b)*get_BTag_eff((*jets)[i].pt(),(*jets)[i].eta(), UpDown_b); //true b
		  }else if(fabs((*jets)[i].mc_match_id()) == 4){
			  Prob *= get_SF((*jets)[i].pt(), 2*UpDown_b)*get_BTag_eff_c((*jets)[i].pt(),(*jets)[i].eta(), UpDown_b); //true c
		  }else{
			  Prob *= get_SF_light((*jets)[i].pt(), (*jets)[i].eta(), UpDown_light)*get_BTag_eff_light((*jets)[i].pt(),(*jets)[i].eta(), UpDown_light);//true light
		  }
	  }else{//non tagged jets
		  if(fabs((*jets)[i].mc_match_id()) == 5){
			  Prob *= (1. - get_SF((*jets)[i].pt(), UpDown_b)*get_BTag_eff((*jets)[i].pt(),(*jets)[i].eta(), UpDown_b));//true b  
		  }else if(fabs((*jets)[i].mc_match_id()) == 4){
			  Prob *= (1. - get_SF((*jets)[i].pt(), 2*UpDown_b)*get_BTag_eff_c((*jets)[i].pt(),(*jets)[i].eta(), UpDown_b)); //true c
		  }else{
			  Prob *= (1. - get_SF_light((*jets)[i].pt(), (*jets)[i].eta(), UpDown_light)*get_BTag_eff_light((*jets)[i].pt(),(*jets)[i].eta(), UpDown_light));//true light
		  }
	  } 
  }
  return Prob;
}

double broc::BTagEfficiencyWeightProvider::get_MC_Prob(int UpDown_b, int UpDown_light)
{
  double Prob = 1;
  for(unsigned int i = 0; i < jets->size(); i++){
	  if((*jets)[i].is_bjet()){
		  if(fabs((*jets)[i].mc_match_id()) == 5){
			  Prob *= get_BTag_eff((*jets)[i].pt(),(*jets)[i].eta(), UpDown_b);//true b 
		  }else if (fabs((*jets)[i].mc_match_id()) == 4){
			  Prob *= get_BTag_eff_c((*jets)[i].pt(),(*jets)[i].eta(), UpDown_light);//true c
		  }else{
			  Prob *= get_BTag_eff_light((*jets)[i].pt(),(*jets)[i].eta(), UpDown_light);//true light
		  }
	  }else{
		  if(fabs((*jets)[i].mc_match_id()) == 5){
			  Prob *= 1 - get_BTag_eff((*jets)[i].pt(),(*jets)[i].eta(), UpDown_b);//true b 
		  }else if (fabs((*jets)[i].mc_match_id()) == 4){Prob *= 1 - get_BTag_eff_c((*jets)[i].pt(),(*jets)[i].eta(), UpDown_light);//true c
		  }else{
			  Prob *= 1 - get_BTag_eff_light((*jets)[i].pt(),(*jets)[i].eta(), UpDown_light);//true light
		  }
	  }
  }
  return Prob;
}

double broc::BTagEfficiencyWeightProvider::get_SF(double pt, int UpDown)
{
    if(working_point == "CSVM"){
		//CSVM with eta < 2.4
		double SFb = (0.938887+(0.00017124*pt))+(-2.76366e-07*(pt*pt));
		double ud = 0.;
		if(up || UpDown == 1) ud = 1.;
		else if(down || UpDown == -1) ud = -1;
		if(pt < 20.){ SFb = SFb + ud*0.0415707*2;}
		else if(pt < 30){ SFb = SFb + ud*0.0415707;}
		else if(pt < 40){ SFb = SFb + ud*0.0204209;}
		else if(pt < 50){ SFb = SFb + ud*0.0223227;}
		else if(pt < 60){ SFb = SFb + ud*0.0206655;}
		else if(pt < 70){ SFb = SFb + ud*0.0199325;}
		else if(pt < 80){ SFb = SFb + ud*0.0174121;}
		else if(pt < 100){ SFb = SFb + ud*0.0202332;}
		else if(pt < 120){ SFb = SFb + ud*0.0182446;}
		else if(pt < 160){ SFb = SFb + ud*0.0159777;}
		else if(pt < 210){ SFb = SFb + ud*0.0218531;}
		else if(pt < 260){ SFb = SFb + ud*0.0204688;}
		else if(pt < 320){ SFb = SFb + ud*0.0265191;}
		else if(pt < 400){ SFb = SFb + ud*0.0313175;}
		else if(pt < 500){ SFb = SFb + ud*0.0415417;}
		else if(pt < 600){ SFb = SFb + ud*0.0740446;}
		else if(pt < 800){ SFb = SFb + ud*0.0596716;}
		else{ SFb = SFb + ud*0.0596716*2;}
		return SFb;}
    else if(working_point == "CSVT"){
		double SFb = (0.9203+(-3.32421e-05*pt))+(-7.74664e-08*(pt*pt));
		double ud = 0.;
		if(up || UpDown == 1) ud = 1.;
        else if(down || UpDown == -1) ud = -1;
        if(pt < 20.){ SFb = SFb + ud*0.0511028*2;}
        else if(pt < 30){ SFb = SFb + ud*0.0511028;}
        else if(pt < 40){ SFb = SFb + ud*0.0306671;}
        else if(pt < 50){ SFb = SFb + ud*0.0317498;}
        else if(pt < 60){ SFb = SFb + ud*0.032779;}
        else if(pt < 70){ SFb = SFb + ud*0.0291528;}
        else if(pt < 80){ SFb = SFb + ud*0.0249308;}
        else if(pt < 100){ SFb = SFb + ud*0.0301118;}
        else if(pt < 120){ SFb = SFb + ud*0.032047;}
        else if(pt < 160){ SFb = SFb + ud*0.0348072;}
        else if(pt < 210){ SFb = SFb + ud*0.0357745;}
        else if(pt < 260){ SFb = SFb + ud*0.0378756;}
        else if(pt < 320){ SFb = SFb + ud*0.0412608;}
        else if(pt < 400){ SFb = SFb + ud*0.0777516;}
        else if(pt < 500){ SFb = SFb + ud*0.0860741;}
        else if(pt < 600){ SFb = SFb + ud*0.0942209;}
        else if(pt < 800){ SFb = SFb + ud*0.104106;}
        else{ SFb = SFb + ud*0.104106*2;}
        return SFb;}
    else
		return 1.;
}

double broc::BTagEfficiencyWeightProvider::get_SF_light(double pt, double eta, int UpDown)
{
  if(working_point == "CSVM"){
      double SFb = 1.;
      int ud = 0;
      if(up || UpDown == 1) ud = 1;
      else if(down || UpDown == -1) ud = -1;
      if(pt > 20){
	  if(fabs(eta) < 0.8){
	      switch (ud){
	      case 0:
		  SFb = ((1.07541+(0.00231827*pt))+(-4.74249e-06*(pt*pt)))+(2.70862e-09*(pt*(pt*pt)));
		  break;
	      case 1:
		  SFb = ((1.18638+(0.00314148*pt))+(-6.68993e-06*(pt*pt)))+(3.89288e-09*(pt*(pt*pt)));
		  break;
	      case -1:
		  SFb = ((0.964527+(0.00149055*pt))+(-2.78338e-06*(pt*pt)))+(1.51771e-09*(pt*(pt*pt)));
		  break;
	      }
	  }
	  else if(fabs(eta) < 1.6){
	      switch (ud){
	      case 0:
		  SFb = ((1.05613+(0.00114031*pt))+(-2.56066e-06*(pt*pt)))+(1.67792e-09*(pt*(pt*pt)));
		  break;
	      case 1:
		  SFb = ((1.16624+(0.00151884*pt))+(-3.59041e-06*(pt*pt)))+(2.38681e-09*(pt*(pt*pt)));
		  break;
	      case -1:
		  SFb = ((0.946051+(0.000759584*pt))+(-1.52491e-06*(pt*pt)))+(9.65822e-10*(pt*(pt*pt)));
		  break;
	      }
	  }
	  else if(fabs(eta) < 2.4){
	      switch (ud){
	      case 0:
		  SFb = ((1.05625+(0.000487231*pt))+(-2.22792e-06*(pt*pt)))+(1.70262e-09*(pt*(pt*pt)));
		  break;
	      case 1:
		  SFb = ((1.15575+(0.000693344*pt))+(-3.02661e-06*(pt*pt)))+(2.39752e-09*(pt*(pt*pt)));
		  break;
	      case -1:
		  SFb = ((0.956736+(0.000280197*pt))+(-1.42739e-06*(pt*pt)))+(1.0085e-09*(pt*(pt*pt)));
		  break;
	      }
	  }
      }
      return SFb;}
  else if(working_point == "CSVT"){
      double SFb = 1.;
      int ud = 0;
      if(up || UpDown == 1) ud = 1;
      else if(down || UpDown == -1) ud = -1;
      if(pt > 20){
	  switch (ud){
	  case 0:
	      SFb = ((1.00462+(0.00325971*pt))+(-7.79184e-06*(pt*pt)))+(5.22506e-09*(pt*(pt*pt)));
	      break;
	  case 1:
	      SFb = ((1.16361+(0.00464695*pt))+(-1.09467e-05*(pt*pt)))+(7.21896e-09*(pt*(pt*pt)));
	      break;
	  case -1:
	      SFb = ((0.845757+(0.00186422*pt))+(-4.6133e-06*(pt*pt)))+(3.21723e-09*(pt*(pt*pt)));
	      break;
	  }
      }
      return SFb;}
  else
      return 1.;
}

double broc::BTagEfficiencyWeightProvider::get_BTag_eff(double pt, double eta, int UpDown)
{
    double eff = 0.;
    double ud = 0.;
    if(up || UpDown == 1) ud = 1.;
    else if(down || UpDown == -1) ud = -1;
    if(working_point == "CSVM" && sample == "MC@NLO"){
	//5.3.X TTbar MCatNLO
	if(fabs(eta) < 0.4){
            if(pt < 20){eff = 55.7721+ 2*ud*0.573758;}
	    else if(pt < 30){eff = 55.7721+ ud*0.573758;}
	    else if(pt < 40){eff = 62.284+ ud*0.326405;}
	    else if(pt < 50){eff = 65.1263+ ud*0.28624;}
	    else if(pt < 60){eff = 68.9956+ ud*0.273369;}
	    else if(pt < 70){eff = 70.8129+ ud*0.280263;}
	    else if(pt < 80){eff = 72.9647+ ud*0.295269;}
	    else if(pt < 90){eff = 73.5929+ ud*0.324154;}
	    else if(pt < 100){eff = 74.264+ ud*0.360065;}
	    else if(pt < 120){eff = 74.2698+ ud*0.301252;}
	    else if(pt < 160){eff = 74.6826+ ud*0.302711;}
	    else if(pt < 210){eff = 70.4054+ ud*0.502919;}
	    else if(pt < 260){eff = 67.2112+ ud*0.937016;}
	    else if(pt < 320){eff = 62.4327+ ud*1.58892;}
	    else if(pt < 400){eff = 57.3407+ ud*2.60306;}
	    else if(pt > 400){eff = 57.3407+ ud*2.60306*2;}
	}
	else if(fabs(eta) < 0.8){
	    if(pt < 20){eff = 55.1456+ ud*0.58572;}
	    else if(pt < 30){eff = 55.1456+ ud*0.58572;}
	    else if(pt < 40){eff = 62.3004+ ud*0.33869;}
	    else if(pt < 50){eff = 65.888+ ud*0.298303;}
	    else if(pt < 60){eff = 70.2153+ ud*0.284072;}
	    else if(pt < 70){eff = 71.8819+ ud*0.290315;}
	    else if(pt < 80){eff = 73.686+ ud*0.307898;}
	    else if(pt < 90){eff = 74.315+ ud*0.337193;}
	    else if(pt < 100){eff = 75.408+ ud*0.374307;}
	    else if(pt < 120){eff = 75.1476+ ud*0.315125;}
	    else if(pt < 160){eff = 74.6173+ ud*0.319517;}
	    else if(pt < 210){eff = 72.2444+ ud*0.507841;}
	    else if(pt < 260){eff = 67.7101+ ud*0.968268;}
	    else if(pt < 320){eff = 62.6682+ ud*1.6195;}
	    else if(pt < 400){eff = 52.819+ ud*2.71934;}
	    else if(pt > 400){eff = 52.819+ ud*2.71934*2;}
	}
	else if(fabs(eta) < 1.2){
	    if(pt < 20){eff = 52.5571+ 2*ud*0.616054;}
	    else if(pt < 30){eff = 52.5571+ ud*0.616054;}
	    else if(pt < 40){eff = 59.1713+ ud*0.366559;}
	    else if(pt < 50){eff = 62.7912+ ud*0.331958;}
	    else if(pt < 60){eff = 66.7471+ ud*0.324086;}
	    else if(pt < 70){eff = 69.1065+ ud*0.331386;}
	    else if(pt < 80){eff = 70.5649+ ud*0.352945;}
	    else if(pt < 90){eff = 71.8663+ ud*0.39109;}
	    else if(pt < 100){eff = 73.2615+ ud*0.430211;}
	    else if(pt < 120){eff = 72.566+ ud*0.36511;}
	    else if(pt < 160){eff = 72.6768+ ud*0.371091;}
	    else if(pt < 210){eff = 68.7673+ ud*0.60895;}
	    else if(pt < 260){eff = 65.1791+ ud*1.11824;}
	    else if(pt < 320){eff = 60.64+ ud*1.95419;}
	    else if(pt < 400){eff = 50.5455+ ud*3.01493;}
	    else if(pt > 400){eff = 50.5455+ ud*3.01493*2;}
	}
	else if(fabs(eta) < 1.6){
	    if (pt < 20){eff = 47.7885+ 2*ud*0.650252;}
	    else if(pt < 30){eff = 47.7885+ ud*0.650252;}
	    else if(pt < 40){eff = 53.9983+ ud*0.406683;}
	    else if(pt < 50){eff = 56.9424+ ud*0.384224;}
	    else if(pt < 60){eff = 61.3982+ ud*0.384636;}
	    else if(pt < 70){eff = 64.0172+ ud*0.403419;}
	    else if(pt < 80){eff = 64.9721+ ud*0.44509;}
	    else if(pt < 90){eff = 66.0037+ ud*0.492499;}
	    else if(pt < 100){eff = 67.453+ ud*0.548735;}
	    else if(pt < 120){eff = 67.1154+ ud*0.467416;}
	    else if(pt < 160){eff = 67.2584+ ud*0.483835;}
	    else if(pt < 210){eff = 61.9086+ ud*0.798448;}
	    else if(pt < 260){eff = 57.4796+ ud*1.48856;}
	    else if(pt < 320){eff = 54.8969+ ud*2.52616;}
	    else if(pt < 400){eff = 48.1013+ ud*3.97492;}
	    else if(pt > 400){eff = 48.1013+ ud*3.97492*2;}
	}
	else if(fabs(eta) < 2){
	    if(pt < 20){eff = 47.7537+ 2*ud*0.770096;}
	    else if(pt < 30){eff = 47.7537+ ud*0.770096;}
	    else if(pt < 40){eff = 55.342+ ud*0.484903;}
	    else if(pt < 50){eff = 57.7296+ ud*0.468326;}
	    else if(pt < 60){eff = 61.9113+ ud*0.480021;}
	    else if(pt < 70){eff = 64.2016+ ud*0.510181;}
	    else if(pt < 80){eff = 65.0716+ ud*0.562278;}
	    else if(pt < 90){eff = 66.2167+ ud*0.630348;}
	    else if(pt < 100){eff = 67.2795+ ud*0.715762;}
	    else if(pt < 120){eff = 66.7436+ ud*0.627618;}
	    else if(pt < 160){eff = 65.1459+ ud*0.666788;}
	    else if(pt < 210){eff = 61.0587+ ud*1.11632;}
	    else if(pt < 260){eff = 58.8583+ ud*2.1833;}
	    else if(pt < 320){eff = 53.7975+ ud*3.9663;}
	    else if(pt < 400){eff = 51.7241+ ud*6.56142;}
	    else if(pt > 400){eff = 51.7241+ ud*6.56142*2;}
	}
	else if(fabs(eta) < 2.4){
	    if(pt < 20){eff = 39.0397+ 2*ud*0.887715;}
	    else if(pt < 30){eff = 39.0397+ ud*0.887715;}
	    else if(pt < 40){eff = 47.9605+ ud*0.593524;}
	    else if(pt < 50){eff = 51.2369+ ud*0.595986;}
	    else if(pt < 60){eff = 52.7321+ ud*0.624701;}
	    else if(pt < 70){eff = 56.3927+ ud*0.692833;}
	    else if(pt < 80){eff = 56.3017+ ud*0.773699;}
	    else if(pt < 90){eff = 58.7055+ ud*0.88574;}
	    else if(pt < 100){eff = 57.7208+ ud*1.03029;}
	    else if(pt < 120){eff = 59.298+ ud*0.902531;}
	    else if(pt < 160){eff = 58.7167+ ud*0.989048;}
	    else if(pt < 210){eff = 52.1144+ ud*1.76179;}
	    else if(pt < 260){eff = 48.4211+ ud*3.62557;}
	    else if(pt < 320){eff = 33.9623+ ud*6.50514;}
	    else if(pt < 400){eff = 47.3684+ ud*11.4549;}
	    else if(pt > 400){eff = 47.3684+ ud*11.4549*2;}
	}
	
	eff *= 0.01;
	return eff;}
    else if(working_point == "CSVT"&& sample == "MadGraph"){
      if(fabs(eta) < 0.4){
	if(pt < 30){eff = 45.5408+ 2*ud*0.959522;}
	else if(pt < 40){eff = 45.5408+ ud*0.959522;}
	else if(pt < 50){eff = 49.3544+ ud*0.897925;}
	else if(pt < 60){eff = 54.1579+ ud*0.898914;}
	else if(pt < 70){eff = 55.0718+ ud*0.946258;}
	else if(pt < 80){eff = 57.2997+ ud*0.998344;}
	else if(pt < 90){eff = 56.7852+ ud*1.09796;}
	else if(pt < 100){eff = 55.6872+ ud*1.20798;}
	else if(pt < 120){eff = 55.6796+ ud*1.0025;}
	else if(pt < 160){eff = 56.1867+ ud*1.01561;}
	else if(pt < 210){eff = 46.3019+ ud*1.58671;}
	else if(pt < 260){eff = 44.5297+ ud*3.0371;}
	else if(pt < 320){eff = 40.3785+ ud*4.52321;}
	else if(pt < 400){eff = 25.9261+ ud*6.30505;}
	else if(pt > 400){eff = 25.9261+ ud*6.30505*2;}
      }
      else if(fabs(eta) < 0.8){
	if(pt < 30){eff = 45.9813+ 2*ud*0.993749;}
	else if(pt < 40){eff = 45.9813+ ud*0.993749;}
	else if(pt < 50){eff = 48.8474+ ud*0.954798;}
	else if(pt < 60){eff = 56.1661+ ud*0.933798;}
	else if(pt < 70){eff = 56.108+ ud*0.987639;}
	else if(pt < 80){eff = 59.0162+ ud*1.02485;}
	else if(pt < 90){eff = 59.4698+ ud*1.14872;}
	else if(pt < 100){eff = 57.9034+ ud*1.26653;}
	else if(pt < 120){eff = 56.6174+ ud*1.08115;}
	else if(pt < 160){eff = 56.9812+ ud*1.09175;}
	else if(pt < 210){eff = 45.8531+ ud*1.66931;}
	else if(pt < 260){eff = 42.1521+ ud*3.33489;}
	else if(pt < 320){eff = 35.0746+ ud*5.0145;}
	else if(pt < 400){eff = 28.8952+ ud*7.24233;}
	else if(pt > 400){eff = 28.8952+ ud*7.24233*2;}
      }
      else if(fabs(eta) < 1.2){
	if(pt < 30){eff = 42.6661+ 2*ud*1.06523;}
	else if(pt < 40){eff = 42.6661+ ud*1.06523;}
	else if(pt < 50){eff = 46.2147+ ud*1.02028;}
	else if(pt < 60){eff = 51.0956+ ud*1.03406;}
	else if(pt < 70){eff = 50.9334+ ud*1.10756;}
	else if(pt < 80){eff = 53.0705+ ud*1.19777;}
	else if(pt < 90){eff = 56.9466+ ud*1.30462;}
	else if(pt < 100){eff = 53.9784+ ud*1.44309;}
	else if(pt < 120){eff = 53.8449+ ud*1.20207;}
	else if(pt < 160){eff = 55.1361+ ud*1.23866;}
	else if(pt < 210){eff = 43.1162+ ud*1.95663;}
	else if(pt < 260){eff = 45.9177+ ud*3.71286;}
	else if(pt < 320){eff = 35.2376+ ud*5.91386;}
	else if(pt < 400){eff = 23.3818+ ud*8.35191;}
	else if(pt > 400){eff = 23.3818+ ud*8.35191*2;}
      }
      else if(fabs(eta) < 1.6){
	if(pt < 30){eff = 37.9267+ 2*ud*1.15827;}
	else if(pt < 40){eff = 37.9267+ ud*1.15827;}
	else if(pt < 50){eff = 41.1675+ ud*1.1487;}
	else if(pt < 60){eff = 46.2141+ ud*1.18705;}
	else if(pt < 70){eff = 45.7869+ ud*1.31457;}
	else if(pt < 80){eff = 47.7574+ ud*1.46586;}
	else if(pt < 90){eff = 51.0426+ ud*1.53998;}
	else if(pt < 100){eff = 45.784+ ud*1.78049;}
	else if(pt < 120){eff = 46.5416+ ud*1.52394;}
	else if(pt < 160){eff = 47.6272+ ud*1.51784;}
	else if(pt < 210){eff = 34.8958+ ud*2.46791;}
	else if(pt < 260){eff = 33.7106+ ud*4.48318;}
	else if(pt < 320){eff = 33.1392+ ud*8.02515;}
	else if(pt < 400){eff = 14.3887+ ud*10.8442;}
	else if(pt > 400){eff = 14.3887+ ud*10.8442*2;}
      }
      else if(fabs(eta) < 2){
	if(pt < 30){eff = 36.2652+ 2*ud*1.38166;}
	else if(pt < 40){eff = 36.2652+ ud*1.38166;}
	else if(pt < 50){eff = 39.3126+ ud*1.42621;}
	else if(pt < 60){eff = 42.6655+ ud*1.48807;}
	else if(pt < 70){eff = 46.9279+ ud*1.65887;}
	else if(pt < 80){eff = 48.6735+ ud*1.85066;}
	else if(pt < 90){eff = 49.1576+ ud*2.02855;}
	else if(pt < 100){eff = 49.2026+ ud*2.46328;}
	else if(pt < 120){eff = 45.262+ ud*2.00914;}
	else if(pt < 160){eff = 44.1126+ ud*2.14372;}
	else if(pt < 210){eff = 34.3621+ ud*3.6386;}
	else if(pt < 260){eff = 31.3474+ ud*6.74493;}
	else if(pt < 320){eff = 18.1693+ ud*11.5616;}
	else if(pt < 400){eff = 17.2964+ ud*17.522;}
	else if(pt > 400){eff = 17.2964+ ud*17.522*2;}
      }
      else if(fabs(eta) < 2.4){
	if(pt < 30){eff = 28.3581+ 2*ud*1.5906;}
	else if(pt < 40){eff = 28.3581+ ud*1.5906;}
	else if(pt < 50){eff = 30.0736+ ud*1.61837;}
	else if(pt < 60){eff = 35.9009+ ud*1.91682;}
	else if(pt < 70){eff = 37.8618+ ud*1.99109;}
	else if(pt < 80){eff = 38.0374+ ud*2.33761;}
	else if(pt < 90){eff = 41.8058+ ud*2.75226;}
	else if(pt < 100){eff = 34.166+ ud*2.90747;}
	else if(pt < 120){eff = 37.3123+ ud*2.78314;}
	else if(pt < 160){eff = 41.692+ ud*3.2458;}
	else if(pt < 210){eff = 28.9992+ ud*5.09946;}
	else if(pt < 260){eff = 24.803+ ud*15.0614;}
	else if(pt < 320){eff = 17.5327+ ud*16.3853;}
	else if(pt < 400){eff = 17.5327+ ud*16.3853;}
	else if(pt > 400){eff = 17.5327+ ud*16.3853*2;}
      }
	eff *= 0.01;
	return eff;}
       else if(working_point == "CSVM" && sample == "MadSpin"){
	 if(fabs(eta) < 0.4){
	   if(pt < 30){eff = 59.4346+ 2*ud*0.579891;}
	   else if(pt < 40){eff = 59.4346+ ud*0.579891;}
	   else if(pt < 50){eff = 62.6723+ ud*0.489566;}
	   else if(pt < 60){eff = 66.2674+ ud*0.460146;}
	   else if(pt < 70){eff = 68.4895+ ud*0.461644;}
	   else if(pt < 80){eff = 71.3516+ ud*0.477023;}
	   else if(pt < 90){eff = 71.7745+ ud*0.511429;}
	   else if(pt < 100){eff = 72.9309+ ud*0.562408;}
	   else if(pt < 120){eff = 73.1892+ ud*0.459727;}
	   else if(pt < 160){eff = 74.4399+ ud*0.441028;}
	   else if(pt < 210){eff = 72.0414+ ud*0.682022;}
	   else if(pt < 260){eff = 69.2353+ ud*1.22483;}
	   else if(pt < 320){eff = 65.0404+ ud*1.96969;}
	   else if(pt < 400){eff = 59.2299+ ud*3.1873;}
	   else if(pt > 400){eff = 59.2299+ ud*3.1873*2;}
	 }
	 else if(fabs(eta) < 0.8){
	   if(pt < 30){eff = 60.2476+ 2*ud*0.597118;}
	   else if(pt < 40){eff = 60.2476+ ud*0.597118;}
	   else if(pt < 50){eff = 62.8362+ ud*0.511089;}
	   else if(pt < 60){eff = 66.8813+ ud*0.476655;}
	   else if(pt < 70){eff = 69.5387+ ud*0.481008;}
	   else if(pt < 80){eff = 71.1984+ ud*0.501033;}
	   else if(pt < 90){eff = 72.3051+ ud*0.540798;}
	   else if(pt < 100){eff = 73.6983+ ud*0.586324;}
	   else if(pt < 120){eff = 74.3901+ ud*0.478763;}
	   else if(pt < 160){eff = 74.6952+ ud*0.470424;}
	   else if(pt < 210){eff = 73.6516+ ud*0.711143;}
	   else if(pt < 260){eff = 71.6825+ ud*1.28685;}
	   else if(pt < 320){eff = 65.8744+ ud*2.16336;}
	   else if(pt < 400){eff = 61.2288+ ud*3.27355;}
	   else if(pt > 400){eff = 61.2288+ ud*3.27355*2;}
	 }
	 else if(fabs(eta) < 1.2){
	   if(pt < 30){eff = 55.6568+ 2*ud*0.650499;}
	   else if(pt < 40){eff = 55.6568+ ud*0.650499;}
	   else if(pt < 50){eff = 60.8933+ ud*0.564052;}
	   else if(pt < 60){eff = 64.1413+ ud*0.543756;}
	   else if(pt < 70){eff = 67.3339+ ud*0.551232;}
	   else if(pt < 80){eff = 68.5115+ ud*0.579114;}
	   else if(pt < 90){eff = 70.6977+ ud*0.618786;}
	   else if(pt < 100){eff = 70.6493+ ud*0.687069;}
	   else if(pt < 120){eff = 72.8003+ ud*0.552768;}
	   else if(pt < 160){eff = 72.2685+ ud*0.556941;}
	   else if(pt < 210){eff = 69.3395+ ud*0.884335;}
	   else if(pt < 260){eff = 64.119+ ud*1.63534;}
	   else if(pt < 320){eff = 63.4131+ ud*2.63894;}
	   else if(pt < 400){eff = 59.177+ ud*4.41122;}
	   else if(pt > 400){eff = 59.177+ ud*4.41122*2;}
	 }
	 else if(fabs(eta) < 1.6){
	   if(pt < 30){eff = 51.6851+ 2*ud*0.715415;}
	   else if(pt < 40){eff = 51.6851+ ud*0.715415;}
	   else if(pt < 50){eff = 55.0522+ ud*0.655879;}
	   else if(pt < 60){eff = 58.9059+ ud*0.647252;}
	   else if(pt < 70){eff = 62.151+ ud*0.666952;}
	   else if(pt < 80){eff = 62.5603+ ud*0.715411;}
	   else if(pt < 90){eff = 66.4453+ ud*0.774487;}
	   else if(pt < 100){eff = 66.7552+ ud*0.880484;}
	   else if(pt < 120){eff = 66.8249+ ud*0.73847;}
	   else if(pt < 160){eff = 67.1591+ ud*0.732998;}
	   else if(pt < 210){eff = 64.8301+ ud*1.21261;}
	   else if(pt < 260){eff = 59.1551+ ud*2.32697;}
	   else if(pt < 320){eff = 50.6507+ ud*3.88315;}
	   else if(pt < 400){eff = 47.273+ ud*6.51883;}
	   else if(pt > 400){eff = 47.273+ ud*6.51883*2;}
	 }
	 else if(fabs(eta) < 2){
	   if(pt < 30){eff = 53.0912+ 2*ud*0.859209;}
	   else if(pt < 40){eff = 53.0912+ ud*0.859209;}
	   else if(pt < 50){eff = 55.7974+ ud*0.793824;}
	   else if(pt < 60){eff = 59.7721+ ud*0.797362;}
	   else if(pt < 70){eff = 61.9553+ ud*0.859411;}
	   else if(pt < 80){eff = 63.0457+ ud*0.936192;}
	   else if(pt < 90){eff = 66.0961+ ud*1.03051;}
	   else if(pt < 100){eff = 63.6156+ ud*1.20936;}
	   else if(pt < 120){eff = 68.1744+ ud*0.993558;}
	   else if(pt < 160){eff = 64.5112+ ud*1.08242;}
	   else if(pt < 210){eff = 63.7865+ ud*1.85241;}
	   else if(pt < 260){eff = 60.7268+ ud*3.72836;}
	   else if(pt < 320){eff = 52.0308+ ud*6.8618;}
	   else if(pt < 400){eff = 52.5927+ ud*12.6743;}
	   else if(pt > 400){eff = 52.5927+ ud*12.6743*2;}
	 }
	 else if(fabs(eta) < 2.4){
	   if(pt < 30){eff = 45.7634+ 2*ud*1.04879;}
	   else if(pt < 40){eff = 45.7634+ ud*1.04879;}
	   else if(pt < 50){eff = 46.5664+ ud*1.00603;}
	   else if(pt < 60){eff = 50.8382+ ud*1.03849;}
	   else if(pt < 70){eff = 53.0865+ ud*1.13995;}
	   else if(pt < 80){eff = 56.3181+ ud*1.27625;}
	   else if(pt < 90){eff = 56.7003+ ud*1.53593;}
	   else if(pt < 100){eff = 56.116+ ud*1.74962;}
	   else if(pt < 120){eff = 57.7976+ ud*1.50956;}
	   else if(pt < 160){eff = 59.3349+ ud*1.68883;}
	   else if(pt < 210){eff = 53.7117+ ud*3.10046;}
	   else if(pt < 260){eff = 49.7133+ ud*6.55805;}
	   else if(pt < 320){eff = 48.2648+ ud*14.8318;}
	   else if(pt < 400){eff = 74.706+ ud*21.2441;}
	   else if(pt > 400){eff = 74.706+ ud*21.2441*2;}
	 }
	 eff *= 0.01;
	 return eff;}
       else if(working_point == "CSVT" && sample == "MadSpin"){
		   if(fabs(eta) < 0.4){
			   if(pt < 30){eff = 45.9578+ 2*ud*0.609279;}
			   else if(pt < 40){eff = 45.9578+ ud*0.609279;}
			   else if(pt < 50){eff = 50.0356+ ud*0.56875;}
			   else if(pt < 60){eff = 53.5675+ ud*0.562024;}
			   else if(pt < 70){eff = 54.1225+ ud*0.584147;}
			   else if(pt < 80){eff = 57.0568+ ud*0.629529;}
			   else if(pt < 90){eff = 58.4976+ ud*0.670668;}
			   else if(pt < 100){eff = 55.5469+ ud*0.753914;}
			   else if(pt < 120){eff = 56.2125+ ud*0.627262;}
			   else if(pt < 160){eff = 55.2436+ ud*0.627733;}
			   else if(pt < 210){eff = 46.7676+ ud*0.986741;}
			   else if(pt < 260){eff = 39.9688+ ud*1.72867;}
			   else if(pt < 320){eff = 36.3318+ ud*2.84117;}
			   else if(pt < 400){eff = 25.7953+ ud*3.97814;}
			   else if(pt > 400){eff = 25.7953+ ud*3.97814*2;}
		   }
		   else if(fabs(eta) < 0.8){
			   if(pt < 30){eff = 46.8045+ 2*ud*0.629924;}
			   else if(pt < 40){eff = 46.8045+ ud*0.629924;}
			   else if(pt < 50){eff = 50.157+ ud*0.59567;}
			   else if(pt < 60){eff = 53.6911+ ud*0.586996;}
			   else if(pt < 70){eff = 55.3039+ ud*0.613401;}
			   else if(pt < 80){eff = 58.6422+ ud*0.648361;}
			   else if(pt < 90){eff = 58.6727+ ud*0.708896;}
			   else if(pt < 100){eff = 57.7092+ ud*0.788149;}
			   else if(pt < 120){eff = 57.3842+ ud*0.654206;}
			   else if(pt < 160){eff = 56.8815+ ud*0.665966;}
			   else if(pt < 210){eff = 49.489+ ud*1.05278;}
			   else if(pt < 260){eff = 46.5296+ ud*1.89073;}
			   else if(pt < 320){eff = 36.7875+ ud*3.00991;}
			   else if(pt < 400){eff = 35.8024+ ud*4.48757;}
			   else if(pt > 400){eff = 35.8024+ ud*4.48757*2;}
		   }
		   else if(fabs(eta) < 1.2){
			   if(pt < 30){eff = 41.1512+ 2*ud*0.667254;}
			   else if(pt < 40){eff = 41.1512+ ud*0.667254;}
			   else if(pt < 50){eff = 46.148+ ud*0.652202;}
			   else if(pt < 60){eff = 50.3412+ ud*0.653778;}
			   else if(pt < 70){eff = 52.1954+ ud*0.688552;}
			   else if(pt < 80){eff = 53.2839+ ud*0.745115;}
			   else if(pt < 90){eff = 56.0362+ ud*0.805126;}
			   else if(pt < 100){eff = 54.8081+ ud*0.903043;}
			   else if(pt < 120){eff = 55.0142+ ud*0.755775;}
			   else if(pt < 160){eff = 53.477+ ud*0.766728;}
			   else if(pt < 210){eff = 45.5982+ ud*1.23513;}
			   else if(pt < 260){eff = 41.5098+ ud*2.21694;}
			   else if(pt < 320){eff = 37.6977+ ud*3.71802;}
			   else if(pt < 400){eff = 31.4927+ ud*5.4578;}
			   else if(pt > 400){eff = 31.4927+ ud*5.4578*2;}
		   }
		   else if(fabs(eta) < 1.6){
			   if(pt < 30){eff = 37.078+ 2*ud*0.704798;}
			   else if(pt < 40){eff = 37.078+ ud*0.704798;}
			   else if(pt < 50){eff = 41.1847+ ud*0.7287;}
			   else if(pt < 60){eff = 44.9721+ ud*0.763116;}
			   else if(pt < 70){eff = 47.8342+ ud*0.817995;}
			   else if(pt < 80){eff = 48.0832+ ud*0.867402;}
			   else if(pt < 90){eff = 52.9426+ ud*0.974696;}
			   else if(pt < 100){eff = 49.4059+ ud*1.10438;}
			   else if(pt < 120){eff = 47.0699+ ud*0.938641;}
			   else if(pt < 160){eff = 47.5357+ ud*0.953277;}
			   else if(pt < 210){eff = 36.9+ ud*1.55339;}
			   else if(pt < 260){eff = 25.713+ ud*2.68972;}
			   else if(pt < 320){eff = 29.3705+ ud*4.93708;}
			   else if(pt < 400){eff = 19.5898+ ud*6.83178;}
			   else if(pt > 400){eff = 19.5898+ ud*6.83178*2;}
		   }
		   else if(fabs(eta) < 2){
			   if(pt < 30){eff = 36.6763+ 2*ud*0.858226;}
			   else if(pt < 40){eff = 36.6763+ ud*0.858226;}
			   else if(pt < 50){eff = 41.8536+ ud*0.878993;}
			   else if(pt < 60){eff = 45.3125+ ud*0.937705;}
			   else if(pt < 70){eff = 48.4982+ ud*1.02648;}
			   else if(pt < 80){eff = 49.0228+ ud*1.14543;}
			   else if(pt < 90){eff = 50.9106+ ud*1.28863;}
			   else if(pt < 100){eff = 48.3217+ ud*1.50238;}
			   else if(pt < 120){eff = 47.6428+ ud*1.27975;}
			   else if(pt < 160){eff = 43.5607+ ud*1.3547;}
			   else if(pt < 210){eff = 34.8489+ ud*2.34013;}
			   else if(pt < 260){eff = 26.9577+ ud*4.34629;}
			   else if(pt < 320){eff = 27.7429+ ud*7.71215;}
			   else if(pt < 400){eff = 9.80664+ ud*9.34688;}
			   else if(pt > 400){eff = 9.80664+ ud*9.34688*2;}
		   }
		   else if(fabs(eta) < 2.4){
			   if(pt < 30){eff = 29.1233+ 2*ud*0.982941;}
			   else if(pt < 40){eff = 29.1233+ ud*0.982941;}
			   else if(pt < 50){eff = 30.1805+ ud*1.05087;}
			   else if(pt < 60){eff = 35.006+ ud*1.13674;}
			   else if(pt < 70){eff = 36.8881+ ud*1.27125;}
			   else if(pt < 80){eff = 38.8134+ ud*1.47616;}
			   else if(pt < 90){eff = 39.15+ ud*1.76404;}
			   else if(pt < 100){eff = 41.6317+ ud*2.07423;}
			   else if(pt < 120){eff = 37.4886+ ud*1.73216;}
			   else if(pt < 160){eff = 37.6067+ ud*2.00196;}
			   else if(pt < 210){eff = 27.855+ ud*3.39976;}
			   else if(pt < 260){eff = 18.054+ ud*6.78208;}
			   else if(pt < 320){eff = 24.4699+ ud*16.8262;}
			   else if(pt < 400){eff = 24.4699+ ud*16.8262;}
			   else if(pt > 400){eff = 24.4699+ ud*16.8262*2;}
		   }
	eff *= 0.01;
	return eff;}
     else if(working_point == "CSVM" && sample == "MadGraph"){
	if(fabs(eta) < 0.4){
	    if(pt < 30){eff = 59.2917+ 2*ud*0.902248;}
	    else if(pt < 40){eff = 59.2917+ ud*0.902248;}
	    else if(pt < 50){eff = 62.3494+ ud*0.840878;}
	    else if(pt < 60){eff = 66.7451+ ud*0.808213;}
	    else if(pt < 70){eff = 69.3987+ ud*0.819789;}
	    else if(pt < 80){eff = 71.5326+ ud*0.860987;}
	    else if(pt < 90){eff = 71.863+ ud*0.930568;}
	    else if(pt < 100){eff = 72.246+ ud*1.0355;}
	    else if(pt < 120){eff = 73.3309+ ud*0.844682;}
	    else if(pt < 160){eff = 73.8032+ ud*0.853676;}
	    else if(pt < 210){eff = 73.8806+ ud*1.34168;}
	    else if(pt < 260){eff = 73.3542+ ud*2.47532;}
	    else if(pt < 320){eff = 72.973+ ud*4.2152;}
	    else if(pt < 400){eff = 57.4468+ ud*7.21191;}
	    else if(pt > 400){eff = 57.4468+ ud*7.21191*2;}
	}
	else if(fabs(eta) < 0.8){
	    if(pt < 30){eff = 60.7731+ 2*ud*0.923708;}
	    else if(pt < 40){eff = 60.7731+ ud*0.923708;}
	    else if(pt < 50){eff = 62.0388+ ud*0.873016;}
	    else if(pt < 60){eff = 65.8128+ ud*0.859594;}
	    else if(pt < 70){eff = 68.9114+ ud*0.864586;}
	    else if(pt < 80){eff = 71.1997+ ud*0.917862;}
	    else if(pt < 90){eff = 72.9924+ ud*0.970736;}
	    else if(pt < 100){eff = 74.7506+ ud*1.08475;}
	    else if(pt < 120){eff = 72.9718+ ud*0.910518;}
	    else if(pt < 160){eff = 74.0885+ ud*0.91281;}
	    else if(pt < 210){eff = 73.3954+ ud*1.42175;}
	    else if(pt < 260){eff = 64.4531+ ud*2.99159;}
	    else if(pt < 320){eff = 60.7843+ ud*4.83421;}
	    else if(pt < 400){eff = 51.8519+ ud*6.79947;}
	    else if(pt > 400){eff = 51.8519+ ud*6.79947*2;}
	}
	else if(fabs(eta) < 1.2){
	    if(pt < 30){eff = 55.8388+ 2*ud*1.00695;}
	    else if(pt < 40){eff = 55.8388+ ud*1.00695;}
	    else if(pt < 50){eff = 59.7806+ ud*0.937604;}
	    else if(pt < 60){eff = 64.4522+ ud*0.943454;}
	    else if(pt < 70){eff = 66.6092+ ud*0.97891;}
	    else if(pt < 80){eff = 68.147+ ud*1.05264;}
	    else if(pt < 90){eff = 71.4459+ ud*1.11329;}
	    else if(pt < 100){eff = 70.5336+ ud*1.26783;}
	    else if(pt < 120){eff = 72.812+ ud*1.01251;}
	    else if(pt < 160){eff = 74.1157+ ud*1.03787;}
	    else if(pt < 210){eff = 71.8973+ ud*1.69774;}
	    else if(pt < 260){eff = 70.9184+ ud*3.24385;}
	    else if(pt < 320){eff = 58.9744+ ud*5.56945;}
	    else if(pt < 400){eff = 50+ ud*10.66;}
	    else if(pt > 400){eff = 50+ ud*10.66*2;}
	}
	else if(fabs(eta) < 1.6){
	    if(pt < 30){eff = 50.4491+ 2*ud*1.11687;}
	    else if(pt < 40){eff = 50.4491+ ud*1.11687;}
	    else if(pt < 50){eff = 55.3181+ ud*1.08335;}
	    else if(pt < 60){eff = 57.9215+ ud*1.11427;}
	    else if(pt < 70){eff = 59.6429+ ud*1.19697;}
	    else if(pt < 80){eff = 64.9925+ ud*1.30597;}
	    else if(pt < 90){eff = 65.8662+ ud*1.38859;}
	    else if(pt < 100){eff = 65.3261+ ud*1.5691;}
	    else if(pt < 120){eff = 66.1111+ ud*1.33346;}
	    else if(pt < 160){eff = 66.6376+ ud*1.39343;}
	    else if(pt < 210){eff = 64.9652+ ud*2.29801;}
	    else if(pt < 260){eff = 49.1379+ ud*4.64169;}
	    else if(pt < 320){eff = 59.0909+ ud*7.41215;}
	    else if(pt < 400){eff = 33.3333+ ud*15.7135;}
	    else if(pt > 400){eff = 33.3333+ ud*15.7135*2;}
	}
	else if(fabs(eta) < 2){
	    if(pt < 30){eff = 50.392+ 2*ud*1.33484;}
	    else if(pt < 40){eff = 50.392+ ud*1.33484;}
	    else if(pt < 50){eff = 54.5977+ ud*1.33446;}
	    else if(pt < 60){eff = 58.9357+ ud*1.38646;}
	    else if(pt < 70){eff = 62.7993+ ud*1.46669;}
	    else if(pt < 80){eff = 62.6374+ ud*1.69042;}
	    else if(pt < 90){eff = 64.7975+ ud*1.88494;}
	    else if(pt < 100){eff = 66.0819+ ud*2.09025;}
	    else if(pt < 120){eff = 62.9518+ ud*1.87415;}
	    else if(pt < 160){eff = 63.4545+ ud*2.05337;}
	    else if(pt < 210){eff = 64.3216+ ud*3.3959;}
	    else if(pt < 260){eff = 59.6154+ ud*6.80433;}
	    else if(pt < 320){eff = 44.4444+ ud*16.5635;}
	    else if(pt < 400){eff = 50+ ud*35.3553;}
	    else if(pt > 400){eff = 50+ ud*35.3553*2;}
	}
	else if(fabs(eta) < 2.4){
	    if(pt < 30){eff = 46.5739+ 2*ud*1.6322;}
	    else if(pt < 40){eff = 46.5739+ ud*1.6322;}
	    else if(pt < 50){eff = 47.8163+ ud*1.67159;}
	    else if(pt < 60){eff = 50.9409+ ud*1.83276;}
	    else if(pt < 70){eff = 53.8806+ ud*1.92584;}
	    else if(pt < 80){eff = 54.6025+ ud*2.27724;}
	    else if(pt < 90){eff = 53.2338+ ud*2.48855;}
	    else if(pt < 100){eff = 58.2759+ ud*2.8956;}
	    else if(pt < 120){eff = 56.2674+ ud*2.61809;}
	    else if(pt < 160){eff = 64.9805+ ud*2.97564;}
	    else if(pt < 210){eff = 56.6265+ ud*5.4398;}
	    else if(pt < 260){eff = 38.4615+ ud*13.4932;}
	    else if(pt < 320){eff = 66.6667+ ud*27.2166;}
	    else if(pt < 400){eff = 66.6667+ ud*27.2166;}
	    else if(pt > 400){eff = 66.6667+ ud*27.2166*2;}
	}
	eff *= 0.01;
	return eff;}
    else if(working_point == "CSVT"&& sample == "MC@NLO"){
	if(fabs(eta) < 0.4){
	    if(pt < 30){eff = 48.3065+ 2*ud*0.541271;}
	    else if(pt < 40){eff = 48.3065+ ud*0.541271;}
	    else if(pt < 50){eff = 51.8789+ ud*0.519595;}
	    else if(pt < 60){eff = 56.0032+ ud*0.525447;}
	    else if(pt < 70){eff = 56.0699+ ud*0.539357;}
	    else if(pt < 80){eff = 57.406+ ud*0.569536;}
	    else if(pt < 90){eff = 58.5954+ ud*0.612673;}
	    else if(pt < 100){eff = 57.3102+ ud*0.676019;}
	    else if(pt < 120){eff = 55.9523+ ud*0.573575;}
	    else if(pt < 160){eff = 54.0455+ ud*0.569242;}
	    else if(pt < 210){eff = 45.5817+ ud*0.880153;}
	    else if(pt < 260){eff = 40.4686+ ud*1.60084;}
	    else if(pt < 320){eff = 38.2134+ ud*2.63924;}
	    else if(pt < 400){eff = 29.3014+ ud*3.96888;}
	    else if(pt > 400){eff = 29.3014+ ud*3.96888*2;}
	}
	else if(fabs(eta) < 0.8){
	    if(pt < 30){eff = 49.2207+ 2*ud*0.561708;}
	    else if(pt < 40){eff = 49.2207+ ud*0.561708;}
	    else if(pt < 50){eff = 51.5232+ ud*0.538759;}
	    else if(pt < 60){eff = 57.6695+ ud*0.541539;}
	    else if(pt < 70){eff = 57.4053+ ud*0.557211;}
	    else if(pt < 80){eff = 58.0898+ ud*0.600836;}
	    else if(pt < 90){eff = 58.9212+ ud*0.641914;}
	    else if(pt < 100){eff = 57.7226+ ud*0.717226;}
	    else if(pt < 120){eff = 57.6869+ ud*0.591744;}
	    else if(pt < 160){eff = 54.9036+ ud*0.595622;}
	    else if(pt < 210){eff = 48.1043+ ud*0.933264;}
	    else if(pt < 260){eff = 37.5584+ ud*1.72781;}
	    else if(pt < 320){eff = 35.0463+ ud*2.75673;}
	    else if(pt < 400){eff = 24.98+ ud*4.06541;}
	    else if(pt > 400){eff = 24.98+ ud*4.06541*2;}
	}
	else if(fabs(eta) < 1.2){
	    if(pt < 30){eff = 43.8248+ 2*ud*0.596154;}
	    else if(pt < 40){eff = 43.8248+ ud*0.596154;}
	    else if(pt < 50){eff = 48.2971+ ud*0.588838;}
	    else if(pt < 60){eff = 51.9537+ ud*0.58546;}
	    else if(pt < 70){eff = 53.7953+ ud*0.625639;}
	    else if(pt < 80){eff = 55.9591+ ud*0.66735;}
	    else if(pt < 90){eff = 55.6734+ ud*0.722373;}
	    else if(pt < 100){eff = 53.9742+ ud*0.794381;}
	    else if(pt < 120){eff = 54.1656+ ud*0.665508;}
	    else if(pt < 160){eff = 51.7972+ ud*0.66685;}
	    else if(pt < 210){eff = 45.6939+ ud*1.06017;}
	    else if(pt < 260){eff = 37.2776+ ud*1.92996;}
	    else if(pt < 320){eff = 31.5837+ ud*3.13868;}
	    else if(pt < 400){eff = 24.7466+ ud*4.32713;}
	    else if(pt > 400){eff = 24.7466+ ud*4.32713*2;}
	}
	else if(fabs(eta) < 1.6){
	    if(pt < 30){eff = 39.264+ 2*ud*0.655426;}
	    else if(pt < 40){eff = 39.264+ ud*0.655426;}
	    else if(pt < 50){eff = 42.7569+ ud*0.654517;}
	    else if(pt < 60){eff = 47.6208+ ud*0.670556;}
	    else if(pt < 70){eff = 48.4004+ ud*0.730104;}
	    else if(pt < 80){eff = 51.3397+ ud*0.790326;}
	    else if(pt < 90){eff = 50.3243+ ud*0.866014;}
	    else if(pt < 100){eff = 49.5648+ ud*0.957874;}
	    else if(pt < 120){eff = 47.7094+ ud*0.796783;}
	    else if(pt < 160){eff = 44.3797+ ud*0.82904;}
	    else if(pt < 210){eff = 34.0499+ ud*1.34363;}
	    else if(pt < 260){eff = 29.3069+ ud*2.24759;}
	    else if(pt < 320){eff = 19.2825+ ud*3.8592;}
	    else if(pt < 400){eff = 23.1586+ ud*6.51479;}
	    else if(pt > 400){eff = 23.1586+ ud*6.51479*2;}
	}
	else if(fabs(eta) < 2){
	    if(pt < 30){eff = 38.449+ 2*ud*0.763101;}
	    else if(pt < 40){eff = 38.449+ ud*0.763101;}
	    else if(pt < 50){eff = 42.4532+ ud*0.806004;}
	    else if(pt < 60){eff = 46.7259+ ud*0.842044;}
	    else if(pt < 70){eff = 49.5499+ ud*0.917004;}
	    else if(pt < 80){eff = 49.3149+ ud*1.00761;}
	    else if(pt < 90){eff = 51.161+ ud*1.1197;}
	    else if(pt < 100){eff = 49.945+ ud*1.23198;}
	    else if(pt < 120){eff = 47.4081+ ud*1.06611;}
	    else if(pt < 160){eff = 45.3645+ ud*1.13295;}
	    else if(pt < 210){eff = 32.2476+ ud*1.76245;}
	    else if(pt < 260){eff = 28.8796+ ud*3.44042;}
	    else if(pt < 320){eff = 17.7637+ ud*5.17477;}
	    else if(pt < 400){eff = 20.6996+ ud*9.7806;}
	    else if(pt > 400){eff = 20.6996+ ud*9.7806*2;}
	}
	else if(fabs(eta) < 2.4){
	    if(pt < 30){eff = 31.0927+ 2*ud*0.86764;}
	    else if(pt < 40){eff = 31.0927+ ud*0.86764;}
	    else if(pt < 50){eff = 34.8953+ ud*0.935467;}
	    else if(pt < 60){eff = 37.663+ ud*1.04526;}
	    else if(pt < 70){eff = 39.5153+ ud*1.14909;}
	    else if(pt < 80){eff = 41.6338+ ud*1.27837;}
	    else if(pt < 90){eff = 37.9271+ ud*1.45802;}
	    else if(pt < 100){eff = 41.5243+ ud*1.68965;}
	    else if(pt < 120){eff = 35.4637+ ud*1.40907;}
	    else if(pt < 160){eff = 35.5726+ ud*1.55669;}
	    else if(pt < 210){eff = 21.8455+ ud*2.36661;}
	    else if(pt < 260){eff = 18.0614+ ud*4.38748;}
	    else if(pt < 320){eff = 16.4909+ ud*7.99605;}
	    else if(pt < 400){eff = 20.4459+ ud*13.5696;}
	    else if(pt > 400){eff = 20.4459+ ud*13.5696*2;}
	}
	eff = eff*0.01;
	return eff;}
    else
		return 1.;
}

double broc::BTagEfficiencyWeightProvider::get_BTag_eff_light(double pt, double eta, int UpDown)
{
    double eff = 0.;
    double ud = 0.;
    if(up || UpDown == 1) ud = 1.;
    else if(down || UpDown == -1) ud = -1;
    if(working_point=="CSVM"&&sample=="MC@NLO"){
		//5.3.X TTbar MCatNLO
	if(fabs(eta) < 0.4){
	    if(pt < 20){eff = 1.34482+ 2*ud*0.0858649;}
	    else if(pt < 30){eff = 1.34482+ ud*0.0858649;}
	    else if(pt < 40){eff = 1.31039+ ud*0.0603035;}
	    else if(pt < 50){eff = 1.08782+ ud*0.0585018;}
	    else if(pt < 60){eff = 1.15237+ ud*0.0670479;}
	    else if(pt < 70){eff = 1.19168+ ud*0.0772723;}
	    else if(pt < 80){eff = 1.39777+ ud*0.0953261;}
	    else if(pt < 90){eff = 1.43332+ ud*0.111118;}
	    else if(pt < 100){eff = 1.6438+ ud*0.135384;}
	    else if(pt < 120){eff = 2.05696+ ud*0.129528;}
	    else if(pt < 160){eff = 2.06475+ ud*0.131079;}
	    else if(pt < 210){eff = 2.20791+ ud*0.199316;}
	    else if(pt < 260){eff = 3.95948+ ud*0.418424;}
	    else if(pt < 320){eff = 2.10816+ ud*0.434923;}
	    else if(pt < 400){eff = 3.63901+ ud*0.714437;}
	    else if(pt > 400){eff = 3.63901+ ud*0.714437*2;}
	}
	else if(fabs(eta) < 0.8){
	    if (pt < 20){eff = 1.40829+ 2*ud*0.0897033;}
	    else if(pt < 30){eff = 1.40829+ ud*0.0897033;}
	    else if(pt < 40){eff = 1.42275+ ud*0.0646103;}
	    else if(pt < 50){eff = 1.14229+ ud*0.0620526;}
	    else if(pt < 60){eff = 1.16861+ ud*0.0696777;}
	    else if(pt < 70){eff = 1.25568+ ud*0.0819204;}
	    else if(pt < 80){eff = 1.50569+ ud*0.10144;}
	    else if(pt < 90){eff = 1.58099+ ud*0.119942;}
	    else if(pt < 100){eff = 1.73985+ ud*0.142247;}
	    else if(pt < 120){eff = 1.71419+ ud*0.12047;}
	    else if(pt < 160){eff = 2.17031+ ud*0.136037;}
	    else if(pt < 210){eff = 2.70123+ ud*0.218284;}
	    else if(pt < 260){eff = 2.51514+ ud*0.337935;}
	    else if(pt < 320){eff = 3.45821+ ud*0.566315;}
	    else if(pt < 400){eff = 4.18535+ ud*0.774228;}
	    else if(pt > 400){eff = 4.18535+ ud*0.774228*2;}
	}
	else if(fabs(eta) < 1.2){
	    if(pt < 20){eff = 1.48327+ 2*ud*0.0977154;}
	    else if(pt < 30){eff = 1.48327+ ud*0.0977154;}
	    else if(pt < 40){eff = 1.41614+ ud*0.0683656;}
	    else if(pt < 50){eff = 1.20463+ ud*0.0682255;}
	    else if(pt < 60){eff = 1.19263+ ud*0.0758936;}
	    else if(pt < 70){eff = 1.34676+ ud*0.0914405;}
	    else if(pt < 80){eff = 1.17965+ ud*0.0977227;}
	    else if(pt < 90){eff = 1.60226+ ud*0.128493;}
	    else if(pt < 100){eff = 1.51643+ ud*0.144808;}
	    else if(pt < 120){eff = 2.02091+ ud*0.141097;}
	    else if(pt < 160){eff = 2.03843+ ud*0.139556;}
	    else if(pt < 210){eff = 2.18526+ ud*0.203314;}
	    else if(pt < 260){eff = 2.11031+ ud*0.314767;}
	    else if(pt < 320){eff = 2.97834+ ud*0.510683;}
	    else if(pt < 400){eff = 3.39943+ ud*0.68201;}
	    else if(pt > 400){eff = 3.39943+ ud*0.68201*2;}
	}
	else if(fabs(eta) < 1.6){
	    if(pt < 20){eff = 1.39111+ 2*ud*0.0999549;}
	    else if(pt < 30){eff = 1.39111+ ud*0.0999549;}
	    else if(pt < 40){eff = 1.52925+ ud*0.0775411;}
	    else if(pt < 50){eff = 1.06557+ ud*0.0711341;}
	    else if(pt < 60){eff = 1.19664+ ud*0.0838981;}
	    else if(pt < 70){eff = 1.31083+ ud*0.100768;}
	    else if(pt < 80){eff = 1.30874+ ud*0.115369;}
	    else if(pt < 90){eff = 1.48798+ ud*0.139551;}
	    else if(pt < 100){eff = 1.42534+ ud*0.156277;}
	    else if(pt < 120){eff = 1.63491+ ud*0.140601;}
	    else if(pt < 160){eff = 1.76888+ ud*0.142201;}
	    else if(pt < 210){eff = 2.04035+ ud*0.212867;}
	    else if(pt < 260){eff = 2.2293+ ud*0.340132;}
	    else if(pt < 320){eff = 2.59259+ ud*0.483561;}
	    else if(pt < 400){eff = 3.84088+ ud*0.711782;}
	    else if(pt > 400){eff = 3.84088+ ud*0.711782*2;}
	}
	else if(fabs(eta) < 2){
	    if(pt < 20){eff = 1.78971+ 2*ud*0.128;}
	    else if(pt < 30){eff = 1.78971+ ud*0.128;}
	    else if(pt < 40){eff = 1.9309+ ud*0.0994091;}
	    else if(pt < 50){eff = 1.64156+ ud*0.101951;}
	    else if(pt < 60){eff = 1.62575+ ud*0.115177;}
	    else if(pt < 70){eff = 1.50464+ ud*0.125757;}
	    else if(pt < 80){eff = 1.77809+ ud*0.157617;}
	    else if(pt < 90){eff = 1.83251+ ud*0.181564;}
	    else if(pt < 100){eff = 1.5043+ ud*0.188093;}
	    else if(pt < 120){eff = 2.06373+ ud*0.182671;}
	    else if(pt < 160){eff = 2.28996+ ud*0.184208;}
	    else if(pt < 210){eff = 2.89467+ ud*0.274478;}
	    else if(pt < 260){eff = 3.14353+ ud*0.424957;}
	    else if(pt < 320){eff = 3.59955+ ud*0.624759;}
	    else if(pt < 400){eff = 2.95139+ ud*0.705175;}
	    else if(pt > 400){eff = 2.95139+ ud*0.705175*2;}
	}
	else if(fabs(eta) < 2.4){
	    if(pt < 20){eff = 1.93247+ 2*ud*0.141853;}
	    else if(pt < 30){eff = 1.93247+ ud*0.141853;}
	    else if(pt < 40){eff = 2.21549+ ud*0.116937;}
	    else if(pt < 50){eff = 1.95934+ ud*0.125491;}
	    else if(pt < 60){eff = 1.48728+ ud*0.127049;}
	    else if(pt < 70){eff = 1.78699+ ud*0.158399;}
	    else if(pt < 80){eff = 1.7622+ ud*0.183094;}
	    else if(pt < 90){eff = 1.92167+ ud*0.214118;}
	    else if(pt < 100){eff = 1.98738+ ud*0.247886;}
	    else if(pt < 120){eff = 2.31643+ ud*0.222371;}
	    else if(pt < 160){eff = 2.17814+ ud*0.203561;}
	    else if(pt < 210){eff = 2.5224+ ud*0.285667;}
	    else if(pt < 260){eff = 3.68421+ ud*0.51653;}
	    else if(pt < 320){eff = 4.16667+ ud*0.75744;}
	    else if(pt < 400){eff = 4.73934+ ud*1.03433;}
	    else if(pt > 400){eff = 4.73934+ ud*1.03433*2;}
	}

	eff = eff*0.01;
	return eff;}
    else if(working_point=="CSVT"&&sample=="MadGraph"){
      if(fabs(eta) < 0.4){
	if(pt < 30){eff = 0.390503+ 2*ud*0.0898098;}
	else if(pt < 40){eff = 0.390503+ ud*0.0898098;}
	else if(pt < 50){eff = 0.192868+ ud*0.0664836;}
	else if(pt < 60){eff = 0.167192+ ud*0.0723271;}
	else if(pt < 70){eff = 0.319405+ ud*0.121793;}
	else if(pt < 80){eff = 0.35346+ ud*0.141094;}
	else if(pt < 90){eff = 0.267182+ ud*0.143834;}
	else if(pt < 100){eff = 0.876428+ ud*0.287212;}
	else if(pt < 120){eff = 0.613964+ ud*0.200506;}
	else if(pt < 160){eff = 0.633966+ ud*0.208035;}
	else if(pt < 210){eff = 0.708783+ ud*0.34935;}
	else if(pt < 260){eff = 1.12383+ ud*0.606454;}
	else if(pt < 320){eff = 0.692465+ ud*0.690407;}
	else if(pt < 400){eff = 1.40074+ ud*1.39054;}
	else if(pt > 400){eff = 1.40074+ ud*1.39054*2;}
      }
      else if(fabs(eta) < 0.8){
	if(pt < 30){eff = 0.184818+ 2*ud*0.0662603;}
	else if(pt < 40){eff = 0.184818+ ud*0.0662603;}
	else if(pt < 50){eff = 0.42807+ ud*0.114942;}
	else if(pt < 60){eff = 0.417411+ ud*0.130634;}
	else if(pt < 70){eff = 0.524802+ ud*0.156563;}
	else if(pt < 80){eff = 0.714477+ ud*0.212072;}
	else if(pt < 90){eff = 1.02924+ ud*0.299641;}
	else if(pt < 100){eff = 1.08505+ ud*0.361476;}
	else if(pt < 120){eff = 0.687598+ ud*0.242438;}
	else if(pt < 160){eff = 1.06168+ ud*0.300379;}
	else if(pt < 210){eff = 2.26645+ ud*0.633854;}
	else if(pt < 260){eff = 2.03035+ ud*0.91474;}
	else if(pt < 320){eff = 2.13626+ ud*1.15302;}
	else if(pt < 400){eff = 2.13626+ ud*1.15302;}
	else if(pt > 400){eff = 2.13626+ ud*1.15302*2;}
      }
      else if(fabs(eta) < 1.2){
	if(pt < 30){eff = 0.360117+ 2*ud*0.0944331;}
	else if(pt < 40){eff = 0.360117+ ud*0.0944331;}
	else if(pt < 50){eff = 0.393983+ ud*0.12169;}
	else if(pt < 60){eff = 0.381478+ ud*0.129755;}
	else if(pt < 70){eff = 0.415096+ ud*0.157354;}
	else if(pt < 80){eff = 0.740182+ ud*0.235195;}
	else if(pt < 90){eff = 0.978763+ ud*0.310901;}
	else if(pt < 100){eff = 1.26278+ ud*0.416607;}
	else if(pt < 120){eff = 1.30522+ ud*0.351987;}
	else if(pt < 160){eff = 1.44228+ ud*0.372615;}
	else if(pt < 210){eff = 0.845789+ ud*0.410972;}
	else if(pt < 260){eff = 1.43306+ ud*0.849742;}
	else if(pt < 320){eff = 3.62822+ ud*2.0744;}
	else if(pt < 400){eff = 1.80301+ ud*1.78922;}
	else if(pt > 400){eff = 1.80301+ ud*1.78922*2;}
      }
      else if(fabs(eta) < 1.6){
	if(pt < 30){eff = 0.474314+ 2*ud*0.117977;}
	else if(pt < 40){eff = 0.474314+ ud*0.117977;}
	else if(pt < 50){eff = 0.150283+ ud*0.0768772;}
	else if(pt < 60){eff = 0.424793+ ud*0.154649;}
	else if(pt < 70){eff = 0.138101+ ud*0.098005;}
	else if(pt < 80){eff = 0.30149+ ud*0.166443;}
	else if(pt < 90){eff = 0.627907+ ud*0.314248;}
	else if(pt < 100){eff = 0.991548+ ud*0.450341;}
	else if(pt < 120){eff = 0.86223+ ud*0.332619;}
	else if(pt < 160){eff = 0.347396+ ud*0.19204;}
	else if(pt < 210){eff = 2.09441+ ud*0.78489;}
	else if(pt < 260){eff = 0.547056+ ud*0.545821;}
	else if(pt < 320){eff = 2.03269+ ud*1.28133;}
	else if(pt < 400){eff = 1.99003+ ud*1.96732;}
	else if(pt > 400){eff = 1.99003+ ud*1.96732*2;}
      }
      else if(fabs(eta) < 2){
	if(pt < 30){eff = 0.471432+ 2*ud*0.144721;}
	else if(pt < 40){eff = 0.471432+ ud*0.144721;}
	else if(pt < 50){eff = 0.635288+ ud*0.201351;}
	else if(pt < 60){eff = 0.3906+ ud*0.176707;}
	else if(pt < 70){eff = 0.394147+ ud*0.227271;}
	else if(pt < 80){eff = 0.723973+ ud*0.3096;}
	else if(pt < 90){eff = 0.326382+ ud*0.22973;}
	else if(pt < 100){eff = 0.75919+ ud*0.449079;}
	else if(pt < 120){eff = 0.895708+ ud*0.416633;}
	else if(pt < 160){eff = 0.438265+ ud*0.277058;}
	else if(pt < 210){eff = 0.359523+ ud*0.358928;}
	else if(pt < 260){eff = 0.359523+ ud*0.358928;}
	else if(pt < 320){eff = 1.34262+ ud*1.33534;}
	else if(pt < 400){eff = 1.34262+ ud*1.33534;}
	else if(pt > 400){eff = 1.34262+ ud*1.33534*2;}
      }
      else if(fabs(eta) < 2.4){
	if(pt < 30){eff = 0.41324+ 2*ud*0.14788;}
	else if(pt < 40){eff = 0.41324+ ud*0.14788;}
	else if(pt < 50){eff = 0.222567+ ud*0.114099;}
	else if(pt < 60){eff = 0.348027+ ud*0.203084;}
	else if(pt < 70){eff = 0.446384+ ud*0.233152;}
	else if(pt < 80){eff = 0.416018+ ud*0.29382;}
	else if(pt < 90){eff = 1.24663+ ud*0.628844;}
	else if(pt < 100){eff = 1.24663+ ud*0.628844;}
	else if(pt < 120){eff = 1.24663+ ud*0.628844;}
	else if(pt < 160){eff = 1.24663+ ud*0.628844;}
	else if(pt < 210){eff = 0.232279+ ud*0.23234;}
	else if(pt < 260){eff = 1.43991+ ud*1.42826;}
	else if(pt < 320){eff = 3.24366+ ud*3.18555;}
	else if(pt < 400){eff = 5.22823+ ud*5.08764;}
	else if(pt > 400){eff = 5.22823+ ud*5.08764*2;}
      }
	eff *= 0.01;
	return eff;}
    else if(working_point=="CSVM"&&sample=="MadSpin"){
      if(fabs(eta) < 0.4){
	if(pt < 30){eff = 1.39832+ 2*ud*0.102301;}
	else if(pt < 40){eff = 1.39832+ ud*0.102301;}
	else if(pt < 50){eff = 1.25846+ ud*0.0973095;}
	else if(pt < 60){eff = 1.33376+ ud*0.108907;}
	else if(pt < 70){eff = 1.72798+ ud*0.13639;}
	else if(pt < 80){eff = 1.77425+ ud*0.152889;}
	else if(pt < 90){eff = 1.96995+ ud*0.183064;}
	else if(pt < 100){eff = 1.74216+ ud*0.191235;}
	else if(pt < 120){eff = 2.34948+ ud*0.188596;}
	else if(pt < 160){eff = 3.09052+ ud*0.210806;}
	else if(pt < 210){eff = 4.1939+ ud*0.344846;}
	else if(pt < 260){eff = 3.33427+ ud*0.454517;}
	else if(pt < 320){eff = 4.80746+ ud*0.786904;}
	else if(pt < 400){eff = 4.66407+ ud*0.917005;}
	else if(pt > 400){eff = 4.66407+ ud*0.917005*2;}
      }
      else if(fabs(eta) < 0.8){
	if(pt < 30){eff = 1.50459+ 2*ud*0.108196;}
	else if(pt < 40){eff = 1.50459+ ud*0.108196;}
	else if(pt < 50){eff = 1.312+ ud*0.103875;}
	else if(pt < 60){eff = 1.56416+ ud*0.119724;}
	else if(pt < 70){eff = 1.53935+ ud*0.133433;}
	else if(pt < 80){eff = 1.96262+ ud*0.165893;}
	else if(pt < 90){eff = 2.52384+ ud*0.21233;}
	else if(pt < 100){eff = 2.1304+ ud*0.219681;}
	else if(pt < 120){eff = 2.66818+ ud*0.207764;}
	else if(pt < 160){eff = 3.49265+ ud*0.228755;}
	else if(pt < 210){eff = 3.59379+ ud*0.32094;}
	else if(pt < 260){eff = 4.45568+ ud*0.537325;}
	else if(pt < 320){eff = 4.07369+ ud*0.70504;}
	else if(pt < 400){eff = 3.70501+ ud*0.832591;}
	else if(pt > 400){eff = 3.70501+ ud*0.832591*2;}
      }
      else if(fabs(eta) < 1.2){
	if(pt < 30){eff = 1.39618+ 2*ud*0.108599;}
	else if(pt < 40){eff = 1.39618+ ud*0.108599;}
	else if(pt < 50){eff = 1.44406+ ud*0.115177;}
	else if(pt < 60){eff = 1.47543+ ud*0.128142;}
	else if(pt < 70){eff = 1.73057+ ud*0.148322;}
	else if(pt < 80){eff = 1.81158+ ud*0.175959;}
	else if(pt < 90){eff = 2.12294+ ud*0.20724;}
	else if(pt < 100){eff = 2.15728+ ud*0.23472;}
	else if(pt < 120){eff = 2.66612+ ud*0.225165;}
	else if(pt < 160){eff = 3.04331+ ud*0.250426;}
	else if(pt < 210){eff = 3.48112+ ud*0.346412;}
	else if(pt < 260){eff = 4.4411+ ud*0.624095;}
	else if(pt < 320){eff = 4.57711+ ud*0.859883;}
	else if(pt < 400){eff = 4.24967+ ud*0.914575;}
	else if(pt > 400){eff = 4.24967+ ud*0.914575*2;}
      }
      else if(fabs(eta) < 1.6){
	if(pt < 30){eff = 1.63373+ 2*ud*0.129436;}
	else if(pt < 40){eff = 1.63373+ ud*0.129436;}
	else if(pt < 50){eff = 1.56759+ ud*0.129901;}
	else if(pt < 60){eff = 1.62766+ ud*0.145173;}
	else if(pt < 70){eff = 1.65844+ ud*0.164922;}
	else if(pt < 80){eff = 1.44367+ ud*0.17252;}
	else if(pt < 90){eff = 1.84123+ ud*0.221538;}
	else if(pt < 100){eff = 2.24793+ ud*0.277096;}
	else if(pt < 120){eff = 1.83562+ ud*0.212633;}
	else if(pt < 160){eff = 2.64393+ ud*0.241561;}
	else if(pt < 210){eff = 2.79883+ ud*0.343977;}
	else if(pt < 260){eff = 3.79034+ ud*0.609485;}
	else if(pt < 320){eff = 2.19077+ ud*0.604791;}
	else if(pt < 400){eff = 3.62066+ ud*1.12952;}
	else if(pt > 400){eff = 3.62066+ ud*1.12952*2;}
      }
      else if(fabs(eta) < 2){
	if(pt < 30){eff = 1.96108+ 2*ud*0.162162;}
	else if(pt < 40){eff = 1.96108+ ud*0.162162;}
	else if(pt < 50){eff = 1.78302+ ud*0.160417;}
	else if(pt < 60){eff = 1.8449+ ud*0.184846;}
	else if(pt < 70){eff = 2.19965+ ud*0.225934;}
	else if(pt < 80){eff = 2.17532+ ud*0.246602;}
	else if(pt < 90){eff = 1.85589+ ud*0.264986;}
	else if(pt < 100){eff = 1.66976+ ud*0.280731;}
	else if(pt < 120){eff = 2.14747+ ud*0.265439;}
	else if(pt < 160){eff = 3.37987+ ud*0.325301;}
	else if(pt < 210){eff = 4.43377+ ud*0.509505;}
	else if(pt < 260){eff = 4.20728+ ud*0.710275;}
	else if(pt < 320){eff = 3.27309+ ud*0.85449;}
	else if(pt < 400){eff = 5.31532+ ud*1.44501;}
	else if(pt > 400){eff = 5.31532+ ud*1.44501*2;}
      }
      else if(fabs(eta) < 2.4){
	if(pt < 30){eff = 2.52641+ 2*ud*0.203533;}
	else if(pt < 40){eff = 2.52641+ ud*0.203533;}
	else if(pt < 50){eff = 1.88512+ ud*0.184721;}
	else if(pt < 60){eff = 1.78144+ ud*0.205213;}
	else if(pt < 70){eff = 2.0733+ ud*0.24072;}
	else if(pt < 80){eff = 1.65357+ ud*0.238442;}
	else if(pt < 90){eff = 2.64031+ ud*0.36232;}
	else if(pt < 100){eff = 2.43293+ ud*0.392744;}
	else if(pt < 120){eff = 2.54971+ ud*0.339742;}
	else if(pt < 160){eff = 3.14984+ ud*0.354155;}
	else if(pt < 210){eff = 2.83133+ ud*0.467039;}
	else if(pt < 260){eff = 3.87509+ ud*0.825995;}
	else if(pt < 320){eff = 4.05988+ ud*1.26416;}
	else if(pt < 400){eff = 5.25986+ ud*2.12558;}
	else if(pt > 400){eff = 5.25986+ ud*2.12558*2;}
      }
	eff *= 0.01;
	return eff;}
    else if(working_point=="CSVT"&&sample=="MadSpin"){
		if(fabs(eta) < 0.4){
			if(pt < 30){eff = 0.346093+ 2*ud*0.0527756;}
			else if(pt < 40){eff = 0.346093+ ud*0.0527756;}
			else if(pt < 50){eff = 0.353001+ ud*0.0600874;}
			else if(pt < 60){eff = 0.384562+ ud*0.0714872;}
			else if(pt < 70){eff = 0.384549+ ud*0.0793192;}
			else if(pt < 80){eff = 0.623667+ ud*0.11678;}
			else if(pt < 90){eff = 0.588153+ ud*0.135023;}
			else if(pt < 100){eff = 0.477562+ ud*0.13608;}
			else if(pt < 120){eff = 0.72017+ ud*0.142217;}
			else if(pt < 160){eff = 1.00716+ ud*0.166075;}
			else if(pt < 210){eff = 1.66206+ ud*0.334915;}
			else if(pt < 260){eff = 1.87751+ ud*0.519326;}
			else if(pt < 320){eff = 1.64446+ ud*0.780278;}
			else if(pt < 400){eff = 1.64957+ ud*0.95417;}
			else if(pt > 400){eff = 1.64957+ ud*0.95417*2;}
		}
		else if(fabs(eta) < 0.8){
			if(pt < 30){eff = 0.362863+ 2*ud*0.0559464;}
			else if(pt < 40){eff = 0.362863+ ud*0.0559464;}
			else if(pt < 50){eff = 0.347888+ ud*0.0637492;}
			else if(pt < 60){eff = 0.403931+ ud*0.075254;}
			else if(pt < 70){eff = 0.392001+ ud*0.0857517;}
			else if(pt < 80){eff = 0.49781+ ud*0.108056;}
			else if(pt < 90){eff = 0.700385+ ud*0.137686;}
			else if(pt < 100){eff = 0.97935+ ud*0.201267;}
			else if(pt < 120){eff = 0.982718+ ud*0.179392;}
			else if(pt < 160){eff = 1.26845+ ud*0.193822;}
			else if(pt < 210){eff = 1.12851+ ud*0.255193;}
			else if(pt < 260){eff = 2.72164+ ud*0.641876;}
			else if(pt < 320){eff = 2.12604+ ud*0.895907;}
			else if(pt < 400){eff = 0.505736+ ud*0.504521;}
			else if(pt > 400){eff = 0.505736+ ud*0.504521*2;}
		}
		else if(fabs(eta) < 1.2){
			if(pt < 30){eff = 0.178036+ 2*ud*0.0406571;}
			else if(pt < 40){eff = 0.178036+ ud*0.0406571;}
			else if(pt < 50){eff = 0.434165+ ud*0.0765247;}
			else if(pt < 60){eff = 0.45155+ ud*0.0904908;}
			else if(pt < 70){eff = 0.477104+ ud*0.0975883;}
			else if(pt < 80){eff = 0.360529+ ud*0.101592;}
			else if(pt < 90){eff = 0.799+ ud*0.170665;}
			else if(pt < 100){eff = 0.875344+ ud*0.211814;}
			else if(pt < 120){eff = 1.00854+ ud*0.19236;}
			else if(pt < 160){eff = 0.966441+ ud*0.195763;}
			else if(pt < 210){eff = 1.14429+ ud*0.304504;}
			else if(pt < 260){eff = 1.07179+ ud*0.445242;}
			else if(pt < 320){eff = 0.609261+ ud*0.433815;}
			else if(pt < 400){eff = 0.99537+ ud*0.70301;}
			else if(pt > 400){eff = 0.99537+ ud*0.70301*2;}
		}
		else if(fabs(eta) < 1.6){
			if(pt < 30){eff = 0.282128+ 2*ud*0.0581053;}
			else if(pt < 40){eff = 0.282128+ ud*0.0581053;}
			else if(pt < 50){eff = 0.401915+ ud*0.0779645;}
			else if(pt < 60){eff = 0.396248+ ud*0.0875704;}
			else if(pt < 70){eff = 0.476901+ ud*0.116194;}
			else if(pt < 80){eff = 0.445039+ ud*0.124595;}
			else if(pt < 90){eff = 0.371589+ ud*0.133098;}
			else if(pt < 100){eff = 0.437937+ ud*0.166384;}
			else if(pt < 120){eff = 0.432902+ ud*0.14061;}
			else if(pt < 160){eff = 0.778287+ ud*0.188098;}
			else if(pt < 210){eff = 0.546281+ ud*0.220808;}
			else if(pt < 260){eff = 1.03013+ ud*0.456507;}
			else if(pt < 320){eff = 0.413138+ ud*0.390853;}
			else if(pt < 400){eff = 0.0341235+ ud*0.0342747;}
			else if(pt > 400){eff = 0.0341235+ ud*0.0342747*2;}
		}
		else if(fabs(eta) < 2){
			if(pt < 30){eff = 0.349371+ 2*ud*0.0701262;}
			else if(pt < 40){eff = 0.349371+ ud*0.0701262;}
			else if(pt < 50){eff = 0.376159+ ud*0.0927251;}
			else if(pt < 60){eff = 0.442598+ ud*0.112535;}
			else if(pt < 70){eff = 0.385306+ ud*0.116695;}
			else if(pt < 80){eff = 0.312028+ ud*0.124147;}
			else if(pt < 90){eff = 0.353671+ ud*0.157988;}
			else if(pt < 100){eff = 0.843777+ ud*0.281405;}
			else if(pt < 120){eff = 0.649997+ ud*0.207319;}
			else if(pt < 160){eff = 0.670171+ ud*0.217123;}
			else if(pt < 210){eff = 0.620128+ ud*0.267942;}
			else if(pt < 260){eff = 1.36103+ ud*0.615183;}
			else if(pt < 320){eff = 1.72021+ ud*1.02513;}
			else if(pt < 400){eff = 2.05892+ ud*1.60599;}
			else if(pt > 400){eff = 2.05892+ ud*1.60599*2;}
		}
		else if(fabs(eta) < 2.4){
			if(pt < 30){eff = 0.476423+ 2*ud*0.0967275;}
			else if(pt < 40){eff = 0.476423+ ud*0.0967275;}
			else if(pt < 50){eff = 0.438314+ ud*0.110093;}
			else if(pt < 60){eff = 0.306727+ ud*0.112906;}
			else if(pt < 70){eff = 0.531194+ ud*0.163045;}
			else if(pt < 80){eff = 0.147288+ ud*0.0926259;}
			else if(pt < 90){eff = 0.466766+ ud*0.200268;}
			else if(pt < 100){eff = 0.603074+ ud*0.311453;}
			else if(pt < 120){eff = 0.691951+ ud*0.263859;}
			else if(pt < 160){eff = 0.610218+ ud*0.241035;}
			else if(pt < 210){eff = 0.512426+ ud*0.320774;}
			else if(pt < 260){eff = 0.394693+ ud*0.394091;}
			else if(pt < 320){eff = 0.394693+ ud*0.394091;}
			else if(pt < 400){eff = 0.394693+ ud*0.394091;}
			else if(pt > 400){eff = 0.394693+ ud*0.394091*2;}
		}
		eff *= 0.01;
	return eff;}
    else if(working_point == "CSVM" && sample == "MadGraph"){
	if(fabs(eta) < 0.4){
	    if(pt < 30){eff = 1.5631+ 2*ud*0.172315;}
	    else if(pt < 40){eff = 1.5631+ ud*0.172315;}
	    else if(pt < 50){eff = 0.994904+ ud*0.154603;}
	    else if(pt < 60){eff = 1.36434+ ud*0.204274;}
	    else if(pt < 70){eff = 1.11924+ ud*0.21827;}
	    else if(pt < 80){eff = 1.09349+ ud*0.243172;}
	    else if(pt < 90){eff = 1.7192+ ud*0.3479;}
	    else if(pt < 100){eff = 1.69492+ ud*0.385531;}
	    else if(pt < 120){eff = 1.78808+ ud*0.341026;}
	    else if(pt < 160){eff = 2.87908+ ud*0.422964;}
	    else if(pt < 210){eff = 4.6131+ ud*0.8092;}
	    else if(pt < 260){eff = 5.74713+ ud*1.44063;}
	    else if(pt < 320){eff = 5.10949+ ud*1.88122;}
	    else if(pt < 400){eff = 5.20833+ ud*2.26777;}
	    else if(pt > 400){eff = 5.20833+ ud*2.26777*2;}
	}
	else if(fabs(eta) < 0.8){
	    if(pt < 30){eff = 1.20216+ 2*ud*0.154263;}
	    else if(pt < 40){eff = 1.20216+ ud*0.154263;}
	    else if(pt < 50){eff = 1.11111+ ud*0.17256;}
	    else if(pt < 60){eff = 1.59268+ ud*0.230459;}
	    else if(pt < 70){eff = 1.11163+ ud*0.225645;}
	    else if(pt < 80){eff = 1.09589+ ud*0.243702;}
	    else if(pt < 90){eff = 2.49622+ ud*0.429078;}
	    else if(pt < 100){eff = 2.29226+ ud*0.462512;}
	    else if(pt < 120){eff = 2.10822+ ud*0.380828;}
	    else if(pt < 160){eff = 2.69313+ ud*0.43096;}
	    else if(pt < 210){eff = 4.20757+ ud*0.751859;}
	    else if(pt < 260){eff = 6.77291+ ud*1.58607;}
	    else if(pt < 320){eff = 6.06061+ ud*2.0768;}
	    else if(pt < 400){eff = 5.88235+ ud*2.85336;}
	    else if(pt > 400){eff = 5.88235+ ud*2.85336*2;}
	}
	else if(fabs(eta) < 1.2){
	    if(pt < 30){eff = 1.3946+ 2*ud*0.177311;}
	    else if(pt < 40){eff = 1.3946+ ud*0.177311;}
	    else if(pt < 50){eff = 1.24962+ ud*0.193935;}
	    else if(pt < 60){eff = 1.73317+ ud*0.262006;}
	    else if(pt < 70){eff = 1.87913+ ud*0.30601;}
	    else if(pt < 80){eff = 2.08333+ ud*0.37026;}
	    else if(pt < 90){eff = 2.16667+ ud*0.42029;}
	    else if(pt < 100){eff = 1.84758+ ud*0.457607;}
	    else if(pt < 120){eff = 3.25543+ ud*0.51273;}
	    else if(pt < 160){eff = 3.75747+ ud*0.555716;}
	    else if(pt < 210){eff = 4.80591+ ud*0.91959;}
	    else if(pt < 260){eff = 3.41463+ ud*1.26838;}
	    else if(pt < 320){eff = 6.25+ ud*2.47053;}
	    else if(pt < 400){eff = 5.35714+ ud*3.00896;}
	    else if(pt > 400){eff = 5.35714+ ud*3.00896*2;}
	}
	else if(fabs(eta) < 1.6){
	    if(pt < 30){eff = 1.59753+ 2*ud*0.209902;}
	    else if(pt < 40){eff = 1.59753+ ud*0.209902;}
	    else if(pt < 50){eff = 1.27964+ ud*0.218048;}
	    else if(pt < 60){eff = 1.43351+ ud*0.264282;}
	    else if(pt < 70){eff = 1.34953+ ud*0.29972;}
	    else if(pt < 80){eff = 1.97256+ ud*0.407229;}
	    else if(pt < 90){eff = 2.33372+ ud*0.515711;}
	    else if(pt < 100){eff = 1.64179+ ud*0.490938;}
	    else if(pt < 120){eff = 2.0339+ ud*0.474494;}
	    else if(pt < 160){eff = 2.93478+ ud*0.55645;}
	    else if(pt < 210){eff = 4.13793+ ud*0.954927;}
	    else if(pt < 260){eff = 3.18471+ ud*1.40138;}
	    else if(pt < 320){eff = 5.61798+ ud*2.44084;}
	    else if(pt < 400){eff = 10+ ud*4.24264;}
	    else if(pt > 400){eff = 10+ ud*4.24264*2;}
	}
	else if(fabs(eta) < 2){
	    if(pt < 30){eff = 2.07499+ 2*ud*0.271973;}
	    else if(pt < 40){eff = 2.07499+ ud*0.271973;}
	    else if(pt < 50){eff = 1.6622+ ud*0.296048;}
	    else if(pt < 60){eff = 1.8974+ ud*0.361674;}
	    else if(pt < 70){eff = 1.97044+ ud*0.436242;}
	    else if(pt < 80){eff = 2.65604+ ud*0.585969;}
	    else if(pt < 90){eff = 2.19224+ ud*0.601317;}
	    else if(pt < 100){eff = 1.88235+ ud*0.659219;}
	    else if(pt < 120){eff = 3.64501+ ud*0.746056;}
	    else if(pt < 160){eff = 3.2646+ ud*0.736625;}
	    else if(pt < 210){eff = 4.811+ ud*1.25448;}
	    else if(pt < 260){eff = 4.38596+ ud*1.91797;}
	    else if(pt < 320){eff = 1.72414+ ud*1.70921;}
	    else if(pt < 400){eff = 1.72414+ ud*1.70921;}
	    else if(pt > 400){eff = 1.72414+ ud*1.70921*2;}
	}
	else if(fabs(eta) < 2.4){
	    if(pt < 30){eff = 2.48531+ 2*ud*0.330929;}
	    else if(pt < 40){eff = 2.48531+ ud*0.330929;}
	    else if(pt < 50){eff = 1.5775+ ud*0.326327;}
	    else if(pt < 60){eff = 1.56098+ ud*0.387186;}
	    else if(pt < 70){eff = 2.1164+ ud*0.523472;}
	    else if(pt < 80){eff = 2.70758+ ud*0.689565;}
	    else if(pt < 90){eff = 3.88601+ ud*0.983675;}
	    else if(pt < 100){eff = 3.41297+ ud*1.0607;}
	    else if(pt < 120){eff = 2.89157+ ud*0.822566;}
	    else if(pt < 160){eff = 3.125+ ud*0.822038;}
	    else if(pt < 210){eff = 3.87931+ ud*1.26777;}
	    else if(pt < 260){eff = 5.15464+ ud*2.24503;}
	    else if(pt < 320){eff = 3.33333+ ud*3.27731;}
	    else if(pt < 400){eff = 12.5+ ud*8.26797;}
	    else if(pt > 400){eff = 12.5+ ud*8.26797*2;}
	}
	eff *= 0.01;
	return eff;}
    if(working_point=="CSVT"&&sample=="MC@NLO"){
	if(fabs(eta) < 0.4){
	    if(pt < 30){eff = 0.296132+ 2*ud*0.0452381;}
	    else if(pt < 40){eff = 0.296132+ ud*0.0452381;}
	    else if(pt < 50){eff = 0.221204+ ud*0.0516273;}
	    else if(pt < 60){eff = 0.238632+ ud*0.0503434;}
	    else if(pt < 70){eff = 0.254067+ ud*0.0668486;}
	    else if(pt < 80){eff = 0.402238+ ud*0.0804725;}
	    else if(pt < 90){eff = 0.364025+ ud*0.113024;}
	    else if(pt < 100){eff = 0.22008+ ud*0.130892;}
	    else if(pt < 120){eff = 0.403985+ ud*0.120359;}
	    else if(pt < 160){eff = 0.433147+ ud*0.121617;}
	    else if(pt < 210){eff = 0.563393+ ud*0.18034;}
	    else if(pt < 260){eff = 0.762961+ ud*0.384817;}
	    else if(pt < 320){eff = 1.20093+ ud*0.542295;}
	    else if(pt < 400){eff = -0.190876+ ud*0.331898;}
	    else if(pt > 400){eff = -0.190876+ ud*0.331898*2;}
	}
	else if(fabs(eta) < 0.8){
	    if(pt < 30){eff = 0.300955+ 2*ud*0.048061;}
	    else if(pt < 40){eff = 0.300955+ ud*0.048061;}
	    else if(pt < 50){eff = 0.172722+ ud*0.0467717;}
	    else if(pt < 60){eff = 0.357103+ ud*0.0653694;}
	    else if(pt < 70){eff = 0.247677+ ud*0.0775202;}
	    else if(pt < 80){eff = 0.353467+ ud*0.0846131;}
	    else if(pt < 90){eff = 0.337423+ ud*0.104457;}
	    else if(pt < 100){eff = 0.549491+ ud*0.132181;}
	    else if(pt < 120){eff = 0.546597+ ud*0.126193;}
	    else if(pt < 160){eff = 0.722168+ ud*0.161177;}
	    else if(pt < 210){eff = 0.863065+ ud*0.218788;}
	    else if(pt < 260){eff = 0.929687+ ud*0.350424;}
	    else if(pt < 320){eff = 0.853319+ ud*0.554052;}
	    else if(pt < 400){eff = 1.15689+ ud*0.69563;}
	    else if(pt > 400){eff = 1.15689+ ud*0.69563*2;}
	}
	else if(fabs(eta) < 1.2){
	    if(pt < 30){eff = 0.219806+ 2*ud*0.0476434;}
	    else if(pt < 40){eff = 0.219806+ ud*0.0476434;}
	    else if(pt < 50){eff = 0.111858+ ud*0.0478923;}
	    else if(pt < 60){eff = 0.243355+ ud*0.0572305;}
	    else if(pt < 70){eff = 0.1981+ ud*0.0684963;}
	    else if(pt < 80){eff = 0.195613+ ud*0.0903859;}
	    else if(pt < 90){eff = 0.136341+ ud*0.0943402;}
	    else if(pt < 100){eff = 0.351311+ ud*0.130831;}
	    else if(pt < 120){eff = 0.552058+ ud*0.147022;}
	    else if(pt < 160){eff = 0.566601+ ud*0.130856;}
	    else if(pt < 210){eff = 0.279659+ ud*0.188961;}
	    else if(pt < 260){eff = 0.698269+ ud*0.371921;}
	    else if(pt < 320){eff = 0.679546+ ud*0.51396;}
	    else if(pt < 400){eff = 0.000631487+ ud*0.496977;}
	    else if(pt > 400){eff = 0.000631487+ ud*0.496977*2;}
	}
	else if(fabs(eta) < 1.6){
	    if(pt < 30){eff = 0.300391+ 2*ud*0.0601646;}
	    else if(pt < 40){eff = 0.300391+ ud*0.0601646;}
	    else if(pt < 50){eff = 0.285337+ ud*0.0610283;}
	    else if(pt < 60){eff = 0.251053+ ud*0.0566961;}
	    else if(pt < 70){eff = 0.415759+ ud*0.0973634;}
	    else if(pt < 80){eff = 0.431773+ ud*0.122182;}
	    else if(pt < 90){eff = 0.238684+ ud*0.117254;}
	    else if(pt < 100){eff = 0.399629+ ud*0.143309;}
	    else if(pt < 120){eff = 0.278522+ ud*0.105852;}
	    else if(pt < 160){eff = 0.572249+ ud*0.134524;}
	    else if(pt < 210){eff = 0.452144+ ud*0.165103;}
	    else if(pt < 260){eff = 0.452144+ ud*0.165103;}
	    else if(pt < 320){eff = 0.338985+ ud*0.327449;}
	    else if(pt < 400){eff = 0.511835+ ud*0.511013;}
	    else if(pt > 400){eff = 0.511835+ ud*0.511013*2;}
	}
	else if(fabs(eta) < 2){
	    if(pt < 30){eff = 0.341337+ 2*ud*0.0699983;}
	    else if(pt < 40){eff = 0.341337+ ud*0.0699983;}
	    else if(pt < 50){eff = 0.290676+ ud*0.0711419;}
	    else if(pt < 60){eff = 0.290495+ ud*0.0791968;}
	    else if(pt < 70){eff = 0.396697+ ud*0.120001;}
	    else if(pt < 80){eff = 0.0516845+ ud*0.101085;}
	    else if(pt < 90){eff = 0.468554+ ud*0.139695;}
	    else if(pt < 100){eff = 0.241776+ ud*0.16671;}
	    else if(pt < 120){eff = -0.0176457+ ud*0.121779;}
	    else if(pt < 160){eff = 0.643435+ ud*0.168889;}
	    else if(pt < 210){eff = 0.358655+ ud*0.186923;}
	    else if(pt < 260){eff = 0.20763+ ud*0.282964;}
	    else if(pt < 320){eff = 0.35291+ ud*0.352313;}
	    else if(pt < 400){eff = 0.385349+ ud*0.793742;}
	    else if(pt > 400){eff = 0.385349+ ud*0.793742*2;}
	}
	else if(fabs(eta) < 2.4){
	    if(pt < 30){eff = 0.218104+ 2*ud*0.0605142;}
	    else if(pt < 40){eff = 0.218104+ ud*0.0605142;}
	    else if(pt < 50){eff = 0.142497+ ud*0.0620365;}
	    else if(pt < 60){eff = 0.155213+ ud*0.0720276;}
	    else if(pt < 70){eff = 0.282621+ ud*0.101209;}
	    else if(pt < 80){eff = 0.129075+ ud*0.113064;}
	    else if(pt < 90){eff = -0.0673989+ ud*0.0822003;}
	    else if(pt < 100){eff = 0.0849679+ ud*0.0849397;}
	    else if(pt < 120){eff = 0.0929604+ ud*0.0691141;}
	    else if(pt < 160){eff = 0.408598+ ud*0.187971;}
	    else if(pt < 210){eff = 0.110232+ ud*0.110176;}
	    else if(pt < 260){eff = 0.110232+ ud*0.110176;}
	    else if(pt < 320){eff = 0.110232+ ud*0.110176;}
	    else if(pt < 400){eff = 0.110232+ ud*0.110176;}
	    else if(pt > 400){eff = 0.110232+ ud*0.110176*2;}
	}
	eff = eff*0.01;
	return eff;}
    else{
	return 0;}
}
double broc::BTagEfficiencyWeightProvider::get_BTag_eff_c(double pt, double eta, int UpDown)
{
    double eff = 0.;
    double ud = 0.;
    if(up || UpDown == 1) ud = 1.;
    else if(down || UpDown == -1) ud = -1;
    if(working_point=="CSVM"&&sample=="MC@NLO"){
	if(fabs(eta) < 0.4){
	    if(pt < 20){eff = 19.0766+ 2*ud*0.703526;}
	    else if(pt < 30){eff = 19.0766+ ud*0.703526;}
	    else if(pt < 40){eff = 20.6091+ ud*0.463441;}
	    else if(pt < 50){eff = 20.2823+ ud*0.457614;}
	    else if(pt < 60){eff = 22.4266+ ud*0.49803;}
	    else if(pt < 70){eff = 22.1623+ ud*0.54759;}
	    else if(pt < 80){eff = 21.3845+ ud*0.598392;}
	    else if(pt < 90){eff = 23.0472+ ud*0.69966;}
	    else if(pt < 100){eff = 23.3822+ ud*0.800316;}
	    else if(pt < 120){eff = 23.3119+ ud*0.677484;}
	    else if(pt < 160){eff = 24.2456+ ud*0.694226;}
	    else if(pt < 210){eff = 22.589+ ud*1.06386;}
	    else if(pt < 260){eff = 17.803+ ud*1.66478;}
	    else if(pt < 320){eff = 18.9055+ ud*2.7618;}
	    else if(pt < 400){eff = 10.2941+ ud*2.60577;}
	    else if(pt > 400){eff = 10.2941+ ud*2.60577*2;}
	}
	else if(fabs(eta) < 0.8){
	    if(pt < 20){eff = 19.4046+ 2*ud*0.748964;}
	    else if(pt < 30){eff = 19.4046+ ud*0.748964;}
	    else if(pt < 40){eff = 21.7278+ ud*0.495674;}
	    else if(pt < 50){eff = 21.4256+ ud*0.48365;}
	    else if(pt < 60){eff = 23.8432+ ud*0.534581;}
	    else if(pt < 70){eff = 23.703+ ud*0.575782;}
	    else if(pt < 80){eff = 25.8971+ ud*0.673077;}
	    else if(pt < 90){eff = 26.9506+ ud*0.765688;}
	    else if(pt < 100){eff = 27.7906+ ud*0.878873;}
	    else if(pt < 120){eff = 26.3527+ ud*0.737631;}
	    else if(pt < 160){eff = 27.1591+ ud*0.749677;}
	    else if(pt < 210){eff = 24.9833+ ud*1.1189;}
	    else if(pt < 260){eff = 20.4082+ ud*1.91919;}
	    else if(pt < 320){eff = 20.8531+ ud*2.7968;}
	    else if(pt < 400){eff = 14.2857+ ud*3.20778;}
	    else if(pt > 400){eff = 14.2857+ ud*3.20778*2;}
	}
	else if(fabs(eta) < 1.2){
	    if(pt < 20){eff = 19.0012+ 2*ud*0.790493;}
	    else if(pt < 30){eff = 19.0012+ ud*0.790493;}
	    else if(pt < 40){eff = 20.3532+ ud*0.52722;}
	    else if(pt < 50){eff = 20.6885+ ud*0.523645;}
	    else if(pt < 60){eff = 22.3831+ ud*0.567838;}
	    else if(pt < 70){eff = 22.1768+ ud*0.626222;}
	    else if(pt < 80){eff = 22.9494+ ud*0.710889;}
	    else if(pt < 90){eff = 22.922+ ud*0.804323;}
	    else if(pt < 100){eff = 24.4963+ ud*0.920255;}
	    else if(pt < 120){eff = 24.4209+ ud*0.78151;}
	    else if(pt < 160){eff = 24.9388+ ud*0.809167;}
	    else if(pt < 210){eff = 21.6084+ ud*1.17305;}
	    else if(pt < 260){eff = 19.3467+ ud*1.98004;}
	    else if(pt < 320){eff = 15.873+ ud*2.65807;}
	    else if(pt < 400){eff = 10.5263+ ud*3.14865;}
	    else if(pt > 400){eff = 10.5263+ ud*3.14865*2;}
	}
	else if(fabs(eta) < 1.6){
	    if(pt < 20){eff = 14.8692+ 2*ud*0.762182;}
	    else if(pt < 30){eff = 14.8692+ ud*0.762182;}
	    else if(pt < 40){eff = 17.0571+ ud*0.539208;}
	    else if(pt < 50){eff = 17.2414+ ud*0.554541;}
	    else if(pt < 60){eff = 18.4476+ ud*0.615747;}
	    else if(pt < 70){eff = 19.7143+ ud*0.70885;}
	    else if(pt < 80){eff = 20.3784+ ud*0.799724;}
	    else if(pt < 90){eff = 21.05+ ud*0.915925;}
	    else if(pt < 100){eff = 19.3673+ ud*1.00407;}
	    else if(pt < 120){eff = 20.4373+ ud*0.87911;}
	    else if(pt < 160){eff = 21.1664+ ud*0.915929;}
	    else if(pt < 210){eff = 22.3171+ ud*1.45403;}
	    else if(pt < 260){eff = 12.963+ ud*2.04419;}
	    else if(pt < 320){eff = 15.6863+ ud*3.60088;}
	    else if(pt < 400){eff = 12.9032+ ud*4.2575;}
	    else if(pt > 400){eff = 12.9032+ ud*4.2575*2;}
	}
	else if(fabs(eta) < 2){
	    if(pt < 20){eff = 14.3243+ 2*ud*0.910615;}
	    else if(pt < 30){eff = 14.3243+ ud*0.910615;}
	    else if(pt < 40){eff = 18.0551+ ud*0.669481;}
	    else if(pt < 50){eff = 18.1848+ ud*0.696944;}
	    else if(pt < 60){eff = 18.9675+ ud*0.7753;}
	    else if(pt < 70){eff = 18.2638+ ud*0.841525;}
	    else if(pt < 80){eff = 21.9343+ ud*1.04037;}
	    else if(pt < 90){eff = 21.6172+ ud*1.18238;}
	    else if(pt < 100){eff = 22.6496+ ud*1.36812;}
	    else if(pt < 120){eff = 21.6915+ ud*1.15333;}
	    else if(pt < 160){eff = 21.372+ ud*1.21571;}
	    else if(pt < 210){eff = 18.1628+ ud*1.76157;}
	    else if(pt < 260){eff = 19.5946+ ud*3.26272;}
	    else if(pt < 320){eff = 22.7273+ ud*5.1584;}
	    else if(pt < 400){eff = 10+ ud*5.47723;}
	    else if(pt > 400){eff = 10+ ud*5.47723*2;}
	}
	else if(fabs(eta) < 2.4){
	    if(pt < 20){eff = 11.9349+ 2*ud*0.974841;}
	    else if(pt < 30){eff = 11.9349+ ud*0.974841;}
	    else if(pt < 40){eff = 15.0519+ ud*0.743667;}
	    else if(pt < 50){eff = 14.4477+ ud*0.780695;}
	    else if(pt < 60){eff = 15.6707+ ud*0.89766;}
	    else if(pt < 70){eff = 15.6711+ ud*1.01254;}
	    else if(pt < 80){eff = 19.5931+ ud*1.29875;}
	    else if(pt < 90){eff = 17.6634+ ud*1.42223;}
	    else if(pt < 100){eff = 18.5484+ ud*1.74527;}
	    else if(pt < 120){eff = 16.4326+ ud*1.38877;}
	    else if(pt < 160){eff = 16.6096+ ud*1.54004;}
	    else if(pt < 210){eff = 17.9104+ ud*2.34223;}
	    else if(pt < 260){eff = 16.4179+ ud*4.52562;}
	    else if(pt < 320){eff = 8.33333+ ud*5.64169;}
	    else if(pt < 400){eff = 22.2222+ ud*13.858;}
	    else if(pt > 400){eff = 22.2222+ ud*13.858*2;}
	}
	eff *= 0.01;
        return eff;}
    else if(working_point == "CSVM" && sample == "MadSpin"){
      if(fabs(eta) < 0.4){
	if(pt < 30){eff = 18.4239+ 2*ud*0.750573;}
	else if(pt < 40){eff = 18.4239+ ud*0.750573;}
	else if(pt < 50){eff = 17.577+ ud*0.702811;}
	else if(pt < 60){eff = 19.4815+ ud*0.753702;}
	else if(pt < 70){eff = 19.6495+ ud*0.834651;}
	else if(pt < 80){eff = 19.4637+ ud*0.905867;}
	else if(pt < 90){eff = 20.8434+ ud*1.04032;}
	else if(pt < 100){eff = 21.9637+ ud*1.19126;}
	else if(pt < 120){eff = 22.4657+ ud*1.01514;}
	else if(pt < 160){eff = 20.8906+ ud*0.964747;}
	else if(pt < 210){eff = 21.0933+ ud*1.48379;}
	else if(pt < 260){eff = 17.5846+ ud*2.33602;}
	else if(pt < 320){eff = 18.0786+ ud*3.59643;}
	else if(pt < 400){eff = 13.4886+ ud*4.5453;}
	else if(pt > 400){eff = 13.4886+ ud*4.5453*2;}
      }
      else if(fabs(eta) < 0.8){
	if(pt < 30){eff = 18.8444+ 2*ud*0.791525;}
	else if(pt < 40){eff = 18.8444+ ud*0.791525;}
	else if(pt < 50){eff = 17.8307+ ud*0.740548;}
	else if(pt < 60){eff = 19.6062+ ud*0.790116;}
	else if(pt < 70){eff = 19.5737+ ud*0.85332;}
	else if(pt < 80){eff = 21.0031+ ud*0.98445;}
	else if(pt < 90){eff = 22.1706+ ud*1.11248;}
	else if(pt < 100){eff = 20.3741+ ud*1.22462;}
	else if(pt < 120){eff = 21.2166+ ud*1.04999;}
	else if(pt < 160){eff = 23.7034+ ud*1.06899;}
	else if(pt < 210){eff = 22.4561+ ud*1.61831;}
	else if(pt < 260){eff = 21.3124+ ud*2.68594;}
	else if(pt < 320){eff = 14.091+ ud*3.67894;}
	else if(pt < 400){eff = 18.5302+ ud*4.68669;}
	else if(pt > 400){eff = 18.5302+ ud*4.68669*2;}
      }
      else if(fabs(eta) < 1.2){
	if(pt < 30){eff = 18.0837+ 2*ud*0.861181;}
	else if(pt < 40){eff = 18.0837+ ud*0.861181;}
	else if(pt < 50){eff = 17.8324+ ud*0.824824;}
	else if(pt < 60){eff = 19.1778+ ud*0.891712;}
	else if(pt < 70){eff = 20.6895+ ud*0.989375;}
	else if(pt < 80){eff = 22.1792+ ud*1.09669;}
	else if(pt < 90){eff = 21.9559+ ud*1.22561;}
	else if(pt < 100){eff = 21.1295+ ud*1.39422;}
	else if(pt < 120){eff = 24.1424+ ud*1.2253;}
	else if(pt < 160){eff = 21.0207+ ud*1.184;}
	else if(pt < 210){eff = 21.856+ ud*1.85861;}
	else if(pt < 260){eff = 21.4672+ ud*3.29653;}
	else if(pt < 320){eff = 23.9857+ ud*4.94864;}
	else if(pt < 400){eff = 19.4977+ ud*7.29555;}
	else if(pt > 400){eff = 19.4977+ ud*7.29555*2;}
      }
      else if(fabs(eta) < 1.6){
	if(pt < 30){eff = 15.2794+ 2*ud*0.883299;}
	else if(pt < 40){eff = 15.2794+ ud*0.883299;}
	else if(pt < 50){eff = 15.2841+ ud*0.856549;}
	else if(pt < 60){eff = 14.7938+ ud*0.921468;}
	else if(pt < 70){eff = 15.6687+ ud*1.02014;}
	else if(pt < 80){eff = 17.5983+ ud*1.1897;}
	else if(pt < 90){eff = 18.7467+ ud*1.41265;}
	else if(pt < 100){eff = 21.5112+ ud*1.70923;}
	else if(pt < 120){eff = 18.1726+ ud*1.34567;}
	else if(pt < 160){eff = 19.2893+ ud*1.41332;}
	else if(pt < 210){eff = 18.6689+ ud*2.20848;}
	else if(pt < 260){eff = 16.4755+ ud*3.63682;}
	else if(pt < 320){eff = 9.56502+ ud*3.84993;}
	else if(pt < 400){eff = 20.8227+ ud*8.93606;}
	else if(pt > 400){eff = 20.8227+ ud*8.93606*2;}
      }
      else if(fabs(eta) < 2){
	if(pt < 30){eff = 15.5531+ 2*ud*1.09482;}
	else if(pt < 40){eff = 15.5531+ ud*1.09482;}
	else if(pt < 50){eff = 18.0924+ ud*1.17675;}
	else if(pt < 60){eff = 15.1228+ ud*1.13477;}
	else if(pt < 70){eff = 15.7899+ ud*1.31794;}
	else if(pt < 80){eff = 19.654+ ud*1.63386;}
	else if(pt < 90){eff = 17.2661+ ud*1.7828;}
	else if(pt < 100){eff = 19.749+ ud*2.15832;}
	else if(pt < 120){eff = 20.6099+ ud*1.90177;}
	else if(pt < 160){eff = 18.7245+ ud*1.99931;}
	else if(pt < 210){eff = 16.3033+ ud*3.01613;}
	else if(pt < 260){eff = 25.388+ ud*6.11194;}
	else if(pt < 320){eff = 10.9589+ ud*10.3758;}
	else if(pt < 400){eff = 14.5968+ ud*13.4655;}
	else if(pt > 400){eff = 14.5968+ ud*13.4655*2;}
      }
      else if(fabs(eta) < 2.4){
	if(pt < 30){eff = 15.6168+ 2*ud*1.33498;}
	else if(pt < 40){eff = 15.6168+ ud*1.33498;}
	else if(pt < 50){eff = 13.7577+ ud*1.31159;}
	else if(pt < 60){eff = 13.205+ ud*1.36995;}
	else if(pt < 70){eff = 15.8152+ ud*1.69058;}
	else if(pt < 80){eff = 14.6647+ ud*1.86689;}
	else if(pt < 90){eff = 15.7531+ ud*2.29197;}
	else if(pt < 100){eff = 22.1754+ ud*3.0745;}
	else if(pt < 120){eff = 13.3412+ ud*2.35856;}
	else if(pt < 160){eff = 14.0263+ ud*2.62327;}
	else if(pt < 210){eff = 19.07+ ud*5.12239;}
	else if(pt < 260){eff = 24.498+ ud*12.3236;}
	else if(pt < 320){eff = 24.498+ ud*12.3236;}
	else if(pt < 400){eff = 100+ ud*0;}
	else if(pt > 400){eff = 100+ ud*0*2;}
      }
	eff *= 0.01;
        return eff;}
    else if(working_point == "CSVT" && sample == "MadSpin"){
		if(fabs(eta) < 0.4){
			if(pt < 30){eff = 6.64339+ 2*ud*0.488993;}
			else if(pt < 40){eff = 6.64339+ ud*0.488993;}
			else if(pt < 50){eff = 6.97704+ ud*0.535639;}
			else if(pt < 60){eff = 6.87013+ ud*0.570201;}
			else if(pt < 70){eff = 6.56897+ ud*0.627521;}
			else if(pt < 80){eff = 6.63863+ ud*0.671943;}
			else if(pt < 90){eff = 5.71851+ ud*0.724083;}
			else if(pt < 100){eff = 5.41677+ ud*0.833692;}
			else if(pt < 120){eff = 6.11668+ ud*0.720608;}
			else if(pt < 160){eff = 5.70289+ ud*0.706533;}
			else if(pt < 210){eff = 3.38084+ ud*0.877554;}
			else if(pt < 260){eff = 4.74673+ ud*1.70119;}
			else if(pt < 320){eff = 0.627041+ ud*0.627633;}
			else if(pt < 400){eff = 0.627041+ ud*0.627633;}
			else if(pt > 400){eff = 0.627041+ ud*0.627633*2;}
		}
		else if(fabs(eta) < 0.8){
			if(pt < 30){eff = 6.94335+ 2*ud*0.532156;}
			else if(pt < 40){eff = 6.94335+ ud*0.532156;}
			else if(pt < 50){eff = 5.30132+ ud*0.482828;}
			else if(pt < 60){eff = 6.9339+ ud*0.581593;}
			else if(pt < 70){eff = 6.1091+ ud*0.604407;}
			else if(pt < 80){eff = 6.65829+ ud*0.709692;}
			else if(pt < 90){eff = 6.81027+ ud*0.840405;}
			else if(pt < 100){eff = 6.77116+ ud*0.942225;}
			else if(pt < 120){eff = 5.91391+ ud*0.745983;}
			else if(pt < 160){eff = 6.52819+ ud*0.803521;}
			else if(pt < 210){eff = 2.8407+ ud*0.918759;}
			else if(pt < 260){eff = 0.763314+ ud*0.760732;}
			else if(pt < 320){eff = 1.70704+ ud*1.69383;}
			else if(pt < 400){eff = 2.33298+ ud*2.30688;}
			else if(pt > 400){eff = 2.33298+ ud*2.30688*2;}
		}
		else if(fabs(eta) < 1.2){
			if(pt < 30){eff = 5.87099+ 2*ud*0.547692;}
			else if(pt < 40){eff = 5.87099+ ud*0.547692;}
			else if(pt < 50){eff = 4.93634+ ud*0.515428;}
			else if(pt < 60){eff = 7.26257+ ud*0.706481;}
			else if(pt < 70){eff = 5.39166+ ud*0.665765;}
			else if(pt < 80){eff = 5.87372+ ud*0.740061;}
			else if(pt < 90){eff = 6.14397+ ud*0.857729;}
			else if(pt < 100){eff = 7.39295+ ud*1.12183;}
			else if(pt < 120){eff = 6.01464+ ud*0.84251;}
			else if(pt < 160){eff = 5.40005+ ud*0.867214;}
			else if(pt < 210){eff = 4.55436+ ud*1.17534;}
			else if(pt < 260){eff = 0.78712+ ud*0.785179;}
			else if(pt < 320){eff = 2.2557+ ud*1.79621;}
			else if(pt < 400){eff = 2.2557+ ud*1.79621;}
			else if(pt > 400){eff = 2.2557+ ud*1.79621*2;}
		}
		else if(fabs(eta) < 1.6){
			if(pt < 30){eff = 5.29281+ 2*ud*0.557909;}
			else if(pt < 40){eff = 5.29281+ ud*0.557909;}
			else if(pt < 50){eff = 6.44101+ ud*0.652917;}
			else if(pt < 60){eff = 5.14867+ ud*0.661182;}
			else if(pt < 70){eff = 3.96031+ ud*0.652896;}
			else if(pt < 80){eff = 5.25211+ ud*0.847545;}
			else if(pt < 90){eff = 7.17229+ ud*1.12462;}
			else if(pt < 100){eff = 5.37345+ ud*1.19924;}
			else if(pt < 120){eff = 4.64615+ ud*0.898998;}
			else if(pt < 160){eff = 4.50556+ ud*0.94222;}
			else if(pt < 210){eff = 3.39148+ ud*1.35215;}
			else if(pt < 260){eff = 3.39148+ ud*1.35215;}
			else if(pt < 320){eff = 3.38001+ ud*3.3282;}
			else if(pt < 400){eff = 3.38001+ ud*3.3282;}
			else if(pt > 400){eff = 3.38001+ ud*3.3282*2;}
		}
		else if(fabs(eta) < 2){
			if(pt < 30){eff = 5.33016+ 2*ud*0.683206;}
			else if(pt < 40){eff = 5.33016+ ud*0.683206;}
			else if(pt < 50){eff = 6.1779+ ud*0.817825;}
			else if(pt < 60){eff = 5.05374+ ud*0.824203;}
			else if(pt < 70){eff = 4.90066+ ud*0.901156;}
			else if(pt < 80){eff = 7.11388+ ud*1.28663;}
			else if(pt < 90){eff = 5.26845+ ud*1.27309;}
			else if(pt < 100){eff = 4.46953+ ud*1.43621;}
			else if(pt < 120){eff = 4.43911+ ud*1.21227;}
			else if(pt < 160){eff = 2.2068+ ud*0.945823;}
			else if(pt < 210){eff = 2.98194+ ud*2.1125;}
			else if(pt < 260){eff = 3.12567+ ud*3.08234;}
			else if(pt < 320){eff = 3.12567+ ud*3.08234;}
			else if(pt < 400){eff = 3.12567+ ud*3.08234;}
			else if(pt > 400){eff = 3.12567+ ud*3.08234*2;}
		}
		else if(fabs(eta) < 2.4){
			if(pt < 30){eff = 3.06233+ 2*ud*0.62557;}
			else if(pt < 40){eff = 3.06233+ ud*0.62557;}
			else if(pt < 50){eff = 3.96141+ ud*0.872359;}
			else if(pt < 60){eff = 2.88828+ ud*0.757013;}
			else if(pt < 70){eff = 4.6538+ ud*1.23064;}
			else if(pt < 80){eff = 3.26601+ ud*1.05475;}
			else if(pt < 90){eff = 4.315+ ud*1.59902;}
			else if(pt < 100){eff = 4.56741+ ud*1.9057;}
			else if(pt < 120){eff = 2.90896+ ud*1.47238;}
			else if(pt < 160){eff = 0.172326+ ud*0.172724;}
			else if(pt < 210){eff = 5.60019+ ud*3.91172;}
			else if(pt < 260){eff = 11.4026+ ud*10.6148;}
			else if(pt < 320){eff = 11.4026+ ud*10.6148;}
			else if(pt < 400){eff = 11.4026+ ud*10.6148;}
			else if(pt > 400){eff = 11.4026+ ud*10.6148*2;}
		}
		eff *= 0.01;
	return eff;}
    else if(working_point == "CSVT" && sample == "MadGraph"){
      if(fabs(eta) < 0.4){
	if(pt < 30){eff = 6.43596+ 2*ud*0.777147;}
	else if(pt < 40){eff = 6.43596+ ud*0.777147;}
	else if(pt < 50){eff = 5.70384+ ud*0.789313;}
	else if(pt < 60){eff = 7.08687+ ud*0.896875;}
	else if(pt < 70){eff = 6.20047+ ud*0.943534;}
	else if(pt < 80){eff = 6.97669+ ud*1.09103;}
	else if(pt < 90){eff = 5.83642+ ud*1.17018;}
	else if(pt < 100){eff = 8.10446+ ud*1.51925;}
	else if(pt < 120){eff = 7.25846+ ud*1.31791;}
	else if(pt < 160){eff = 7.16315+ ud*1.34119;}
	else if(pt < 210){eff = 4.50769+ ud*1.74431;}
	else if(pt < 260){eff = 3.58677+ ud*2.48796;}
	else if(pt < 320){eff = 3.58677+ ud*2.48796;}
	else if(pt < 400){eff = 3.58677+ ud*2.48796;}
	else if(pt > 400){eff = 3.58677+ ud*2.48796*2;}
      }
      else if(fabs(eta) < 0.8){
	if(pt < 30){eff = 5.84464+ 2*ud*0.818228;}
	else if(pt < 40){eff = 5.84464+ ud*0.818228;}
	else if(pt < 50){eff = 7.01539+ ud*0.884296;}
	else if(pt < 60){eff = 6.38749+ ud*0.950835;}
	else if(pt < 70){eff = 5.19955+ ud*0.957136;}
	else if(pt < 80){eff = 8.57943+ ud*1.36221;}
	else if(pt < 90){eff = 8.10439+ ud*1.44104;}
	else if(pt < 100){eff = 6.91284+ ud*1.51069;}
	else if(pt < 120){eff = 7.77083+ ud*1.3858;}
	else if(pt < 160){eff = 7.87362+ ud*1.39233;}
	else if(pt < 210){eff = 2.68661+ ud*1.19609;}
	else if(pt < 260){eff = 2.06005+ ud*2.03966;}
	else if(pt < 320){eff = 2.06005+ ud*2.03966;}
	else if(pt < 400){eff = 2.06005+ ud*2.03966;}
	else if(pt > 400){eff = 2.06005+ ud*2.03966*2;}
      }
      else if(fabs(eta) < 1.2){
	if(pt < 30){eff = 6.73115+ 2*ud*0.872119;}
	else if(pt < 40){eff = 6.73115+ ud*0.872119;}
	else if(pt < 50){eff = 5.11367+ ud*0.815571;}
	else if(pt < 60){eff = 6.69217+ ud*1.04571;}
	else if(pt < 70){eff = 5.67254+ ud*1.07761;}
	else if(pt < 80){eff = 6.90289+ ud*1.36771;}
	else if(pt < 90){eff = 6.64885+ ud*1.48399;}
	else if(pt < 100){eff = 4.96133+ ud*1.40942;}
	else if(pt < 120){eff = 5.30682+ ud*1.28357;}
	else if(pt < 160){eff = 5.45264+ ud*1.30554;}
	else if(pt < 210){eff = 4.0837+ ud*1.81225;}
	else if(pt < 260){eff = 6.30195+ ud*4.06021;}
	else if(pt < 320){eff = 6.30195+ ud*4.06021;}
	else if(pt < 400){eff = 6.30195+ ud*4.06021;}
	else if(pt > 400){eff = 6.30195+ ud*4.06021*2;}
      }
      else if(fabs(eta) < 1.6){
	if(pt < 30){eff = 5.22294+ 2*ud*0.883745;}
	else if(pt < 40){eff = 5.22294+ ud*0.883745;}
	else if(pt < 50){eff = 4.47469+ ud*0.915723;}
	else if(pt < 60){eff = 7.0321+ ud*1.27135;}
	else if(pt < 70){eff = 4.88361+ ud*1.08389;}
	else if(pt < 80){eff = 5.13473+ ud*1.4157;}
	else if(pt < 90){eff = 6.32004+ ud*1.7088;}
	else if(pt < 100){eff = 7.3222+ ud*2.1891;}
	else if(pt < 120){eff = 3.53126+ ud*1.27019;}
	else if(pt < 160){eff = 2.62318+ ud*1.06562;}
	else if(pt < 210){eff = 7.0417+ ud*3.03748;}
	else if(pt < 260){eff = 7.0417+ ud*3.03748;}
	else if(pt < 320){eff = 7.0417+ ud*3.03748;}
	else if(pt < 400){eff = 7.0417+ ud*3.03748;}
	else if(pt > 400){eff = 7.0417+ ud*3.03748*2;}
      }
      else if(fabs(eta) < 2){
	if(pt < 30){eff = 3.37566+ 2*ud*0.895559;}
	else if(pt < 40){eff = 3.37566+ ud*0.895559;}
	else if(pt < 50){eff = 3.96739+ ud*1.02552;}
	else if(pt < 60){eff = 3.0468+ ud*0.978744;}
	else if(pt < 70){eff = 5.34485+ ud*1.56583;}
	else if(pt < 80){eff = 8.27147+ ud*2.08439;}
	else if(pt < 90){eff = 6.91107+ ud*2.24409;}
	else if(pt < 100){eff = 9.46602+ ud*3.15643;}
	else if(pt < 120){eff = 7.33725+ ud*2.61865;}
	else if(pt < 160){eff = 8.50412+ ud*3.07834;}
	else if(pt < 210){eff = 3.70087+ ud*3.62415;}
	else if(pt < 260){eff = 8.21344+ ud*7.89057;}
	else if(pt < 320){eff = 8.21344+ ud*7.89057;}
	else if(pt < 400){eff = 8.21344+ ud*7.89057;}
	else if(pt > 400){eff = 8.21344+ ud*7.89057*2;}
      }
      else if(fabs(eta) < 2.4){
	if(pt < 30){eff = 4.18262+ 2*ud*1.1381;}
	else if(pt < 40){eff = 4.18262+ ud*1.1381;}
	else if(pt < 50){eff = 3.00688+ ud*1.12937;}
	else if(pt < 60){eff = 2.93723+ ud*1.1138;}
	else if(pt < 70){eff = 4.39663+ ud*1.7269;}
	else if(pt < 80){eff = 4.01571+ ud*2.02029;}
	else if(pt < 90){eff = 10.586+ ud*3.83567;}
	else if(pt < 100){eff = 2.381+ ud*2.24902;}
	else if(pt < 120){eff = 1.60751+ ud*1.59772;}
	else if(pt < 160){eff = 2.81752+ ud*2.77277;}
	else if(pt < 210){eff = 2.81752+ ud*2.77277;}
	else if(pt < 260){eff = 2.81752+ ud*2.77277;}
	else if(pt < 320){eff = 2.81752+ ud*2.77277;}
	else if(pt < 400){eff = 2.81752+ ud*2.77277;}
	else if(pt > 400){eff = 2.81752+ ud*2.77277*2;}
      }
	eff *= 0.01;
        return eff;}
    else if(working_point == "CSVM" && sample == "MadGraph"){
	if(fabs(eta) < 0.4){
	    if(pt < 30){eff = 19.5187+ 2*ud*1.18325;}
	    else if(pt < 40){eff = 19.5187+ ud*1.18325;}
	    else if(pt < 50){eff = 18.5185+ ud*1.22899;}
	    else if(pt < 60){eff = 19.7714+ ud*1.34642;}
	    else if(pt < 70){eff = 19.943+ ud*1.50809;}
	    else if(pt < 80){eff = 22.5326+ ud*1.80293;}
	    else if(pt < 90){eff = 18.9247+ ud*1.81649;}
	    else if(pt < 100){eff = 23.5821+ ud*2.31935;}
	    else if(pt < 120){eff = 17.3719+ ud*1.78799;}
	    else if(pt < 160){eff = 26.0674+ ud*2.08107;}
	    else if(pt < 210){eff = 24.8677+ ud*3.14413;}
	    else if(pt < 260){eff = 22.449+ ud*5.96066;}
	    else if(pt < 320){eff = 6.66667+ ud*4.5542;}
	    else if(pt < 400){eff = 15+ ud*7.98436;}
	    else if(pt > 400){eff = 15+ ud*7.98436*2;}
	}
	else if(fabs(eta) < 0.8){
	    if(pt < 30){eff = 17.0061+ 2*ud*1.19886;}
	    else if(pt < 40){eff = 17.0061+ ud*1.19886;}
	    else if(pt < 50){eff = 18.5841+ ud*1.29372;}
	    else if(pt < 60){eff = 22.1118+ ud*1.46268;}
	    else if(pt < 70){eff = 22.5197+ ud*1.65764;}
	    else if(pt < 80){eff = 22.869+ ud*1.91499;}
	    else if(pt < 90){eff = 26.9136+ ud*2.20382;}
	    else if(pt < 100){eff = 28.6645+ ud*2.58081;}
	    else if(pt < 120){eff = 23.7557+ ud*2.02431;}
	    else if(pt < 160){eff = 21.8527+ ud*2.01404;}
	    else if(pt < 210){eff = 21.1429+ ud*3.08662;}
	    else if(pt < 260){eff = 20.7547+ ud*5.57067;}
	    else if(pt < 320){eff = 18.75+ ud*6.89981;}
	    else if(pt < 400){eff = 5.26316+ ud*5.12278;}
	    else if(pt > 400){eff = 5.26316+ ud*5.12278*2;}
	}
	else if(fabs(eta) < 1.2){
	    if(pt < 30){eff = 15.3763+ 2*ud*1.18285;}
	    else if(pt < 40){eff = 15.3763+ ud*1.18285;}
	    else if(pt < 50){eff = 14.4487+ ud*1.25167;}
	    else if(pt < 60){eff = 19.1919+ ud*1.61582;}
	    else if(pt < 70){eff = 20.4545+ ud*1.8335;}
	    else if(pt < 80){eff = 22.0049+ ud*2.04848;}
	    else if(pt < 90){eff = 24.0854+ ud*2.36104;}
	    else if(pt < 100){eff = 20.8333+ ud*2.62147;}
	    else if(pt < 120){eff = 18.9744+ ud*1.98547;}
	    else if(pt < 160){eff = 22.7425+ ud*2.42412;}
	    else if(pt < 210){eff = 25.1908+ ud*3.79283;}
	    else if(pt < 260){eff = 21.6216+ ud*6.7677;}
	    else if(pt < 320){eff = 33.3333+ ud*11.1111;}
	    else if(pt < 400){eff = 10+ ud*9.48683;}
	    else if(pt > 400){eff = 10+ ud*9.48683*2;}
	}
	else if(fabs(eta) < 1.6){
	    if(pt < 30){eff = 14.3062+ 2*ud*1.32433;}
	    else if(pt < 40){eff = 14.3062+ ud*1.32433;}
	    else if(pt < 50){eff = 16.1398+ ud*1.50068;}
	    else if(pt < 60){eff = 15.5009+ ud*1.57354;}
	    else if(pt < 70){eff = 18.8776+ ud*1.97652;}
	    else if(pt < 80){eff = 15.8088+ ud*2.21207;}
	    else if(pt < 90){eff = 21.0526+ ud*2.59402;}
	    else if(pt < 100){eff = 21.4689+ ud*3.08631;}
	    else if(pt < 120){eff = 17.9167+ ud*2.47543;}
	    else if(pt < 160){eff = 22.0657+ ud*2.84141;}
	    else if(pt < 210){eff = 18.8235+ ud*4.23991;}
	    else if(pt < 260){eff = 23.5294+ ud*10.2879;}
	    else if(pt < 320){eff = 22.2222+ ud*13.858;}
	    else if(pt < 400){eff = 22.2222+ ud*13.858;}
	    else if(pt > 400){eff = 22.2222+ ud*13.858*2;}
	}
	else if(fabs(eta) < 2){
	    if(pt < 30){eff = 14.1372+ 2*ud*1.58859;}
	    else if(pt < 40){eff = 14.1372+ ud*1.58859;}
	    else if(pt < 50){eff = 15.9817+ ud*1.7509;}
	    else if(pt < 60){eff = 14.3345+ ud*2.0472;}
	    else if(pt < 70){eff = 15+ ud*2.30489;}
	    else if(pt < 80){eff = 16.9399+ ud*2.77285;}
	    else if(pt < 90){eff = 18.4932+ ud*3.21311;}
	    else if(pt < 100){eff = 23.1579+ ud*4.328;}
	    else if(pt < 120){eff = 23.5294+ ud*3.63733;}
	    else if(pt < 160){eff = 24.1758+ ud*4.48822;}
	    else if(pt < 210){eff = 21.2766+ ud*5.96972;}
	    else if(pt < 260){eff = 25+ ud*21.6506;}
	    else if(pt < 320){eff = 25+ ud*21.6506;}
	    else if(pt < 400){eff = 25+ ud*21.6506;}
	    else if(pt > 400){eff = 25+ ud*21.6506*2;}
	}
	else if(fabs(eta) < 2.4){
	    if(pt < 30){eff = 12.3529+ 2*ud*1.78449;}
	    else if(pt < 40){eff = 12.3529+ ud*1.78449;}
	    else if(pt < 50){eff = 13.5659+ ud*2.13185;}
	    else if(pt < 60){eff = 12.8713+ ud*2.35622;}
	    else if(pt < 70){eff = 15.8824+ ud*2.80334;}
	    else if(pt < 80){eff = 16.9811+ ud*3.64686;}
	    else if(pt < 90){eff = 18.5714+ ud*4.64796;}
	    else if(pt < 100){eff = 14.5833+ ud*5.09424;}
	    else if(pt < 120){eff = 10+ ud*3.3541;}
	    else if(pt < 160){eff = 20.339+ ud*5.24036;}
	    else if(pt < 210){eff = 13.3333+ ud*8.77707;}
	    else if(pt < 260){eff = 25+ ud*21.6506;}
	    else if(pt < 320){eff = 25+ ud*21.6506;}
	    else if(pt < 400){eff = 25+ ud*21.6506;}
	    else if(pt > 400){eff = 25+ ud*21.6506*2;}
	}
	eff *= 0.01;
        return eff;}
    else if(working_point == "CSVT" && sample == "MC@NLO"){
	if(fabs(eta) < 0.4){
	    if(pt < 30){eff = 8.80463+ 2*ud*0.505706;}
	    else if(pt < 40){eff = 8.80463+ ud*0.505706;}
	    else if(pt < 50){eff = 7.86047+ ud*0.527484;}
	    else if(pt < 60){eff = 7.95603+ ud*0.533692;}
	    else if(pt < 70){eff = 8.22254+ ud*0.593592;}
	    else if(pt < 80){eff = 8.31284+ ud*0.695419;}
	    else if(pt < 90){eff = 6.66923+ ud*0.731689;}
	    else if(pt < 100){eff = 7.91744+ ud*0.846526;}
	    else if(pt < 120){eff = 7.12206+ ud*0.725025;}
	    else if(pt < 160){eff = 6.04259+ ud*0.654821;}
	    else if(pt < 210){eff = 4.22551+ ud*0.834511;}
	    else if(pt < 260){eff = 2.79784+ ud*1.26012;}
	    else if(pt < 320){eff = -0.311714+ ud*1.88879;}
	    else if(pt < 400){eff = 3.37053+ ud*2.06864;}
	    else if(pt > 400){eff = 3.37053+ ud*2.06864*2;}
	}
	else if(fabs(eta) < 0.8){
	    if(pt < 30){eff = 7.42399+ 2*ud*0.504956;}
	    else if(pt < 40){eff = 7.42399+ ud*0.504956;}
	    else if(pt < 50){eff = 8.96735+ ud*0.554611;}
	    else if(pt < 60){eff = 8.11177+ ud*0.588514;}
	    else if(pt < 70){eff = 8.01966+ ud*0.637157;}
	    else if(pt < 80){eff = 8.56683+ ud*0.755386;}
	    else if(pt < 90){eff = 7.50438+ ud*0.800574;}
	    else if(pt < 100){eff = 8.17164+ ud*0.916455;}
	    else if(pt < 120){eff = 7.37056+ ud*0.727939;}
	    else if(pt < 160){eff = 8.74162+ ud*0.803726;}
	    else if(pt < 210){eff = 3.72789+ ud*0.934448;}
	    else if(pt < 260){eff = 4.90536+ ud*1.60855;}
	    else if(pt < 320){eff = 4.69668+ ud*2.36364;}
	    else if(pt < 400){eff = -1.88951+ ud*2.72044;}
	    else if(pt > 400){eff = -1.88951+ ud*2.72044*2;}
	}
	else if(fabs(eta) < 1.2){
	    if(pt < 30){eff = 7.13488+ 2*ud*0.54899;}
	    else if(pt < 40){eff = 7.13488+ ud*0.54899;}
	    else if(pt < 50){eff = 6.80608+ ud*0.551915;}
	    else if(pt < 60){eff = 6.43646+ ud*0.602686;}
	    else if(pt < 70){eff = 6.86236+ ud*0.634102;}
	    else if(pt < 80){eff = 7.55827+ ud*0.76515;}
	    else if(pt < 90){eff = 7.87831+ ud*0.86513;}
	    else if(pt < 100){eff = 8.94921+ ud*1.00448;}
	    else if(pt < 120){eff = 7.0247+ ud*0.876758;}
	    else if(pt < 160){eff = 5.85144+ ud*0.756131;}
	    else if(pt < 210){eff = 3.31381+ ud*1.01483;}
	    else if(pt < 260){eff = 5.94835+ ud*2.08123;}
	    else if(pt < 320){eff = 2.33007+ ud*1.73444;}
	    else if(pt < 400){eff = -2.47093+ ud*2.57284;}
	    else if(pt > 400){eff = -2.47093+ ud*2.57284*2;}
	}
	else if(fabs(eta) < 1.6){
	    if(pt < 30){eff = 7.15457+ 2*ud*0.583954;}
	    else if(pt < 40){eff = 7.15457+ ud*0.583954;}
	    else if(pt < 50){eff = 6.78994+ ud*0.616532;}
	    else if(pt < 60){eff = 5.78198+ ud*0.665075;}
	    else if(pt < 70){eff = 6.90233+ ud*0.758919;}
	    else if(pt < 80){eff = 7.22619+ ud*0.844357;}
	    else if(pt < 90){eff = 5.65573+ ud*0.962479;}
	    else if(pt < 100){eff = 4.81446+ ud*0.884579;}
	    else if(pt < 120){eff = 5.38058+ ud*0.847111;}
	    else if(pt < 160){eff = 5.60565+ ud*0.777939;}
	    else if(pt < 210){eff = 1.59079+ ud*0.963265;}
	    else if(pt < 260){eff = 2.62514+ ud*1.49901;}
	    else if(pt < 320){eff = 1.18606+ ud*1.19244;}
	    else if(pt < 400){eff = 1.18606+ ud*1.19244;}
	    else if(pt > 400){eff = 1.18606+ ud*1.19244*2;}
	}
	else if(fabs(eta) < 2){
	    if(pt < 30){eff = 6.38404+ 2*ud*0.685314;}
	    else if(pt < 40){eff = 6.38404+ ud*0.685314;}
	    else if(pt < 50){eff = 6.7464+ ud*0.720821;}
	    else if(pt < 60){eff = 7.03133+ ud*0.819569;}
	    else if(pt < 70){eff = 6.98073+ ud*0.91529;}
	    else if(pt < 80){eff = 6.55667+ ud*1.13887;}
	    else if(pt < 90){eff = 6.14145+ ud*1.16254;}
	    else if(pt < 100){eff = 8.54363+ ud*1.53665;}
	    else if(pt < 120){eff = 4.828+ ud*1.03795;}
	    else if(pt < 160){eff = 4.81366+ ud*1.00009;}
	    else if(pt < 210){eff = 1.61249+ ud*1.60914;}
	    else if(pt < 260){eff = -1.79813+ ud*1.84639;}
	    else if(pt < 320){eff = -1.79813+ ud*1.84639;}
	    else if(pt < 400){eff = -1.79813+ ud*1.84639;}
	    else if(pt > 400){eff = -1.79813+ ud*1.84639*2;}
	}
	else if(fabs(eta) < 2.4){
	    if(pt < 30){eff = 5.96427+ 2*ud*0.724405;}
	    else if(pt < 40){eff = 5.96427+ ud*0.724405;}
	    else if(pt < 50){eff = 4.13059+ ud*0.852874;}
	    else if(pt < 60){eff = 3.82334+ ud*0.819042;}
	    else if(pt < 70){eff = 5.47659+ ud*1.03558;}
	    else if(pt < 80){eff = 4.20424+ ud*1.10347;}
	    else if(pt < 90){eff = 5.0264+ ud*1.5285;}
	    else if(pt < 100){eff = 3.63488+ ud*1.149;}
	    else if(pt < 120){eff = 3.97924+ ud*1.35524;}
	    else if(pt < 160){eff = 3.34734+ ud*1.24978;}
	    else if(pt < 210){eff = 3.34734+ ud*1.24978;}
	    else if(pt < 260){eff = 6.20264+ ud*4.3475;}
	    else if(pt < 320){eff = 6.20264+ ud*4.3475;}
	    else if(pt < 400){eff = 17.757+ ud*16.1511;}
	    else if(pt > 400){eff = 17.757+ ud*16.1511*2;}
	}
	
	eff *= 0.01;
        return eff;}

    else{
	return 0;
    }
}

double broc::BTagEfficiencyWeightProvider::get_weight(int UpDown_b, int UpDown_light)
{
	double weight = 1.;

	if(do_not_reweight)
		weight = 1.;
	else{
	  if(jets->size() >= 1){
	    if(get_MC_Prob(UpDown_b, UpDown_light) > 0){
	      weight = get_Data_Prob(UpDown_b, UpDown_light)/get_MC_Prob(UpDown_b, UpDown_light);
	    }
	  }else{
		  std::cerr << "No jets to apply b-tag weights upon in event!" << std::endl;
	  }
	}

	if(apply_sync_factor && jets->size() >= 1){
		weight *= get_sync_factor((*jets)[0].bDiscriminator("combinedSecondaryVertexBJetTags"));
	}
	return weight;
}

double broc::BTagEfficiencyWeightProvider::get_sync_factor(double bDiscrim)
{
	double sync_factor = 1.;

	if(bDiscrim < 0.05){ sync_factor = 0.995078; }
	else if(bDiscrim < 0.10){ sync_factor = 1.094705; }
	else if(bDiscrim < 0.15){ sync_factor = 1.245757; }
	else if(bDiscrim < 0.20){ sync_factor = 1.260674; }
	else if(bDiscrim < 0.25){ sync_factor = 1.188845; }
	else if(bDiscrim < 0.30){ sync_factor = 1.287781; }
	else if(bDiscrim < 0.35){ sync_factor = 1.558122; }
	else if(bDiscrim < 0.40){ sync_factor = 1.467609; }
	else if(bDiscrim < 0.45){ sync_factor = 1.272275; }
	else if(bDiscrim < 0.50){ sync_factor = 1.234178; }
	else if(bDiscrim < 0.55){ sync_factor = 1.282457; }
	else if(bDiscrim < 0.60){ sync_factor = 1.329020; }
	else if(bDiscrim < 0.65){ sync_factor = 1.115744; }
	else if(bDiscrim < 0.70){ sync_factor = 1.349657; }
	else if(bDiscrim < 0.75){ sync_factor = 1.283039; }
	else if(bDiscrim < 0.80){ sync_factor = 1.062466; }
	else if(bDiscrim < 0.85){ sync_factor = 1.023583; }
	else if(bDiscrim < 0.90){ sync_factor = 1.276426; }
	else if(bDiscrim < 0.95){ sync_factor = 1.116366; }
	else if(bDiscrim < 1.00){ sync_factor = 1.098246; }

	return sync_factor;

}
