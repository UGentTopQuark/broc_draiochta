#include "../../interface/EventSelection/SpinPlots.h"

SpinPlots::SpinPlots(std::string ident, bool gen_evt_info)
{
  id = ident;
  plot_gen_evt_info = gen_evt_info;
  nmatched = 0;
  total = 0;
  matched_merged_event = 0;
}

SpinPlots::~SpinPlots()
{
  this->ttbar = false;
  this->decay = NULL;
  this->lepton = NULL;
  this->quark = NULL;
  this->quark_bar = NULL;
  this->Top_had = NULL;
  this->Top_lep = NULL;
  this->B_had = NULL;
  this->B_lep = NULL;
  this->W_had = NULL;
  this->W_lep = NULL;
  this->evt_info = NULL;
  this->plot_gen_evt_info = false;
  std::cout<<"nmatched = "<<(double) nmatched/total<<std::endl;
  std::cout<<"matched_merged_event = "<<matched_merged_event<<std::endl;
  //  nmatched = 0;
  //total = 0;
}

void SpinPlots::plot_all()
{
  initialise();
  select_and_plot();
  print_info(0.,0.,0.,0.,0.);
}

void SpinPlots::initialise()
{
  this->ttbar = gen_evt->is_ttbar();
  this->decay = gen_evt->decay_channel();
  this->lepton = gen_evt->reco_lepton();
  this->quark = gen_evt->q();
  this->quark_bar = gen_evt->qbar();
  this->Top_had = gen_evt->hadT();
  this->Top_lep = gen_evt->lepT();
  this->B_had = gen_evt->hadB();
  this->B_lep = gen_evt->lepB();
  this->W_had = gen_evt->hadW();
  this->W_lep = gen_evt->lepW();
  this->evt_info = handle_holder->get_event_information();
  this->jets = handle_holder->get_tight_jets();
}

void SpinPlots::book_histos()
{
  histos1d[("downtaggingeff"+id).c_str()]=histo_writer->create_1d(("downtaggingeff"+id).c_str(),"efficiency of downtagging",3,0.,3.,"Lowest Energy - Closest to B - both");
  histos1d[("downtaggingpur"+id).c_str()]=histo_writer->create_1d(("downtaggingpur"+id).c_str(),"purity of downtagging",4,-1,3.,"Lowest Energy - Closest to B - both");

  histos1d[("coslb"+id).c_str()]=histo_writer->create_1d(("coslb"+id).c_str(),"cos phi between lepton and b-type quark in their top rest frame",50,-1,1,"cos delta phi");
  histos1d[("cosdb"+id).c_str()]=histo_writer->create_1d(("cosdb"+id).c_str(),"cos phi between down and b-type quark in their top rest frame",50,-1,1,"cos delta phi");
  histos1d[("cosld"+id).c_str()]=histo_writer->create_1d(("cosld"+id).c_str(),"cos phi between lepton and d-type quark in top parent rest frame",50,-1,1,"cos delta phi");
  histos1d[("cosldZMF"+id).c_str()]=histo_writer->create_1d(("cosldZMF"+id).c_str(),"cos phi between lepton and d-type quark in top parent rest frame",50,-1,1,"cos delta phi");
  histos1d[("coslu"+id).c_str()]=histo_writer->create_1d(("coslu"+id).c_str(),"cos phi between lepton and u-type quark in top parent rest frame",50,-1,1,"cos delta phi");
  histos1d[("coslW"+id).c_str()]=histo_writer->create_1d(("coslW"+id).c_str(),"cos phi between lepton and hadronic W in top parent rest frame",50,-1,1,"cos delta phi");
  histos2d[("cosltdt"+id).c_str()]=histo_writer->create_2d(("cosltdt"+id).c_str(),"cos phi between lepton and t and d-type quark and t quark in top rest frame",50,-1,1,50,-1,1,"cos lt","cos dt");
  histos2d[("cosltbt"+id).c_str()]=histo_writer->create_2d(("cosltbt"+id).c_str(),"cos phi between lepton and t and b-type quark and t quark in top rest frame",50,-1,1,50,-1,1,"cos lt","cos bt");

  histos1d[("coslb_M"+id).c_str()]=histo_writer->create_1d(("coslb_M"+id).c_str(),"cos phi between lepton and b-type quark in their top rest frame",50,-1,1,"cos delta phi");
  histos1d[("cosdb_M"+id).c_str()]=histo_writer->create_1d(("cosdb_M"+id).c_str(),"cos phi between down and b-type quark in their top rest frame",50,-1,1,"cos delta phi");
  histos1d[("cosld_M"+id).c_str()]=histo_writer->create_1d(("cosld_M"+id).c_str(),"cos phi between lepton and d-type quark in top parent rest frame",50,-1,1,"cos delta phi");
  histos1d[("cosldZMF_M"+id).c_str()]=histo_writer->create_1d(("cosldZMF_M"+id).c_str(),"cos phi between lepton and d-type quark in top parent rest frame",50,-1,1,"cos delta phi");
  histos1d[("coslu_M"+id).c_str()]=histo_writer->create_1d(("coslu_M"+id).c_str(),"cos phi between lepton and u-type quark in top parent rest frame",50,-1,1,"cos delta phi");
  histos2d[("cosltdt_M"+id).c_str()]=histo_writer->create_2d(("cosltdt_M"+id).c_str(),"cos phi between lepton and t and d-type quark and t quark in top rest frame",50,-1,1,50,-1,1,"cos lt","cos dt");
  histos2d[("cosltbt_M"+id).c_str()]=histo_writer->create_2d(("cosltbt_M"+id).c_str(),"cos phi between lepton and t and b-type quark and t quark in top rest frame",50,-1,1,50,-1,1,"cos lt","cos bt");

  histos1d[("phi_l"+id).c_str()]=histo_writer->create_1d(("phi_l"+id).c_str(),"phi of lepton",50,-3.14,3.14,"phi_l");
  histos1d[("phi_top"+id).c_str()]=histo_writer->create_1d(("phi_top"+id).c_str(),"phi of top",50,-3.14,3.14,"phi_t");
  histos1d[("phi_lToprest"+id).c_str()]=histo_writer->create_1d(("phi_lToprest"+id).c_str(),"phi of lepton in top rest frame",50,-3.14,3.14,"phi_lToprest");
  histos1d[("phi_d"+id).c_str()]=histo_writer->create_1d(("phi_d"+id).c_str(),"phi of down",50,-3.14,3.14,"phi_d");
  histos1d[("phi_dToprest"+id).c_str()]=histo_writer->create_1d(("phi_dToprest"+id).c_str(),"phi of down in top rest frame",50,-3.14,3.14,"phi_d");

  histos1d[("eta_l"+id).c_str()]=histo_writer->create_1d(("eta_l"+id).c_str(),"eta of lepton ",50,-3.14,3.14,"eta_l");
  histos1d[("eta_top"+id).c_str()]=histo_writer->create_1d(("eta_top"+id).c_str(),"eta of top",50,-3.14,3.14,"eta_top");
  histos1d[("eta_lToprest"+id).c_str()]=histo_writer->create_1d(("eta_lToprest"+id).c_str(),"eta of lepton in top rest frame",50,-3.14,3.14,"eta_lToprest");
  histos1d[("eta_d"+id).c_str()]=histo_writer->create_1d(("eta_d"+id).c_str(),"eta of down",50,-3.14,3.14,"eta_d");
  histos1d[("eta_dToprest"+id).c_str()]=histo_writer->create_1d(("eta_dToprest"+id).c_str(),"eta of down in top rest frame",50,-3.14,3.14,"eta_d");

  histos1d[("pt_TTbar"+id).c_str()]=histo_writer->create_1d(("pt_TTbar"+id).c_str(),"pt of ttbar pair",50,0.,20.,"pt_TTbar");
  histos1d[("pt_TTbar_matched_4jets"+id).c_str()]=histo_writer->create_1d(("pt_TTbar_matched_4jets"+id).c_str(),"pt of ttbar pair",50,0.,200.,"pt_TTbar");
  histos1d[("pt_TTbar_matched_5jets"+id).c_str()]=histo_writer->create_1d(("pt_TTbar_matched_5jets"+id).c_str(),"pt of ttbar pair",50,0.,200.,"pt_TTbar");
  histos1d[("DeltaPt_TTbar_5thjet"+id).c_str()]=histo_writer->create_1d(("DeltaPt_TTbar_5thjet"+id).c_str(),"delta pt",100,-50.,50.,"delta_pt");
  histos1d[("pt_TTbar_jet"+id).c_str()]=histo_writer->create_1d(("pt_TTbar_jet"+id).c_str(),"pt of ttbar pair",50,0.,20.,"pt_TTbar_jet");
  histos1d[("M_Thad"+id).c_str()]=histo_writer->create_1d(("M_Thad"+id).c_str(),"M of t_had",100,0.,200.,"M_Thad");
  histos1d[("M_Tlep"+id).c_str()]=histo_writer->create_1d(("M_Tlep"+id).c_str(),"M of t_lep",100,0.,200.,"M_Tlep");
  histos1d[("M_Thad_5jets"+id).c_str()]=histo_writer->create_1d(("M_Thad_5jets"+id).c_str(),"M of t_had",100,0.,200.,"M_Thad");
  histos1d[("M_Tlep_5jets"+id).c_str()]=histo_writer->create_1d(("M_Tlep_5jets"+id).c_str(),"M of t_lep",100,0.,200.,"M_Tlep");
  histos1d[("Categories"+id).c_str()]=histo_writer->create_1d(("Categories"+id).c_str(),"ID",5,0.,5.,"ID");
}

void SpinPlots::print_info(double x1, double x2, double x3, double x4, double x5){
  std::cout<<"%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"<<std::endl;
  std::cout<<"%%%%%%Printing info Spin Correlations%%%%%%"<<std::endl;
  std::cout<<std::setprecision(10)<<"%%Event number: "<<evt_info->event_number()<<"  lumi block: "<<evt_info->lumi_block()<<"  run: "<<evt_info->run()<<std::endl;
  std::cout<<"%%hadronic top: pt= "<<Top_had->p4().Pt()<<" phi= "<<Top_had->p4().Phi()<<" eta= "<<Top_had->p4().Eta()<<"vs mc_matched= "<<Top_had->mc_matched()<<" pt= "<<Top_had->mc_p4().Pt()<<" phi= "<<Top_had->mc_p4().Phi()<<" eta= "<<Top_had->mc_p4().Eta()<<std::endl;
  std::cout<<"%%leptonic top: pt= "<<Top_lep->p4().Pt()<<" phi= "<<Top_lep->p4().Phi()<<" eta= "<<Top_lep->p4().Eta()<<"vs mc_matched= "<<Top_lep->mc_matched()<<" pt= "<<Top_lep->mc_p4().Pt()<<" phi= "<<Top_lep->mc_p4().Phi()<<" eta= "<<Top_lep->mc_p4().Eta()<<std::endl;
  //  std::cout<<"%%hadronic W: pt= "<<W_had->p4().Pt()<<" phi= "<<W_had->p4().Phi()<<" eta= "<<W_had->p4().Eta()<<"vs mc_matched= "<<W_had->mc_matched()<<" pt= "<<W_had->mc_p4().Pt()<<" phi= "<<W_had->mc_p4().Phi()<<" eta= "<<W_had->mc_p4().Eta()<<std::endl;
  //std::cout<<"%%leptonic W: pt= "<<W_lep->p4().Pt()<<" phi= "<<W_lep->p4().Phi()<<" eta= "<<W_lep->p4().Eta()<<"vs mc_matched= "<<W_lep->mc_matched()<<" pt= "<<W_lep->mc_p4().Pt()<<" phi= "<<W_lep->mc_p4().Phi()<<" eta= "<<W_lep->mc_p4().Eta()<<std::endl;
 std::cout<<"%%hadronic B: pt= "<<B_had->p4().Pt()<<" phi= "<<B_had->p4().Phi()<<" eta= "<<B_had->p4().Eta()<<"vs mc_matched= "<<B_had->mc_matched()<<" pt= "<<B_had\
   ->mc_p4().Pt()<<" phi= "<<B_had->mc_p4().Phi()<<" eta= "<<B_had->mc_p4().Eta()<<std::endl;
 std::cout<<"%%leptonic B: pt= "<<B_lep->p4().Pt()<<" phi= "<<B_lep->p4().Phi()<<" eta= "<<B_lep->p4().Eta()<<"vs mc_matched= "<<B_lep->mc_matched()<<" pt= "<<B_lep->mc_p4().Pt()<<" phi= "<<B_lep->mc_p4().Phi()<<" eta= "<<B_lep->mc_p4().Eta()<<std::endl;
 // std::cout<<"%%lepton: pt= "<<lepton->p4().Pt()<<" phi= "<<lepton->p4().Phi()<<" eta= "<<lepton->p4().Eta()<<"vs mc_matched= "<<lepton->mc_matched()<<" pt= "<<lepton->mc_p4().Pt()<<" phi= "<<lepton->mc_p4().Phi()<<" eta= "<<lepton->mc_p4().Eta()<<std::endl;
 std::cout<<"%%decay quark: pt= "<<quark->p4().Pt()<<" phi= "<<quark->p4().Phi()<<" eta= "<<quark->p4().Eta()<<"vs mc_matched= "<<quark->mc_matched()<<" pt= "<<quark->mc_p4().Pt()<<" phi= "<<quark->mc_p4().Phi()<<" eta= "<<quark->mc_p4().Eta()<<std::endl;
 std::cout<<"%%decay anti-quark: pt= "<<quark_bar->p4().Pt()<<" phi= "<<quark_bar->p4().Phi()<<" eta= "<<quark_bar->p4().Eta()<<"vs mc_matched= "<<quark_bar->mc_matched()<<" pt= "<<quark_bar->mc_p4().Pt()<<" phi= "<<quark_bar->mc_p4().Phi()<<" eta= "<<quark_bar->mc_p4().Eta()<<std::endl;
 std::cout<<"%%%%%%%1D variables%%%%%%%%%%%%%%%"<<std::endl;
 // std::cout<<"coslb= "<<x1<<std::endl;
 //std::cout<<"cosdb= "<<x2<<std::endl;
 //std::cout<<"cosld= "<<x3<<std::endl;
 //std::cout<<"coslu= "<<x4<<std::endl;
 //std::cout<<"coslW= "<<x5<<std::endl;
 mor::Jet q_jet;
 mor::Jet qb_jet;
 mor::Jet bhad_jet;
 mor::Jet blep_jet;
 // std::cout<<"q_jet "<<" pt= "<<q_jet->p4().Pt()<<" phi= "<<q_jet->p4().Phi()<<" eta= "<<q_jet->p4().Eta()<<std::endl; 
 //std::cout<<"qb_jet "<<" pt= "<<qb_jet->p4().Pt()<<" phi= "<<qb_jet->p4().Phi()<<" eta= "<<qb_jet->p4().Eta()<<std::endl; 
 //std::cout<<"b_jet "<<" pt= "<<b_jet->p4().Pt()<<" phi= "<<b_jet->p4().Phi()<<" eta= "<<b_jet->p4().Eta()<<std::endl; 
 //std::cout<<"bbar_jet "<<" pt= "<<bbar_jet->p4().Pt()<<" phi= "<<bbar_jet->p4().Phi()<<" eta= "<<bbar_jet->p4().Eta()<<std::endl; 

 bool matched_q = false;
 bool matched_qb = false;
 bool matched_b = false;
 bool matched_bbar = false;
 int n_unmatched = -1;
 int n_unmatched_2 = -1;
 int number_unmatched = 0;
 for(unsigned int i = 0; i < jets->size(); i++){
   double matched_jet = false;

   double dR_q = sqrt(((*jets)[i].p4().Phi() - quark->p4().Phi())*((*jets)[i].p4().Phi() - quark->p4().Phi()) + ((*jets)[i].p4().Eta() - quark->p4().Eta())*((*jets)[i].p4().Eta() - quark->p4().Eta()));
   double dR_qb = sqrt(((*jets)[i].p4().Phi() - quark_bar->p4().Phi())*((*jets)[i].p4().Phi() - quark_bar->p4().Phi()) + ((*jets)[i].p4().Eta() - quark_bar->p4().Eta())*((*jets)[i].p4().Eta() - quark_bar->p4().Eta()));
   double dR_b = sqrt(((*jets)[i].p4().Phi() - B_had->p4().Phi())*((*jets)[i].p4().Phi() - B_had->p4().Phi()) + ((*jets)[i].p4().Eta() - B_had->p4().Eta())*((*jets)[i].p4().Eta() - B_had->p4().Eta()));
   double dR_bb = sqrt(((*jets)[i].p4().Phi() - B_lep->p4().Phi())*((*jets)[i].p4().Phi() - B_lep->p4().Phi()) + ((*jets)[i].p4().Eta() - B_lep->p4().Eta())*((*jets)[i].p4().Eta() - B_lep->p4().Eta()));
   if(!matched_q && dR_q < 0.7){matched_q = true; matched_jet = true; q_jet = (*jets)[i];}
   if(!matched_qb && dR_qb < 0.7 && !matched_jet){matched_qb = true; matched_jet = true; qb_jet = (*jets)[i];}
   if(!matched_b && dR_b < 0.7 && !matched_jet){matched_b = true; matched_jet = true; bhad_jet = (*jets)[i];}
   if(!matched_bbar && dR_bb < 0.7 && !matched_jet){matched_bbar = true; matched_jet = true; blep_jet = (*jets)[i];}
   std::cout<<"dR_q = "<<dR_q<<" dR_qb = "<<dR_qb<<" dR_b = "<<dR_b<<" dR_bb = "<<dR_bb<<std::endl;
   std::cout<<"jet "<<i<<" pt= "<<(*jets)[i].p4().Pt()<<" phi= "<<(*jets)[i].p4().Phi()<<" eta= "<<(*jets)[i].p4().Eta()<<" matched= "<<matched_jet<<std::endl; 
   if(matched_jet == false){number_unmatched++;if(n_unmatched == -1){n_unmatched = i;}else{n_unmatched_2 = i;}}
   matched_jet = false;
 }
 total++;
 bool matched_event = false;
 if(matched_q && matched_qb && matched_b && matched_bbar){std::cout<<"matched event!"<<std::endl; matched_event = true; nmatched++;}
  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > TTbar = Top_had->p4() + Top_lep->p4();
  std::cout<<"gen TTbar pt = "<<TTbar.Pt()<<std::endl;
  if(matched_event){ 
    ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > TTbar_decay_prod = q_jet.p4() + qb_jet.p4() + bhad_jet.p4() + blep_jet.p4() + gen_evt->lepton()->p4() + gen_evt->neutrino()->p4();
    std::cout<<"TTbar decay prod pt = "<<TTbar_decay_prod.Pt()<<std::endl;
    ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > T_had = q_jet.p4() + qb_jet.p4() + bhad_jet.p4();
    ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > T_lep = gen_evt->lepton()->p4() + gen_evt->neutrino()->p4() + blep_jet.p4();
    std::cout<<"M_Thad = "<<T_had.M()<<" M_Tlep = "<<T_lep.M()<<std::endl;
    if(jets->size() == 4){
      histos1d[("M_Thad"+id).c_str()]->Fill(T_had.M());
      histos1d[("M_Tlep"+id).c_str()]->Fill(T_lep.M());
      histos1d[("Categories"+id).c_str()]->Fill(0);
      histos1d[("pt_TTbar_matched_4jets"+id).c_str()]->Fill(TTbar_decay_prod.Pt());
    } 
   if(jets->size() == 5){
      histos1d[("M_Thad_5jets"+id).c_str()]->Fill(T_had.M());
      histos1d[("M_Tlep_5jets"+id).c_str()]->Fill(T_lep.M());
      histos1d[("Categories"+id).c_str()]->Fill(1);
      histos1d[("pt_TTbar_matched_5jets"+id).c_str()]->Fill(TTbar_decay_prod.Pt());
      histos1d[("DeltaPt_TTbar_5thjet"+id).c_str()]->Fill(TTbar.Pt() - (*jets)[n_unmatched].p4().Pt());
    }
  }
  if(!matched_event){
    if(jets->size() == 5){histos1d[("Categories"+id).c_str()]->Fill(2);}
    else{if(number_unmatched == 3){histos1d[("Categories"+id).c_str()]->Fill(3);}
      else{histos1d[("Categories"+id).c_str()]->Fill(4);}
    }
  }
	
  std::cout<<"%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"<<std::endl;
  std::cout<<"%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"<<std::endl;


  if(!matched_event && jets->size() == 5 && number_unmatched == 2){
    ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D <double> > merged_jet = (*jets)[n_unmatched].p4() + (*jets)[n_unmatched_2].p4();
    double matched_jet = false;

    double dR_q = sqrt((merged_jet.Phi() - quark->p4().Phi())*(merged_jet.Phi() - quark->p4().Phi()) + (merged_jet.Eta() - quark->p4().Eta())*(merged_jet.Eta() - quark->p4().Eta()));
    double dR_qb = sqrt((merged_jet.Phi() - quark_bar->p4().Phi())*(merged_jet.Phi() - quark_bar->p4().Phi()) + (merged_jet.Eta() - quark_bar->p4().Eta())*(merged_jet.Eta() - quark_bar->p4().Eta()));
    double dR_b = sqrt((merged_jet.Phi() - B_had->p4().Phi())*(merged_jet.Phi() - B_had->p4().Phi()) + (merged_jet.Eta() - B_had->p4().Eta())*(merged_jet.Eta() - B_had->p4().Eta()));
    double dR_bb = sqrt((merged_jet.Phi() - B_lep->p4().Phi())*(merged_jet.Phi() - B_lep->p4().Phi()) + (merged_jet.Eta() - B_lep->p4().Eta())*(merged_jet.Eta() - B_lep->p4().Eta()));
    if(!matched_q && dR_q < 0.7){matched_q = true; matched_jet = true;}
    if(!matched_qb && dR_qb < 0.7 && !matched_jet){matched_qb = true; matched_jet = true;}
    if(!matched_b && dR_b < 0.7 && !matched_jet){matched_b = true; matched_jet = true;}
    if(!matched_bbar && dR_bb < 0.7 && !matched_jet){matched_bbar = true; matched_jet = true;}
    std::cout<<"dR_q = "<<dR_q<<" dR_qb = "<<dR_qb<<" dR_b = "<<dR_b<<" dR_bb = "<<dR_bb<<std::endl;
    std::cout<<"merged jet "<<" pt= "<<merged_jet.Pt()<<" phi= "<<merged_jet.Phi()<<" eta= "<<merged_jet.Eta()<<" matched= "<<matched_jet<<std::endl; 
    //    if(matched_jet == false){if(n_unmatched == -1){n_unmatched = i;}else{n_unmatched_2 = i;}}
    //matched_jet = false;
    if(matched_q && matched_qb && matched_b && matched_bbar){std::cout<<"matched merged event!"<<std::endl; matched_merged_event++; matched_event = true;}
  }
}

void SpinPlots::select_and_plot()
{
  //check if it is a semi-leptonic decay
  if(!ttbar || (decay != 1 && decay != 2)){return;}


  //boost all the particles to the parent top rest frame
  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > Lep = ToTopRestFrame(lepton, true);
  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > T_h = ToTopPairRestFrame(Top_had);
  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > T_l = ToTopPairRestFrame(Top_lep);
  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > B_h = ToTopRestFrame(B_had, false);
  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > B_l = ToTopRestFrame(B_lep, true);

  //boost all the particles to the ttbar pair rest frame
  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > LepTTbar = ToTopPairRestFrame(lepton);

  mor::Particle* Down = ChooseD();
  mor::Particle* U;
  if(Down->mc_match_id() == -3 || Down->mc_match_id() == -1){U = quark;}
  else{U = quark_bar;}
  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > DownT = ToTopRestFrame(Down, false);
  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > UT = ToTopRestFrame(U, false);
  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > DownTpair = ToTopPairRestFrame(Down);

  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > WT = ToTopRestFrame(W_had, false);

  //checkplots
  histos1d[("phi_l"+id).c_str()]->Fill(lepton->p4().Phi());
  histos1d[("phi_top"+id).c_str()]->Fill(Top_had->p4().Phi());
  histos1d[("phi_top"+id).c_str()]->Fill(Top_lep->p4().Phi());
  histos1d[("eta_top"+id).c_str()]->Fill(Top_had->p4().Eta());
  histos1d[("eta_top"+id).c_str()]->Fill(Top_lep->p4().Eta());
  histos1d[("eta_l"+id).c_str()]->Fill(lepton->p4().Eta());
  histos1d[("phi_lToprest"+id).c_str()]->Fill(Lep.Phi());
  histos1d[("eta_lToprest"+id).c_str()]->Fill(Lep.Eta());
  histos1d[("phi_d"+id).c_str()]->Fill(Down->p4().Phi());
  histos1d[("eta_d"+id).c_str()]->Fill(Down->p4().Eta());
  histos1d[("phi_dToprest"+id).c_str()]->Fill(DownT.Phi());
  histos1d[("eta_dToprest"+id).c_str()]->Fill(DownT.Eta());

  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > TTbar = Top_had->p4() + Top_lep->p4();
  histos1d[("pt_TTbar"+id).c_str()]->Fill(TTbar.Pt());
  //in helicity frame
  double x1 = plot1D(Lep,B_h,"coslb");
  double x2 = plot1D(DownT, B_l,"cosdb");
  double x3 = plot1D(Lep, DownT,"cosld");
  double x4 = plot1D(Lep, UT, "coslu");
  double x5 = plot1D(Lep, WT, "coslW");
  plot2D(Lep,T_l,DownT,T_h,"cosltdt");

  if(plot_gen_evt_info){print_info(x1,x2,x3,x4,x5);}

  plot2D(lepton->p4(),Top_lep->p4(),B_had->p4(),Top_had->p4(),"cosltbt");

  //in ZMF
  double x = plot1D(LepTTbar, DownTpair,"cosldZMF");

  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > topPairCmf( Top_had->p4() + Top_lep->p4() );
  if(topPairCmf.M() > 400){return;}
  //if invariant mass cut is passed
  //in helicity frame
  x= plot1D(Lep,B_h,"coslb_M");
  x= plot1D(DownT, B_l,"cosdb_M");
  x= plot1D(Lep, DownT,"cosld_M");
  plot2D(Lep,T_l,DownT,T_h,"cosltdt_M");

  plot2D(Lep,T_l,B_h,T_h,"cosltbt_M");

  //in ZMF
  x= plot1D(LepTTbar, DownTpair,"cosldZMF_M");

}

mor::Particle* SpinPlots::ChooseD()
{
  mor::Particle* D;
  mor::Particle* U;

  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > QT = ToTopRestFrame(quark, false);
  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > QT_bar = ToTopRestFrame(quark_bar, false);
  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > QW = ToWRestFrame(quark, false);
  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > QW_bar = ToWRestFrame(quark_bar, false);
  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > BW = ToWRestFrame(B_had, false);
  double dRQ = ROOT::Math::VectorUtil::DeltaR(QW, BW);
  double dRQ_bar = ROOT::Math::VectorUtil::DeltaR(QW_bar, BW);
  if(QT.E() < QT_bar.E()){
    if(quark->mc_match_id() == 3 || quark->mc_match_id() == 1){ histos1d[("downtaggingpur"+id).c_str()]->Fill(0.5);}
    if(dRQ < dRQ_bar){
      histos1d[("downtaggingpur"+id).c_str()]->Fill(-0.5);
      if(quark->mc_match_id() == 3 || quark->mc_match_id() == 1){ histos1d[("downtaggingpur"+id).c_str()]->Fill(2.5);}
    }
  }
  else{
    if(quark_bar->mc_match_id() == (-3) || quark_bar->mc_match_id() == (-1)){histos1d[("downtaggingpur"+id).c_str()]->Fill(0.5);}
    if(dRQ_bar < dRQ){
      histos1d[("downtaggingpur"+id).c_str()]->Fill(-0.5);
      if(quark_bar->mc_match_id() == (-3) || quark_bar->mc_match_id() == (-1)){ histos1d[("downtaggingpur"+id).c_str()]->Fill(2.5);}
    }
  }
  if(dRQ < dRQ_bar){
    if(quark->mc_match_id() == 3 || quark->mc_match_id() == 1){ histos1d[("downtaggingpur"+id).c_str()]->Fill(1.5);}
  }
  else{
      if(quark_bar->mc_match_id() == (-3) || quark_bar->mc_match_id() == (-1)){ histos1d[("downtaggingpur"+id).c_str()]->Fill(1.5);}
  }

  if(quark->mc_match_id() == 3 || quark->mc_match_id() == 1){D = quark; U = quark_bar;}
  else{D = quark_bar; U = quark;}
  
  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > DT = ToTopRestFrame(D, false);
  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > UT = ToTopRestFrame(U, false);
  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > DW = ToWRestFrame(D, false);
  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > UW = ToWRestFrame(U, false);
  if(DT.E() < UT.E()){histos1d[("downtaggingeff"+id).c_str()]->Fill(0.5);}
  //  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > BW = ToWRestFrame(B_had, false);
  double dRD = ROOT::Math::VectorUtil::DeltaR(DW, BW);
  double dRU = ROOT::Math::VectorUtil::DeltaR(UW, BW);
  if(dRD < dRU){histos1d[("downtaggingeff"+id).c_str()]->Fill(1.5);}
  if((dRD < dRU) && (DT.E() < UT.E())){histos1d[("downtaggingeff"+id).c_str()]->Fill(2.5);}
  return D;

}

double SpinPlots::plot1D(ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > part1, ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > part2, std::string histname)
{
  double Cos = part1.Vect().Unit().Dot(part2.Vect().Unit());
  histos1d[(histname+id).c_str()]->Fill(Cos);
  return Cos;
}

void SpinPlots::plot2D(ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > part1, ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > part2, ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > part3, ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > part4, std::string histname)
{
  double Cos1 = part1.Vect().Unit().Dot(part2.Vect().Unit());
  double Cos2 = part3.Vect().Unit().Dot(part4.Vect().Unit());
  histos2d[(histname+id).c_str()]->Fill(Cos1,Cos2);
}

ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > SpinPlots::ToZeroMom(ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > particle0, ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > particle1)
{
  ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > p0(particle0.Px(), particle0.Py(), particle0.Pz(), particle0.E());
  ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > p1(particle1.Px(), particle1.Py(), particle1.Pz(), particle1.E());
 
  double mass, gamma, beta[3], bdotp;
  mass = sqrt(fabs(p0.E()*p0.E()-p0.Px()*p0.Px()-p0.Py()*p0.Py()-p0.Pz()*p0.Pz()));
  gamma = p0.E()/mass;
  bdotp = 0.0;
  beta[0]= p0.Px()/p0.E();
  beta[1]= p0.Py()/p0.E();
  beta[2]= p0.Pz()/p0.E();
  bdotp = p1.Px()*beta[0] + p1.Py()*beta[1] + p1.Pz()*beta[2]; 

  ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > p_out(p1.Px()+gamma*beta[0]*(gamma/(gamma+1.)*bdotp-p1.E()),p1.Py()+gamma*beta[1]*(gamma/(gamma+1.)*bdotp-p1.E()),p1.Pz()+gamma*beta[2]*(gamma/(gamma+1.)*bdotp-p1.E()), gamma*(p1.E()-bdotp));

  return p_out;
}

ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > SpinPlots::ToWRestFrame(mor::Particle * particle, bool Leptonic)
{
  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> >  ParticleTRest;
  if(Leptonic){ParticleTRest = ROOT::Math::VectorUtil::boost( particle->p4(), W_lep->p4().BoostToCM() ) ;}
  else{ParticleTRest = ROOT::Math::VectorUtil::boost( particle->p4(), W_had->p4().BoostToCM() ) ;}

  return ParticleTRest;
}

ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > SpinPlots::ToTopRestFrame(mor::Particle * particle, bool Leptonic)
{
  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> >  ParticleTRest;
  if(Leptonic){ParticleTRest = ROOT::Math::VectorUtil::boost( particle->p4(), Top_lep->p4().BoostToCM() ) ;}
  else{ParticleTRest = ROOT::Math::VectorUtil::boost( particle->p4(), Top_had->p4().BoostToCM() ) ;}

  return ParticleTRest;
}

ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > SpinPlots::ToTopPairRestFrame(mor::Particle * particle)
{
  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > topPairCmf( Top_had->p4() + Top_lep->p4() );
  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > tParticleCmf( ROOT::Math::VectorUtil::boost( particle->p4(), topPairCmf.BoostToCM() ) );

  return tParticleCmf;
}

