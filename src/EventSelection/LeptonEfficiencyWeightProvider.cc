#include "../../interface/EventSelection/LeptonEfficiencyWeightProvider.h"

broc::LeptonEfficiencyWeightProvider::LeptonEfficiencyWeightProvider(std::string weight_file, eire::HandleHolder *handle_holder): broc::EventWeightProvider(weight_file, handle_holder)
{
	if(handle_holder->get_ident().find("Data") != std::string::npos)
		do_not_reweight = true;
	else
		do_not_reweight = false;

	muons = handle_holder->get_tight_muons();
	electrons = handle_holder->get_tight_electrons();

	process_muons = handle_holder->get_config_reader()->get_bool_var("process_muon_efficiencies", "event_weights", false);
	process_electrons = handle_holder->get_config_reader()->get_bool_var("process_electron_efficiencies", "event_weights", false);

	if(process_muons && process_electrons){
		std::cerr << "ERROR: LeptonEfficiencyWeightProvider: cannot process muon and electron reweighting simultaneously, exiting..." << std::endl;	
		exit(1);
	}
}

broc::LeptonEfficiencyWeightProvider::~LeptonEfficiencyWeightProvider()
{
}
double broc::LeptonEfficiencyWeightProvider::get_weight()
{
	/*
	double weight = 1;
	for(std::vector<mor::Electron>::iterator e = electrons->begin();
		e != electrons->end();
		++e){
		weight *= weights[find_bin(e->eta())];	
	}
	return weight;
	*/
	double value = get_value_for_event();

	if(do_not_reweight)
		return 1.;
	else{
		if(value == -999.)
			return 1.;
		else{
			if(scale_factor_2D){
				double value2 = get_2nd_value_for_event();
				if(value2 == -999.)
					return 1.;
				return weights[find_bin(value,value2)];
			}else{		
				return weights[find_bin(value)];

			}
		}
	}
}

double broc::LeptonEfficiencyWeightProvider::get_weight(mor::Electron *electron, bool is_1D)
{
	double weight = 1;
	weight *= weights[find_bin(electron->eta())];
	return weight;
}

double broc::LeptonEfficiencyWeightProvider::get_weight(mor::Muon *muon, bool is_1D)
{
	double weight = 1;
	if(is_1D){
		weight *= weights[find_bin(muon->eta())];
	}else{
		weight *= weights[find_bin(muon->eta(),muon->dbeta_PFrelIso())];
	}
	return weight;
}
