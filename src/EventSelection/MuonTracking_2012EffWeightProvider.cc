#include "../../interface/EventSelection/MuonTracking_2012EffWeightProvider.h"

broc::MuonTracking_2012EffWeightProvider::MuonTracking_2012EffWeightProvider(eire::HandleHolder *handle_holder)
{
	if(handle_holder->get_ident().find("Data") != std::string::npos)
		do_not_reweight = true;
	else
		do_not_reweight = false;

	muons = handle_holder->get_tight_muons();

	double study_unc = handle_holder->get_cuts_set()->get_cut_value("study_mu_Tracking_unc");
	
	up = false;
	down = false;

	if(study_unc == 1){ down = true; }
	else if(study_unc == 2){ up = true; }
}

broc::MuonTracking_2012EffWeightProvider::~MuonTracking_2012EffWeightProvider()
{
}

double broc::MuonTracking_2012EffWeightProvider::get_weight(int UpDown)
{
	double weight = 1.;

	if(do_not_reweight)
		weight = 1.;
	else{
		if(muons->size() >= 1){
			std::vector<mor::Muon>::iterator m = muons->begin();
			double eta = m->eta();
			double abseta = std::abs(eta);
			double pt = m->pt();
			double ud = 0.; // up-down switch for systematics
			if(up || UpDown == 1) ud = 1.;
			else if(down || UpDown == -1) ud = -1.;
			if(eta < -2.4){
			  weight = 0.;
			}else if(eta < -2.1){
			  weight = 0.9869+ud*0.0007;
			}else if(eta < -1.6){
			  weight = 0.9948+ud*0.0002;
			}else if(eta < -1.2){
			  weight = 0.9967+ud*0.0002;
			}else if(eta < -0.9){
			  weight = 0.9974+ud*0.0002;
			}else if(eta < -0.6){
			  weight = 0.9980+ud*0.0001;
			}else if(eta < -0.3){
			  weight = 0.9980+ud*0.0001;
			}else if(eta < -0.2){
			  weight = 0.9972+ud*0.0002;
			}else if(eta < 0.2){
			  weight = 0.9963+ud*0.0001;
			}else if(eta < 0.3){
			  weight = 0.9978+ud*0.0002;
			}else if(eta < 0.6){
			  weight = 0.9977+ud*0.0001;
			}else if(eta < 0.9){
			  weight = 0.9976+ud*0.0001;
			}else if(eta < 1.2){
			  weight = 0.9968+ud*0.0002;
			}else if(eta < 1.6){
			  weight = 0.9959+ud*0.0003;
			}else if(eta < 2.1){
			  weight = 0.9970+ud*0.0002;
			}else if(eta < 2.4){
			  weight = 0.9836+ud*0.0008;
			}else{weight = 0.;}
		}
	}

	return weight;
}
