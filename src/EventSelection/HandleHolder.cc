#include "../../interface/EventSelection/HandleHolder.h"

eire::HandleHolder::HandleHolder()
{  
	tight_electrons = 0;
	tight_muons = 0;
	tight_jets = 0;
	
	corrected_mets = 0;
	mets = 0;
	selected_mets = 0;
	
	jets = 0;
	electrons = 0;
	muons = 0;

	hitfit_combi_seed.push_back(-1);
	hitfit_combi_seed.push_back(-1);
	hitfit_combi_seed.push_back(-1);
	hitfit_combi_seed.push_back(-1);
	
	loose_muons = 0;
	loose_electrons = 0;
	
	loose_mm_muons = 0;
	loose_mm_electrons = 0;
	
	noe_jets = 0;
	noniso_electrons = 0;
	selected_noe_jets = 0;
	selected_noniso_electrons = 0;
	
	nomu_jets = 0;
	noniso_muons = 0;
	selected_nomu_jets = 0;
	selected_noniso_muons = 0;
	
	primary_vertices = 0;
	tight_primary_vertices = 0;
	trigger_objects = 0;
	gen_particles = 0;
	event_information = 0;
        trigger = 0;

        cuts_set = 0;
        gen_evt = 0;
        config_reader = 0;
        pdf_weight_provider = 0;

        prescale_prov = 0;
        trigger_module_manager = 0;

        histo_writer = 0;

	services_ = 0;
	
	event_weight = 0;
	failing_event_weight = 0;

	has_btag_cut = 0;
	
	ident = "none";

}

eire::HandleHolder::~HandleHolder()
{
}
