
#include "../../interface/EventSelection/JetSelector.h"

/*************************************************************************
Jets which are in returned vector:
If eta not set; Jets which pass pt cut + all other jets.
If eta set; Jets which pass pt and eta cut + other jets which pass eta cut.

If one of first few jets passes pt cut but not eta it will be discarded and the same cut will be applied to the next mor::Jet  

Jet Correction Uncertainties adapted from here: https://twiki.cern.ch/twiki/bin/viewauth/CMS/JECUncertaintySources#Main_uncertainties_2012_53X
***************************************************************************/

template bool JetSelector::cut_leptons(mor::Jet *jet_iter, std::vector<mor::Electron> *leptons, double min_dR);
template bool JetSelector::cut_leptons(mor::Jet *jet_iter, std::vector<mor::Muon> *leptons, double min_dR);

JetSelector::JetSelector(eire::HandleHolder *handle_holder)
{
	this->handle_holder = handle_holder;

	cut_jets = NULL;
	min_pt = NULL;
	max_pt = NULL;
	max_eta = -1;
	min_e_dR = -1;
	min_mu_dR = -1;
	max_fHPD = -1;
	min_n90Hits = -1;
	min_emf = -1;
	min_nconstituents = -1;	// number of constituents
	min_chf = -1;		// charged hardron energy fraction
	max_nhf = -1;		// neutral hardon energy fraction
	max_nemf = -1;		// neutral em energy fraction
	max_cemf = -1;		// charged em energy fraction
	min_cmulti = -1;	// charged multiplicity
	//study_jes_unc = -1;
	cut_jets = new std::vector<mor::Jet>();
	//jecUnc = NULL;
	jer_corrector = new broc::JERCorrector();

	suppress_JER_bias_correction = false;

	apply_JEC_uncertainty = false;
	JEC_uncertainty_variation = "";
	JEC_unc_type = "";

	if(handle_holder->get_ident().find("Data") != std::string::npos)
		is_real_data = true;
	else
		is_real_data = false;
	//is_real_data = handle_holder->get_event_information()->is_real_data();
	//jecUnc = std::vector<JetCorrectionUncertainty*>();	

}

// Initialisation of names will need an update of the C++ standard in the Makefile, add to flags: -std=c++0x
const char* JetSelector::uncorrelatedgroup[nsrc] =
	{"AbsoluteStat", "AbsoluteScale", "HighPtExtra", "SinglePionECAL", "SinglePionHCAL", "Time", "RelativeJEREC1", "RelativeJEREC2", "RelativeJERHF", "RelativePtBB", "RelativePtEC1", "RelativePtEC2", "RelativePtHF", "RelativeStatEC2", "RelativeStatHF", "PileUpDataMC", "PileUpPtBB", "PileUpPtEC", "PileUpPtHF", "PileUpBias"};
	
JetSelector::~JetSelector()
{
	jecUnc.clear();
	//if(jecUnc){ delete jecUnc; jecUnc = NULL; }
	//if(JEC_params){ delete JEC_params; JEC_params = NULL; }
	if(cut_jets){ delete cut_jets; cut_jets = NULL; }
	if(jer_corrector){ delete jer_corrector; jer_corrector = NULL; }
}

void JetSelector::initialise_jec_uncertainty(eire::ConfigReader *config_reader)
{
	suppress_JER_bias_correction = config_reader->get_bool_var("suppress_JER_bias_correction","jet_corrections",false);
	std::string JEC_uncertainty_file = config_reader->get_var("JEC_uncertainty_file","jet_corrections",true);
	apply_JEC_uncertainty = config_reader->get_bool_var("apply_JEC_uncertainty","jet_corrections",true);
	JEC_unc_type = config_reader->get_var("JEC_uncertainty_type","jet_corrections",true);
	JEC_uncertainty_variation = config_reader->get_var("JEC_uncertainty_variation","jet_corrections",false);
	std::string JEC_dir = config_reader->get_var("JEC_dir","jet_corrections",true);
	std::string full_path = JEC_dir+JEC_uncertainty_file;

	if (JEC_unc_type == "UncorrelatedGroup"){
		if(JEC_uncertainty_file.size() > 0){
			for (int isrc = 0; isrc < 20; isrc++) {
				const char *name = uncorrelatedgroup[isrc];
				JetCorrectorParameters *p = new JetCorrectorParameters(full_path, name);
				jecUnc.push_back(new JetCorrectionUncertainty(*p));
			} // for isrc
		}
	}else{
		JetCorrectorParameters *p = new JetCorrectorParameters(full_path, JEC_unc_type);
		jecUnc.push_back(new JetCorrectionUncertainty(*p));
		
		// if(JEC_uncertainty_file.size() > 0){
		// 	JEC_params = new JetCorrectorParameters(full_path, JEC_unc_type);
		// 	jecUnc = new JetCorrectionUncertainty(*JEC_params);
		// }
	}
}

void JetSelector::set_cuts_set(broc::CutsSet *cuts_set)
{
	max_eta = cuts_set->get_cut_value("max_jet_eta");
	min_pt = cuts_set->get_vcut_value("min_jet_pt");
	max_pt = cuts_set->get_vcut_value("max_jet_pt");
	min_e_dR = cuts_set->get_cut_value("min_jet_e_dR");
	min_mu_dR = cuts_set->get_cut_value("min_jet_mu_dR");

	//study_jes_unc = cuts_set->get_cut_value("study_jes_unc");
	study_jer_unc = cuts_set->get_cut_value("study_jer_unc");
	
	// calo & jpt jets
	max_fHPD = cuts_set->get_cut_value("max_jet_fHPD");
	min_n90Hits = cuts_set->get_cut_value("min_jet_n90Hits");
	min_emf = cuts_set->get_cut_value("min_jet_emf");

	// pf jets
	min_nconstituents = cuts_set->get_cut_value("min_jet_nconstituents");	// number of constituents
	min_chf = cuts_set->get_cut_value("min_jet_chf");		// charged hardron energy fraction
	max_nhf = cuts_set->get_cut_value("max_jet_nhf");		// neutral hardon energy fraction
	max_nemf = cuts_set->get_cut_value("max_jet_nemf");		// neutral em energy fraction
	max_cemf = cuts_set->get_cut_value("max_jet_cemf");		// charged em energy fraction
	min_cmulti = cuts_set->get_cut_value("min_jet_cmulti");	// charged multiplicity
}

// Here the functions for any cuts which have been set are called
std::vector<mor::Jet>* JetSelector::get_jets(std::vector<mor::Jet> *uncut_jets, std::vector<mor::Muon> *muons, std::vector<mor::Electron> *electrons)
{
	cut_jets->clear();
	std::vector<mor::Jet> unsorted_jets;

	PtSorter<mor::Jet> jet_sorter;
	std::vector<mor::Jet*> jets;

	// fill all jets to jets vector
	for(std::vector<mor::Jet>::iterator jet_iter = uncut_jets->begin();
	    jet_iter!=uncut_jets->end();
	    ++jet_iter){
	  
	  	mor::Jet *tmp_jet = new mor::Jet(*jet_iter);

		// do JER smearing and bias correction if necessary
		if(!is_real_data && !suppress_JER_bias_correction) jer_corrector->correctJER(tmp_jet, study_jer_unc);

		if(apply_JEC_uncertainty and JEC_uncertainty_variation != "" ){

			double sum2_up = 0;
			double sum2_dw = 0;

			double sum_up = 0;
			double sum_dw = 0;

			// For looping over a list of JEC uncertainty sources and adding in quadrature

			for (unsigned int isrc = 0; isrc < jecUnc.size(); isrc++) {

				JetCorrectionUncertainty *unc = jecUnc[isrc];
				unc->setJetEta(tmp_jet->eta());
				unc->setJetPt(tmp_jet->pt());
				double sup = unc->getUncertainty(false);
				unc->setJetEta(tmp_jet->eta());
				unc->setJetPt(tmp_jet->pt());
				double sdw = unc->getUncertainty(false);

				sum2_up += pow(std::max(sup,sdw),2);
				sum2_dw += pow(std::min(sup,sdw),2);

			} // for isrc

			// Take the square of the sums if they were added in quadrature
			if (jecUnc.size() > 1){
				sum_up = sqrt(sum2_up);
				sum_dw = sqrt(sum2_dw);
			}else{
				sum_up = sum2_up;
				sum_dw = sum2_dw;
			}

			// Adjust the jet pt by the up or down variation
			double pt_scale_unc = 0;
			if(JEC_uncertainty_variation == "Up" or JEC_uncertainty_variation == "up"){
			 	pt_scale_unc = 1. + sum_up;
			}else if(JEC_uncertainty_variation == "Down" or JEC_uncertainty_variation == "down"){
				pt_scale_unc = 1. - sum_dw;
			}else{
				std::cerr << "JetSelector | Please specify either 'Up' or 'Down' for the JEC_uncertainty_variation" << std::endl;
			}

			// Copy all original values across to the tmp jet
			tmp_jet->SetPt(jet_iter->Pt());
			tmp_jet->SetEta(jet_iter->eta());
			tmp_jet->SetPhi(jet_iter->Phi());
			tmp_jet->SetM(jet_iter->mass());
			// Multiply the tmp jet by the scale factor from the uncertainty calculation
			if (JEC_unc_type.find("FlavorPure") != std::string::npos){ // Flavor dependent JEC variations only
				if (std::abs(tmp_jet->mc_match_id()) == 21 && JEC_unc_type == "FlavorPureGluon"){
					*tmp_jet *= pt_scale_unc;
				}else if(std::abs(tmp_jet->mc_match_id()) == 5 && JEC_unc_type == "FlavorPureBottom"){
					*tmp_jet *= pt_scale_unc;
				}else if(std::abs(tmp_jet->mc_match_id()) == 4 && JEC_unc_type == "FlavorPureCharm"){
					*tmp_jet *= pt_scale_unc;
				}else if(std::abs(tmp_jet->mc_match_id()) < 4 && std::abs(tmp_jet->mc_match_id()) != 0 && JEC_unc_type == "FlavorPureQuark"){
					*tmp_jet *= pt_scale_unc;
				}
			}else{  // All other JEC uncertainty variations (not flavour dependent)
				*tmp_jet *= pt_scale_unc;
			}
		} // if(apply_JEC_uncertainty and JEC_uncertainty_variation != "" ){

		jets.push_back(tmp_jet);  // Add the uncertainty-adjusted jet to the collection of jets

	}

	if((min_pt != NULL && min_pt->size() >1) ||
		(max_pt != NULL && max_pt->size() > 1)){
	  
		double max_size = min_pt->size();
		
		// Loop over the number of values in the min_jet_pt vector cut
		for(unsigned int nasym_cut = 0; nasym_cut < max_size; ++nasym_cut){
			for(std::vector<mor::Jet*>::iterator jet_iter =
			jets.begin();
			jet_iter!=jets.end();
			){
				double cut_out=false;
				     
				if(min_pt != NULL && min_pt->size() > nasym_cut && (*min_pt)[nasym_cut] != -1)
					cut_out = cut_out || cut_pt(*jet_iter, (*min_pt)[nasym_cut]);
				if(max_pt != NULL && max_pt->size() > nasym_cut && (*max_pt)[nasym_cut] != -1)
					cut_out = cut_out || cut_max_pt(*jet_iter, (*max_pt)[nasym_cut]);
				if(max_eta != -1)
					cut_out = cut_out || cut_eta(*jet_iter, max_eta);
				if(electrons->size() > 0 && min_e_dR != -1)
					cut_out = cut_out || cut_leptons(*jet_iter, electrons, min_e_dR);
				if(muons->size() > 0 && min_mu_dR != -1)
					cut_out = cut_out || cut_leptons(*jet_iter, muons, min_mu_dR);
				if(max_fHPD != -1) cut_out = cut_out || cut_max_fHPD(*jet_iter,max_fHPD);
				if(min_n90Hits != -1) cut_out = cut_out || cut_min_n90Hits(*jet_iter, min_n90Hits);
				if(min_emf != -1) cut_out = cut_out || cut_min_emf(*jet_iter, min_emf);
				if(min_nconstituents != -1) cut_out = cut_out || cut_min_nconstituents(*jet_iter, min_nconstituents);
				if(min_chf != -1) cut_out = cut_out || cut_min_chf(*jet_iter, min_chf);
				if(max_nhf != -1) cut_out = cut_out || cut_max_nhf(*jet_iter, max_nhf);
				if(max_nemf != -1) cut_out = cut_out || cut_max_nemf(*jet_iter, max_nemf);
				if(max_cemf != -1) cut_out = cut_out || cut_max_cemf(*jet_iter, max_cemf);
				if(min_cmulti != -1) cut_out = cut_out || cut_min_cmulti(*jet_iter, min_cmulti);
				    
				if(!cut_out)
				{
					unsorted_jets.push_back(*(*jet_iter));
					delete *jet_iter;
					jet_iter = jets.erase(jet_iter);
				}else{
					jet_iter++;
				}
			}
	    

			// this *is* important, don't delete it -- again
			// if N unsorted jets is less than the size of the min_jet_pt vector + 1:
			if(unsorted_jets.size() < nasym_cut+1){ 
				for(std::vector<mor::Jet>::iterator jet_iter = unsorted_jets.begin(); 
				    jet_iter!=unsorted_jets.end(); ++jet_iter){ 
				        jet_sorter.add(&(*jet_iter)); 
				} 
				 
				std::vector<mor::Jet*> sorted_jets = jet_sorter.get_sorted(); 
				 
				for(std::vector<mor::Jet*>::iterator cut_jet = sorted_jets.begin(); 
				    cut_jet != sorted_jets.end(); 
				    ++cut_jet){ 
				        cut_jets->push_back(**cut_jet); 
				} 
				
				for(std::vector<mor::Jet*>::iterator jet = jets.begin(); 
				    jet != jets.end(); 
				    ++jet){ 
				  delete *jet; 
				} 
				return cut_jets; 
			}
		} // for(unsigned int nasym_cut = 0; nasym_cut < max_size; ++nasym_cut){
	}// if((min_pt != NULL && min_pt->size() >1) || (max_pt != NULL && max_pt->size() > 1)){
	else{
	  // In case of a single value instead of a vector of values for min_jet_pt
        	for(std::vector<mor::Jet*>::iterator jet_iter = jets.begin();
		    jet_iter!=jets.end();
		    ++jet_iter){
        	        double cut_out=false;
		 
			if(min_pt != NULL && min_pt->size() == 1 && (*min_pt)[0] != -1)
				cut_out = cut_out || cut_pt(*jet_iter, (*min_pt)[0]);
			if(max_pt != NULL && max_pt->size() == 1 && (*max_pt)[0] != -1)
				cut_out = cut_out || cut_max_pt(*jet_iter, (*max_pt)[0]);
			if(max_eta != -1)
				cut_out = cut_out || cut_eta(*jet_iter, max_eta);
			if(electrons->size() > 0 && min_e_dR != -1)
				cut_out = cut_out || cut_leptons(*jet_iter, electrons, min_e_dR);
			if(muons->size() > 0 && min_mu_dR != -1)
				cut_out = cut_out || cut_leptons(*jet_iter, muons, min_mu_dR);
				     
			if(max_fHPD != -1)
				cut_out = cut_out || cut_max_fHPD(*jet_iter,max_fHPD);
			
			if(min_n90Hits != -1)
				cut_out = cut_out || cut_min_n90Hits(*jet_iter, min_n90Hits);
			
			if(min_emf != -1)
				cut_out = cut_out || cut_min_emf(*jet_iter, min_emf);
			if(min_nconstituents != -1) cut_out = cut_out || cut_min_nconstituents(*jet_iter, min_nconstituents);
			if(min_chf != -1) cut_out = cut_out || cut_min_chf(*jet_iter, min_chf);
			if(max_nhf != -1) cut_out = cut_out || cut_max_nhf(*jet_iter, max_nhf);
			if(max_nemf != -1) cut_out = cut_out || cut_max_nemf(*jet_iter, max_nemf);
			if(max_cemf != -1) cut_out = cut_out || cut_max_cemf(*jet_iter, max_cemf);
			if(min_cmulti != -1) cut_out = cut_out || cut_min_cmulti(*jet_iter, min_cmulti);

        	        if(!cut_out)
        	        {
        	                unsorted_jets.push_back(**jet_iter);
        	        }
        	}
	}// if((min_pt != NULL && min_pt->size() >1) || (max_pt != NULL && max_pt->size() > 1)){

        for(std::vector<mor::Jet>::iterator jet_iter = unsorted_jets.begin();
	    jet_iter!=unsorted_jets.end(); ++jet_iter){
		jet_sorter.add(&(*jet_iter));
        }

        std::vector<mor::Jet*> sorted_jets = jet_sorter.get_sorted();

	for(std::vector<mor::Jet*>::iterator cut_jet = sorted_jets.begin();
	    cut_jet != sorted_jets.end();
	    ++cut_jet){
		cut_jets->push_back(**cut_jet);
	}


	for(std::vector<mor::Jet*>::iterator jet = jets.begin();
		    jet != jets.end();
		    ++jet){
		  delete *jet;
		}

	return cut_jets; 
}

template <typename myLepton>
bool JetSelector::cut_leptons(mor::Jet *jet_iter, std::vector<myLepton> *leptons, double min_dR)
{
	for(typename std::vector<myLepton>::iterator lepton_iter = leptons->begin();
		lepton_iter != leptons->end();
		++lepton_iter){
		if(ROOT::Math::VectorUtil::DeltaR(*dynamic_cast<ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> >*>(jet_iter), *dynamic_cast<ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> >*>(&(*lepton_iter))) < min_dR)
			return true;
	}

	return false;
}

bool JetSelector::cut_min_cmulti(mor::Jet *jet_iter, double &min_cmulti)
{
	if(verbose) std::cout << "cmulti: " << jet_iter->cmulti() << " min cmulti: " << min_cmulti << std::endl;
        if(jet_iter->cmulti() > min_cmulti)
                return false;
        else
                return true;
}

bool JetSelector::cut_max_cemf(mor::Jet *jet_iter, double &max_cemf)
{
	if(verbose) std::cout << "cemf: " << jet_iter->cemf() << " max cemf: " << max_cemf << std::endl;
        if(jet_iter->cemf() < max_cemf)
                return false;
        else
                return true;
}

bool JetSelector::cut_max_nemf(mor::Jet *jet_iter, double &max_nemf)
{
	if(verbose) std::cout << "nemf: " << jet_iter->nemf() << " max nemf: " << max_nemf << std::endl;
        if(jet_iter->nemf() < max_nemf)
                return false;
        else
                return true;
}

bool JetSelector::cut_max_nhf(mor::Jet *jet_iter, double &max_nhf)
{
	if(verbose) std::cout << "nhf: " << jet_iter->nhf() << " max nhf: " << max_nhf << std::endl;
        if(jet_iter->nhf() < max_nhf)
                return false;
        else
                return true;
}

bool JetSelector::cut_min_chf(mor::Jet *jet_iter, double &min_chf)
{
	if(verbose) std::cout << "chf: " << jet_iter->chf() << " min chf: " << min_chf << std::endl;
        if(jet_iter->chf() > min_chf)
                return false;
        else
                return true;
}

bool JetSelector::cut_min_nconstituents(mor::Jet *jet_iter, double &min_nconstituents)
{
	if(verbose) std::cout << "nconstituents: " << jet_iter->nconstituents() << " min nconstituents: " << min_nconstituents << std::endl;
        if(jet_iter->nconstituents() > min_nconstituents)
                return false;
        else
                return true;
}


bool JetSelector::cut_pt(mor::Jet *jet_iter, double &min_pt)
{
        if(jet_iter->Pt() > min_pt)
                return false;
        else
                return true;
}

bool JetSelector::cut_max_pt(mor::Jet *jet_iter, double &max_pt)
{
        if(jet_iter->Pt() < max_pt)
                return false;
        else
                return true;
}

bool JetSelector::cut_eta(mor::Jet *jet_iter, double &max_eta)
{
        if(fabs(jet_iter->Eta()) < max_eta)
                return false;
        else
                return true;
}

bool JetSelector::cut_max_fHPD(mor::Jet *jet_iter, double &max_fHPD)
{

	/*
	 * to be in sync we allow jets with an electromagnetic fraction of 1.
	 * this results in a fHPD value of -1 what we accept for the moment
	 */
        //if(jet_iter->fHPD() >= 0 && jet_iter->fHPD() < max_fHPD)
        if(jet_iter->fHPD() < max_fHPD)
                return false;
        else
                return true;
}

bool JetSelector::cut_min_n90Hits(mor::Jet *jet_iter, double &min_n90Hits)
{
        if(jet_iter->n90Hits() >= min_n90Hits)
                return false;
        else
                return true;
}

bool JetSelector::cut_min_emf(mor::Jet *jet_iter, double &min_emf)
{
        if(jet_iter->emf() > min_emf)
                return false;
        else
                return true;
}
