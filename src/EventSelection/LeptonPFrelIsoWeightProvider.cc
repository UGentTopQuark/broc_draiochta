#include "../../interface/EventSelection/LeptonPFrelIsoWeightProvider.h"

broc::LeptonPFrelIsoWeightProvider::LeptonPFrelIsoWeightProvider(std::string weight_file, eire::HandleHolder *handle_holder): broc::LeptonEfficiencyWeightProvider(weight_file, handle_holder)
{
}

broc::LeptonPFrelIsoWeightProvider::~LeptonPFrelIsoWeightProvider()
{
}

double broc::LeptonPFrelIsoWeightProvider::get_value_for_event()
{
	std::string conesize = "0.3";
	double lepton_relIso=-999;
	if(process_muons && muons->size() > 0){
		lepton_relIso = (muons->begin()->chargedHadronIso(conesize) + muons->begin()->neutralHadronIso(conesize) + muons->begin()->photonIso(conesize))/muons->begin()->Pt();
	}else if(process_electrons && electrons->size() > 0){
		lepton_relIso = (electrons->begin()->chargedHadronIso(conesize) + electrons->begin()->neutralHadronIso(conesize) + electrons->begin()->photonIso(conesize))/electrons->begin()->Pt();
	}

	return lepton_relIso;
}
