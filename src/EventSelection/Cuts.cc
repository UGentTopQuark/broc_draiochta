#include "../../interface/EventSelection/Cuts.h"

broc::Cuts::Cuts(broc::CutsSet *cuts_set, eire::HandleHolder *handle_holder)
{
	identifier=handle_holder->get_ident();

	this->config_reader = handle_holder->get_config_reader();

	this->cuts_set = cuts_set;

	// class to write out kinematic quantities to shell
	event_identifier = new EventIdentifier(handle_holder);

	// initialise cuts
	ht = -1;

	min_event_discriminator = -1;
	max_event_discriminator = -1;
	pvertex_max_z=-1;
	pvertex_min_ndof=-1;
	pvertex_max_rho=-1;
	pvertex_not_fake=-1;
	min_npvertices=-1;
	max_lep_b_chi2 = -1;
	min_M3=-1;
	max_M3=-1;
	min_met=-1;
	max_met=-1;
	max_ht=-1;
	min_ht=-1;
	max_missing_ht=-1;
	min_missing_ht=-1;
	min_no_jets=-1;
	max_no_jets=-1;
	min_nloose_mu=-1;//jl 15.07.10
	min_nisolated_lep=-1;
	min_nloose_e=-1;//jl 01.09.10
	max_nisolated_e=-1;
	max_nisolated_mu=-1;
	min_nisolated_e=-1;
	min_nisolated_mu=-1;
	max_nnoniso_e=-1;
	max_nnoniso_mu=-1;
	min_nnoniso_e=-1;
	min_nnoniso_mu=-1;
	Z_window_min=-1;
	Z_window_max=-1;
	num_name_btag = -1;
	min_chi2 = -1;
	max_chi2 = -1;
	name_btag = "-1";
	set_trigger_OR = -1;

	max_e_dist = -1;
	max_e_dcot = -1;
	max_e_nLostTrackerHits = -1;

	identify_events = false;

	cuts_passed_counter=0;
	cuts_overall_counter=0;

	isolated_electrons = NULL;
	loose_electrons = NULL;
	isolated_muons = NULL;
	loose_muons = NULL;
	corrected_mets = NULL;
	uncorrected_mets = NULL;
	my_mets = NULL; //jl 14.01.11
	event_information = NULL;

	min_btag = NULL;
	max_consid_bjets = -1;
	min_n_bjets = -1;
	max_n_bjets = -1;
	trigger = NULL;

	hitfit_state = -1;

	set_handles(handle_holder);

	// check for an electron if it comes from a photon conversion
	conversion_identifier = new eire::ConversionIdentifier();

	event_weight = 1;

	// check all jets in the event if they pass b-tag requirement
	bjet_finder = new BJetFinder();
	bjet_finder->set_handles(handle_holder);

	// reconstruct particle from two leptons, here Z->ll events
	z_reconstructor = handle_holder->services()->dilepton_reco();
	// various (simple) mass reconstruction algorithms
	mass_reco = handle_holder->services()->mass_reco();
	// total transverse energy in the event sum(pt(jets)+pt(leptons))
	ht_calc = handle_holder->services()->ht_calc();
	// MVA classification of an event
	event_classifier = handle_holder->services()->event_classifier();

	set_all_cuts();
	set_all_vcuts();

    hitfit_study = NULL;

	enable_kinematic_fit_module = config_reader->get_bool_var("enable_kinematic_fit_module", "global", false);
	//enable_hitfit_combi_seed = config_reader->get_bool_var("use_hitfit_combi_seed", "lhco_writer", false);

    if(enable_kinematic_fit_module) hitfit_study = new hitfit::HitFitManager(cuts_set, handle_holder);


}

broc::Cuts::~Cuts()
{
	print_cuts();

	if(conversion_identifier){
		delete conversion_identifier;
		conversion_identifier = NULL;
	}

	if(bjet_finder){
		delete bjet_finder;
		bjet_finder = NULL;
	}
	if(event_identifier){ delete event_identifier; event_identifier = NULL; }

    if(hitfit_study){delete hitfit_study; hitfit_study = NULL;}

}

void broc::Cuts::set_event_weight(double weight)
{
	this->event_weight = weight;
}

void broc::Cuts::set_handles(eire::HandleHolder *handle_holder)
{
  this->handle_holder = handle_holder;
  this->isolated_electrons = handle_holder->get_tight_electrons();
  this->isolated_muons = handle_holder->get_tight_muons();
  this->loose_electrons = handle_holder->get_loose_electrons();
  this->loose_muons = handle_holder->get_loose_muons();
  this->corrected_mets = handle_holder->get_corrected_mets();
  this->jets = handle_holder->get_tight_jets();
  this->HLTR = handle_holder->get_trigger();
  this->pvertices = handle_holder->get_primary_vertices();
  this->event_information = handle_holder->get_event_information();
}

bool broc::Cuts::cut()
{
	bool cut = false;

	// Call functions to apply cuts set
	if(trigger != NULL) cut = cut || cut_hltrigger();
	if(min_no_jets != -1 ||
	   max_no_jets != -1)
		cut = cut || cut_njets();

	// check for minimum/maximum number of leptons
	cut = cut || cut_nisolated_electrons();
	cut = cut || cut_nloose_electrons();
	cut = cut || cut_nloose_muons();
	cut = cut || cut_nisolated_muons();
	if(min_nnoniso_e != -1 || max_nnoniso_e != -1){
		cut = cut || cut_nnoniso_electrons();
	}
	if(min_nnoniso_mu != -1 || max_nnoniso_mu != -1){
		cut = cut || cut_nnoniso_muons();
	}
	if(min_nisolated_lep != -1) cut = cut || cut_nisolated_leptons();
	if(min_met != -1 || max_met != -1) cut = cut || cut_met();
	if(min_M3 != -1 || max_M3 != -1) cut = cut || cut_M3();
	if(max_ht != -1) cut = cut || cut_max_ht();
	if(min_ht != -1) cut = cut || cut_min_ht();
	if(max_missing_ht != -1) cut = cut || cut_max_missing_ht();
	if(min_missing_ht != -1) cut = cut || cut_min_missing_ht();
	if(max_lep_b_chi2 != -1) cut = cut || cut_lep_b_chi2();

	if(Z_window_max != -1 && Z_window_min != -1 &&
		(Z_requirement == 1 || Z_requirement == 3 || Z_requirement == 5)) cut = cut || is_Z();	// requirement 1: cut out if event is Z
	if(Z_window_max != -1 && Z_window_min != -1 &&
		(Z_requirement == 2 || Z_requirement == 4 || Z_requirement == 6)) cut = cut || !is_Z();	// requirement 2: cut out if event is NOT Z
	//if(max_e_dcot != -1 || max_e_dist != -1 || max_e_nLostTrackerHits != -1) cut = cut || cut_electron_conv_rej();

	// Turn on b-tag weights only if there is a b-tag cut defined in that cutstep
	if(min_btag != NULL && name_btag != "-1"){
	    cut = cut || cut_btag();
		if(!cut){
			handle_holder->set_btag_cut_state(true);
		}else{
			handle_holder->set_btag_cut_state(false);
		}
	}else{
	    handle_holder->set_btag_cut_state(false);
	}

//	
//	if(min_chi2 != -1 || max_chi2 != -1) cut = cut || cut_chi2();
	if(pvertex_max_z != -1 ||
	   pvertex_min_ndof != -1 ||
	   pvertex_max_rho != -1 ||
	   pvertex_not_fake != -1 ||
	   min_npvertices != -1)
	    cut = cut || cut_primary_vertex();
	if(min_event_discriminator != -1 || max_event_discriminator != -1) cut = cut || cut_event_classifier();

	// Only run HitFit on cut events (performance reasons)
	if(enable_kinematic_fit_module && hitfit_state == 1 && !cut){
		cut = cut || cut_hitfit();
	}

	event_weight = handle_holder->get_event_weight();
	// count events that passed cuts and those that were discarded
	//cuts_overall_counter += handle_holder->get_failing_event_weight();
	//cuts_overall_counter += event_weight;

	if(!cut){
	    cuts_passed_counter += event_weight;
	    cuts_overall_counter += event_weight;
	}else{
	    cuts_overall_counter += handle_holder->get_failing_event_weight();
	}

	if(!cut && identify_events == 1){
	    event_identifier->print();
	}

	// Returns false if event passed cuts, true if event should be discarded
	return cut;
}

// MVA classification
bool broc::Cuts::cut_event_classifier()
{
	if(!event_classifier){
		std::cerr << "WARNING: event classifier not available, is the classification module enabled in your configuration file? exiting..." << std::endl;
		exit(1);
	}
	
	double discriminator = event_classifier->classify();

	if(((discriminator > min_event_discriminator) || min_event_discriminator == -1) && (discriminator < max_event_discriminator || max_event_discriminator == -1))
		return false;
	else
		return true;
}

bool broc::Cuts::cut_primary_vertex()
{
	int ngood_vertices=0;
	if(pvertices->size() > 0){
		for(std::vector<mor::PrimaryVertex>::iterator vertex = pvertices->begin();
			vertex != pvertices->end();
			++vertex){
			if(((pvertex_max_z == -1) || pvertex_max_z > fabs(vertex->z())) &&
			   ((pvertex_min_ndof == -1) || pvertex_min_ndof < vertex->ndof()) &&
			   ((pvertex_max_rho == -1) || pvertex_max_rho > vertex->rho()) &&
			   ((pvertex_not_fake == -1) || pvertex_not_fake == !vertex->is_fake())){
				ngood_vertices++;
			}
			
			break;	// INFO: for the moment only consider first vertex in collection
				// cf. 'reference selection'
		}
	}
	if(!(min_npvertices == -1) && ngood_vertices < min_npvertices)
		return true;
	else
		return false;
}

// chi2 calculated for lep-b mass
bool broc::Cuts::cut_lep_b_chi2()
{
	double lep_b_chi2 = mass_reco->get_lep_b_chi2();
	if(lep_b_chi2 > max_lep_b_chi2 || lep_b_chi2 < 0.)
		return true;
	else
		return false;
}

// Apply cut for min pt and number of jets
bool broc::Cuts::cut_njets()
{
	int njets=jets->size();
	if(njets < min_no_jets || (njets > max_no_jets && max_no_jets >= 0))
		return true;
	else
		return false;
}

// min/max M3 mass cuts
bool broc::Cuts::cut_M3()
{
	double M3_mass = mass_reco->calculate_M3();
	if(((M3_mass > min_M3) || min_M3 == -1) && (M3_mass < max_M3 || max_M3 == -1))
		return false;
	else
		return true;
}

bool broc::Cuts::cut_met()
{
	if(corrected_mets->size() < 1)
		return true;

        std::vector<mor::MET>::const_iterator met_iter = corrected_mets->begin();
	if((min_met == -1 || met_iter->Et() > min_met) && (met_iter->Et() < max_met || max_met == -1))
		return false;
	else
		return true;
}

bool broc::Cuts::cut_chi2()
{
	if((min_chi2 != -1 && mass_reco->get_chi2() < min_chi2) || (max_chi2 != -1 && mass_reco->get_chi2() > max_chi2))
		return true;
	else
		return false;
}

// Cut on total transverse energy
bool broc::Cuts::cut_min_ht()
{
	ht = ht_calc->get_ht();

	// Sum pt of all jets + one lepton

	if(ht < min_ht)
		return true;
	else
		return false;
}

// Cut on total energy
bool broc::Cuts::cut_max_ht()
{
	ht = ht_calc->get_ht();

	if(ht > max_ht)
		return true;
	else
		return false;
}

// Cut on total transverse energy
bool broc::Cuts::cut_min_missing_ht()
{
	double missing_ht = ht_calc->get_missing_ht();

	// Sum pt of all jets + one lepton

	if(missing_ht < min_missing_ht)
		return true;
	else
		return false;
}

// Cut on total energy
bool broc::Cuts::cut_max_missing_ht()
{
	double missing_ht = ht_calc->get_missing_ht();

	if(missing_ht > max_missing_ht)
		return true;
	else
		return false;
}

bool broc::Cuts::is_Z()
{
	return z_reconstructor->get_candidates()->size() > 0;
}

bool broc::Cuts::cut_nisolated_leptons()
{
	int niso_lep = isolated_electrons->size() + isolated_muons->size();
		
	if((niso_lep >= min_nisolated_lep) || (min_nisolated_lep == -1))
		return false;
	else
		return true;   		
}

bool broc::Cuts::cut_electron_conv_rej()
{
	int niso_e = 0;
	for(std::vector<mor::Electron>::iterator electron = isolated_electrons->begin();
		electron != isolated_electrons->end();
		++electron){
		if(!(conversion_identifier->from_conversion(&(*electron))))
			++niso_e;
	}

	if((niso_e >= min_nisolated_e && niso_e <= max_nisolated_e ) || ((niso_e >= min_nisolated_e) && (max_nisolated_e == -1)) || ((min_nisolated_e == -1) && (niso_e <= max_nisolated_e)) ||  ((min_nisolated_e == -1) && (max_nisolated_e == -1)))
		return false;
	else
		return true;
}

// Set max and min number of each lepton allowed
bool broc::Cuts::cut_nisolated_electrons()
{
	int niso_e = isolated_electrons->size();

	if(niso_e == 0 && max_nisolated_e == 0)
		return false;
	
	if((niso_e >= min_nisolated_e && niso_e <= max_nisolated_e ) || ((niso_e >= min_nisolated_e) && (max_nisolated_e == -1)) || ((min_nisolated_e == -1) && (niso_e <= max_nisolated_e)) ||  ((min_nisolated_e == -1) && (max_nisolated_e == -1)))
		return false;
	else
		return true;
}

bool broc::Cuts::cut_nloose_electrons()
{
	int nloose_e = loose_electrons->size();

	if(nloose_e == 0 && max_nloose_e == 0)
		return false;
	
	if((nloose_e >= min_nloose_e && nloose_e <= max_nloose_e ) || ((nloose_e >= min_nloose_e) && (max_nloose_e == -1)) || ((min_nloose_e == -1) && (nloose_e <= max_nloose_e)) ||  ((min_nloose_e == -1) && (max_nloose_e == -1))) //jl 01.09.10
		return false;
	else
		return true;
}


bool broc::Cuts::cut_nnoniso_electrons()
{
	if(!handle_holder->get_selected_noniso_electrons()){
		std::cout << "ERROR: trying to select min/max noniso electrons but iso collection not available. Exiting program." << std::endl;
		exit(1);
	}

	int niso_e = handle_holder->get_selected_noniso_electrons()->size();

	if(niso_e == 0 && max_nnoniso_e == 0)
		return false;
	
	if((niso_e >= min_nnoniso_e && niso_e <= max_nnoniso_e ) || ((niso_e >= min_nnoniso_e) && (max_nnoniso_e == -1)) || ((min_nnoniso_e == -1) && (niso_e <= max_nnoniso_e)) ||  ((min_nnoniso_e == -1) && (max_nnoniso_e == -1)))
		return false;
	else
		return true;
}


bool broc::Cuts::cut_nloose_muons()
{
	int nloose_mu = loose_muons->size();

	if(nloose_mu == 0 && max_nloose_mu == 0)
		return false; //jl 15.07.10
	
	if(((nloose_mu >= min_nloose_mu) && (nloose_mu <= max_nloose_mu)) || ((nloose_mu >= min_nloose_mu) && (max_nloose_mu == -1)) || ((min_nloose_mu == -1) && (nloose_mu <= max_nloose_mu)) ||  ((min_nloose_mu == -1) && (max_nloose_mu == -1)))  //jl 15.07.10
		return false;		
	else
		return true;
}

bool broc::Cuts::cut_nisolated_muons()
{
	int niso_mu = isolated_muons->size();

	if(niso_mu == 0 && max_nisolated_mu == 0)
		return false;

	if(((niso_mu >= min_nisolated_mu) && (niso_mu <= max_nisolated_mu)) || ((niso_mu >= min_nisolated_mu) && (max_nisolated_mu == -1)) || ((min_nisolated_mu == -1) && (niso_mu <= max_nisolated_mu)) ||  ((min_nisolated_mu == -1) && (max_nisolated_mu == -1))) 
		return false;		
	else
		return true;
}

bool broc::Cuts::cut_nnoniso_muons()
{
	if(!handle_holder->get_selected_noniso_muons()){
		std::cout << "ERROR: trying to select min/max noniso muons but iso collection not available. Exiting program." << std::endl;
		exit(1);
	}
	int niso_mu = handle_holder->get_selected_noniso_muons()->size();

	if(niso_mu == 0 && max_nnoniso_mu == 0)
		return false;

	if(((niso_mu >= min_nnoniso_mu) && (niso_mu <= max_nnoniso_mu)) || ((niso_mu >= min_nnoniso_mu) && (max_nnoniso_mu == -1)) || ((min_nnoniso_mu == -1) && (niso_mu <= max_nnoniso_mu)) ||  ((min_nnoniso_mu == -1) && (max_nnoniso_mu == -1))) 
		return false;		
	else
		return true;
}

// Check if HitFit returned a successful fit
bool broc::Cuts::cut_hitfit()
{
	bool fit_status;

	// run hitfit to get the estimate on the jet kinematics for the event
	fit_status = hitfit_study->fill_trees();

	if (fit_status == true)
		return false;
	else
		return true;
}



//Calculate efficiency and purity for different btag cuts
bool broc::Cuts::cut_btag()
{
  int nbjets = 0;


  for(unsigned int i = 0; i < jets->size(); i++){
    if((*jets)[i].is_bjet()){nbjets++;}
  }

  // Ideogram
  //for(unsigned int i = 0; i < 4; i++){
  //  if((*jets)[i].is_bjet()){nbjets++;}
  //}


  if((min_n_bjets == -1 || nbjets >= min_n_bjets) && (max_n_bjets == -1 || nbjets <= max_n_bjets)){
	  return false;
  }
  else{
	  return true;
  }

	//get back vector of jet(id,btag) in order of decreasing btag
	//bjet_finder->set_min_btag_value(*min_btag);
	//if(max_consid_bjets != -1) bjet_finder->set_max_considered_jets((int) max_consid_bjets);
	//std::vector<std::pair<int,double> > *btag_ids_this_algo = bjet_finder->get_btag_sorted_jets(name_btag);
	
	//if(btag_ids_this_algo->size() >= min_btag->size())
  //return false;
  //else
  //	return true;
}

//Apply trigger cuts
bool broc::Cuts::cut_hltrigger()
{
	eire::TriggerNameProvider trig_name_prov;

	if(set_trigger_OR == 1){ // OR of multiple triggers
		bool cut_event = true;
		for(std::vector<double>::iterator trig_iter = trigger->begin(); trig_iter!=trigger->end(); ++ trig_iter){ 
			if(HLTR->triggered(trig_name_prov.hlt_name(int(*trig_iter)))) {
				cut_event = false;
			}
		}
		return cut_event;
	}
	else{	// AND of multiple triggers
		for(std::vector<double>::iterator trig_iter = trigger->begin(); trig_iter!=trigger->end(); ++ trig_iter){
			if(!HLTR->triggered(trig_name_prov.hlt_name(int(*trig_iter)))) {
				//HLT_QuadJet30 = 24, //HLT_Mu11 = 83,//HLT_Mu15 = 85,  //HLT_Ele_15_LW_L1R = 50
				return true;
			}
		}
	}
	return false;
}

void broc::Cuts::set_all_vcuts()
{
	trigger = cuts_set->get_vcut_value("trigger");
	
	min_btag = cuts_set->get_vcut_value("min_btag");

	conversion_identifier->set_cuts(cuts_set);

	std::vector<double> *lost_hits = cuts_set->get_vcut_value("max_e_nLostTrackerHits");
	if(lost_hits != NULL && lost_hits->size() > 0)
		max_e_nLostTrackerHits = (*(cuts_set->get_vcut_value("max_e_nLostTrackerHits")))[0];
	else
		max_e_nLostTrackerHits = -1;
}

void broc::Cuts::set_all_cuts()
{
	set_trigger_OR = int(cuts_set->get_cut_value("set_trigger_OR"));

	if(cuts_set->get_cut_value("identify_events") == -1 || cuts_set->get_cut_value("identify_events") == 0)
		identify_events = 0;
	else
		identify_events = 1;

	min_no_jets = (int) cuts_set->get_cut_value("min_no_jets");
	max_no_jets = (int) cuts_set->get_cut_value("max_no_jets");

	max_lep_b_chi2 = cuts_set->get_cut_value("max_lep_b_chi2");

	max_e_dcot = cuts_set->get_cut_value("max_e_dcot");
	max_e_dist = cuts_set->get_cut_value("max_e_dist");
	//jl 08.12.10
	max_loose_e_dcot = cuts_set->get_cut_value("max_loose_e_dcot");
	max_loose_e_dist = cuts_set->get_cut_value("max_loose_e_dist");

	pvertex_max_z = cuts_set->get_cut_value("pvertex_max_z");
	pvertex_min_ndof = cuts_set->get_cut_value("pvertex_min_ndof");
	pvertex_max_rho = cuts_set->get_cut_value("pvertex_max_rho");
	pvertex_not_fake = cuts_set->get_cut_value("pvertex_not_fake");
	min_npvertices = cuts_set->get_cut_value("min_npvertices");

	min_event_discriminator = cuts_set->get_cut_value("min_event_discriminator");
	max_event_discriminator = cuts_set->get_cut_value("max_event_discriminator");

	min_nisolated_lep = (int) cuts_set->get_cut_value("min_nisolated_lep");
	min_nisolated_e = (int) cuts_set->get_cut_value("min_nisolated_e");
	min_nisolated_mu = (int) cuts_set->get_cut_value("min_nisolated_mu");
	max_nisolated_e = (int) cuts_set->get_cut_value("max_nisolated_e");
	max_nisolated_mu = (int) cuts_set->get_cut_value("max_nisolated_mu");
	max_nloose_mu = (int) cuts_set->get_cut_value("max_nloose_mu");
	max_nloose_e = (int) cuts_set->get_cut_value("max_nloose_e");
	min_nloose_mu = (int) cuts_set->get_cut_value("min_nloose_mu"); //jl 15.07.10
	min_nloose_e = (int) cuts_set->get_cut_value("min_nloose_e"); //jl 01.09.10
	min_nnoniso_e = (int) cuts_set->get_cut_value("min_nnoniso_e");
	min_nnoniso_mu = (int) cuts_set->get_cut_value("min_nnoniso_mu");
	max_nnoniso_e = (int) cuts_set->get_cut_value("max_nnoniso_e");
	max_nnoniso_mu = (int) cuts_set->get_cut_value("max_nnoniso_mu");
	min_n_bjets = (int) cuts_set->get_cut_value("min_n_bjets");
	max_n_bjets = (int) cuts_set->get_cut_value("max_n_bjets");

	min_met = cuts_set->get_cut_value("min_met");
	max_met = cuts_set->get_cut_value("max_met");
	min_M3 = cuts_set->get_cut_value("min_M3");
	max_M3 = cuts_set->get_cut_value("max_M3");
	min_ht = cuts_set->get_cut_value("min_ht");
	max_ht = cuts_set->get_cut_value("max_ht");
	min_missing_ht = cuts_set->get_cut_value("min_missing_ht");
	max_missing_ht = cuts_set->get_cut_value("max_missing_ht");
	Z_window_max = cuts_set->get_cut_value("Z_window_max");
	Z_window_min = cuts_set->get_cut_value("Z_window_min");
	z_reconstructor->set_mass_window(Z_window_min, Z_window_max);

	/*
	 * 1: reject all Z
	 * 2: force all Z
	 * 3: reject muon Z
	 * 4: force muon Z
	 * 5: reject electron Z
	 * 6: force electron Z
	 */
	Z_requirement = cuts_set->get_cut_value("Z_requirement");
	if(cuts_set->get_cut_value("Z_allow_equal_charge") == 1)
		z_reconstructor->set_allow_equal_charge(true);
	switch((int) Z_requirement){
		case 3:
			z_reconstructor->select_lepton(13);
			break;
		case 4:
			z_reconstructor->select_lepton(13);
			break;
		case 5:
			z_reconstructor->select_lepton(11);
			break;
		case 6:
			z_reconstructor->select_lepton(11);
			break;
	};
	min_chi2 = cuts_set->get_cut_value("min_chi2");
	max_chi2 = cuts_set->get_cut_value("max_chi2");
	
	num_name_btag = cuts_set->get_cut_value("name_btag");
	max_consid_bjets = cuts_set->get_cut_value("max_consid_bjets");

	broc::BTagAlgoNameProvider btag_name_prov;
	if (num_name_btag != -1) name_btag = btag_name_prov.get_name((int) num_name_btag);
	else name_btag = "-1";

	hitfit_state = cuts_set->get_cut_value("hitfit_state");
}


/*
 *	Print all cuts for Cuts object
 */

void broc::Cuts::print_cuts()
{
	std::cout << "=-----------IMPOSED-CUTS-----------=" << std::endl;
	std::cout << "identifier: " << identifier << std::endl;
	std::cout << "cuts_passed: " << std::setprecision(20) << cuts_passed_counter << std::endl;
	std::cout << "cuts_overall: " << std::setprecision(20) << cuts_overall_counter << std::endl;
	std::cout << "eff: " << cuts_passed_counter/(cuts_overall_counter) << std::endl;
	std::cout << "=----------------------------------=" << std::endl;
}
