#include "../../interface/EventSelection/CutsSet.h"

double broc::CutsSet::get_cut_value(std::string value)
{
	if(cuts_set.find(value) != cuts_set.end()){
		return cuts_set[value];
	}else{
		std::cerr << "ERROR: Cuts::get_cut_value: invalid cut value requested: " << value << std::endl;	
		return 0;
	}
}

std::vector<double>* broc::CutsSet::get_vcut_value(std::string value)
{
	if(vcuts_set.find(value) != vcuts_set.end()){
		return vcuts_set[value];
	}else{
		std::cerr << "ERROR: Cuts::get_vcut_value: invalid cut value requested: " << value << std::endl;	
		return 0;
	}
}

void broc::CutsSet::set_cuts(std::map<std::string, std::vector<double>* > &vcuts_set)
{
	this->vcuts_set = vcuts_set;
}

void broc::CutsSet::set_cuts(std::map<std::string, double> &cuts_set)
{
	this->cuts_set = cuts_set;
}

void broc::CutsSet::set_cut(std::string name, double cut)
{
	cuts_set[name] = cut;
}

void broc::CutsSet::set_cut(std::string name, std::vector<double> *cut)
{
	vcuts_set[name] = cut;
}
