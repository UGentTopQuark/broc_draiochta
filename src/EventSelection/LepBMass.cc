#include "../../interface/EventSelection/LepBMass.h"

broc::LepBMass::LepBMass(eire::HandleHolder *handle_holder)
{
	this->handle_holder = handle_holder;
	this->electrons = handle_holder->get_tight_electrons();
	this->muons = handle_holder->get_tight_muons();
	this->jets = handle_holder->get_tight_jets();

	chi2_sorter = new broc::BrusselsChi2JetSorter(handle_holder);
}

broc::LepBMass::~LepBMass()
{
	if(chi2_sorter){ delete chi2_sorter; chi2_sorter = 0; }
}

void broc::LepBMass::next_event()
{
	lep_b_mass = -1;
	min_chi2 = -1;
	calc_lep_b_mass();
}

void broc::LepBMass::calc_lep_b_mass()
{
	chi2_sorter->reconstruct_ttbar();

	int lep_b = chi2_sorter->get_lep_b_id();
	min_chi2 = chi2_sorter->get_min_chi2();

	if(electrons->begin() != electrons->end() && lep_b != -1 && lep_b < jets->size()){
		lep_b_mass = (electrons->begin()->p4()+(*jets)[lep_b].p4()).mass();
	}else if(muons->begin() != muons->end() && lep_b != -1 && lep_b < jets->size()){
		lep_b_mass = (muons->begin()->p4()+(*jets)[lep_b].p4()).mass();
	}
}
