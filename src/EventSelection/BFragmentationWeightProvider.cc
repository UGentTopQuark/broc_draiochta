#include "../../interface/EventSelection/BFragmentationWeightProvider.h"

broc::BFragmentationWeightProvider::BFragmentationWeightProvider(eire::HandleHolder *handle_holder)
{
	if(handle_holder->get_ident().find("Data") != std::string::npos)
		do_not_reweight = true;
	else
		do_not_reweight = false;

	jets = handle_holder->get_tight_jets();
	gen_event = handle_holder->get_ttbar_gen_evt();

	std::string sourceFileName = handle_holder->get_config_reader()->get_var("BFragZ2", "event_weights", true);
	std::string targetFileName = handle_holder->get_config_reader()->get_var("BFragZ2rbLEP", "event_weights", true);
	
	TFile* sourceFile = new TFile(sourceFileName.c_str(),"read");
	TFile* targetFile = new TFile(targetFileName.c_str(),"read");

	TH1F* sourceHist = (TH1F*) sourceFile->Get("EventWeightBJES/genBHadronPtFraction")->Clone();
	TH1F* targetHist = (TH1F*) targetFile->Get("EventWeightBJES/genBHadronPtFraction")->Clone();
  
	if (sourceHist->GetNbinsX() != targetHist->GetNbinsX()) std::cout << "Incompatible b-fragmentation histograms: Number of bins not equal" << std::endl;
  
	sourceHist->Scale(1./sourceHist->Integral());
	targetHist->Scale(1./targetHist->Integral());
  
	hists = (TH1F*) targetHist->Clone();
	hists->Divide(sourceHist);
  
	/*
	std::cout << "Weights for b-fragmentation" << std::endl;
	for (int i = 0; i < hists->GetNbinsX(); ++i) {
	    std::cout << std::setiosflags(std::ios::left)
		      << "[" << i << "] "
		      << std::setw(10) << hists->GetBinContent(i);
	}
	std::cout << std::endl;
	*/
}

broc::BFragmentationWeightProvider::~BFragmentationWeightProvider()
{ 

}

double broc::BFragmentationWeightProvider::get_weight()
{
    if(do_not_reweight){return 1.;}
    else{
	std::vector< mor::Particle* > genJets;
	genJets.push_back(gen_event->hadB());
	genJets.push_back(gen_event->lepB());
	
	double eventWeight = 1.;
	
	for(size_t i = 0; i < genJets.size(); ++i) {
	    mor::Particle* p = (genJets)[i];
	    double mindR = 10;
	    double pt_mindR;
	    for (size_t j = 0; j<jets->size(); ++j) {
		mor::Jet jet_j = (*jets)[j];
		if (p->p4().pt() == 0 || jet_j.pt() == 0) continue;
		double dR = ROOT::Math::VectorUtil::DeltaR(p->p4(), jet_j.p4());
		// Simple dR match of hadron and GenJet
		if(dR<mindR) {
		    mindR = dR; 
		    pt_mindR=jet_j.pt();
		}
	    }
	    if (mindR < 0.5) {
		//std::cout<<"jet pt = "<<pt_mindR<<"\tparton pt = "<<p->p4().pt()<<std::endl;
		double xb = p->p4().pt()/pt_mindR;
		//std::cout<<"xb = "<<xb<<std::endl;
		if (xb < 2.) {
		    eventWeight *= hists->GetBinContent(hists->FindFixBin(xb));
		}
		break;
	    }
	}
	//std::cout<<"BFWP : bfrag weight = "<<eventWeight<<std::endl;
	return eventWeight;
    }
    
}
