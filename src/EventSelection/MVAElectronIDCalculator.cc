#include "../../interface/EventSelection/MVAElectronIDCalculator.h"

broc::MVAElectronIDCalculator::MVAElectronIDCalculator(eire::HandleHolder *handle_holder)
{
	this->handle_holder = handle_holder;
	this->config_reader = handle_holder->get_config_reader();
	this->id = "_"+handle_holder->get_ident();

	//MVANonTrig = NULL;
	//myMVATrig = NULL;

	MVANonTrig = new EGammaMvaEleEstimator();
	MVATrig = new EGammaMvaEleEstimator();

	histo_writer = handle_holder->get_histo_writer();


	if(control_plots){
		book_histos();
	}

	initialise();
}

broc::MVAElectronIDCalculator::~MVAElectronIDCalculator()
{
	if(MVANonTrig){ delete MVANonTrig; MVANonTrig = NULL; }
	if(MVATrig){ delete MVATrig; MVATrig = NULL; }
}

bool broc::MVAElectronIDCalculator::is_trig_e(mor::Electron& ele)
{
  bool TrigPresel = false;
  if(fabs(ele.supercluster_eta()) < 1.485) {
    if(ele.sigmaIetaIeta() < 0.014 &&
       ele.hadronicOverEm() < 0.15 &&
       ele.trackIso()/ele.pt() < 0.2 &&
       ele.ecalIso()/ele.pt() < 0.2 &&
       ele.hcalIso()/ele.pt() < 0.2 &&
       ele.nLostTrackerHits() == 0)
      TrigPresel = true;
  }
  else {
    if(ele.sigmaIetaIeta() < 0.035 &&
       ele.hadronicOverEm() < 0.10 &&
       ele.trackIso()/ele.pt() < 0.2 &&
       ele.ecalIso()/ele.pt() < 0.2 &&
       ele.hcalIso()/ele.pt() < 0.2 &&
       ele.nLostTrackerHits() == 0)
      TrigPresel = true;
  }
  
  
  return TrigPresel;
}

double broc::MVAElectronIDCalculator::discriminator(mor::Electron& ele)
{
	if(!MVATrig || !MVANonTrig)
		return -999;

	double MVAVar_fbrem = ele.MVAVar_fbrem(); 
	double MVAVar_kfchi2 = ele.MVAVar_kfchi2();
	int MVAVar_kfhits = (int) ele.MVAVar_kfhits();
	double MVAVar_gsfchi2 = ele.MVAVar_gsfchi2();
	double MVAVar_deta = ele.MVAVar_deta();
	double MVAVar_dphi = ele.MVAVar_dphi();
	double MVAVar_detacalo = ele.MVAVar_detacalo();
	//double MVAVar_dphicalo = ele.MVAVar_dphicalo();
	double MVAVar_see = ele.MVAVar_see();
	double MVAVar_spp = ele.MVAVar_spp();
	double MVAVar_etawidth = ele.MVAVar_etawidth();
	double MVAVar_phiwidth = ele.MVAVar_phiwidth();
	double MVAVar_e1x5e5x5 = ele.MVAVar_e1x5e5x5();
	double MVAVar_R9 = ele.MVAVar_R9();
	//double MVAVar_nbrems = ele.MVAVar_nbrems();
	double MVAVar_HoE = ele.MVAVar_HoE();
	double MVAVar_EoP = ele.MVAVar_EoP();
	double MVAVar_IoEmIoP = ele.MVAVar_IoEmIoP();
	double MVAVar_eleEoPout = ele.MVAVar_eleEoPout();
	double MVAVar_PreShowerOverRaw = ele.MVAVar_PreShowerOverRaw();
	//double MVAVar_EoPout = ele.MVAVar_EoPout();
	double MVAVar_d0 = ele.MVAVar_d0();
	double MVAVar_ip3d = ele.MVAVar_ip3d();
	double MVAVar_eta = ele.MVAVar_eta();
	double MVAVar_pt = ele.MVAVar_pt();

	bool debugVar = false;

	double mva_output=0.;
	if(is_trig_e(ele)){
		mva_output = MVATrig->mvaValue( MVAVar_fbrem, 
						    MVAVar_kfchi2,
						    MVAVar_kfhits,
						    MVAVar_gsfchi2,
						    MVAVar_deta,
						    MVAVar_dphi,
						    MVAVar_detacalo,
						    // MVAVar_dphicalo,
						    MVAVar_see,
						    MVAVar_spp,
						    MVAVar_etawidth,
						    MVAVar_phiwidth,
						    MVAVar_e1x5e5x5,
						    MVAVar_R9,
						    //MVAVar_nbrems,
						    MVAVar_HoE,
						    MVAVar_EoP,
						    MVAVar_IoEmIoP,
						    MVAVar_eleEoPout,
						    MVAVar_PreShowerOverRaw,
						    // MVAVar_EoPout,
						    MVAVar_d0,
						    MVAVar_ip3d,
						    MVAVar_eta,
						    MVAVar_pt,
						    debugVar);
		if(control_plots) histos1d[("eMVA_trig_frac"+id).c_str()]->Fill(1);
	}else{
		mva_output = MVANonTrig->mvaValue(MVAVar_fbrem,
						    MVAVar_kfchi2,
						    MVAVar_kfhits,
						    MVAVar_gsfchi2,
						    MVAVar_deta,
						    MVAVar_dphi,
						    MVAVar_detacalo,
						    // MVAVar_dphicalo,
						    MVAVar_see,
						    MVAVar_spp,
						    MVAVar_etawidth,
						    MVAVar_phiwidth,
						    MVAVar_e1x5e5x5,
						    MVAVar_R9,
						    //MVAVar_nbrems,
						    MVAVar_HoE,
						    MVAVar_EoP,
						    MVAVar_IoEmIoP,
						    MVAVar_eleEoPout,
						    MVAVar_PreShowerOverRaw,
						    // MVAVar_EoPout,
						    MVAVar_d0,
						    MVAVar_ip3d,
						    MVAVar_eta,
						    MVAVar_pt,
						    debugVar);
		if(control_plots) histos1d[("eMVA_trig_frac"+id).c_str()]->Fill(0);
	}

	return mva_output;
}

void broc::MVAElectronIDCalculator::initialise()
{	
	std::string directory = "share/weights/";

	std::vector<std::string> weight_files = config_reader->get_vec_var("non_trig_weights", "e_mva_id");
	std::vector<std::string> ManualCatWeights;

	for(std::vector<std::string>::iterator weight_file = weight_files.begin();
	    weight_file != weight_files.end();
	    ++weight_file){
	    ManualCatWeights.push_back(directory+*weight_file);
	}
		
	Bool_t manualCat = true;
	
	MVANonTrig->initialize("BDT",
				 EGammaMvaEleEstimator::kNonTrig,
				 manualCat, 
				 ManualCatWeights);
	
	// NOTE: it is better if you copy the MVA weight files locally
	weight_files = config_reader->get_vec_var("trig_weights", "e_mva_id");
	
	std::vector<std::string> ManualCatWeightsTrig;
	
	for(std::vector<std::string>::iterator weight_file = weight_files.begin();
	    weight_file != weight_files.end();
	    ++weight_file){
	    ManualCatWeightsTrig.push_back(directory+*weight_file);
	}
		
	MVATrig->initialize("BDT",
			      EGammaMvaEleEstimator::kTrig,
			      manualCat,
			      ManualCatWeightsTrig);

}

void broc::MVAElectronIDCalculator::book_histos()
{
	histos1d[("eMVA_trig_frac"+id).c_str()]=histo_writer->create_1d(("eMVA_trig_frac"+id).c_str(),"amount of electrons in triggering vs non-triggering MVA",5,-0.5,4.5, "0=non-trig, 1=trig");
}
