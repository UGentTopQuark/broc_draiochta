#include "../../interface/EventSelection/PEPlots.h"

int broc::PEPlots::ninstances = 0;
std::vector<bool> broc::PEPlots::fill_PE;
std::map<std::string, eire::TH1D*> broc::PEPlots::histos1d;
TRandom3 *broc::PEPlots::rnd = 0;

broc::PEPlots::PEPlots(eire::HandleHolder *handle_holder)
{
	id = "_"+handle_holder->get_ident();
	this->histo_writer = handle_holder->get_histo_writer();
	if(!(handle_holder->get_config_reader()->get_bool_var("dont_split_ttbar","global", false))){
		std::cerr << "ERROR: broc::PEPlots::PEPlots(): in config file dont_split_ttbar = true has to be set, exiting..." << std::endl;
		exit(1);
	}

	mass_reco = NULL;

	NPE = 1000;
	sel_prob = 1./4.;

	plot_M3 = false;
	plot_Mlb = false;

	if(handle_holder->get_cuts_set()->get_cut_value("m3_PE_cs") == 1)
		plot_M3 = true;
	else if(handle_holder->get_cuts_set()->get_cut_value("mlb_PE_cs") == 1)
		plot_Mlb = true;

	if(ninstances < 1){
		book_histos();
		rnd = new TRandom3();
		rnd->SetSeed();
	};

	mass_reco=handle_holder->services()->mass_reco();

	instance_id = ninstances;

	++ninstances;
}

broc::PEPlots::~PEPlots()
{
}

void broc::PEPlots::book_histos()
{
	std::string ident = id;
	int first_pos = ident.find("|");
	int second_pos = ident.find("|", first_pos+1);
	ident.replace(second_pos+1, ident.size()-(second_pos+1), "");

	Tools t;
	for(int i = 0; i < NPE; ++i){
		std::string n = t.stringify(i);
		std::string suffix = ident+n+"_cutset";
		histos1d[("top_mass_tf"+suffix).c_str()]=histo_writer->create_1d(("top_mass_tf"+suffix).c_str(),"Invariant mass of 3 jets with highest vectorially summed Pt",560,0, 1400, "M3 [GeV]");
		histos1d[("lep_b_mass_tf"+suffix).c_str()]=histo_writer->create_1d(("lep_b_mass_tf"+suffix).c_str(),"Invariant mass of 3 jets with highest vectorially summed Pt",560,0, 1400, "M3 [GeV]");
	}
}

void broc::PEPlots::plot()
{
	plot_all();
}


void broc::PEPlots::plot_all()
{
	if(fill_PE.size() == 0 || instance_id == 0){
		select_PE();
	}
	
	std::string ident = id;
	int first_pos = ident.find("|");
	int second_pos = ident.find("|", first_pos+1);
	ident.replace(second_pos+1, ident.size()-(second_pos+1), "");

	Tools t;
	for(int i = 0; i < NPE; ++i){
		std::string n = t.stringify(i);
		std::string suffix = ident+n+"_cutset";

		if(plot_Mlb){
			if(fill_PE[i]){
				double mlb = mass_reco->calculate_lep_b_mass();
				histos1d[("lep_b_mass_tf"+suffix).c_str()]->Fill(mlb);
			}
		}else if(plot_M3){
			if(fill_PE[i]){
				double m3 = mass_reco->calculate_M3();
				histos1d[("top_mass_tf"+suffix).c_str()]->Fill(m3);
			}
		}
	}

	if(instance_id == ninstances-1){
		fill_PE.clear();
	}
}

void broc::PEPlots::select_PE()
{
	fill_PE.clear();
	for(int i= 0; i < NPE; ++i){
		if(rnd->Rndm() <= sel_prob)
			fill_PE.push_back(true);
		else
			fill_PE.push_back(false);
	}
}
