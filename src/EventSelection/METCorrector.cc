#include "../../interface/EventSelection/METCorrector.h"

METCorrector::METCorrector()
{
	MEzCal = new MyMEzCalculator();
	corrected_mets = new std::vector<mor::MET>();
	corrected = false;
	correction_method = 1;
}

METCorrector::~METCorrector()
{
	if(MEzCal != NULL){
		delete MEzCal;
		MEzCal = NULL;
	}
	if(corrected_mets != NULL){
		delete corrected_mets;
		corrected_mets=NULL;
	}
}

void METCorrector::set_handles(std::vector<mor::MET> *mets_handle, mor::Lepton *lepton, bool is_muon)
{
	corrected = false;
	corrected_mets->clear();

	this->lepton = lepton;
	this->mets_handle = mets_handle;
	this->is_muon = is_muon;

	if(lepton != NULL){
		correct_MET();
		corrected=true;
	}else{
		prepare_corrected_MET();
		corrected=false;
	}
}

bool METCorrector::is_corrected()
{
	return corrected;
}

void METCorrector::set_correction_method(int method)
{
	correction_method = method;
}

void METCorrector::prepare_corrected_MET()
{
	for(std::vector<mor::MET>::iterator met = mets_handle->begin();
		met != mets_handle->end();
		++met){
		corrected_mets->push_back(*met);
	}
}

void METCorrector::correct_MET()
{
	double p_z=0, p_z_solution1=0, p_z_solution2=0;
	for(std::vector<mor::MET>::iterator met = mets_handle->begin();
		met != mets_handle->end();
		++met){

		if(verbose){
			std::cout << "uncorrected MET: " << std::endl;
			std::cout << "uMET pt: " << met->Pt() << std::endl;
			std::cout << "uMET et: " << met->Et() << std::endl;
			std::cout << "uMET px: " << met->Px() << std::endl;
			std::cout << "uMET py: " << met->Py() << std::endl;
			std::cout << "uMET pz: " << met->Pz() << std::endl;
			std::cout << "uMET mass: " << met->M() << std::endl;
		}
		
		MEzCal->SetLepton(*lepton);
		MEzCal->SetMET(*met);
		p_z = MEzCal->Calculate(correction_method);
		p_z_solution1 = MEzCal->get_solution1();
		p_z_solution2 = MEzCal->get_solution2();

		double px = met->Px();
		double py = met->Py();
		double pz = met->Pz();
		double e = sqrt(px*px + py*py + pz*pz);
		mor::MET corrected_met;
		corrected_met.SetPxPyPzE(px,py,pz,e);
		corrected_mets->push_back(corrected_met);

		mor::MET corrected_met2(corrected_met);
		if(p_z == p_z_solution1){
			e = sqrt(px*px + py*py + p_z_solution2*p_z_solution2);
			corrected_met2.SetPxPyPzE(px,py,p_z_solution2,e);
		}
		else{
			e = sqrt(px*px + py*py + p_z_solution1*p_z_solution1);
			corrected_met2.SetPxPyPzE(px,py,p_z_solution1,e);
		}

		if(corrected_met2.Pz() != 0){
			corrected_mets->push_back(corrected_met2);
		}

		if(verbose){
			std::cout << "corrected MET: " << std::endl;
			std::cout << "cMET pt: " << corrected_met.Pt() << std::endl;
			std::cout << "cMET et: " << corrected_met.Et() << std::endl;
			std::cout << "cMET px: " << corrected_met.Px() << std::endl;
			std::cout << "cMET py: " << corrected_met.Py() << std::endl;
			std::cout << "cMET pz: " << corrected_met.Pz() << std::endl;
			std::cout << "cMET mass: " << corrected_met.M() << std::endl;
			std::cout << "corrected MET (2nd solution): " << std::endl;
			std::cout << "cMET pt: " << corrected_met2.Pt() << std::endl;
			std::cout << "cMET et: " << corrected_met2.Et() << std::endl;
			std::cout << "cMET px: " << corrected_met2.Px() << std::endl;
			std::cout << "cMET py: " << corrected_met2.Py() << std::endl;
			std::cout << "cMET pz: " << corrected_met2.Pz() << std::endl;
			std::cout << "cMET mass: " << corrected_met2.M() << std::endl;
		}
	}
}

std::vector<mor::MET>* METCorrector::get_MET()
{
	return mets_handle;
}

std::vector<mor::MET>* METCorrector::get_corrected_MET()
{
	return corrected_mets;
}
