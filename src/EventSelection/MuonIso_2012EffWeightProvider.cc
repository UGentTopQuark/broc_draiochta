#include "../../interface/EventSelection/MuonIso_2012EffWeightProvider.h"

broc::MuonIso_2012EffWeightProvider::MuonIso_2012EffWeightProvider(eire::HandleHolder *handle_holder)
{
	if(handle_holder->get_ident().find("Data") != std::string::npos)
		do_not_reweight = true;
	else
		do_not_reweight = false;

	muons = handle_holder->get_tight_muons();

	double study_unc = handle_holder->get_cuts_set()->get_cut_value("study_mu_Iso_unc");
	
	up = false;
	down = false;

	if(study_unc == 1){ down = true; }
	else if(study_unc == 2){ up = true; }
}

broc::MuonIso_2012EffWeightProvider::~MuonIso_2012EffWeightProvider()
{
}

double broc::MuonIso_2012EffWeightProvider::get_weight(int UpDown)
{
	double weight = 1.;

	if(do_not_reweight)
		weight = 1.;
	else{
		if(muons->size() >= 1){
			std::vector<mor::Muon>::iterator m = muons->begin();
			double eta = m->eta();
			double abseta = std::abs(eta);
			double pt = m->pt();
			double ud = 0.; // up-down switch for systematics
			if(up || UpDown == 1) ud = 1.;
			else if(down || UpDown == -1) ud = -1.;
			if(abseta < 0.9){
			  if(pt > 20.){ weight = 0.9959+ud*0.0002; }
			  else{ weight = 0.; }
			}else if(abseta < 1.2){
			  if(pt > 20.){ weight = 0.9878+ud*0.0003; }
			  else{ weight = 0.; }
			}else if(abseta < 2.1){
			  if(pt > 20.){ weight = 1.0027+ud*0.0002; }
			  else{ weight = 0.; }
			}else{
			  if(pt > 20.){ weight = 1.0633+ud*0.0007; }
			  else{ weight = 0.; }
			}
		}
	}

	return weight;
}
