#include "../../interface/EventSelection/IsoMuSFProvider2012.h"

broc::IsoMuSFProvider2012::IsoMuSFProvider2012(eire::HandleHolder *handle_holder)
{
	if(handle_holder->get_ident().find("Data") != std::string::npos)
		do_not_reweight = true;
	else
		do_not_reweight = false;

	this->handle_holder = handle_holder;

            double study_unc = handle_holder->get_cuts_set()->get_cut_value("study_m_trigger_unc");

            up = false;
            down = false;

            if(study_unc == 1){ down = true; }
            else if(study_unc == 2){ up = true; }

}

broc::IsoMuSFProvider2012::~IsoMuSFProvider2012()
{
}

double broc::IsoMuSFProvider2012::get_scale_factor_IsoMu24(int UpDown)
{
  double weight = 1.;
  
  if(do_not_reweight) return 1.;
  
  mor::Muon *muon = 0;
  if(handle_holder->get_tight_muons()->size() == 1){
    muon = &(*handle_holder->get_tight_muons()->begin());
  }else{
    //		if(first_warning){
    //			std::cerr << "WARNING: broc::IsoMuSFProvider2012::get_scale_factor_IsoMu24(): number of muons in event != 1: " << handle_holder->get_tight_muons()->size() << std::endl;
    //			first_warning = false;
    //		}
    return 0.;
  }
  
  
  double eta = muon->eta();
  double abseta = std::abs(eta);
  double pt = muon->pt();
  double ud = 0.; // up-down switch for systematics                                                                                                                        
  if(up || UpDown == 1) ud = 1.;
  else if(down || UpDown == -1) ud = -1.;
  if(abseta < 0.9){
    if(pt > 25.){ weight = 0.9837+ud*0.00021; }
    else{ weight = 0.; }
  }else if(abseta < 1.2){
    if(pt > 25.){ weight = 0.9656+ud*0.00066; }
    else{ weight = 0.; }
  }else if(abseta < 2.1){
    if(pt > 25.){ weight = 0.9962+ud*0.00052; }
    else{ weight = 0.; }
  }
  else{
    weight = 0.;
  }

  return weight;
  
  /*
    const int nbinsx = 6;
    const int nbinsy = 18;
    
	double bin_max_sf_x[nbinsx] = {5, 10, 15, 20, 25, 30};
	double bin_max_sf_y[nbinsy] = {28, 30, 32, 34, 36, 38, 40, 45, 50, 55, 60, 65, 70, 75, 80, 90, 100, 400};
	double sf[6][18] = {{1.04935, 0.966299, 0.985963, 0.998025, 0.984463, 0.99938, 0.979043, 0.989938, 0.979279, 0.985228, 1.01411, 0.942771, 1.03929, 1.02587, 1.03896, 1.0088, 1.09329, 1.05595}
	, {1.0064, 0.99337, 0.996815, 0.991745, 0.992042, 0.988292, 0.987956, 0.985209, 0.986359, 0.97271, 0.981344, 0.980625, 0.987032, 0.967468, 0.981479, 0.981733, 1.0228, 0.983736}
	, {1.00188, 1.0072, 1.00041, 0.997785, 0.992407, 0.989781, 0.994698, 0.986766, 0.985468, 0.981963, 0.983256, 0.995049, 0.977376, 0.990258, 0.97412, 0.981796, 0.993107, 0.981824}
	, {1.01811, 1.02483, 1.01883, 1.00834, 1.01408, 1.00712, 0.998028, 0.991917, 0.987834, 0.986844, 0.981189, 0.986959, 0.965853, 0.982115, 0.987665, 0.951721, 0.954697, 0.968945}
	, {1.05589, 1.04803, 1.0412, 1.04378, 1.02163, 1.02111, 1.01851, 1.01115, 0.995446, 0.990344, 0.997863, 0.989385, 0.968501, 0.969817, 0.992195, 0.954295, 0.944365, 0.991424}
	, {1.13483, 1.02672, 1.03372, 1.05179, 1.03511, 1.04567, 1.05184, 1.03291, 1.01388, 1.0118, 1.03951, 1.00793, 0.948633, 1.04067, 0.919248, 0.916383, 1.01838, 0.973097}
	};


	int bin_x = -1;
	int bin_y = -1;

	double mu_pt = muon->pt();
	for(int i=0; i < nbinsy; ++i){
		if(mu_pt < bin_max_sf_y[i]){
			bin_y = i;
			break;
		}
	}
	if(bin_y == -1){ bin_y = nbinsy - 1; }
	
	double npv = handle_holder->get_tight_primary_vertices()->size();
	for(int i=0; i < nbinsx; ++i){
		if(npv < bin_max_sf_x[i]){
			bin_x = i;
			break;
		}
	}
	if(bin_x == -1){ bin_x = nbinsx - 1; }

	return sf[bin_x][bin_y];
	*/
}
