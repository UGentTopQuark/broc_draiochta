#include "../../interface/EventSelection/CrossTriggerWeightProvider.h"

broc::CrossTriggerWeightProvider::CrossTriggerWeightProvider(eire::HandleHolder *handle_holder)
{
	if(handle_holder->get_ident().find("Data") != std::string::npos)
		do_not_reweight = true;
	else
		do_not_reweight = false;

	electrons = handle_holder->get_tight_electrons();
	muons = handle_holder->get_tight_muons();
	jets = handle_holder->get_tight_jets();
	vertices = handle_holder->get_tight_primary_vertices();
	event_information = handle_holder->get_event_information();
	weight_provider = new TopTriggerEfficiencyProvider();
	weight_provider->setLumi(TopTriggerEfficiencyProvider::RunB,0);
	weight_provider->setLumi(TopTriggerEfficiencyProvider::RunA,0);
	weight_provider->setLumi(TopTriggerEfficiencyProvider::RunC,0);
	weight_provider->setLumi(TopTriggerEfficiencyProvider::RunD,7.317);

	up = false;
	down = false;

}

broc::CrossTriggerWeightProvider::~CrossTriggerWeightProvider()
{
}

double broc::CrossTriggerWeightProvider::get_weight(double pu)
{
	double weight = 1.;

	if(do_not_reweight)
		weight = 1.;
	else{
		if(muons->size() >= 1){
		  std::vector<mor::Muon>::iterator m = muons->begin();
		  std::vector<double> weight_vector = weight_provider->get_weight(m->pt(), m->eta(), (*jets)[3].pt(), (*jets)[3].eta(), event_information->true_npu(), jets->size(), true, TopTriggerEfficiencyProvider::NOMINAL);
		  weight = (weight_vector)[0];
		  if(weight > 0.9){
		    //		    std::cout<<"weight = "<<weight<<" muon pt = "<<m->pt()<<" eta = "<<m->eta()<<" jet pt = "<<(*jets)[3].pt()<<" eta = "<<(*jets)[3].eta()<<" njets = "<<jets->size()<<" nvertices = "<<vertices->size()<<std::endl;
		    //std::cout<<"true pile up = "<<event_information->true_npu()*pu<<std::endl;
		  }
		}
	}

	return weight;
}
