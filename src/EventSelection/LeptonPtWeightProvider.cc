#include "../../interface/EventSelection/LeptonPtWeightProvider.h"

broc::LeptonPtWeightProvider::LeptonPtWeightProvider(std::string weight_file, eire::HandleHolder *handle_holder): broc::LeptonEfficiencyWeightProvider(weight_file, handle_holder)
{
}

broc::LeptonPtWeightProvider::~LeptonPtWeightProvider()
{
}

double broc::LeptonPtWeightProvider::get_value_for_event()
{
	double lepton_pt=-999;
	if(process_muons && muons->size() > 0){
		lepton_pt = muons->begin()->Pt();
	}else if(process_electrons && electrons->size() > 0){
		lepton_pt = electrons->begin()->Pt();
	}

	return lepton_pt;
}
