#include "../../interface/EventSelection/ElectronSFProviderThesis.h"

broc::ElectronSFProviderThesis::ElectronSFProviderThesis(eire::HandleHolder *handle_holder)
{
	if(handle_holder->get_ident().find("Data") != std::string::npos)
		do_not_reweight = true;
	else
		do_not_reweight = false;

	this->handle_holder = handle_holder;
	first_warning = true;
}

broc::ElectronSFProviderThesis::~ElectronSFProviderThesis()
{
}

double broc::ElectronSFProviderThesis::get_scale_factor()
{
	if(do_not_reweight) return 1.;

	mor::Electron *electron = 0;
	if(handle_holder->get_tight_electrons()->size() == 1){
		electron = &(*handle_holder->get_tight_electrons()->begin());
	}else{
//		if(first_warning){
//			std::cerr << "WARNING: broc::ElectronSFProviderThesis::get_scale_factor_IsoMu24(): number of muons in event != 1: " << handle_holder->get_tight_muons()->size() << std::endl;
//			first_warning = false;
//		}
		return 1.;
	}

	const int nbinsx = 12;
	const int nbinsy = 8;
	
	double bin_max_sf_x[nbinsx] = {-2, -1.566, -1.4442, -1, -0.5, 0, 0.5, 1, 1.4442, 1.566, 2, 2.5};
	double bin_max_sf_y[nbinsy] = {35, 40, 45, 50, 60, 80, 100, 1e+07};
	double sf[12][8] = {{0.883293, 0.974772, 0.997317, 1.00082, 1.01065, 1.02177, 1.01803, 1.01296}
	, {0.98205, 1.00106, 1.00217, 1.0114, 1.01607, 1.02548, 1.0128, 0.998191}
	, {0, 0, 0, 0, 0, 0, 0, 0}
	, {0.994729, 1.00813, 1.00698, 1.00863, 1.01584, 1.01463, 0.996779, 0.992617}
	, {0.98912, 1.00058, 1.00151, 1.00256, 1.00587, 1.00654, 0.996317, 0.989785}
	, {0.992415, 0.999611, 0.998781, 1.00019, 1.00525, 1.00622, 0.999773, 0.998818}
	, {0.990412, 0.995935, 0.997298, 1.00161, 1.0063, 1.00176, 0.998229, 1.01406}
	, {0.992412, 1.0032, 1.00226, 1.00509, 1.00836, 1.01291, 1.00227, 1.00846}
	, {1.00306, 1.00815, 1.0096, 1.01232, 1.01981, 1.01794, 1.0108, 1.00153}
	, {0, 0, 0, 0, 0, 0, 0, 0}
	, {0.950456, 0.98369, 0.993333, 1.00308, 1.01041, 1.01109, 1.00913, 1.00599}
	, {0.848846, 0.972177, 0.99252, 1.00765, 1.00959, 1.01951, 0.996159, 1.01034}
	};

	int bin_x = -1;
	int bin_y = -1;

	double e_pt = electron->pt();
	for(int i=0; i < nbinsy; ++i){
		if(e_pt < bin_max_sf_y[i]){
			bin_y = i;
			break;
		}
	}
	if(bin_y == -1){ bin_y = nbinsy - 1; }
	
	double sc_eta = electron->supercluster_eta();
	for(int i=0; i < nbinsx; ++i){
		if(sc_eta < bin_max_sf_x[i]){
			bin_x = i;
			break;
		}
	}
	if(bin_x == -1){ bin_x = nbinsx - 1; }

	return sf[bin_x][bin_y];
}
