#include "../../interface/EventSelection/TriggerPlots.h"

TriggerPlots::TriggerPlots(std::string ident)
{
	id = ident;

	gen_evt = NULL;
	obj_selector = NULL;
	trigger_name_provider = new eire::TriggerNameProvider();
}

TriggerPlots::~TriggerPlots()
{
	if(obj_selector){ delete obj_selector; obj_selector = NULL; }
	if(trigger_name_provider){ delete trigger_name_provider; trigger_name_provider = NULL; }
}

void TriggerPlots::set_trigger(mor::Trigger *HLTR)
{
	this->HLTR = HLTR;
}

void TriggerPlots::set_gen_evt(mor::TTbarGenEvent *gen_evt)
{
	this->gen_evt = gen_evt;
}

void TriggerPlots::set_handles(eire::HandleHolder *handle_holder)
{
	this->jets = handle_holder->get_tight_jets();
	this->isolated_electrons = handle_holder->get_tight_electrons();
	this->loose_electrons = handle_holder->get_loose_electrons();
	this->isolated_muons = handle_holder->get_tight_muons();
	this->loose_muons = handle_holder->get_loose_muons();
	this->corrected_mets = handle_holder->get_selected_mets();
	this->gen_evt = handle_holder->get_ttbar_gen_evt();
	this->trigger_objects = handle_holder->get_trigger_objects();
	this->handle_holder = handle_holder;

	obj_selector = new truicear::TriggerObjectSelector(handle_holder);

	this->histo_writer = handle_holder->get_histo_writer();
	book_histos();
}

void TriggerPlots::set_muons(std::vector<mor::Muon> *muons)
{
	this->muons = muons;
}

void TriggerPlots::plot_all()
{
	plot_trigger_objects();
	plot_triggerHLT_electrons();
	plot_triggerL1_electrons();
//	plot_trigger();
//	plot_trigger_muons();
}

void TriggerPlots::book_histos()
{
	histos1d[("trigger_objects_eta"+id).c_str()]=histo_writer->create_1d(("trigger_objects_eta"+id).c_str(),"#eta of trigger objects",100,-2.4,2.4,"trigger object #eta");
	histos1d[("trigger_objects_eta_unmatched"+id).c_str()]=histo_writer->create_1d(("trigger_objects_eta_unmatched"+id).c_str(),"#eta of trigger objects not matched to offlien muon",100,-2.4,2.4,"trigger object #eta");
	histos1d[("offline_mu_eta_matched_trigger_obj"+id).c_str()]=histo_writer->create_1d(("offline_mu_eta_matched_trigger_obj"+id).c_str(),"#eta of trigger objects",100,-2.4,2.4,"trigger object #eta");

	histos1d[("trigger_objects_mindR_HLT"+id).c_str()]=histo_writer->create_1d(("trigger_objects_mindR_HLT"+id).c_str(),"minimum dR between electron and HLT trigger object",500,0.,5,"min dR(electron, trigger object)");
histos1d[("trigger_objects_dR_HLT"+id).c_str()]=histo_writer->create_1d(("trigger_objects_dR_HLT"+id).c_str(),"dR between electron and HLT trigger object",500,0.,5,"dR(electron, trigger object)");
histos1d[("trigger_objects_secondClosestdR_HLT"+id).c_str()]=histo_writer->create_1d(("trigger_objects_secondClosestR_HLT"+id).c_str(),"dR between electron and HLT trigger object excluding closest",500,0.,5,"dR(electron, trigger object)");
	histos1d[("trigger_objects_mindRSc_HLT"+id).c_str()]=histo_writer->create_1d(("trigger_objects_mindRSc_HLT"+id).c_str(),"minimum dR between electron supercluster and HLT trigger object",100,0.,0.4,"min dR(electron, trigger object)");
	histos1d[("trigger_objects_mindR_L1"+id).c_str()]=histo_writer->create_1d(("trigger_objects_mindR_L1"+id).c_str(),"minimum dR between electron and L1 trigger object",100,0.,0.5,"min dR(electron, trigger object)");
histos1d[("trigger_objects_mindRSc_L1"+id).c_str()]=histo_writer->create_1d(("trigger_objects_mindRSc_L1"+id).c_str(),"minimum dR between electron supercluster and L1 trigger object",100,0.,0.5,"min dR(electron, trigger object)");
 

	histos2d[("trigger_objects_eta_on_off"+id).c_str()]=histo_writer->create_2d(("trigger_objects_eta_on_off"+id).c_str(),"#eta of offline muons matched to trigger objects",100,-2.4,2.4,100,-2.4,2.4,"offline muon #eta", "online muon #eta");
	histos1d[("trigger_objects_eta2124_pt"+id).c_str()]=histo_writer->create_1d(("trigger_objects_eta2124_pt"+id).c_str(),"p_{T} of offline muons matched to trigger objects in 2.1 < |eta| < 2.4",100,0.,100.,"offline muon p_{T}");
	histos2d[("ntrigger_objects_unmatched_vs_nmuons"+id).c_str()]=histo_writer->create_2d(("ntrigger_objects_unmatched_vs_nmuons"+id).c_str(),"number of trigger objects in events that could not be matched to offline muons vs nmuons in event",10,-0.5,9.5, 10,-0.5,9.5,"number of trigger objects", "number of offline muons");
	histos2d[("ntrigger_objects_vs_nmuons"+id).c_str()]=histo_writer->create_2d(("ntrigger_objects_vs_nmuons"+id).c_str(),"number of trigger objects vs nmuons in event",10,-0.5,9.5, 10,-0.5,9.5,"number of trigger objects", "number of offline muons");
	histos2d[("trigger_objects_eta2124_pt_on_off"+id).c_str()]=histo_writer->create_2d(("trigger_objects_eta2124_pt_on_off"+id).c_str(),"p_{T} of offline muons matched to trigger objects in 2.1 < |eta| < 2.4",100,0.,100.,100,0.,100.,"offline muon p_{T}", "online muon p_{T}");
	histos2d[("trigger_objects_off_eta_dpt"+id).c_str()]=histo_writer->create_2d(("trigger_objects_off_eta_dpt"+id).c_str(),"dp_{T} of muons matched to trigger objects vs eta",100,-2.4,2.4,100,-30.,30.,"#eta offline muon","#Delta p_{T}(offline muon, online muon)");
	histos2d[("trigger_objects_eta_dR"+id).c_str()]=histo_writer->create_2d(("trigger_objects_eta_dR"+id).c_str(),"dR trigger objects to closest muon vs eta",100,-2.4,2.4,100,-2.,4.,"#eta online muon","min #Delta R(offline muon, online muon)");

}

/**************************************************Trigger*********************************************************/

void TriggerPlots::print_event_id()
{
	double run = handle_holder->get_event_information()->run();
	double lumi_block = handle_holder->get_event_information()->lumi_block();
	double event = handle_holder->get_event_information()->event_number();

	std::cout 
		<< run << ","
		<< std::setprecision(10) << event << ","
		<< lumi_block
		<< std::endl;
}

void TriggerPlots::plot_trigger_objects()
{ 
	int trig_id = 0;
	if(handle_holder->get_cuts_set()->get_vcut_value("hlt_lep_triggers")->size() > 0){
		trig_id = (int) (*(handle_holder->get_cuts_set()->get_vcut_value("hlt_lep_triggers")))[0];
	}
	obj_selector->set_trigger_name(trigger_name_provider->hlt_name(trig_id), 0);
	obj_selector->set_trigger_name(trigger_name_provider->hlt_name(trig_id), 1);
	std::vector<mor::TriggerObject> *sel_objects = obj_selector->get_selected_objects(handle_holder->get_trigger_objects());
	histos2d[("ntrigger_objects_vs_nmuons"+id).c_str()]->Fill(sel_objects->size(), muons->size());
	if(sel_objects->size() > 0 && loose_muons->size() == 0){
		print_event_id();
	}
	for(std::vector<mor::TriggerObject>::iterator sel_object = sel_objects->begin();
		sel_object != sel_objects->end();
		++sel_object){
		double obj_eta = sel_object->eta();
		histos1d[("trigger_objects_eta"+id).c_str()]->Fill(obj_eta);
			bool unmatched = true;
			bool filled = false;
			double min_diff = -1;
			for(std::vector<mor::Muon>::iterator mu = loose_muons->begin();
				mu != loose_muons->end();
				++mu){
				double dR = ROOT::Math::VectorUtil::DeltaR(*mu, *sel_object);
				if(dR < min_diff || min_diff == -1){
					min_diff = dR;
				}
				double mu_eta = mu->eta();
				if(dR < 0.2 && !filled){
					unmatched = false;
					filled = true;
					if(fabs(mu_eta) > 2.1 && fabs(mu_eta) < 2.4){
						histos1d[("trigger_objects_eta2124_pt"+id).c_str()]->Fill(mu->pt());
						histos2d[("trigger_objects_eta2124_pt_on_off"+id).c_str()]->Fill(mu->pt(), sel_object->pt());

						if(mu->pt() < 10.){
							std::cout << "======" << std::endl;
							std::cout << "=== ";
							print_event_id();
							std::cout << "======" << std::endl;
							std::cout << "+++ online mu pt: " << sel_object->pt() << std::endl;
							std::cout << "+++ online mu eta: " << sel_object->eta() << std::endl;
							std::cout << "+++ online mu phi: " << sel_object->phi() << std::endl;
							std::cout << "<<< off mu triggered writeout pt: " << mu->pt() << std::endl;
							std::cout << "<<< off mu triggered writeout eta: " << mu->eta() << std::endl;
							std::cout << "<<< off mu triggered writeout phi: " << mu->phi() << std::endl;
								for(std::vector<mor::Muon>::iterator unsel_mu = muons->begin();
									unsel_mu != muons->end();
									++unsel_mu){
									if(!mu->lepton_id_passed("AllGlobalMuons") && mu->lepton_id_passed("AllStandAloneMuons")){

									std::cout << "-- offline mu STANDALONE ONLY" << std::endl;
									}
									std::cout << "-- offline mu pt: " << unsel_mu->pt() << std::endl;
									std::cout << "-- offline mu eta: " << unsel_mu->eta() << std::endl;
									std::cout << "-- offline mu phi: " << unsel_mu->phi() << std::endl;
									std::cout << "-----" << std::endl;
								}
							std::cout << "======" << std::endl;
						}
					}

					histos2d[("trigger_objects_off_eta_dpt"+id).c_str()]->Fill(mu->pt(), sel_object->pt());

					histos1d[("offline_mu_eta_matched_trigger_obj"+id).c_str()]->Fill(mu->eta());
					histos2d[("trigger_objects_eta_on_off"+id).c_str()]->Fill(mu->eta(), sel_object->eta());
					histos2d[("trigger_objects_off_eta_dpt"+id).c_str()]->Fill(mu->eta(), mu->pt()-sel_object->pt());
				}
			}
			if(unmatched){
				histos1d[("trigger_objects_eta_unmatched"+id).c_str()]->Fill(obj_eta);
				histos2d[("ntrigger_objects_unmatched_vs_nmuons"+id).c_str()]->Fill(sel_objects->size(), loose_muons->size());
			}
		histos2d[("trigger_objects_eta_dR"+id).c_str()]->Fill(obj_eta, min_diff);
	}
}

void TriggerPlots::plot_triggerHLT_electrons()
{
  int trig_id = 0;
  if(handle_holder->get_cuts_set()->get_vcut_value("hlt_lep_triggers")->size() > 0){
    trig_id = (int) (*(handle_holder->get_cuts_set()->get_vcut_value("hlt_lep_triggers")))[0];
  }
  obj_selector->set_trigger_name(trigger_name_provider->hlt_name(trig_id));
  std::vector<mor::TriggerObject> *sel_objects = obj_selector->get_selected_objects(handle_holder->get_trigger_objects());
  for(std::vector<mor::Electron>::iterator el = loose_electrons->begin(); el != loose_electrons->end(); ++el){
    double min_diff = -1;
    double min_diffSc = -1;
    ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > propagated_el(*el);
    propagated_el.SetEta(el->supercluster_eta());
    propagated_el.SetPhi(el->supercluster_phi());    

    int minIndex = -1;
    for(int i= 0; i < sel_objects->size(); i++){
      double dR = 0;

      dR = ROOT::Math::VectorUtil::DeltaR(*el, (*sel_objects)[i]);
      histos1d[("trigger_objects_dR_HLT"+id).c_str()]->Fill(dR);
      if(dR < min_diff || min_diff == -1){
	min_diff = dR;
	minIndex = i;
      }      
      double dRSc = ROOT::Math::VectorUtil::DeltaR(propagated_el, (*sel_objects)[i]);
      if(dRSc < min_diffSc || min_diffSc == -1){
	min_diffSc = dRSc;
      }      
    }
    histos1d[("trigger_objects_mindR_HLT"+id).c_str()]->Fill(min_diff);
    histos1d[("trigger_objects_mindRSc_HLT"+id).c_str()]->Fill(min_diffSc);
    
    for(int i= 0; i < sel_objects->size(); i++){
      if(i != minIndex){
	double dR = 0;
	dR = ROOT::Math::VectorUtil::DeltaR(*el, (*sel_objects)[i]);
	histos1d[("trigger_objects_secondClosestdR_HLT"+id).c_str()]->Fill(dR);
      }
    }
  }
}

void TriggerPlots::plot_triggerL1_electrons()
{
	/*
  int trig_id = 0;
  if(handle_holder->get_cuts_set()->get_vcut_value("hlt_lep_triggers")->size() > 0){
    trig_id = (int) (*(handle_holder->get_cuts_set()->get_vcut_value("hlt_lep_triggers")))[0];
  }
  obj_selector->set_trigger_name(trigger_name_provider->l1seed_name(trig_id), 1);
  std::vector<mor::TriggerObject> *sel_objects = obj_selector->get_selected_objects(handle_holder->get_trigger_objects());
  for(std::vector<mor::Electron>::iterator el = loose_electrons->begin(); el != loose_electrons->end(); ++el){
    double min_diff = -1;
    double min_diffSc =-1;
    ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > propagated_el(*el);
    propagated_el.SetEta(el->supercluster_eta());
    propagated_el.SetPhi(el->supercluster_phi());

    for(std::vector<mor::TriggerObject>::iterator sel_object = sel_objects->begin(); sel_object != sel_objects->end();++sel_object){
      double dR = 0;
      dR = ROOT::Math::VectorUtil::DeltaR(*el, *sel_object);

      if(dR < min_diff || min_diff == -1){
        min_diff = dR;
      }
      double dRSc = ROOT::Math::VectorUtil::DeltaR(propagated_el, *sel_object);
      if(dRSc < min_diffSc || min_diffSc == -1){
        min_diffSc = dRSc;
      }
    }
    histos1d[("trigger_objects_mindR_L1"+id).c_str()]->Fill(min_diff);
    histos1d[("trigger_objects_mindRSc_L1"+id).c_str()]->Fill(min_diffSc);
  }
  */
}

