#include "../../interface/EventSelection/PrescaleProvider.h"

broc::PrescaleProvider::PrescaleProvider()
{
	l1_prescales = NULL;
	hlt_prescales = NULL;
	prescale_checker = new mor::IDChecker();
}

broc::PrescaleProvider::~PrescaleProvider()
{
	if(prescale_checker){ delete prescale_checker; prescale_checker = NULL; }
}

void broc::PrescaleProvider::set_trigger_names(std::vector<std::string> *trigger_names)
{
	prescale_checker->set_id_vector(trigger_names);
}

void broc::PrescaleProvider::set_prescales(std::vector<unsigned int> *l1_prescales, std::vector<unsigned int> *hlt_prescales)
{
	this->l1_prescales = l1_prescales;
	this->hlt_prescales = hlt_prescales;
}

double broc::PrescaleProvider::prescale(std::string trigger)
{
	//std::cout << "l1 prescales: " << l1_prescales << std::endl;
	//std::cout << "l1 prescales size: " << l1_prescales->size() << std::endl;
	double l1 = prescale_checker->id_value<unsigned int>(trigger, l1_prescales);
	double hlt = prescale_checker->id_value<unsigned int>(trigger, hlt_prescales);
	//std::cout << "prescale_factor l1*hlt (" << trigger <<") = " << l1*hlt << std::endl;
	return l1*hlt;
}

double broc::PrescaleProvider::l1_prescale(std::string trigger)
{
	//std::cout << "PrescaleProvider::l1_prescale" << std::endl;
	return prescale_checker->id_value<unsigned int>(trigger, l1_prescales);
}

double broc::PrescaleProvider::hlt_prescale(std::string trigger)
{
	//std::cout << "PrescaleProvider::hlt_prescale" << std::endl;
	return prescale_checker->id_value<unsigned int>(trigger, hlt_prescales);
}
