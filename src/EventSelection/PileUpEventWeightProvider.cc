#include "../../interface/EventSelection/PileUpEventWeightProvider.h"

broc::PileUpEventWeightProvider::PileUpEventWeightProvider(std::string weight_file, eire::HandleHolder *handle_holder)
{
	if(handle_holder->get_ident().find("Data") != std::string::npos)
		do_not_reweight = true;
	else
		do_not_reweight = false;

	this->handle_holder = handle_holder;

	reweighter = 0;
	reweighter_up = 0;
	reweighter_down = 0;
	reweighter_3d = 0;

	use_3d_weights = handle_holder->get_config_reader()->get_bool_var("summer11_pu_reweighting", "event_weights", false);
	systematic_shift = handle_holder->get_cuts_set()->get_cut_value("pu_systematic_shift");

	if(!use_3d_weights){
		std::string mc_pu_file = handle_holder->get_config_reader()->get_var("mc_pu_file", "event_weights", true);
		std::string data_pu_file = "";
	
		if(systematic_shift == 1){
			data_pu_file = handle_holder->get_config_reader()->get_var("data_pu_file_sys_down", "event_weights", true);
		}else if(systematic_shift == 2){
			data_pu_file = handle_holder->get_config_reader()->get_var("data_pu_file_sys_up", "event_weights", true);
		}else{
			data_pu_file = handle_holder->get_config_reader()->get_var("data_pu_file", "event_weights", true);
		}

		reweighter = new edm::LumiReWeighting(mc_pu_file, data_pu_file, "pileup", "pileup");
		if(handle_holder->get_config_reader()->get_var("data_pu_file_sys_up", "event_weights", false) != "")
		    reweighter_up = new edm::LumiReWeighting(mc_pu_file, handle_holder->get_config_reader()->get_var("data_pu_file_sys_up", "event_weights", true), "pileup", "pileup");
		if(handle_holder->get_config_reader()->get_var("data_pu_file_sys_down", "event_weights", false) != "")
		    reweighter_down = new edm::LumiReWeighting(mc_pu_file, handle_holder->get_config_reader()->get_var("data_pu_file_sys_down", "event_weights", true), "pileup", "pileup");
	}else{
		std::string weights_file_3d = handle_holder->get_config_reader()->get_var("3d_pu_weights_file", "event_weights", true);

		if(systematic_shift == 1){
			weights_file_3d = handle_holder->get_config_reader()->get_var("3d_pu_weights_file_sys_down", "event_weights", true);

			if(weights_file_3d == ""){
				std::cerr << "ERROR: 3d_pu_weights_file_sys_down not defined and systematics study enabled, exiting..." << std::endl;
				exit(1);
			}
		}else if(systematic_shift == 2){
			weights_file_3d = handle_holder->get_config_reader()->get_var("3d_pu_weights_file_sys_up", "event_weights", true);

			if(weights_file_3d == ""){
				std::cerr << "ERROR: 3d_pu_weights_file_sys_up not defined and systematics study enabled, exiting..." << std::endl;
				exit(1);
			}
		}
		pu_info = handle_holder->get_event_information()->pu_info();
		reweighter_3d = new edm::Lumi3DReWeighting();
		reweighter_3d->weight3D_init(weights_file_3d);
	}
}

broc::PileUpEventWeightProvider::~PileUpEventWeightProvider()
{
	if(reweighter_3d){ delete reweighter_3d; reweighter_3d = NULL; }
	if(reweighter){ delete reweighter; reweighter = NULL; }
}

double broc::PileUpEventWeightProvider::get_weight()
{
	if(do_not_reweight) return 1.;

	double MyWeight3D = 0.;
	
	if(use_3d_weights){
		int nm1 = -1; int n0 = -1; int np1 = -1;
		for(std::vector<std::pair<int, double> >::iterator PVI = pu_info->begin(); PVI != pu_info->end(); ++PVI) {
		
		   int BX = PVI->first;
		
		   if(BX == -1) { 
		     nm1 = (int) PVI->second;
		   }
		   if(BX == 0) { 
		     n0 = (int) PVI->second;
		   }
		   if(BX == 1) { 
		     np1 = (int) PVI->second;
		   }
		}
	
		MyWeight3D = reweighter_3d->weight3D(nm1,n0,np1);
	}
	else{
		MyWeight3D = reweighter->weight((int) handle_holder->get_event_information()->true_npu());
	}

	return MyWeight3D;
}

double broc::PileUpEventWeightProvider::get_weight_up()
{
    if(reweighter_up == 0) 
	return 1.;
    double weight_up = 0.;
    weight_up = reweighter_up->weight((int) handle_holder->get_event_information()->true_npu());
    return weight_up;
}

double broc::PileUpEventWeightProvider::get_weight_down()
{
    if(reweighter_down == 0)
	return 1.;
    double weight_down = 0.;
    weight_down = reweighter_down->weight((int) handle_holder->get_event_information()->true_npu());
    return weight_down;
}
