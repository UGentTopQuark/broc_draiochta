#include "../../interface/EventSelection/MassJetMatch.h"

MassJetMatch::MassJetMatch()
{
	reset_values();

	gen_evt = NULL;

	mass_reco = NULL;
}


void MassJetMatch::reset_values()
{
	found_id_Wquark1 = -1;
	found_id_Wquark2 = -1;
	found_id_hadb = -1;
	found_id_lepb = -1;
}

MassJetMatch::~MassJetMatch()
{
}

void MassJetMatch::set_handles(std::vector<mor::Jet>* jets, std::vector<mor::Electron>* electrons, std::vector<mor::Muon>* muons, std::vector<mor::MET> *mets,mor::TTbarGenEvent *gen_evt,std::vector<mor::Muon> *uncut_muons,std::vector<mor::Electron> *uncut_electrons)
{
	reset_values();

	this->jets = jets;
	this->electrons = electrons;
	this->muons = muons;
	this->mets = mets;
	this->gen_evt = gen_evt;
	this->uncut_muons = uncut_muons;
	this->uncut_electrons = uncut_electrons;
	
	if(gen_evt != NULL){
		geninfo_available = true;
		get_matched_recojets_id();
	}
	else
		geninfo_available = false;
}

int MassJetMatch::nmatches_chi2()
{
	std::vector<int>* match_ids = mass_reco->get_ids_chi2();
	if(match_ids->size() > 2)
		return match_jets((*match_ids)[0], (*match_ids)[1], (*match_ids)[2]);
	else
		return -1;
}

int MassJetMatch::nmatches_chi2_1btag()
{
	std::vector<int>* match_ids = mass_reco->get_ids_chi2_1btag();
	if(match_ids->size() > 2)
		return match_jets((*match_ids)[0], (*match_ids)[1], (*match_ids)[2]);
	else
		return -1;
}

int MassJetMatch::nmatches_chi2_2btag()
{
	std::vector<int>* match_ids = mass_reco->get_ids_chi2_2btag();
	if(match_ids->size() > 2)
		return match_jets((*match_ids)[0], (*match_ids)[1], (*match_ids)[2]);
	else
		return -1;
}

int MassJetMatch::nmatches_m3()
{
	std::vector<int>* match_ids = mass_reco->get_ids_m3();
	if(match_ids->size() > 2)
		return match_jets((*match_ids)[0], (*match_ids)[1], (*match_ids)[2]);
	else
		return -1;
}

int MassJetMatch::nmatches_Wb()
{
	std::vector<int>* match_ids = mass_reco->get_ids_Wb();
	if(match_ids->size() > 2)
		return match_jets((*match_ids)[0], (*match_ids)[1], (*match_ids)[2]);
	else
		return -1;
}

int MassJetMatch::nmatches_min_diff_m3()
{
	std::vector<int>* match_ids = mass_reco->get_ids_min_diff_m3();
	if(match_ids->size() > 2)
		return match_jets((*match_ids)[0], (*match_ids)[1], (*match_ids)[2]);
	else
		return -1;
}

int MassJetMatch::nmatches_min_diff_m3_1btag()
{
	std::vector<int>* match_ids = mass_reco->get_ids_min_diff_m3_1btag();
	if(match_ids->size() > 2)
		return match_jets((*match_ids)[0], (*match_ids)[1], (*match_ids)[2]);
	else
		return -1;
}

int MassJetMatch::nmatches_min_diff_m3_2btag()
{
	std::vector<int>* match_ids = mass_reco->get_ids_min_diff_m3_2btag();
	if(match_ids->size() > 2)
		return match_jets((*match_ids)[0], (*match_ids)[1], (*match_ids)[2]);
	else
		return -1;
}

int MassJetMatch::match_jets(int jet_id1, int jet_id2, int jet_id3)
{
  bool dRj1q =false; bool dRj2q = false;bool dRj3q =false;
  
  
  if(geninfo_available){
    //Match jet.genParton with top decay products in GenParticle
    dRj1q = (is_hadW_quark(jet_id1) || is_hadb(jet_id1));
    dRj2q = (is_hadW_quark(jet_id2) || is_hadb(jet_id2));
    dRj3q = (is_hadW_quark(jet_id3) || is_hadb(jet_id3)); 	
    
  }
  else
	return -1;
  
  //check what combinations of jets passed truth matching
  bool jet1jet2jet3 =  (dRj1q && dRj2q && dRj3q);
  
  bool jet1jet2 = (dRj1q && dRj2q);
  bool jet2jet3 = (dRj2q && dRj3q);
  bool jet1jet3 = (dRj1q && dRj3q);
  
  bool jet1 =dRj1q; 
  bool jet2 =dRj2q;   
  bool jet3 =dRj3q;
  
   
  if(jet1jet2jet3){
	  return 3;
  }
    else if(jet1jet2 || jet2jet3 || jet1jet3 ){
	  return 2;
    }
    else if(jet1 || jet2 || jet3 ){
	  return 1;
    }
    else if(!(jet1 || jet2 || jet3 )){
	  return 0;
    }else 
	return -1;
}

bool MassJetMatch::is_hadW_quark(int jet_id)
{
	bool hadW_quark= (jet_id == found_id_Wquark1 || jet_id == found_id_Wquark2);
	return hadW_quark;

}

bool MassJetMatch::is_hadb(int jet_id)
{
        bool hadb = (jet_id == found_id_hadb);
	return hadb;
}

bool MassJetMatch::is_lepb(int jet_id)
{
        bool lepb = (jet_id == found_id_lepb);
	return lepb;
}

void MassJetMatch::get_matched_recojets_id()
{
	//gets std::map<string,int> where string is name (Wquark1,Wquark2,hadb,lepb) and int is id
	for(unsigned int id = 0; id < jets->size(); ++id){
		if(found_id_Wquark1 == -1 && equal_p4(gen_evt->q()->p4(), (*jets)[id].mc_p4())) found_id_Wquark1 = id;
		else if(found_id_Wquark2 == -1 && equal_p4(gen_evt->qbar()->p4(), (*jets)[id].mc_p4())) found_id_Wquark2 = id;
		else if(found_id_hadb == -1 && equal_p4(gen_evt->hadB()->p4(), (*jets)[id].mc_p4())) found_id_hadb = id;
		else if(found_id_lepb == -1 && equal_p4(gen_evt->lepB()->p4(), (*jets)[id].mc_p4())) found_id_lepb = id;
	}
}

bool MassJetMatch::equal_p4(ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > p4_1,
				ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > p4_2)
{
	if(p4_1.pt() == p4_2.pt() && p4_1.eta() == p4_2.eta())
		return true;
	else
		return false;
}

void MassJetMatch::set_mass_reco(MassReconstruction *mass_reco)
{
	this->mass_reco = mass_reco;
}
