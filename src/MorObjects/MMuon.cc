#include "../../interface/MorObjects/MMuon.h"

mor::Muon::Muon(beag::Muon &beag_lepton)
{
	set_beag_info(beag_lepton);
}

void mor::Muon::set_beag_info(beag::Muon &beag_lepton)
{
	mor::Particle::set_beag_info(beag_lepton);
	mor::Lepton::set_beag_info(beag_lepton);

	ecal_iso = beag_lepton.ecal_iso;
	hcal_iso = beag_lepton.hcal_iso;
	track_iso = beag_lepton.track_iso;

	d0_val = beag_lepton.d0;
	d0_sigma_val = beag_lepton.d0_sigma;

	d0_pv_val = beag_lepton.d0_pv;
	d0_sigma_pv_val = beag_lepton.d0_sigma_pv;

	hcal_vcone_val = beag_lepton.hcal_vcone;
	ecal_vcone_val = beag_lepton.ecal_vcone;

	lepton_id = beag_lepton.lepton_id;
	track_available_val = beag_lepton.track_available;

	trigger = beag_lepton.trigger;

	chi2_val = beag_lepton.chi2;
	nHits_val = beag_lepton.nHits;

	nMuonHits_val = beag_lepton.nMuonHits;
	nStripHits_val = beag_lepton.nStripHits;
	nPixelLayers_val = beag_lepton.nPixelLayers;
	nDTstations_val = beag_lepton.nDTstations;
	nCSCstations_val = beag_lepton.nCSCstations;
	nLostTrackerHits_val = beag_lepton.nLostTrackerHits;
	nPixelHits_val = beag_lepton.nPixelHits;
	nMatchedSegments_val = beag_lepton.nMatchedSegments;
	nMatchedStations_val = beag_lepton.nMatchedStations;
	isPFMuon_val = beag_lepton.isPFMuon;
	nTrackerLayers_val = beag_lepton.nTrackerLayers;
	trackerChi2_val = beag_lepton.trackerChi2;

	z0_val = beag_lepton.z0;
	z0_pv_val = beag_lepton.z0_pv;

	vz_val = beag_lepton.vz;

	station2_eta_val = beag_lepton.station2_eta;
	station2_phi_val = beag_lepton.station2_phi;

	photonIso_ = beag_lepton.photonIso;
	neutralHadronIso_ = beag_lepton.neutralHadronIso;
	chargedHadronIso_ = beag_lepton.chargedHadronIso;

	ndf_val = beag_lepton.ndf;
	global_chi2_val = beag_lepton.global_chi2;	// not normalised

	photonIsos_.clear();
	neutralHadronIsos_.clear();
	chargedHadronIsos_.clear();
	photonIsos_ = beag_lepton.photonIsos;
	neutralHadronIsos_ = beag_lepton.neutralHadronIsos;
	chargedHadronIsos_ = beag_lepton.chargedHadronIsos;
	puChargedHadronIso_ = beag_lepton.puChargedHadronIso;
}
