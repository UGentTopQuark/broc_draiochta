#include "../../interface/MorObjects/MMET.h"

mor::MET::MET(beag::MET &beag_met)
{
	SetEta(beag_met.eta);
	SetPhi(beag_met.phi);
	// no pz component calculated so far
	SetM(beag_met.mass);
	SetPt(beag_met.pt);
}
