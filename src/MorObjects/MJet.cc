#include "../../interface/MorObjects/MJet.h"

mor::Jet::Jet(beag::Jet &beag_jet)
{
	set_beag_info(beag_jet);

	is_bjet_ = false;
}

void mor::Jet::set_beag_info(beag::Jet &beag_jet)
{

	mor::Particle::set_beag_info(beag_jet);

	ttbar_decay_product_val = beag_jet.ttbar_decay_product;

	emf_val = beag_jet.emf;
	n90Hits_val = beag_jet.n90Hits;
	fHPD_val = beag_jet.fHPD;

	nconstituents_val = beag_jet.nconstituents;	// number of constituents
	chf_val = beag_jet.chf;		// charged hardron energy fraction
	nhf_val = beag_jet.nhf;		// neutral hardon energy fraction
	nemf_val = beag_jet.nemf;		// neutral em energy fraction
	cemf_val = beag_jet.cemf;		// charged em energy fraction
	cmulti_val = beag_jet.cmulti;		// charged multiplicity
	pt_genjet_ = beag_jet.pt_genjet;
	eta_genjet_ = beag_jet.eta_genjet;
	phi_genjet_ = beag_jet.phi_genjet;
	m_genjet_ = beag_jet.mass_genjet;

	jet_area_ = beag_jet.jet_area;

	btags.clear();
	btags = beag_jet.btags;
}

void mor::Jet::set_bDiscriminator_checker(mor::IDChecker *bchecker)
{
	this->bchecker = bchecker;
}

double mor::Jet::bDiscriminator(std::string btag_algo)
{
	return bchecker->id_value<double>(btag_algo, &btags);
}
