#include "../../interface/MorObjects/MEventInformation.h"

mor::EventInformation::EventInformation(beag::EventInformation &beag_event_information)
{
	set_beag_info(beag_event_information);
}

void mor::EventInformation::set_beag_info(beag::EventInformation &beag_event_information)
{
	lumi_block_val = beag_event_information.lumi_block;
	event_number_val = beag_event_information.event_number;
	run_val = beag_event_information.run;
	trigger_changed_val = beag_event_information.trigger_changed;
	if(beag_event_information.event_weight < 0 || beag_event_information.event_weight > 200){
	  //	  std::cout<<"MCatNLO event"<<std::endl;
	  //event_weight_val = 1.;
	  event_weight_val = beag_event_information.event_weight/fabs(beag_event_information.event_weight);
	}
	else{
	event_weight_val = beag_event_information.event_weight;
	}
	is_real_data_val = beag_event_information.is_real_data;
	W_decay_mode_ = beag_event_information.W_decay_mode;

	true_pu_ = beag_event_information.true_pu;

	rhoFastJet_ = beag_event_information.rhoFastJet;
	rhoFastJetForEIsolation_ = beag_event_information.rhoFastJetIso;

	pu_info_.clear();
	for(std::vector<std::pair<int, double> >::iterator p = beag_event_information.pu_info.begin();
		p != beag_event_information.pu_info.end();
		++p){
		pu_info_.push_back(std::pair<int, double>(p->first,p->second));
	}
}
