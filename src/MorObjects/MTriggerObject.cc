#include "../../interface/MorObjects/MTriggerObject.h"

mor::TriggerObject::TriggerObject(beag::TriggerObject &beag_trigger_object)
{
	set_beag_info(beag_trigger_object);
}

mor::TriggerObject::TriggerObject()
{
}

mor::TriggerObject::~TriggerObject()
{
}

void mor::TriggerObject::set_beag_info(beag::TriggerObject &beag_trigger_object)
{
	SetEta(beag_trigger_object.eta);
	SetPhi(beag_trigger_object.phi);
	SetM(beag_trigger_object.mass);
	SetPt(beag_trigger_object.pt);

	mc_id = (int) beag_trigger_object.mc_id;

	this->path_triggered = beag_trigger_object.triggered;

/*
	int i=0, j =0;
	for(std::vector<std::vector<bool> >::iterator trigger = path_triggered.begin();
		trigger != path_triggered.end();
		++trigger){
		std::cout << "trigger: " << i << std::endl;
		j = 0;
		for(std::vector<bool>::iterator mod = trigger->begin();
			mod != trigger->end();
			++mod){
			std::cout << "module: " << j << " triggered: " << *mod << std::endl;
			++j;
		}
		++i;
	}
*/

	quality_val = beag_trigger_object.quality;
	is_forward_val = beag_trigger_object.is_forward;
	bx_val = beag_trigger_object.bx;
	detector_val = beag_trigger_object.detector;
}

bool mor::TriggerObject::triggered(int trigger_idx, int module_idx)
{
//	std::cout << "trig idx: " << trigger_idx << " mod_idx: " << module_idx << std::endl;
//	std::cout << "mod size: " << path_triggered[trigger_idx].size() << std::endl;
//	std::cout << "mod value: " << path_triggered[trigger_idx][module_idx] << std::endl;
	return path_triggered[trigger_idx][module_idx];
}
