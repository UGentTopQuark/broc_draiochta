Broc Draiochta
==============
*A software project for use in event-selection based on privately-made BeagObjects classes*

Please read the entire README file as many small things might have changed since you last used the broc!

Preinstallation:
--------------
The broc makes use of several CMSSW packages in order to follow recipies for Pile-up reweighting, jet corrections and so on.  To check out these packages, you must use the associated git-addpkg script provided in the share/ directory.  Please note, that due to compatibility issues with the git alternate hosted on cvmfs, the cloning process will take a very long time unless the git version on the local computer is recent (>=1.8):

    cd share
    ./git-addpkg FWCore/Utilities CMSSW_5_3_X
    ./git-addpkg CondFormats/JetMETObjects CMSSW_5_3_X
    ./git-addpkg PhysicsTools/Utilities CMSSW_5_3_X
    ./git-addpkg EgammaAnalysis/ElectronTools CMSSW_5_3_X

One then needs to modify these packages in order to compile in a standalone environment:

    sed -i 's,//#define STANDALONE,#define STANDALONE,' share/EgammaAnalysis/ElectronTools/interface/EGammaMvaEleEstimator.h
    patch -d  share/EgammaAnalysis/ElectronTools/src/ < share/patches/EGammaMvaEleEstimator_CC.patch	
    patch -d  share/PhysicsTools/Utilities/interface/ < share/patches/Lumi3DReWeighting_H.patch
    patch -d  share/PhysicsTools/Utilities/src/ < share/patches/Lumi3DReWeighting_CC.patch
    patch -d  share/PhysicsTools/Utilities/interface/ < share/patches/LumiReWeighting_H.patch
    patch -d  share/PhysicsTools/Utilities/src/ < share/patches/LumiReWeighting_CC.patch

The classes that translate between CMSSW objects and the objects that are read into the broc are named 'BeagObjects'.  These are an external set of classes in order for code to be shared between the broc and the code that makes the ntuples.  The git submodule holding the classes has already been initialised in the master branch, but they will need to be pulled after an initial clone:

    git submodule update --init --recursive

In case the BeagObject classes are updated, you will need to update the submodule too:

    git submodule foreach git pull

Dictionaries must be created for the BeagObjects classes.  This may need to be done every time there is a major change to the source code (just to be safe):

    cd externals/src/BeagObjects
    ./create_dictionaries.sh

Finally, make the obj/ directory where all of the compiled objects will go (this is deliberately left to the user to ensure the directory is not accidently tracked by git)

    cd -
    mkdir obj

Compilation instructions:
--------------

*These instructions have been written with the IIHE cluster system in mind.  The broc has not been tested in other environments.*

Your .bashrc or shell config has to be accessible from your /localgrid/ user area in order for the cluster system to be able to read from it. You can do this via symbolic links:

    cd ~/
    ln -s /localgrid/your_userid/.bashrc .profile

Your shell config must have the following included in order for the broc to pick up on the correct paths at compile time:

    export CPLUS_INCLUDE_PATH=${CPLUS_INCLUDE_PATH}:/path/to/broc_draiochta/share/
    export CLHEPSYS=/nasmount/Gent/software/clhep_2_131/

in addition, 
${CLHEPSYS}/lib must be appended to your LD_LIBRARY_PATH  
${CLHEPSYS}/lib must be appended to your LIBRARY_PATH
${CLHEPSYS}/include must be appended to your CPLUS_INCLUDE_PATH  

You can then compile with

    make -j2 (or more threads if you wish)

Configuration instructions:
--------------

The broc follows many of the CMS guidlines for calculating event weights, however many of these will change corresponding to new releases of CMSSW.  Many of the event weights specified in the parent config in the share/ directory will have to be updated with every new release of a data or MC sample.  Some of the directories might have changed since the old broc repo, and importantly, the new repo should NOT have data files such as pile-up root files, JEC or electron weights commited to it.  These will therefore need to be obtained manually, or by following the instructions below:

**On-the-fly Jet Energy Corrections**
In order to apply the correct JEC, you need to know which global tag you used to process each ntuple.  A list of tags for each release can be found here:
https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuideFrontierConditions

Then you need to create text files holding the informations for JEC for a certain global tag. You can get these for example with this CMSSW config:
https://ssftrac.ugent.be/top/browser/trunk/CMSSW/CMSSW_3_8_X/analysers/FlatNTuples/get_JEC_cfg.py

In this config you have to set the correct global tag. Then a list of text files with names like GR_R_38X_V15_AK5PF_L2Relative.txt will be produced. Copy those files to the share/JEC directory.  The files must then be specified in the parent config, in the share/ directory:

- JEC_dir = /path/to/broc_draiochta/share/JEC/
- JEC = GR_R_38X_V15_AK5PF_L2Relative.txt:GR_R_38X_V15_AK5PF_L3Absolute.txt:GR_R_38X_V15_AK5PF_L2L3Residual.txt

**HitFit Resolution Functions**
Available from here: https://github.com/vadler/cmssw/tree/5_3_13-TopMassSemiLeptonic/TopQuarkAnalysis/TopHitFit/data/resolution
Copy all files *Resolution_*.txt

**Pile-up Reweighting**
To be completed. - Kelly?

**Electron MVA**
To be completed. - Kelly? Guillaume?


Noteable changes from previous broc repository:
--------------
- Moved BeagObjects classes to a separate externals/ directory.  This was needed for the way git handles submodules.
- Moved the electron configuration files from weights/ to share/weights to be in line with the rest of the config files.
