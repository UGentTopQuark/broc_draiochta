#!/usr/bin/python

from optparse import OptionParser
import fileinput, sys, ConfigParser

parser = OptionParser()
(options, args) = parser.parse_args()
header_passed = 0
inFile = args[0]
outFile = args[0].replace("_tmp","")

readFile = open(inFile)
writeFile = open(outFile,"w")

for line in readFile:
    if ((("Dataset" in line) or ("LHCO" in line) or ("File based" in line)) and header_passed > 2):
        writeFile.write(line.replace(line,""))
        
    else:
        writeFile.write(line)
        header_passed = header_passed +1
writeFile.close()
readFile.close()

                                            
