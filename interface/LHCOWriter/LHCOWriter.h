#ifndef BROC_LHCOWRITER_H
#define BROC_LHCOWRITER_H

#include "../MorObjects/MMET.h"
#include "../MorObjects/MJet.h"
#include "../MorObjects/MMuon.h"
#include "../MorObjects/MParticle.h"
#include "../MorObjects/MElectron.h"
#include "../EventSelection/HandleHolder.h"
#include "../EventSelection/Plots.h"
#include "../EventSelection/Tools.h"
#include "../TagAndProbe/TreeProducer.h"
#include "../EventSelection/LeptonSelector.h"
#include "../EventSelection/CentralServices.h"
#include "../EventSelection/EventWeightManager.h"
#include "Math/LorentzVector.h"
#include "Math/VectorUtil.h"
#include <vector>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <TString.h>
#include <fstream>
#include <string>
#include "TH2.h"
#include "TF1.h"
#include "TObjArray.h"
#include "TMap.h"
#include "TParameter.h"
#include "TClonesArray.h"


namespace broc{
	class LHCOWriter: public clibisfiosraigh::TreeProducer{
		//class LHCOWriter{
    public:
	LHCOWriter(eire::HandleHolder *handle_holder);
	~LHCOWriter();
	
	void set_handles(eire::HandleHolder *handle_holder);
	virtual void book_branches();
	int is_matched_event();
	
	bool fill_branches();
	
	void fill_leaves(int is_generated, FILE * file, double pevent_number, double prun_number, int32_t ptyp, float peta, float pphi, float ppt, float pjmass, float pntrk, int32_t pbtag, float phadOverem, double pevent_weight, int pmc_id, int ppartnum, float E, std::string event_type);
	
	void set_header(bool gen, FILE * file);
	void generator_file();
	void match_jets(std::vector<mor::Jet> *jets);
	void unambiguous_matching(std::vector<mor::Jet> *jets);	
    private:
	bool data_only;
	bool write_ntuple;

	FILE *lhcofile_tot;
	FILE *lhcofile_tot_MC;
	FILE *lhcofile_tot_hitfit;

	FILE *lhcofile_pos;
	FILE *lhcofile_pos_MC;
	FILE *lhcofile_pos_hitfit;
	
	FILE *lhcofile_neg;
	FILE *lhcofile_neg_MC;
	FILE *lhcofile_neg_hitfit;
	
	bool split_lepton_charges;

	broc::EventWeightManager *weight_manager;
	std::vector<mor::Electron> *electrons;
	std::vector<mor::Muon> *muons;
	std::vector<mor::Jet> *jets;
	std::vector<mor::Jet> *sorted_jets;
	std::vector<mor::MET> *mets;
	std::vector<mor::Particle> *hitfit_jets;
	std::vector<mor::Particle> *hitfit_electrons;
	std::vector<mor::Particle> *hitfit_muons;
	std::vector<mor::Particle> *hitfit_mets;
	std::vector<int> hitfit_combi_seed;
	double chisq;

	std::string lhconame;
	std::string id;
	std::string btag_algo;
	double btag_working_point;

	bool first;
	bool use_hitfit;
	bool use_hitfit_combi_seed;
	bool use_hitfit_kinematics;

	int32_t gen_filled;
	int32_t reco_filled;

	mor::TTbarGenEvent *gen_event;
	mor::EventInformation *event_information;   
	mor::Particle *phadB;
	mor::Particle *plepB;
	mor::Particle *pq;
	mor::Particle *pqbar;

	//TMap *likelihood;
	TLorentzVector *lepton;
	TLorentzVector *tlepton;
	int32_t lepID;
	TLorentzVector *met;
	TLorentzVector *neutrino;
	TLorentzVector *jet1;
	TLorentzVector *jet2;
	TLorentzVector *jet3;
	TLorentzVector *jet4;
	TLorentzVector *tjet1;
	TLorentzVector *tjet2;
	TLorentzVector *tjet3;
	TLorentzVector *tjet4;
	
	int32_t jet1_typ;
	Double_t jet1_jmass;
	Double_t jet1_ntrack;
	int32_t jet1_btag;
	int32_t jet1_mc_id;
	int32_t jet2_typ;
        Double_t jet2_jmass;
        Double_t jet2_ntrack;
        int32_t jet2_btag;
        int32_t jet2_mc_id;
	int32_t jet3_typ;
        Double_t jet3_jmass;
        Double_t jet3_ntrack;
        int32_t jet3_btag;
        int32_t jet3_mc_id;
	int32_t jet4_typ;
        Double_t jet4_jmass;
        Double_t jet4_ntrack;
        int32_t jet4_btag;
        int32_t jet4_mc_id;
	int32_t lepton_typ;
        Double_t lepton_jmass;
        Double_t lepton_ntrack;
        int32_t lepton_btag;
        int32_t lepton_mc_id;
	int32_t jet1_match;
        int32_t jet2_match;
        int32_t jet3_match;
        int32_t jet4_match;

	bool jets_matched;
	
	Double_t trun_number;
	Double_t tevent_number;
	Double_t tevent_weight;
	Double_t trueMass;
	Double_t trueJES;

	//double pdf_weight;
	std::vector<double> pdf_weight;
	double pu_weight;
        double pu_weight_up;
        double pu_weight_down;
	double trigger_weight_up;
	double trigger_weight_down;
	double trigger_weight;
	double lepID_weight_up;
	double lepID_weight_down;
	double lepID_weight;
	double btag_weight;
	double btag_weight_up;
	double btag_weight_down;
	double mistag_weight_up;
	double mistag_weight_down;
	double topPt_weight;
	double bfrag_weight;
	double hitfit_chisq;

    };
    
}

#endif


