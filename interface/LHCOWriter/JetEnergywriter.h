#ifndef BROC_JETENERGYWRITER_H
#define BROC_JETENERGYWRITER_H

#include "TFile.h"
#include "../MorObjects/MMET.h"
#include "../MorObjects/MJet.h"
#include "../MorObjects/MMuon.h"
#include "../MorObjects/MParticle.h"
#include "../MorObjects/MElectron.h"
#include "../EventSelection/HandleHolder.h"
#include "../EventSelection/Plots.h"
#include "../EventSelection/Tools.h"
#include "../TagAndProbe/TreeProducer.h"
#include "../EventSelection/LeptonSelector.h"
#include "../EventSelection/CentralServices.h"
#include "Math/LorentzVector.h"
#include "Math/VectorUtil.h"
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <TString.h>
#include <fstream>
#include <string>

namespace broc{
  class JetEnergywriter: public clibisfiosraigh::TreeProducer{
	public:
		JetEnergywriter(eire::HandleHolder *handle_holder);
		~JetEnergywriter();
		
		void set_handles(eire::HandleHolder *handle_holder);
		virtual void book_branches();
		virtual void book_histos();
		virtual bool fill_branches();
		void matching_jets(mor::Jet jet);
		
	private:
		bool enable_smearing;
		std::vector<mor::Jet> *jets;
		int n_jets;
		int hadBmatch; 
		int lepBmatch; 
		int qmatch;
		int qbarmatch;
		mor::TTbarGenEvent *gen_event;
		mor::EventInformation *event_information;   
		mor::Particle *phadB;
		mor::Particle *plepB;
		mor::Particle *pq;
		mor::Particle *pqbar;
		HistoWriter *histo_writer;
		std::string id;
		std::map<std::string,eire::TH1D*> histos1d;
		float Ep;
		float Ej;
		float reco_eta; 
		float reco_phi;
		float reco_pt; 
		int btag;
		float event_weight; 
		int32_t event_number;
		int32_t run_number;
	};
}

#endif
