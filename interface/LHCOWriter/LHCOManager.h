#ifndef BROC_LHCOMANAGER_H
#define BROC_LHCOMANAGER_H

#include "LHCOWriter.h"
#include "JetEnergywriter.h"
#include "TFile.h"
#include "../MorObjects/MMET.h"
#include "../MorObjects/MJet.h"
#include "../MorObjects/MMuon.h"
#include "../MorObjects/MParticle.h"
#include "../MorObjects/MElectron.h"
#include "../MorObjects/MTriggerObject.h"
#include "../MorObjects/MTrigger.h"
#include "../EventSelection/CutsSet.h"
#include "../EventSelection/HandleHolder.h"
#include "../EventSelection/EventWeightManager.h"

namespace broc{
  class LHCOManager{
  public:
      LHCOManager(eire::HandleHolder *handle_holder);
      ~LHCOManager();
      void set_handles(eire::HandleHolder *handle_holder);
      void next_event();
      
  private:
	  TFile *ntuple_file;
	  mor::TTbarGenEvent *gen_event;
	  broc::LHCOWriter *lhco_writer;
	  broc::JetEnergywriter *jetenergy_writer;
	  bool enable_jet_energy;
	  bool use_hitfit;
	  bool use_hitfit_kinematics;		
	  bool use_hitfit_combi_seed;	
	  bool write_ntuple;
  };
}

#endif
