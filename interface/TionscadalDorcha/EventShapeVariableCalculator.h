#ifndef TIONSCADALDORCHA_EVENTSHAPEVARIABLECALCULATOR_H
#define TIONSCADALDORCHA_EVENTSHAPEVARIABLECALCULATOR_H

#include "../MorObjects/MMET.h"
#include "../MorObjects/MJet.h"
#include "../MorObjects/MMuon.h"
#include "../MorObjects/MParticle.h"
#include "../MorObjects/MElectron.h"
#include "../MorObjects/MTriggerObject.h"
#include "../MorObjects/MTrigger.h"

#include "../EventSelection/HandleHolder.h"
#include "../EventSelection/EventShapeVariables.h"

#include <iostream>

namespace tionscadaldorcha{
	class EventShapeVariableCalculator{
		public:
			EventShapeVariableCalculator(eire::HandleHolder *handle_holder);
			~EventShapeVariableCalculator();
			void set_handles();
			void calculate_variables();
			void next_event(){ event_processed = false; };

			inline double get_HlT(){return HlT;};
			inline double get_H3T(){return H3T;};
			inline double get_Mevent(){return Mevent;};
			inline double get_Mj2nulT(){return Mj2nulT;};
			inline double get_sphericity(){return sphericity;};
			inline double get_aplanarity(){return aplanarity;};
		private:
			std::vector<mor::Electron> *electrons;
			std::vector<mor::Muon> *muons;
			std::vector<mor::Jet> *jets;
			std::vector<mor::MET> *mets;

			eire::HandleHolder *handle_holder;
			double HlT;
			double H3T;
			double Mevent;
			double Mj2nulT;
			double sphericity;
			double aplanarity;

			bool event_processed;
	};
}

#endif
