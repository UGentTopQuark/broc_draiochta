#ifndef TRUICEAR_MATCHING_CRITERIA_H
#define TRUICEAR_MATCHING_CRITERIA_H

namespace truicear{
	class MatchingCriteria{
		public:
			MatchingCriteria(): maxdR(-1), maxdEta(-1), maxdPt(-1), minPt(-1), minEta(-1), maxEta(-1), matching_method(1){};

			void set_maxdR(double val);
			void set_maxdEta(double val);
			void set_maxdPt(double val);
			void set_minPt(double val);
			void set_minEta(double val);
			void set_maxEta(double val);
			void set_matching_method(int val);

			double get_maxdR();
			double get_maxdEta();
			double get_maxdPt();
			double get_minPt();
			double get_minEta();
			double get_maxEta();
			int get_matching_method();
		private:
			double maxdR;
			double maxdEta;
			double maxdPt;
			double minPt;
			double minEta;
			double maxEta;
			int matching_method;
	};

	inline void MatchingCriteria::set_maxdR(double val) { this->maxdR = val; }
	inline void MatchingCriteria::set_maxdEta(double val) { this->maxdEta = val; }
	inline void MatchingCriteria::set_maxdPt(double val) { this->maxdPt = val; }
	inline void MatchingCriteria::set_minPt(double val) { this->minPt = val; }
	inline void MatchingCriteria::set_minEta(double val) { this->minEta = val; }
	inline void MatchingCriteria::set_maxEta(double val) { this->maxEta = val; }
	inline void MatchingCriteria::set_matching_method(int val) { this->matching_method = val; }

	inline double MatchingCriteria::get_maxdR() { return maxdR; }
	inline double MatchingCriteria::get_maxdEta() { return maxdEta; }
	inline double MatchingCriteria::get_maxdPt() { return maxdPt; }
	inline double MatchingCriteria::get_minPt() { return minPt; }
	inline double MatchingCriteria::get_minEta() { return minEta; }
	inline double MatchingCriteria::get_maxEta() { return maxEta; }
	inline int MatchingCriteria::get_matching_method() { return matching_method; }
}

#endif
