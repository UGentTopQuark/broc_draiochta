#ifndef MATCHEDTRIGGEROBJECTPLOTS_H
#define MATCHEDTRIGGEROBJECTPLOTS_H

#include "TriggerPlot.h"
#include "TriggerMatcher.h"
#include "../MorObjects/MTriggerObject.h"
#include "Math/VectorUtil.h"

namespace truicear{
	class MatchedTriggerObjectPlots: public TriggerPlot{
		public:
			MatchedTriggerObjectPlots();
			~MatchedTriggerObjectPlots();

			void plot(std::vector<mor::TriggerObject> *reco_muons, std::vector<mor::TriggerObject> *trigger_objects, std::vector<std::pair<int, int> >* match_results);
			void set_trigger_matcher(truicear::TriggerMatcher<mor::TriggerObject, mor::TriggerObject> *trigger_matcher);
		private:
			virtual void book_histos();

			truicear::TriggerMatcher<mor::TriggerObject, mor::TriggerObject> *trigger_matcher;
	};
}

#endif
