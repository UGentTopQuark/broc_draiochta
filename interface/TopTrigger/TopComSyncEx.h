#ifndef TOPCOMSYNCEX_H
#define TOPCOMSYNCEX_H

#include <string>
#include <iostream>
#include <vector>
#include "../MorObjects/MEventInformation.h"
#include "../MorObjects/MMuon.h"

namespace truicear{
	class TopComSyncEx{
		public:
			TopComSyncEx(std::string ident);
			~TopComSyncEx();

			void set_handles(mor::EventInformation *evt_info, std::vector<mor::Muon> *reco_muons);
			void set_matches(std::vector<std::pair<int,int> > *matches, bool l1seed);	// not l1seed means hlt
			void print();
		private:
			mor::EventInformation *evt_info;
			std::vector<mor::Muon> *reco_muons;
			/*
			 * [index reco collection]([l1seed_matched], [hlt_matched])
			 */
			std::map<int,std::pair<bool,bool> > muon_matches;

			std::string id;
	};
}

#endif
