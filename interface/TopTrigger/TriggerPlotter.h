#ifndef TRIUCEARTRIGGERPLOTTER_H
#define TRIUCEARTRIGGERPLOTTER_H

#include "../AnalysisTools/HistoWriter.h"
#include "TH1D.h"
#include "TH2D.h"

namespace truicear{
	class TriggerPlotter{
		public:
			TriggerPlotter(HistoWriter *histo_writer, std::string name, int nbinsx, double minx, double maxx);
			~TriggerPlotter();

			fill(double value, bool pass);
		private:
			HistoWriter *histo_writer;
			TH1D *pass_1d;
			TH1D *all_1d;
	};
}

#endif
