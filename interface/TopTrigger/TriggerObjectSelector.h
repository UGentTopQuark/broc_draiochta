#ifndef TRUICEAR_TRIGGEROBJECTSELECTOR_H
#define TRUICEAR_TRIGGEROBJECTSELECTOR_H

#include <vector>
#include "../MorObjects/MTriggerObject.h"
#include "../EventSelection/TriggerModuleManager.h"
#include "../EventSelection/HandleHolder.h"

namespace truicear{
	class TriggerObjectSelector{
		public:
			TriggerObjectSelector(eire::HandleHolder *handle_holder);
			~TriggerObjectSelector();

			std::vector<mor::TriggerObject>* get_selected_objects(std::vector<mor::TriggerObject> *trigger_objects);

			void set_trigger_name(std::string trigger_name, std::string module_name);
			void set_trigger_name(std::string trigger_name, int module=-1);

			inline void set_max_eta(double eta) { this->max_eta = eta; };
			inline void set_min_eta(double eta) { this->min_eta = eta; };
			inline void set_min_pt(double pt) { this->min_pt = pt; };

		private:
			std::vector<mor::TriggerObject> *selected_trigger_objects;

			broc::TriggerModuleManager *trigger_module_manager;
			eire::HandleHolder *handle_holder;

			double max_eta;
			double min_eta;
			double min_pt;

			int trigger_idx;
			int module_idx;

			int last_module_idx;

			bool module_set_by_id;

			std::string trigger_name;
			std::string module_name; 
	};
}

#endif
