#ifndef TRUICEAR_TRIGGER_LEVEL_STUDY_H
#define TRUICEAR_TRIGGER_LEVEL_STUDY_H

#include "BasicMuonPlots.h"
#include "BasicElectronPlots.h"
#include "BasicTriggerObjectPlots.h"
#include "BasicElectronPlots.h"
#include "BasicJetPlots.h"
#include "MatchedMuonPlots.h"
#include "MatchedTriggerObjectPlots.h"
#include "MatchedElectronPlots.h"
#include "MatchingCriteria.h"
#include "TriggerMatcher.h"
#include "TriggerObjectSelector.h"

#include "../EventSelection/HandleHolder.h"

#include "../MorObjects/MJet.h"
#include "../MorObjects/MMuon.h"
#include "../MorObjects/MElectron.h"
#include "../MorObjects/MEventInformation.h"
#include "../AnalysisTools/HistoWriter.h"

namespace truicear{
	template <class LeptonType, class MatchedLeptonType, class BasicLeptonPlotsType, class MatchedLeptonPlotsType>
	class TriggerLevelStudy{
		public:
		TriggerLevelStudy(std::string id, std::string name, std::string trigger_level, std::string l1seed_name);
			~TriggerLevelStudy();

			void set_handles(std::vector<LeptonType> *leptons, std::vector<MatchedLeptonType> *trigger_objects, std::vector<mor::Jet> *jets, mor::EventInformation *event_information);
			void plot();
			void set_matching_criteria(truicear::MatchingCriteria *matching_criteria);
			void set_histo_writer(HistoWriter *histo_writer);

			std::vector<MatchedLeptonType>* get_matched_trigger_objects();
			std::vector<LeptonType>* get_matched_reco_objects();
		private:
			void prepare_objects();

			double nobj_found;
			double nobj_match;
			double nlep_found;

			BasicLeptonPlotsType *basic_lep_plots;
			BasicLeptonPlotsType *unmatched_basic_lep_plots;
			MatchedLeptonPlotsType *matched_lep_plots;
			truicear::BasicJetPlots *basic_jet_plots;
			truicear::BasicTriggerObjectPlots *basic_trig_obj_plots;
			truicear::BasicTriggerObjectPlots *matched_trig_obj_plots;
			truicear::BasicTriggerObjectPlots *unmatched_trig_obj_plots;

			truicear::TriggerMatcher<LeptonType, MatchedLeptonType> *trigger_matcher;
			truicear::TriggerObjectSelector *trigger_obj_selector;

			std::vector<LeptonType> *leptons;
			std::vector<MatchedLeptonType> *trigger_objects;
			std::vector<mor::Jet> *jets;

			std::vector<std::pair<int, int> >* list_of_matches;

			HistoWriter *histo_writer;
			mor::EventInformation *event_information;
			std::string trigger_name;
			std::string l1seed_name;
			std::string trigger_level;
	};
}

#endif
