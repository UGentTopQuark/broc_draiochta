#ifndef MATCHEDMUONPLOTS_H
#define MATCHEDMUONPLOTS_H

#include "TriggerPlot.h"
#include "TriggerMatcher.h"
#include "../MorObjects/MMuon.h"
#include "../MorObjects/MTriggerObject.h"
#include "Math/VectorUtil.h"

namespace truicear{
	class MatchedMuonPlots: public TriggerPlot{
		public:
			MatchedMuonPlots();
			~MatchedMuonPlots();

			void plot(std::vector<mor::Muon> *reco_muons, std::vector<mor::TriggerObject> *trigger_objects, std::vector<std::pair<int, int> >* match_results);
			void set_trigger_matcher(truicear::TriggerMatcher<mor::Muon, mor::TriggerObject> *trigger_matcher);
		private:
			virtual void book_histos();

			truicear::TriggerMatcher<mor::Muon, mor::TriggerObject> *trigger_matcher;
	};
}

#endif
