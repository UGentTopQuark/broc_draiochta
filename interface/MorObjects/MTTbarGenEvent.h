#ifndef MOR_TTBARGENEVENT_H
#define MOR_TTBARGENEVENT_H

#include "MParticle.h"
#include "../../externals/interface/BeagObjects/TTbarGenEvent.h"
#include "MJet.h"
#include "MLepton.h"
#include "../EventSelection/JetCorrectionService.h"

namespace mor{
	class TTbarGenEvent{
		public:
			TTbarGenEvent(){};
			TTbarGenEvent(eire::JetCorrectionService *jec);
			~TTbarGenEvent();

			void set_beag_info(beag::TTbarGenEvent &beag_gen_evt, double rho=-1);
			void set_beag_info();
			mor::Particle* hadT();
			mor::Particle* hadB();
			mor::Particle* hadW();
			mor::Particle* q();
			mor::Particle* qbar();

			mor::Particle* lepT();
			mor::Particle* lepB();
			mor::Particle* lepW();
			mor::Particle* lepton();
			mor::Particle* neutrino();

			mor::Jet* hadB_jet();
			mor::Jet* lepB_jet();
			mor::Jet* q_jet();
			mor::Jet* qbar_jet();

			mor::Lepton* reco_lepton();

			bool hadB_matched();
			bool lepB_matched();
			bool q_matched();
			bool qbar_matched();
			bool lepton_matched();

			char decay_channel();

			bool is_ttbar();

		private:
			mor::Particle hadT_p, lepT_p, hadW_p, lepW_p, hadB_p, lepB_p, lep_p, neu_p, q_p, qbar_p;
			mor::Jet hadB_jet_j, lepB_jet_j, q_jet_j, qbar_jet_j;
			mor::Lepton reco_lep;
			bool hadB_matched_val, lepB_matched_val, q_matched_val, qbar_matched_val, lep_matched_val;
			char decay_channel_val;	// 0: background
						// 1: semi-lept electron
						// 2: semi-lept muon

			eire::JetCorrectionService *jec;
	};

	inline mor::Particle* mor::TTbarGenEvent::hadT() { return &hadT_p; }
	inline mor::Particle* mor::TTbarGenEvent::hadB() { return &hadB_p; }
	inline mor::Particle* mor::TTbarGenEvent::hadW() { return &hadW_p; }
	inline mor::Particle* mor::TTbarGenEvent::q() { return &q_p; }
	inline mor::Particle* mor::TTbarGenEvent::qbar() { return &qbar_p; }

	inline mor::Particle* mor::TTbarGenEvent::lepT() { return &lepT_p; }
	inline mor::Particle* mor::TTbarGenEvent::lepB() { return &lepB_p; }
	inline mor::Particle* mor::TTbarGenEvent::lepW() { return &lepW_p; }
	inline mor::Particle* mor::TTbarGenEvent::lepton() { return &lep_p; }
	inline mor::Particle* mor::TTbarGenEvent::neutrino() { return &neu_p; }

	inline mor::Jet* mor::TTbarGenEvent::hadB_jet() { return &hadB_jet_j; }
	inline mor::Jet* mor::TTbarGenEvent::lepB_jet() { return &lepB_jet_j; }
	inline mor::Jet* mor::TTbarGenEvent::q_jet() { return &q_jet_j; }
	inline mor::Jet* mor::TTbarGenEvent::qbar_jet() { return &qbar_jet_j; }

	inline mor::Lepton* mor::TTbarGenEvent::reco_lepton() { return &reco_lep; }

	inline char mor::TTbarGenEvent::decay_channel() { return decay_channel_val; }

	inline bool mor::TTbarGenEvent::hadB_matched() { return hadB_matched_val; }
	inline bool mor::TTbarGenEvent::lepB_matched() { return lepB_matched_val; }
	inline bool mor::TTbarGenEvent::q_matched() { return q_matched_val; }
	inline bool mor::TTbarGenEvent::qbar_matched() { return qbar_matched_val; }

	inline bool mor::TTbarGenEvent::lepton_matched() { return lep_matched_val; }
	inline bool mor::TTbarGenEvent::is_ttbar() { if(decay_channel_val > 0) return true; else return false; }
}

#endif
