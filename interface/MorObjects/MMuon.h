#ifndef MOR_MUON_H
#define MOR_MUON_H

#include "MLepton.h"
#include "../../externals/interface/BeagObjects/Muon.h"

namespace mor{
	class Muon : public mor::Lepton{
		public:
			Muon(beag::Muon &beag_lepton);
			Muon(){};
			virtual void set_beag_info(beag::Muon &beag_lepton);

			inline double chi2() { return chi2_val; }
			inline double global_chi2() { return global_chi2_val; }
			inline double ndf() { return ndf_val; }
			inline double nHits() { return nHits_val; }
			inline double nMuonHits() { return nMuonHits_val; }
			inline double nStripHits() { return nStripHits_val; }
			inline double nPixelLayers() { return nPixelLayers_val; }
			inline double nDTstations() { return nDTstations_val; }
			inline double nCSCstations() { return nCSCstations_val; }
			inline double nMatchedSegments() { return nMatchedSegments_val; }
			inline double nMatchedStations() { return nMatchedStations_val; }
			inline double isPFMuon() { return isPFMuon_val; }
			inline double nTrackerLayers() { return nTrackerLayers_val; }
			inline double station2_eta() { return station2_eta_val; }
			inline double station2_phi() { return station2_phi_val; }
			inline double trackerChi2() { return trackerChi2_val; }

			inline double EA_PFrelIso(){ std::cerr << "ERROR: mor::Muon PF relIso not defined" << std::endl; return -1; }
			inline void set_year(int year=2011){}
		private:
			double chi2_val;
			double ndf_val;
			double global_chi2_val;	// not normalised
			double nHits_val;
			double nMuonHits_val;
			double nStripHits_val;
			double nPixelLayers_val;
			double nDTstations_val;
			double nCSCstations_val;
			double nMatchedSegments_val;
			double nMatchedStations_val;
			double isPFMuon_val;
			double nTrackerLayers_val;

			double station2_eta_val;
			double station2_phi_val;
			double trackerChi2_val;
	};
}

#endif
