#ifndef MOR_EVENTINFORMATION_H
#define MOR_EVENTINFORMATION_H

#include "TLorentzVector.h"
#include "../../externals/interface/BeagObjects/EventInformation.h"
#include "Math/LorentzVector.h"
#include <vector>

namespace mor{
	class EventInformation{
		public:
			EventInformation(){};
			EventInformation(beag::EventInformation &beag_event_information);
			~EventInformation(){};

			void set_beag_info(beag::EventInformation &beag_event_information);

			double lumi_block();
			double event_number();
			double run();
			inline double rho(){ return rhoFastJet_; };
			inline double e_iso_rho(){ return rhoFastJetForEIsolation_; };
			inline std::vector<std::pair<int, double> > *pu_info(){ return &pu_info_; };

			bool trigger_changed();
			inline double event_weight(){ return event_weight_val; };
			bool is_real_data();
			inline int W_decay_mode(){ return W_decay_mode_; };
			inline double true_npu(){ return true_pu_; };
		private:
			double lumi_block_val;
			double event_number_val;
			double run_val;

			bool trigger_changed_val;
			double event_weight_val;
			bool is_real_data_val;
			double rhoFastJet_;
			double rhoFastJetForEIsolation_;	// rho for electron effective area iso correction
			int W_decay_mode_;

			double true_pu_;

			std::vector<std::pair<int, double> > pu_info_;
	};

	inline double EventInformation::lumi_block() { return lumi_block_val; };
	inline double EventInformation::event_number() { return event_number_val; };
	inline double EventInformation::run() { return run_val; };

	inline bool EventInformation::trigger_changed() { return trigger_changed_val; };
	inline bool EventInformation::is_real_data() { return is_real_data_val; };
}

#endif
