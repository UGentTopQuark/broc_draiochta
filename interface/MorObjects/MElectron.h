#ifndef MOR_ELECTRON_H
#define MOR_ELECTRON_H

#include "MLepton.h"
#include "../../externals/interface/BeagObjects/Electron.h"

namespace mor{
	class Electron : public mor::Lepton{
		public:
                        Electron();
			Electron(beag::Electron &beag_electron);
			void set_beag_info(beag::Electron &beag_lepton);
			void set_year(int year = 2011);
			double supercluster_eta();
			double supercluster_phi();
			inline double dist(){ return dist_val; };
			inline double dcot(){ return dcot_val; };
			inline int passconversionveto() { return passconversionveto_val; }; //jl 04.05.12
			inline double sigmaIetaIeta(){ return sigmaIetaIeta_; };
			inline double deltaPhiSuperClusterTrackAtVtx(){ return deltaPhiSuperClusterTrackAtVtx_; };
			inline double deltaEtaSuperClusterTrackAtVtx(){ return deltaEtaSuperClusterTrackAtVtx_; };
			inline double hadronicOverEm(){ return hadronicOverEm_; };

  			inline double MVAVar_fbrem(){ return MVAVar_fbrem_; };
  			inline double MVAVar_kfchi2(){ return MVAVar_kfchi2_; };
  			inline double MVAVar_kfhits(){ return MVAVar_kfhits_; }; 
  			inline double MVAVar_kfvalidhits(){ return MVAVar_kfvalidhits_; }; 
  			inline double MVAVar_gsfchi2(){ return MVAVar_gsfchi2_; };  // to be checked 

  			inline double MVAVar_deta(){ return MVAVar_deta_; };
  			inline double MVAVar_dphi(){ return MVAVar_dphi_; };
  			inline double MVAVar_detacalo(){ return MVAVar_detacalo_; };
  			inline double MVAVar_dphicalo(){ return MVAVar_dphicalo_; };   

  			inline double MVAVar_see(){ return MVAVar_see_; };    //EleSigmaIEtaIEta

  			inline double MVAVar_spp(){ return MVAVar_spp_; };
  			inline double MVAVar_etawidth(){ return MVAVar_etawidth_; };
  			inline double MVAVar_phiwidth(){ return MVAVar_phiwidth_; };
  			inline double MVAVar_e1x5e5x5(){ return MVAVar_e1x5e5x5_; };
  			inline double MVAVar_R9(){ return MVAVar_R9_; };
  			inline double MVAVar_nbrems(){ return MVAVar_nbrems_; };

  			inline double MVAVar_HoE(){ return MVAVar_HoE_; };
  			inline double MVAVar_EoP(){ return MVAVar_EoP_; };
  			inline double MVAVar_IoEmIoP(){ return MVAVar_IoEmIoP_; };  // in the future to be changed with ele.gsfTrack()->p()
  			inline double MVAVar_eleEoPout(){ return MVAVar_eleEoPout_; };
  			inline double MVAVar_EoPout(){ return MVAVar_EoPout_; };
  			inline double MVAVar_PreShowerOverRaw(){ return MVAVar_PreShowerOverRaw_; };

  			inline double MVAVar_eta(){ return MVAVar_eta_; };         
  			inline double MVAVar_pt(){ return MVAVar_pt_; };       

  			inline double MVAVar_d0(){ return MVAVar_d0_; };
			inline double MVAVar_ip3d(){ return MVAVar_ip3d_; }; 
			
			double EA_PFrelIso();
		private:
			double sc_eta_val;
			double sc_phi_val;
			double dist_val;
			double dcot_val;
			int passconversionveto_val; //jl 04.05.12
			double sigmaIetaIeta_;
			double deltaPhiSuperClusterTrackAtVtx_;
			double deltaEtaSuperClusterTrackAtVtx_;
			double hadronicOverEm_;

  			double MVAVar_fbrem_;
  			double MVAVar_kfchi2_;
  			double MVAVar_kfhits_; 
  			double MVAVar_kfvalidhits_; 
  			double MVAVar_gsfchi2_;  // to be checked 

  			double MVAVar_deta_;
  			double MVAVar_dphi_;
  			double MVAVar_detacalo_;
  			double MVAVar_dphicalo_;   

  			double MVAVar_see_;    //EleSigmaIEtaIEta

  			double MVAVar_spp_;
  			double MVAVar_etawidth_;
  			double MVAVar_phiwidth_;
  			double MVAVar_e1x5e5x5_;
  			double MVAVar_R9_;
  			double MVAVar_nbrems_;

  			double MVAVar_HoE_;
  			double MVAVar_EoP_;
  			double MVAVar_IoEmIoP_;  // in the future to be changed with ele.gsfTrack()->p()
  			double MVAVar_eleEoPout_;
  			double MVAVar_EoPout_;
  			double MVAVar_PreShowerOverRaw_;

  			double MVAVar_eta_;         
  			double MVAVar_pt_;       

  			double MVAVar_d0_;
			double MVAVar_ip3d_;
			double rhocor_year;
	};

	inline double Electron::supercluster_eta() { return sc_eta_val; };
	inline double Electron::supercluster_phi() { return sc_phi_val; };
}

#endif
