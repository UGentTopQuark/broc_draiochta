#ifndef BEAG_MUON_H
#define BEAG_MUON_H

#include "../../externals/interface/BeagObjects/Lepton.h"
#include "Rtypes.h"

namespace beag{
	class Muon : public beag::Lepton{
		public:
	                Muon():chi2(99),nHits(0),nMuonHits(0),
			nStripHits(0),nPixelLayers(0),
			nDTstations(0),nCSCstations(0){};
		        virtual ~Muon(){};

			double chi2;
			double nHits;
			double nMuonHits;
			double nStripHits;
			double nPixelLayers;
			double nDTstations;
			double nCSCstations;

		ClassDef(Muon, 1);
	};
}

#endif
