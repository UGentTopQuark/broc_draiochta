#ifndef MOR_LEPTON_H
#define MOR_LEPTON_H

#include "MParticle.h"
#include "../../externals/interface/BeagObjects/Lepton.h"
#include "../NTupleReader/MIDChecker.h"
#include <map>
#include <iostream>

namespace mor{
	class Lepton : public Particle{
		public:
			Lepton();
			virtual ~Lepton(){};
			double ecalIso();
			double hcalIso();
			double caloIso();
			double trackIso();
			double relIso();

			double hcal_vcone();
			double ecal_vcone();

			double d0();
			double d0_sigma();

			double d0_pv();
			double d0_sigma_pv();

			//int charge();

			double nLostTrackerHits();
			double nPixelHits();

			double z0();
			double z0_pv();

			double vz();

			bool lepton_id_passed(std::string ID_name);	// global muon, eID robust tight
			bool track_available();

			bool triggered(std::string trigger_name);

			virtual void set_beag_info(beag::Lepton &beag_lepton);
			void set_lepton_id_checker(mor::IDChecker *lepton_id_checker);
			void set_trigger_id_checker(mor::IDChecker *trigger_id_checker);
			void set_pfiso_conesize_checker(mor::IDChecker *pfiso_conesize_checker);
			inline void set_rho(double rho){ this->rho_ = rho; };
			inline void set_is_real_data(bool real_data){ this->real_data_ = real_data; };	// necessary for EA corrections

			inline double photonIso(){ return photonIso_; };
			inline double neutralHadronIso(){ return neutralHadronIso_; };
			inline double chargedHadronIso(){ return chargedHadronIso_; };
			inline double puChargedHadronIso(){ return puChargedHadronIso_; };

			double photonIso(std::string conesize);
			double neutralHadronIso(std::string conesize);
			double chargedHadronIso(std::string conesize);

			inline double PFrelIso(){ return (photonIso_+neutralHadronIso_+chargedHadronIso_)/this->pt(); };

			inline double dbeta_PFrelIso(){ return (chargedHadronIso_+std::max(0., photonIso_+neutralHadronIso_-0.5*puChargedHadronIso_))/this->pt(); };

                        inline std::vector<double> photonIsos(){ return photonIsos_; };
                        inline std::vector<double> neutralHadronIsos(){ return neutralHadronIsos_; };
                        inline std::vector<double> chargedHadronIsos(){ return chargedHadronIsos_; };

		protected:
			double ecal_iso;
			double hcal_iso;
			double track_iso;

			double hcal_vcone_val;
			double ecal_vcone_val;

			double d0_val;
			double d0_sigma_val;

			double d0_pv_val;
			double d0_sigma_pv_val;

			unsigned long long lepton_id;
			bool track_available_val;

			double nLostTrackerHits_val;
			double nPixelHits_val;

			double z0_val;
			double z0_pv_val;

			double vz_val;

			double photonIso_;
			double neutralHadronIso_;
			double chargedHadronIso_;
			double puChargedHadronIso_;

			unsigned long long trigger;	// Trigger bits -> lepton triggered by certain trigger
			mor::IDChecker *lepton_id_checker;
			mor::IDChecker *trigger_id_checker;
			mor::IDChecker *pfiso_conesize_checker;

			double rho_;
			bool real_data_;
			
			std::vector<double> photonIsos_;
			std::vector<double> neutralHadronIsos_;
			std::vector<double> chargedHadronIsos_;
	};

	inline double Lepton::ecalIso() { return ecal_iso; }
	inline double Lepton::caloIso() { return ecal_iso+hcal_iso; }
	inline double Lepton::hcalIso() { return hcal_iso; }
	inline double Lepton::trackIso() { return track_iso; }
	inline double Lepton::relIso() { return (track_iso+ecal_iso+hcal_iso)/this->Pt(); }

	inline double Lepton::hcal_vcone() { return hcal_vcone_val; }
	inline double Lepton::ecal_vcone() { return ecal_vcone_val; }

	inline double Lepton::d0() { return d0_val; }
	inline double Lepton::d0_sigma() { return d0_sigma_val; }

	inline double Lepton::d0_pv() { return d0_pv_val; }
	inline double Lepton::d0_sigma_pv() { return d0_sigma_pv_val; }

	//inline int Lepton::charge() { return charge_val; }

	inline double Lepton::nLostTrackerHits() { return nLostTrackerHits_val; }
	inline double Lepton::nPixelHits() { return nPixelHits_val; }

	inline double Lepton::z0() { return z0_val; }
	inline double Lepton::z0_pv() { return z0_pv_val; }

	inline double Lepton::vz() { return vz_val; }

	inline bool Lepton::track_available() { return track_available_val; }
}

#endif
