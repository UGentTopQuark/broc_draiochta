#ifndef MOR_TRIGGER_H
#define MOR_TRIGGER_H

#include <map>
#include <string>
#include <iostream>
#include "../../externals/interface/BeagObjects/Trigger.h"

#include <cstdlib>

namespace mor{
	class Trigger{
		public:
			bool trigger_available(std::string trigger);
			bool triggered(std::string trigger);
			void set_beag_info(beag::Trigger &beag_trigger);
			void set_trigger_names(std::vector<std::string> *trigger_names_vec);
		private:
			std::map<std::string,unsigned long long> trigger_names;
			std::vector<bool> triggered_val;
			//std::vector<bool> triggered_val;
			static int error_counter;

			static bool const verbose = false;
	};
}

#endif
