#ifndef MOR_JET_H
#define MOR_JET_H

#include "MParticle.h"
#include "../../externals/interface/BeagObjects/Jet.h"
#include <map>
#include "../NTupleReader/MIDChecker.h"

namespace mor{
	class Jet : public Particle{
		public:
			Jet(beag::Jet &beag_jet);
			Jet(){};
			double bDiscriminator(std::string btag_algo);
			int ttbar_decay_product();
			void set_beag_info(beag::Jet &beag_jet);
			void set_bDiscriminator_checker(mor::IDChecker *bchecker);
			inline void set_is_bjet(bool yesNo){ this->is_bjet_ = yesNo; };
			inline bool is_bjet(){ return this->is_bjet_; };

			double emf();
			double n90Hits();
			double fHPD();
			inline double nconstituents(){ return nconstituents_val; };	// number of constituents
			inline double chf(){ return chf_val; };		// charged hardron energy fraction
			inline double nhf(){ return nhf_val; };		// neutral hardon energy fraction
			inline double nemf(){ return nemf_val; };		// neutral em energy fraction
			inline double cemf(){ return cemf_val; };		// charged em energy fraction
			inline double cmulti(){ return cmulti_val; };		// charged multiplicity
			inline double pt_genjet(){ return pt_genjet_; };		// pt dR matched genjet
			inline double eta_genjet(){ return eta_genjet_; };		// pt dR matched genjet
			inline double phi_genjet(){ return phi_genjet_; };		// pt dR matched genjet
			inline double m_genjet(){ return m_genjet_; };		// pt dR matched genjet
			inline double jet_area(){ return jet_area_; };
			inline void set_p4(ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > p ){*dynamic_cast<ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> >*>(this) = p; };
		private:
			std::vector<double> btags;	// algo - bDiscriminator
			/*
			 *	ttbar_decay_product:
			 *	is jet from quark from ttbar decay
			 *	possible values:
			 *	1: quark jet
			 *	2: anti-quark jet
			 *	3: hadronically decaying top b-jet
			 *	4: leptonically decaying top b-jet
			 */
			int ttbar_decay_product_val;

			double emf_val;
			double n90Hits_val;
			double fHPD_val;

			double nconstituents_val;	// number of constituents
			double chf_val;		// charged hardron energy fraction
			double nhf_val;		// neutral hardon energy fraction
			double nemf_val;		// neutral em energy fraction
			double cemf_val;		// charged em energy fraction
			double cmulti_val;		// charged multiplicity
			double pt_genjet_;	// pt matched gen jet
			double eta_genjet_;	// eta matched gen jet
			double phi_genjet_;	// phi matched gen jet
			double m_genjet_;	// m matched gen jet

			double jet_area_;	// pt matched gen jet

			bool is_bjet_;

			mor::IDChecker *bchecker;
	};

	inline int mor::Jet::ttbar_decay_product() { return ttbar_decay_product_val; }

	inline double mor::Jet::emf() { return emf_val; }
	inline double mor::Jet::n90Hits() { return n90Hits_val; }
	inline double mor::Jet::fHPD() { return fHPD_val; }
}

#endif
