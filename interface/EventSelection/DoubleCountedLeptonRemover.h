#ifndef DOUBLECOUNTEDLEPTONREMOVER_H
#define DOUBLECOUNTEDLEPTONREMOVER_H

/** \class DoubleCountedLeptonRemover
 * 
 * \brief Allows the removal of leptons which appear in tight as well as in
 * loose lepton collections.
 *
 * If two sets of leptons are defined, loose and tight leptons, and loose is a
 * subset of tight, tight leptons will appear in both collections, the tight
 * collection as well as the loose collection. This class allows to remove
 * tight leptons from the collections of loose leptons.
 * 
 * \authors klein
 */

#include "../MorObjects/MElectron.h"
#include "../MorObjects/MMuon.h"
#include "../MorObjects/MLepton.h"

template <class myLepton>
class DoubleCountedLeptonRemover{
	public:
		DoubleCountedLeptonRemover();
		~DoubleCountedLeptonRemover();
		void set_tight_leptons(std::vector<myLepton> *leptons);
		void set_loose_leptons(std::vector<myLepton> *leptons);
		void clean_loose_leptons();
	private:
		std::vector<myLepton> *tight_leptons;
		std::vector<myLepton> *loose_leptons;
};

#endif
