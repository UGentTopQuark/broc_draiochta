#ifndef BROC_EVENTWEIGHT_H
#define BROC_EVENTWEIGHT_H

/**
 * \class broc::EventWeight
 * 
 * \brief Abstract mother class for calculation of event weights.
 *
 * If a certain event weight needs to be calculated, a new class
 * <MySpecific>EventWeight.h should be created which inherits from
 * broc::EventWeight and implements the new event weight. This new class needs
 * then to be added to broc::EventWeightManager.
 *
 * \author klein
 */

namespace broc{
	class EventWeight{
		public:
			EventWeight(){};
			virtual ~EventWeight(){};

			virtual double get_weight()=0;
		protected:
			double event_weight;
	};
}

#endif
