#ifndef BROC_PVPILEUPEVENTWEIGHTPROVIDER_H
#define BROC_PVPILEUPEVENTWEIGHTPROVIDER_H

/** \class broc::PVPileUpEventWeightProvider
 * 
 * \brief Calculate event weights for reweighting MC for pile up distribution
 * in data.
 *
 * Calculate event weights for compensating MC to data discrepancy in pile-up
 * distribution. Based on primary vertices in event.
 *
 * \author klein
 */

#include "../../src/ConfigReader/ConfigReader.h"
#include "HandleHolder.h"
#include "EventWeightProvider.h"
#include <string>

namespace broc{
	class PVPileUpEventWeightProvider : public EventWeightProvider{
		public:
			PVPileUpEventWeightProvider(std::string weight_file, eire::HandleHolder *handle_holder);
			~PVPileUpEventWeightProvider();
			double get_weight();
		protected:
			virtual double get_value_for_event();
			bool do_not_reweight;

			std::vector<mor::PrimaryVertex> *pvertices;
	};
}

#endif
