#ifndef BROC_BFRAGMENTATIONWEIGHTPROVIDER_H
#define BROC_BFRAGMENTATIONWEIGHTPROVIDER_H


#include "../../src/ConfigReader/ConfigReader.h"
#include "../MorObjects/MParticle.h"
#include "HandleHolder.h"
#include "EventWeightProvider.h"
#include <string>
#include "Math/LorentzVector.h"
#include "Math/VectorUtil.h"


namespace broc{
    class BFragmentationWeightProvider{
    public:
	BFragmentationWeightProvider(eire::HandleHolder *handle_holder);
	~BFragmentationWeightProvider();
	double get_weight();
    protected:
	bool do_not_reweight;
	mor::TTbarGenEvent *gen_event;
	eire::HandleHolder *handle_holder;
	std::vector<mor::Jet> *jets;
	TH1F* hists;
    };
}

#endif
