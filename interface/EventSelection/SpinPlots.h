#ifndef SPINPLOTS_H
#define SPINPLOTS_H

#include "Plots.h"
#include "Tools.h"
#include "Math/LorentzVector.h"
#include "TVector3.h"
#include "Math/VectorUtil.h"
#include <iostream>

class SpinPlots : public Plots{
	public:
                        SpinPlots(std::string ident, bool gen_evt_info);
		~SpinPlots();
		
	protected:
		virtual void plot_all();
		virtual void initialise();
		virtual void book_histos();
		virtual void print_info(double x1, double x2, double x3, double x4, double x5);
		/*		virtual void plot_ZMFPhi();
		virtual void plot_deltaPhi();
		virtual void plot_deltaPhiInvMassConstraint();*/
		virtual void select_and_plot();
		virtual mor::Particle* ChooseD();
		virtual ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > ToWRestFrame(mor::Particle * particle, bool Leptonic);
		virtual ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > ToTopRestFrame(mor::Particle * particle, bool Leptonic);
		virtual ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > ToTopPairRestFrame(mor::Particle * particle);
		virtual ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > ToZeroMom(ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> >, ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> >);
		virtual double plot1D(ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> >, ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> >, std::string);
		virtual void plot2D(ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> >, ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> >, ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> >, ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> >, std::string);

		bool plot_gen_evt_info;
		bool ttbar;
		int decay;
		mor::Particle* lepton;
		mor::Particle* quark;
		mor::Particle* quark_bar;
		mor::Particle* Top_had;
		mor::Particle* Top_lep;
		mor::Particle* B_had;
		mor::Particle* B_lep;
		mor::Particle* W_had;
		mor::Particle* W_lep;

		mor::EventInformation* evt_info;
		std::vector<mor::Jet> *jets;
		double nmatched;
		double total;
		double matched_merged_event;
};

#endif
