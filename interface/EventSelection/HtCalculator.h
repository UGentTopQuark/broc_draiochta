#ifndef HTCALCULATOR_H
#define HTCALCULATOR_H

#include "../MorObjects/MMET.h"
#include "../MorObjects/MElectron.h"
#include "../MorObjects/MMuon.h"
#include "../MorObjects/MJet.h"

#include "HandleHolder.h"

class HtCalculator{
	public:
		HtCalculator(eire::HandleHolder *handle_holder);
		~HtCalculator();

		double get_ht();
		double get_missing_ht();
		inline void next_event(){ ht = -1; missing_ht = -1; };

	private:
		double calculate_ht();
		double calculate_missing_ht();

		std::vector<mor::Jet>* jets;
		std::vector<mor::Electron>* isolated_electrons;
		std::vector<mor::Muon>* isolated_muons;
		std::vector<mor::MET>* corrected_mets;

		eire::HandleHolder *handle_holder;

		double ht;
		double missing_ht;
};

#endif
