#ifndef BROC_BTAGALGONAMEPROVIDER_H
#define BROC_BTAGALGONAMEPROVIDER_H

#include <map>
#include <iostream>
#include <cstdlib>

namespace broc{
	class BTagAlgoNameProvider{
		public:
			BTagAlgoNameProvider();
			~BTagAlgoNameProvider();

			std::string get_name(int algo_id);
		private:
			std::map<int, std::string> algo_names;
	};
}

#endif
