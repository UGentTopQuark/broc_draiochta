#ifndef MVAINPUTPRODUCER_H
#define MVAINPUTPRODUCER_H

/**
 * \class MVAInputProducer
 * 
 * \brief This class is outdated. Class to print text output that can be used for MVA training.
 *
 * Class to print text output that can be used for MVA training.
 * 
 * \warning This class is outdated and will be removed in a future release. Use
 * instead the tionscadaldorcha::ClassificationManager framework.
 *
 * \authors walsh, klein
 */


#include "../MorObjects/MMET.h"
#include "../MorObjects/MElectron.h"
#include "../MorObjects/MMuon.h"
#include "../MorObjects/MJet.h"
#include "MassReconstruction.h"

class MVAInputProducer{
	public:
		MVAInputProducer();
		~MVAInputProducer();
		void set_handles(std::vector<mor::Jet>* jets, std::vector<mor::Electron>* electrons, std::vector<mor::Muon>* muons, std::vector<mor::MET> *mets);
		void print_MVA_input();
		void set_mass_reco(MassReconstruction *mass_reco);
		void set_bjet_finder(BJetFinder *bjet_finder);
		void set_ht(double ht);
		void set_ident(std::string ident);

	private:
		void print_jets();
		void print_electrons();
		void print_muons();
		void print_masses();
		void print_ht();
                void print_btag();
                void print_mu_isolation();
                void print_e_isolation();

		std::string ident;

		int event_counter;
		double ht;
		MassReconstruction *mass_reco;
                BJetFinder *bjet_finder;
		std::vector<mor::Jet>* jets;
		std::vector<mor::Electron>* electrons;
		std::vector<mor::Muon>* muons;
		std::vector<mor::MET>* mets;
		const static bool verbose = true;
};
#endif
