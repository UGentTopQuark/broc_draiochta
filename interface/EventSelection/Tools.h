#ifndef TOOLS_H
#define TOOLS_H

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <sstream>

#include "Math/LorentzVector.h"
#include "Math/VectorUtil.h"

class Tools{
	typedef ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > MyLorentzVector;
	
	public:
		std::string stringify(double x);
		double deltaR(MyLorentzVector p4_1, MyLorentzVector p4_2);
};

#endif
