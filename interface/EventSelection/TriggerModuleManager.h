#ifndef BROC_TRIGGER_MODULE_MANAGER_H
#define BROC_TRIGGER_MODULE_MANAGER_H

#include "../NTupleReader/MIDChecker.h"

namespace broc{
	class TriggerModuleManager{
		public:
			TriggerModuleManager();
			~TriggerModuleManager();

			void list_modules(std::string trigger_name);
			void initialise(mor::IDChecker *trigger_name_checker, std::vector<std::vector<std::string> > *module_names);

			std::pair<int,int> get_module_ids(std::string trigger_name, std::string module);
			int get_trigger_id(std::string trigger_name);
			int get_last_module_index(std::string trigger_name);
		private:
			mor::IDChecker *trigger_name_checker;
			std::vector<std::vector<bool> > *modules_passed;
			std::vector<std::vector<std::string> > *module_names;

			std::vector<std::map<std::string, int> > module_mapping; /// trigger->(module name, id in module vector)
			static int error_counter_trig;
			static int error_counter_mod;
	};
}

#endif
