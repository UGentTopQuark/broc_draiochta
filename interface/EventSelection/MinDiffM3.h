#ifndef MINDIFFM3_H
#define MINDIFFM3_H

#include "MassReconstructionMethodBTag.h"

class MinDiffM3: public MassReconstructionMethodBTag{
	public:

	private:
		void calculate_mass();
		void calculate_mass_1btag();
		void calculate_mass_2btag();
};

#endif
