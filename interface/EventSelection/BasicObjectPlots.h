#ifndef BASICOBJECTPLOTS_H
#define BASICOBJECTPLOTS_H

#include "Plots.h"
#include "../MorObjects/MPrimaryVertex.h"

#include "Math/VectorUtil.h"
#include "HtCalculator.h"
#include "CutsSet.h"
#include "HandleHolder.h"
#include "CentralServices.h"
#include "DiLeptonReconstructor.h"

class BasicObjectPlots : public Plots{
	public:
		BasicObjectPlots(std::string ident);
		BasicObjectPlots(eire::HandleHolder *handle_holder);
		~BasicObjectPlots();

                void set_pvert(std::vector<mor::PrimaryVertex> *pvert); // is it needed?
		void set_uncut_jets(std::vector<mor::Jet>* uncut_jets);
		void set_loose_muons(std::vector<mor::Muon>* loose_muons);
		void set_ht_calc(HtCalculator *ht_calc);
		void set_uncorrected_mets(std::vector<mor::MET> *analyser_mets);
		
	private:
		virtual void plot_all();
		virtual void book_histos();
		bool is_trig_e(mor::Electron& ele);

		void plot_jets();
		void plot_W();
		void plot_muons();
		void plot_electrons();
		void plot_ht();
		void plot_hitfit();
		void plot_electron_quality();
		void plot_muon_quality();
		void plot_dR();
		void plot_DiLeptonMass();
		void plot_vertex();
		void get_triggereff();
		void analyse_recogen_matchedjets();
		void plot_gen_info();

		std::vector<mor::Jet>* uncut_jets;
		std::vector<mor::Muon>* loose_muons;
		std::vector<mor::MET>* uncorrected_mets;
		std::vector<mor::PrimaryVertex> *pvert;

		broc::CutsSet *cuts_set;

		HtCalculator *ht_calc;
		double ht;
};

#endif
