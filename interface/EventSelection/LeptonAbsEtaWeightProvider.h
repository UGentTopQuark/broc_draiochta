#ifndef BROC_LEPABSETAEVENTWEIGHTPROVIDER_H
#define BROC_LEPABSETAEVENTWEIGHTPROVIDER_H

/** \class broc::LeptonAbsEtaWeightProvider
 * 
 * \brief Calculate event weights for reweighting MC for pile up distribution
 * in data.
 *
 * Calculate event weights for compensating MC to data discrepancy in pile-up
 * distribution. Based on abseta of lepton in event.
 *
 * \author klein
 */

#include "LeptonEfficiencyWeightProvider.h"

namespace broc{
	class LeptonAbsEtaWeightProvider : public LeptonEfficiencyWeightProvider{
		public:
			LeptonAbsEtaWeightProvider(std::string weight_file, eire::HandleHolder *handle_holder);
			~LeptonAbsEtaWeightProvider();
		protected:
			virtual double get_value_for_event();

	};
}

#endif
