// interface/EventSelection/SystematicPlots.h
#ifndef SYSTEMATICPLOTS_H
#define SYSTEMATICPLOTS_H

#include "Plots.h"
#include "CentralServices.h"

class SystematicPlots : public Plots{
 public:
  SystematicPlots(std::string ident);
  ~SystematicPlots();

 private:
  void book_histos();
  virtual void plot_all();
  void plot_systematics();
};
#endif
