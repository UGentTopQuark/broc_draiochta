#ifndef CHI2JETSORTINGMASS_H
#define CHI2JETSORTINGMASS_H

/** \class Chi2JetSortingMass
 * 
 * \brief Assign the jets in the event to different positions of the ttbar
 * topology to reconstruct the top mass.
 *
 * The class performs a mass reconstruction according to the Chi2 jet sorting
 * definition: A chi2 is constructed in a way that it describes how good
 * different jet assignments to the jet positions in the ttbar event topology
 * reconstruct the hadronic and the leptonic top mass as well as the mass of
 * the hadronically decaying W. This chi2 is minimised to find the optimal
 * assignment.
 *
 * \authors walsh, klein
 *
 */

#include "MassReconstructionMethod.h"

#include "MassReconstructionMethodBTag.h"

class Chi2JetSortingMass: public MassReconstructionMethodBTag{
	public:
		Chi2JetSortingMass();
		~Chi2JetSortingMass();

		void set_tprime_mass(double mass);

		double get_min_chi2();
		double get_min_chi2_1btag();
		double get_min_chi2_2btag();

	private:
		void set_sigmas();
		void reset_values();

		void calculate_mass();
		void calculate_mass_1btag();
		void calculate_mass_2btag();

                //map assigning sigmas for mass Hadt,Lept and HadW.used for chi?? sorting method
		std::map<double,std::map<std::string,double> > mass_sigma;

		double min_chi2;
		double min_chi2_1btag;
		double min_chi2_2btag;

		double tprime_mass;	// tprime mass for mass hypothesis
};

#endif
