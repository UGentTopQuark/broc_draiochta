#ifndef BROC_MUONTRACKING_2012EFFWEIGHTPROVIDER_H
#define BROC_MUONTRACKING_2012EFFWEIGHTPROVIDER_H

/** \class broc::LeptonEfficiencyWeightProvider
 * 
 * \brief Calculate event weights for reweighting MC for pile up distribution
 * in data.
 *
 * Calculate event weights for compensating MC to data discrepancy in pile-up
 * distribution. Based on efficiency studies of lepton in event.
 *
 * \author beernaert
 */

#include "../../src/ConfigReader/ConfigReader.h"
#include "HandleHolder.h"
#include "EventWeightProvider.h"
#include "../MorObjects/MElectron.h"
#include <string>

namespace broc{
	class MuonTracking_2012EffWeightProvider{
		public:
			MuonTracking_2012EffWeightProvider(eire::HandleHolder *handle_holder);
			~MuonTracking_2012EffWeightProvider();
			double get_weight(int UpDown = 0);
		protected:
			bool do_not_reweight;
			bool up, down; // systematics switch

			std::vector<mor::Muon> *muons;
	};
}

#endif
