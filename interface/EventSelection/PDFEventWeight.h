#ifndef BROC_PDFEVENTWEIGHT_H
#define BROC_PDFEVENTWEIGHT_H

#include "../AnalysisTools/PDFWeightProvider.h"
#include "EventWeight.h"
#include "HandleHolder.h"
#include "CutsSet.h"

namespace broc{
	class PDFEventWeight: public broc::EventWeight{
		public:
			PDFEventWeight(broc::CutsSet *cuts_set, eire::HandleHolder *handle_holder);
			virtual ~PDFEventWeight();

			virtual double get_weight();
			std::vector<double> get_vec_weight();
		private:
			eire::PDFWeightProvider *pdf_weight_prov;
			int pdf_weight_pos;
			int npdfs;
			std::string pdf_name;
			std::vector<double> pdf_weight_vec;

			bool do_not_reweight;
			bool apply_pdf_weights;
	};
}

#endif
