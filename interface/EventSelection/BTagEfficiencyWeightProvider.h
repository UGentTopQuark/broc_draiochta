#ifndef BROC_BTAGEFFICIENCYWEIGHTPROVIDER_H
#define BROC_BTAGEFFICIENCYWEIGHTPROVIDER_H

/** \class broc::LeptonEfficiencyWeightProvider
 * 
 * \brief Calculate event weights for reweighting MC for pile up distribution
 * in data.
 *
 * Calculate event weights for compensating MC to data discrepancy in pile-up
 * distribution. Based on efficiency studies of lepton in event.
 *
 * \author beernaert
 */

#include "../../src/ConfigReader/ConfigReader.h"
#include "HandleHolder.h"
#include "EventWeightProvider.h"
#include "../MorObjects/MJet.h"
#include "../MorObjects/MTTbarGenEvent.h"
#include <string>

namespace broc{
	class BTagEfficiencyWeightProvider{
		public:
			BTagEfficiencyWeightProvider(eire::HandleHolder *handle_holder);
			~BTagEfficiencyWeightProvider();
			double get_weight(int UpDown_b = 0, int UpDown_light = 0);
		protected:
			bool is_true_b(mor::Jet jet);
			double get_BTag_eff(double pt, double eta, int UpDown);
			double get_BTag_eff_light(double pt, double eta, int UpDown);
			double get_BTag_eff_c(double pt, double eta, int UpDown);
			double get_Data_Prob(int UpDown_b, int UpDown_light);
			double get_MC_Prob(int UpDown_b, int UpDown_light);
			double get_SF(double pt, int UpDown);
			double get_SF_light(double pt, double eta, int UpDown);
			double get_sync_factor(double bDiscrim);
			bool do_not_reweight;
			bool up, down; // systematics switch
			std::string working_point;
			std::string sample;
			bool apply_sync_factor;

			std::vector<mor::Jet> *jets;
			mor::TTbarGenEvent *gen_evt;
	};
}

#endif
