#ifndef TRIGGERPLOTS_H
#define TRIGGERPLOTS_H

#include "Plots.h"
#include "Tools.h"
#include "../MorObjects/MTrigger.h"
#include "Math/LorentzVector.h"
#include "Math/VectorUtil.h"
#include "../TopTrigger/TriggerObjectSelector.h"
#include "../MorObjects/MTriggerObject.h"
#include "TriggerNameProvider.h"


class TriggerPlots : public Plots{
	public:
		TriggerPlots(std::string ident);
		~TriggerPlots();
		void set_gen_evt(mor::TTbarGenEvent *gen_evt);
		void set_trigger(mor::Trigger *HLTR);
		void set_muons(std::vector<mor::Muon> *muons);
		virtual void set_handles(eire::HandleHolder *handle_holder);

	private:
		virtual void plot_all();
		void book_histos();
		void plot_trigger();
		void plot_trigger_objects();
		void print_event_id();
		void plot_triggerHLT_electrons();
		void plot_triggerL1_electrons();

		mor::Trigger *HLTR;
		std::vector<mor::Muon> *muons;
		std::vector<mor::Electron> *electrons;
		mor::TTbarGenEvent *gen_evt;
		truicear::TriggerObjectSelector *obj_selector;
		std::vector<mor::TriggerObject> *trigger_objects;
		eire::TriggerNameProvider *trigger_name_provider;
};

#endif
