#ifndef BJETFINDER_H
#define BJETFINDER_H

/** 
 * \class BJetFinder
 * 
 * \brief Finds b-jets in event.
 *
 * Finds jets passing minimum btag value if set and fills to vector of pairs
 * <int, double> where int is the position of the jet in the collection and
 * double is the b-tag value for the input algorithm.
 *
 * \author walsh
 */

#include "../MorObjects/MJet.h"
#include "../EventSelection/HandleHolder.h"

#include <vector>
#include <string>
#include <algorithm>

class BJetFinder{
	public:
		BJetFinder();
		~BJetFinder();
		
		std::vector<std::pair<int, double> >* get_btag_sorted_jets(std::string btag_algo="trackCountingHighEffBJetTags");

		/// Sets jets handle.
		void set_handles(eire::HandleHolder *handle_holder);
		/**
		 * \brief Set btag value jets must pass to be b-tagged.
		 * 
		 * Vector allows for asymmetric b-tag values. 
		 *
		 * \note If there are more jets than elements in the vector
		 * additional jets must pass the last element in the vector.
		 *
		 * \param min_btag_value Vector of asymmetric b-tag cuts.
		 */
		void set_min_btag_value(std::vector<double> min_btag_value);

		/**
		 * \brief Sets the maximum number of jets you want to consider for b-tagging.
		 *
		 * Example: njets 4 means only 4 highest pt jets will be considered.
		 */
		void set_max_considered_jets(int njets);

	private:
		bool static compare_btag(const std::pair<int,double> p1, const std::pair<int,double> p2);
		void sort_btagged_jets(std::string btag_algo="trackCountingHighEffBJetTags");
		void cut_sorted_jets();

		std::vector<std::pair<int, double> > *btag_values;
		std::vector<mor::Jet>* jets;

		bool sorted; // btagged jets already sorted for this event?
		std::map<std::string,std::vector<double> > old_min_btag_value;
		std::vector<double> min_btag;
		int max_njets;
		//1st and 2nd highest ids per algorithm
		std::map<std::string,std::vector<std::pair<int, double> >* > btag_algo_ids;
};

#endif
