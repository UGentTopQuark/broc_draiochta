#ifndef BROC_PRESCALEPROVIDER_H
#define BROC_PRESCALEPROVIDER_H

#include <vector>
#include <string>
#include "../NTupleReader/MIDChecker.h"

namespace broc{
	class PrescaleProvider{
		public:
			PrescaleProvider();
			~PrescaleProvider();
			void set_trigger_names(std::vector<std::string> *trigger_names);
			void set_prescales(std::vector<unsigned int> *l1_prescales, std::vector<unsigned int> *hlt_prescales);
			double prescale(std::string);
			double l1_prescale(std::string);
			double hlt_prescale(std::string);
		private:
			mor::IDChecker *prescale_checker;

			std::vector<unsigned int> *l1_prescales;
			std::vector<unsigned int> *hlt_prescales;
	};
}

#endif
