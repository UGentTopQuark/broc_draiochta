#ifndef CUTSSET_H
#define CUTSSET_H

/** \class broc::CutsSet
 * 
 * \brief Holds list of cuts for one selection step.
 *
 * A list of cuts is maintained which represents the cuts applied in a certain
 * selection step of the analysis. The values of cuts are either double values
 * or vectors to double values.
 *
 * \author klein
 */

#include <map>
#include <string>
#include <vector>
#include <iostream>

namespace broc{
	class CutsSet{
		public:
			void set_cuts(std::map<std::string, std::vector<double>* > &vcuts_set);
			void set_cuts(std::map<std::string, double> &cuts_set);
			void set_cut(std::string name, double cut);
			void set_cut(std::string name, std::vector<double> *cut);
			void set_vcuts();
			double get_cut_value(std::string value);
			std::vector<double>* get_vcut_value(std::string value);
		private:
			std::map<std::string,std::vector<double>*> vcuts_set;
			std::map<std::string,double> cuts_set;
	};
}

#endif
