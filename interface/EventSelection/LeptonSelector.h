#ifndef LEPTONSELECTOR_H
#define LEPTONSELECTOR_H

#include "../MorObjects/MElectron.h"
#include "../MorObjects/MMuon.h"
#include "../MorObjects/MLepton.h"
#include "../MorObjects/MJet.h"
#include "../MorObjects/MPrimaryVertex.h"
#include "../MorObjects/MMET.h" //jl 04.11.10
#include "../MorObjects/MEventInformation.h"
#include "../EventSelection/TriggerNameProvider.h"
#include "CutsSet.h"
#include "Tools.h"
#include "ConversionIdentifier.h"
#include "CentralServices.h"
#include "HandleHolder.h"

#include "Math/LorentzVector.h"
#include "Math/VectorUtil.h"

template <class myLepton>
class LeptonSelector{
	public:
		LeptonSelector(eire::HandleHolder *handle_holder);
		~LeptonSelector();
		void set_cuts_set(broc::CutsSet *cuts_set, std::string lep, std::string type);
		void set_primary_vertices(std::vector<mor::PrimaryVertex> *primary_vertices);

		/*
		 *	electron IDs:
		 *	0: "eidRobustLoose";
		 *	1: "eidRobustTight";
		 *	2: "eidLoose";
		 *	3: "eidTight";
		 */
		/*
		 *	muons:
		 *	0: isGlobalMuon()
		 */
		std::vector<myLepton>* get_leptons(std::vector<myLepton> *leptons, std::vector<mor::Jet> *myjets, std::vector<mor::MET> *mymet); //jl 04.11.10
		
	private:
                std::vector<double> *max_trackiso;
                std::vector<double> *max_caliso;
                std::vector<double> *max_ecaliso;
                std::vector<double> *max_hcaliso;
                std::vector<double> *max_hcal_veto_cone;
                std::vector<double> *max_ecal_veto_cone;
		std::vector<double> *min_dR;
		std::vector<double> *max_dR;
		std::vector<double> *min_pt;
		std::vector<double> *max_pt;
		std::vector<double> *min_et;
		std::vector<double> *max_et;
		//jl 04.11.10: dphi lep/met
		std::vector<double> *min_dphi;
		std::vector<double> *max_dphi;
		std::vector<double> *min_relIso;
		std::vector<double> *max_relIso;
		std::vector<double> *max_PFrelIso;
		std::vector<double> *min_PFrelIso;
		std::vector<double> *electronID;
		std::vector<double> *min_nHits;
		std::vector<double> *max_d0;
		std::vector<double> *min_d0;
		std::vector<double> *max_vz;
		std::vector<double> *max_d0sig;
		std::vector<double> *max_chi2;
		std::vector<double> *min_chi2;
		std::vector<double> *min_eta;
		std::vector<double> *max_eta;
		std::vector<double> *min_nMuonHits;
		std::vector<double> *max_nMuonHits;
		std::vector<double> *min_nStripHits;
		std::vector<double> *min_nPixelHits;
		std::vector<double> *max_nPixelHits;
		std::vector<double> *min_nPixelLayers;
		std::vector<double> *min_nStations;
		std::vector<double> *min_nMatchedSegments;
		std::vector<double> *min_nMatchedStations;
		std::vector<double> *min_nTrackerLayers;
		std::vector<double> *max_nTrackerLayers;
		std::vector<double> *max_nLostTrackerHits;
		double exclude_eta_crack;
		std::vector<myLepton> *isolated_leptons;
		int lepton_type;
		int lepton_isPFMuon;
		int lepton_charge;
		double lep_match_trigger;
		double lepton_scale;

		double max_dcot;
		double max_dist;
		double iso_cone;
		double max_mva_id;
		double min_mva_id;

		double max_dbeta_PFrelIso;
		double min_dbeta_PFrelIso;

		double max_EA_PFrelIso;
		double min_EA_PFrelIso;

		int EA_year;

		double conv_veto; //jl 04.05.12
	
		std::string conesize;

		eire::HandleHolder *handle_holder;

                bool cut_trackiso(myLepton *lepton_iter, double &max_trackiso);
                bool cut_caliso(myLepton *lepton_iter, double &max_caliso);
                bool cut_ecaliso(myLepton *lepton_iter, double &max_ecaliso);
                bool cut_hcaliso(myLepton *lepton_iter, double &max_hcaliso);
                bool cut_hcal_veto_cone(myLepton *lepton_iter, double &max_hcal_veto_cone);
                bool cut_ecal_veto_cone(myLepton *lepton_iter, double &max_ecal_veto_cone);
                bool cut_dR(myLepton *lepton_iter, double &min_dR);
                bool cut_max_dR(myLepton *lepton_iter, double &max_dR);
		bool cut_pt(myLepton *lepton_iter, double &min_pt);
		bool cut_max_pt(myLepton *lepton_iter, double &max_pt);
		bool cut_et(myLepton *lepton_iter, double &min_et);
		bool cut_max_et(myLepton *lepton_iter, double &max_et);
		bool cut_eta(myLepton *lepton_iter, double &limit_eta,bool max_cut = true);
		//jl 04.11.10: dphi lep/met
		bool cut_min_dphi_lepmet(myLepton *lepton_iter, double &min_dphi, std::vector<mor::MET> *mymet);
		bool cut_max_dphi_lepmet(myLepton *lepton_iter, double &max_dphi, std::vector<mor::MET> *mymet);
		bool cut_inverse_relIso(myLepton *lepton_iter, double &min_relIso);
		bool cut_relIso(myLepton *lepton_iter, double &max_relIso);
		bool cut_PFrelIso(myLepton *lepton_iter, double &max_relIso);
		bool cut_min_PFrelIso(myLepton *lepton_iter, double &min_relIso);
		bool cut_electronID(myLepton *lepton_iter, double &electron_id);
		bool cut_lepton_type(myLepton *lepton_iter, int &lepton_type);
		bool cut_lepton_isPFMuon(myLepton *lepton_iter, int &lepton_isPFMuon);
		bool cut_chi2(myLepton *lepton_iter, double &max_chi2,bool max_cut = true);
		bool cut_d0(myLepton *lepton_iter, double &max_d0);
		bool cut_min_d0(myLepton *lepton_iter, double &min_d0);
		bool cut_vz(myLepton *lepton_iter, double &max_vz);
		bool cut_lepton_charge(myLepton *lepton_iter, int &charge);
		bool cut_d0sig(myLepton *lepton_iter, double &max_d0sig);
		bool cut_nHits(myLepton *lepton_iter, double &min_nHits);
		bool cut_trigger(myLepton *lepton_iter);
		bool cut_nMuonHits(myLepton *lepton_iter, double &min_nMuonHits,bool max_cut = false);
		bool cut_nStripHits(myLepton *lepton_iter, double &min_nStripHits);
		bool cut_nPixelHits(myLepton *lepton_iter, double &min_nPixelHits,bool max_cut = false);
		bool cut_nPixelLayers(myLepton *lepton_iter, double &min_nPixelLayers);
		bool cut_nStations(myLepton *lepton_iter, double &min_nStations);
		bool cut_nMatchedSegments(myLepton *lepton_iter, double &min_nMatchedSegments);
		bool cut_nMatchedStations(myLepton *lepton_iter, double &min_nMatchedStations);
		bool cut_nTrackerLayers(myLepton *lepton_iter, double &min_nTrackerLayers,bool max_cut = false);
		bool cut_nLostTrackerHits(myLepton *lepton_iter, double &max_nLostTrackerHits);
		bool cut_conv_rej(myLepton *lepton_iter);
                bool cut_conv_rej_2012(myLepton *lepton_iter); //jl 04.05.12
		bool cut_mva_id(myLepton *lepton_iter, double &mva_id, bool is_max_cut);
		bool cut_dbeta_PFrelIso(myLepton *lepton_iter, double &cut, bool is_max_cut);
		bool cut_EA_PFrelIso(myLepton *lepton_iter, double &cut, bool is_max_cut);
		//jl 22.04.11: triangle cut
		bool cut_triangle(myLepton *lepton_iter, std::vector<mor::MET> *mymet);
		void change_lepton_scale(std::vector<myLepton*> &leptons);

		std::vector<mor::PrimaryVertex> *primary_vertices;

		double deltaR(ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > p4_1,
			      ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > p4_2);

		eire::ConversionIdentifier *conversion_identifier;
		broc::MVAElectronIDCalculator *mva_id_calc;

		std::vector<mor::Jet> *jets;
		std::vector<mor::MET> *met; //jl 04.11.10
		static bool const verbose = false;
};

#endif
