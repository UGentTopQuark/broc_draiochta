#ifndef EVENTIDENTIFIER_H
#define EVENTIDENTIFIER_H

/** \class EventIdentifier
 * 
 * \brief Print information about certain events.
 *
 * Out of all selected events detailed information can be printed (as
 * std::cout) in this class. This is useful mainly for debugging, eg. to print
 * all jet/lepton pt/eta values for selected events as number in high
 * precision, etc.
 *
 * \authors walsh, klein
 */

#include <iostream>
#include <fstream>
#include <string>
#include "../MorObjects/MMET.h"
#include "../MorObjects/MElectron.h"
#include "../MorObjects/MMuon.h"
#include "../MorObjects/MJet.h"
#include "../MorObjects/MPrimaryVertex.h"
#include "../MorObjects/MEventInformation.h"
#include "../EventSelection/MassReconstruction.h"
#include "../EventSelection/HandleHolder.h"

#include "Math/LorentzVector.h"
#include "Math/VectorUtil.h"

#include <iostream>
#include <map>
#include "TH1D.h"
#include "TH2D.h"

class EventIdentifier{
	public:
		EventIdentifier(std::string ident);
		EventIdentifier(eire::HandleHolder *handle_holder);
		~EventIdentifier();
		void set_handles(std::vector<mor::Jet>* jets,
				 std::vector<mor::Electron>* isolated_electrons,
				 std::vector<mor::Muon>* isolated_muons,
				 std::vector<mor::Electron>* loose_electrons,
				 std::vector<mor::Muon>* loose_muons,
				 std::vector<mor::MET>* corrected_mets,
				 std::vector<mor::PrimaryVertex>* pvertices,
				 mor::EventInformation *event_information);
		void print();
		void set_mass_reco(MassReconstruction *mass_reco);
	private:
		void print_event_id();
		void print_muons();
		void print_jets();
		void print_mass();

		std::string id;

		std::vector<mor::Jet>* jets;
		std::vector<mor::Electron>* isolated_electrons;
		std::vector<mor::Electron>* loose_electrons;
		std::vector<mor::Muon>* isolated_muons;
		std::vector<mor::Muon>* loose_muons;
		std::vector<mor::MET>* corrected_mets;
		std::vector<mor::PrimaryVertex>* pvertices;

		eire::HandleHolder *handle_holder;
		mor::EventInformation *event_information;
		MassReconstruction *mass_reco;

		double one_ne_four;
		double four_not_one;
		double one_not_four;
};

#endif
