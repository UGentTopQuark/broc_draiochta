#ifndef MASSRECONSTRUCTIONMETHODBTAG_H
#define MASSRECONSTRUCTIONMETHODBTAG_H

#include "MassReconstructionMethod.h"
#include "BJetFinder.h"

class MassReconstructionMethodBTag: public MassReconstructionMethod{
	public:
		MassReconstructionMethodBTag();
		virtual ~MassReconstructionMethodBTag();

		void set_bjet_finder(BJetFinder *bj_finder);

		// return mass values w/ 1 b-tag value
		double get_hadT_mass_1btag();
		double get_lepT_mass_1btag();
		double get_hadW_mass_1btag();
		double get_lepW_mass_1btag();

		// return mass values w/ 2 b-tag value
		double get_hadT_mass_2btag();
		double get_lepT_mass_2btag();
		double get_hadW_mass_2btag();
		double get_lepW_mass_2btag();

		bool is_processed_1btag();
		bool is_processed_2btag();

		std::vector<int>* get_mass_jet_ids_1btag();
		std::vector<int>* get_mass_jet_ids_2btag();

	protected:
		virtual void calculate_mass_1btag();
		virtual void calculate_mass_2btag();

		virtual void reset_values();
		void book_id_vectors();
		void free_id_vectors();

		std::vector<int> *mass_jet_ids_1btag;
		double hadT_mass_1btag;
		double lepT_mass_1btag;
		double hadW_mass_1btag;
		double lepW_mass_1btag;

		std::vector<int> *mass_jet_ids_2btag;
		double hadT_mass_2btag;
		double lepT_mass_2btag;
		double hadW_mass_2btag;
		double lepW_mass_2btag;

		BJetFinder *bjet_finder;

		bool event_is_processed_1btag;
		bool event_is_processed_2btag;
};

#endif
