#ifndef CUTS_H
#define CUTS_H

/**
 * \class broc::Cuts
 * 
 * \brief Apply event selection for selection step
 *
 * All cuts for event selection are applied within this class.
 * 
 * \warning The classes PlotGenerator and Cuts will in the near future be
 * reorganised. Their role will be restricted to managing plots and applying
 * cuts respectively but object selection and activation as well as booking of
 * sub-modules will be moved to new classes CollectionSelector and
 * SelectionStep
 *
 * \authors walsh, klein
 */


#include "MassReconstruction.h"
#include "BJetFinder.h"
#include "ConversionIdentifier.h"
#include "../EventSelection/TriggerNameProvider.h"

#include "../MorObjects/MMuon.h"
#include "../MorObjects/MElectron.h"
#include "../MorObjects/MJet.h"
#include "../MorObjects/MMET.h"
#include "../MorObjects/MTrigger.h"
#include "../MorObjects/MTTbarGenEvent.h"
#include "../MorObjects/MEventInformation.h"
#include "../MorObjects/MPrimaryVertex.h"
#include "../EventSelection/EventIdentifier.h"

#include "HtCalculator.h"
#include "CutsSet.h"
#include "DiLeptonReconstructor.h"
#include "METCorrector.h"
#include "../../src/ConfigReader/ConfigReader.h"

#include "Math/LorentzVector.h" 
#include "Math/VectorUtil.h" 

#include "../TionscadalDorcha/ClassificationManager.h"

#include "CentralServices.h"
#include "HandleHolder.h"

#include "BTagAlgoNameProvider.h"

#include "../HitFit/HitFitManager.h"


#include <map>
#include <cstdlib>

namespace broc{
	class Cuts{
		public:
			Cuts(broc::CutsSet *cuts_set, eire::HandleHolder *handle_holder);
			~Cuts();
	                void set_handles(eire::HandleHolder *handle_holder);
			/// Returns false if event passed cuts, true if event should be discarded
			bool cut();
	
			void set_cuts_set(broc::CutsSet *cuts_set);
			inline bool get_identify_event_cut() { return identify_events; }
	
			void set_event_weight(double weight);
	
			void print_cuts();
			
		private:
	
			typedef ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > MyLorentzVector;
	
			void set_all_cuts();
			void set_all_vcuts();
	
			bool cut_M3();
			bool cut_event_classifier();
			bool cut_chi2();
			bool cut_mu_pt();
			bool cut_e_pt();
			bool cut_njets();
			bool cut_met();
			bool cut_min_ht();
			bool cut_max_ht();
			bool cut_min_missing_ht();
			bool cut_max_missing_ht();
			bool cut_nisolated_leptons();
			bool cut_nisolated_electrons();
			bool cut_nisolated_muons();
			bool cut_nloose_electrons();
			bool cut_nloose_muons();
			bool cut_nnoniso_electrons();
			bool cut_nnoniso_muons();
			bool is_Z();
			bool cut_btag();
			bool cut_hltrigger();
			bool cut_primary_vertex();
			bool cut_electron_conv_rej();

			bool cut_lep_b_chi2();

			bool cut_hitfit();
	
			broc::CutsSet *cuts_set;
	
			double event_weight;
		
			std::vector<mor::Jet> *jets;
			std::vector<mor::MET>* corrected_mets;
			std::vector<mor::MET>* uncorrected_mets;
			std::vector<mor::MET>* my_mets; //jl 14.01.11
			std::vector<mor::Muon>* isolated_muons;
			std::vector<mor::Electron>* isolated_electrons;
			std::vector<mor::Electron>* mm_loose_electrons;
			std::vector<mor::Muon>* mm_loose_muons; //jl 15.03.11 for MM
			std::vector<mor::Muon>* loose_muons;
			std::vector<mor::Electron>* loose_electrons;
			std::vector<mor::PrimaryVertex>* pvertices;
	
			//for trigger information
			mor::Trigger *HLTR;
	
			mor::EventInformation *event_information;
		
			std::vector<double> *trigger;
			std::vector<double> *min_btag;

			int hitfit_state;
	
			MassReconstruction *mass_reco;
			METCorrector *METCor;
	
			// variable to calculate ht only once for the class
			double ht;
	
			double pvertex_max_z;
			double pvertex_min_ndof;
			double pvertex_max_rho;
			double pvertex_not_fake;
			double min_npvertices;
	
			double min_M3;
			double max_M3;
			double min_met;
			double max_met;
			double max_ht;
			double min_ht;
			double max_missing_ht;
			double min_missing_ht;
			int min_no_jets;
			int max_no_jets;
			int min_nloose_e; //jl 01.09.10
			int max_nloose_e;
			int min_nloose_mu; //jl 15.07.10
			int max_nloose_mu;
			int min_nisolated_lep;
			int max_nisolated_e;
			int max_nisolated_mu;
			int min_nisolated_e;
			int min_nisolated_mu;
			int max_nnoniso_e;
			int max_nnoniso_mu;
			int min_nnoniso_e;
			int min_nnoniso_mu;
			int min_n_bjets;
			int max_n_bjets;
			double Z_window_max;
			double Z_window_min;
			double Z_requirement;
			double num_name_btag;
			double min_chi2;
			double max_chi2;
			double max_consid_bjets;

			double max_lep_b_chi2;

			double min_event_discriminator;
			double max_event_discriminator;
			std::string name_btag;
			bool identify_events;
			int set_trigger_OR;
	
			double max_e_dcot;
			double max_e_dist;
			//jl 08.12.10
			double max_loose_e_dcot;
			double max_loose_e_dist;
			double max_e_nLostTrackerHits;
	
			double cuts_passed_counter;
			double cuts_overall_counter;
	
			bool do_met_recalc; //jl 14.01.11: switch between measured MET and re-calculated MET to get mass plots
	
			std::string identifier;
			
			eire::ConversionIdentifier *conversion_identifier;
	
			DiLeptonReconstructor *z_reconstructor;
			eire::ConfigReader *config_reader;

			EventIdentifier *event_identifier;
	
			BJetFinder *bjet_finder;
			HtCalculator *ht_calc;
	
			eire::HandleHolder *handle_holder;
			tionscadaldorcha::EventClassifier* event_classifier;

			hitfit::HitFitManager *hitfit_study;

			bool enable_kinematic_fit_module;
			//bool enable_hitfit_combi_seed;
			//std::vector<int> hitfit_combi_seed;

	};
}

#endif
