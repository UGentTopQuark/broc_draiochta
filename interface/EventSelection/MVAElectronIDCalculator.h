#ifndef broc_MVAElectronIDCalculator_h
#define broc_MVAElectronIDCalculator_h

#include "../MorObjects/MElectron.h"
#include "HandleHolder.h"
#include "../../src/ConfigReader/ConfigReader.h"
#include "../../share/EgammaAnalysis/ElectronTools/interface/EGammaMvaEleEstimator.h"
#include "../AnalysisTools/ETH1D.h"
#include "../AnalysisTools/HistoWriter.h"
#include <map>
#include <string>

namespace broc{
	class MVAElectronIDCalculator{
		public:
			MVAElectronIDCalculator(eire::HandleHolder *handle_holder);
			~MVAElectronIDCalculator();

			double discriminator(mor::Electron& ele);
		private:
			bool is_trig_e(mor::Electron& ele);
			void initialise();
			void book_histos();

			eire::ConfigReader *config_reader;
			eire::HandleHolder *handle_holder;

			EGammaMvaEleEstimator *MVANonTrig;
			EGammaMvaEleEstimator *MVATrig;

			HistoWriter *histo_writer;

                	std::map<std::string,eire::TH1D*> histos1d;
			std::string id;

			static const bool control_plots = false;
	};
}

#endif
