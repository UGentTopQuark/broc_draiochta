#ifndef BROC_TOPPTWEIGHTPROVIDER_H
#define BROC_TOPPTWEIGHTPROVIDER_H

/** \class broc::TopPtWeightProvider
 * 
 * \brief Calculate event weights for reweighting MC for top pt spectrum
 * in data.
 *
 * Calculate event weights for compensating MC to data discrepancy top pt spectrum
 *
 * \author Kelly Beernaert
 */

#include "HandleHolder.h"

namespace broc{
  class TopPtWeightProvider{
		public:
                                    TopPtWeightProvider(eire::HandleHolder *handle_holder);
                                    ~TopPtWeightProvider();
			double get_value_for_event();

                        private:
			eire::HandleHolder *handle_holder;
	};
}


#endif
