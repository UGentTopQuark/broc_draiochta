#ifndef WBMASS_H
#define WBMASS_H

#include "MassReconstructionMethodBTag.h"

class WbMass: public MassReconstructionMethodBTag{
	public:
	private:
		void calculate_mass();
		void calculate_mass_1btag();
		void calculate_mass_2btag();
};

#endif
