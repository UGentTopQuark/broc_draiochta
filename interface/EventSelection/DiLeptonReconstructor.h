#ifndef DILEPTONRECONSTRUCTOR_H
#define DILEPTONRECONSTRUCTOR_H

#include <vector>
#include "../MorObjects/MElectron.h"
#include "../MorObjects/MMuon.h"
#include "HandleHolder.h"

class DiLeptonReconstructor{
	public:
		DiLeptonReconstructor(eire::HandleHolder *handle_holder);
		~DiLeptonReconstructor();

		inline void next_event(){};	//< not yet implemented, should avoid recalculating within same event
		void set_mass_window(double min, double max);
		std::vector<mor::Particle>* get_candidates();
		inline std::vector<mor::Electron>* get_loose_electron_legs(){ return loose_electron_legs; }
		inline std::vector<mor::Muon>* get_loose_muon_legs(){ return loose_muon_legs; }
		inline std::vector<mor::Electron>* get_tight_electron_legs(){ return tight_electron_legs; }
		inline std::vector<mor::Muon>* get_tight_muon_legs(){ return tight_muon_legs; }
		void select_lepton(int lepton_id);
		void set_allow_equal_charge(bool allow);
	private:
		void reconstruct_all_candidates();
		template <class LeptonType> void reconstruct_candidates(std::vector<LeptonType> *loose_leptons, std::vector<LeptonType> *tight_leptons, std::vector<LeptonType> *loose_lepton_legs, std::vector<LeptonType> *tight_lepton_legs);
		bool already_found(mor::Particle &new_candidate);
		std::vector<mor::Muon> *loose_muons;
		std::vector<mor::Electron> *loose_electrons;
		std::vector<mor::Muon> *tight_muons;
		std::vector<mor::Electron> *tight_electrons;

		std::vector<mor::Muon> *loose_muon_legs;
		std::vector<mor::Electron> *loose_electron_legs;
		std::vector<mor::Muon> *tight_muon_legs;
		std::vector<mor::Electron> *tight_electron_legs;

		std::vector<mor::Particle> *candidates;

		double min_mass;
		double max_mass;

		int selected_lepton_id;

		bool allow_equal_charge;
};

#endif
