#ifndef METCORRECTOR_H
#define METCORRECTOR_H

/** \class METCorrector
 * 
 * \brief Provides MET Collection with an estimated pz component of the neutrino.
 *
 * By means of a constraint on the W mass the pz component of the neutrino in
 * an event is estimated using the measured MET and one selected tight lepton.
 *
 * \author klein
 */

#include "../MorObjects/MMET.h"
#include "../MorObjects/MLepton.h"
#include "../MorObjects/MMuon.h"
#include "../MorObjects/MElectron.h"

#include "MyMEzCalculator.h"

class METCorrector{
	public:
		METCorrector();
		~METCorrector();
		void set_handles(std::vector<mor::MET> *mets_handle, mor::Lepton *lepton=NULL, bool is_muon=true);
		std::vector<mor::MET>* get_MET();
		std::vector<mor::MET>* get_corrected_MET();
		bool is_corrected();
		void set_correction_method(int method);
	private:
		void correct_MET();
		void prepare_corrected_MET();

		mor::Lepton *lepton;
		std::vector<mor::MET>* mets_handle;
		std::vector<mor::MET>* corrected_mets;
		MyMEzCalculator *MEzCal;
		bool is_muon;
		bool corrected;
		int correction_method;
		static const bool verbose = 0;
};

#endif
