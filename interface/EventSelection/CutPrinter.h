#ifndef BROC_CUTPRINTER_H
#define BROC_CUTPRINTER_H

/**
 * \class broc::CutPrinter
 * 
 * \brief Prints the Event Counter at the end of the job
 *
 * Takes the information from the Cuts and HitFit classes and interates for 
 * every event that passes the selection process.  The number is then printed
 * at the end of the job
 *
 * \author mccartin
 */

#include "HandleHolder.h"
#include "Cuts.h"
//#include "EventWeightManager.h"

namespace broc{
	class CutPrinter{
		public:
		CutPrinter(broc::CutsSet *cuts_set, eire::HandleHolder *handle_holder, eire::ConfigReader *config_reader);
			~CutPrinter();
			
			void calculate_efficiency(bool last_iter); // Print the efficiency and number of events passing cuts to the screen

		private:
			void print_efficiency(); // Calculates the efficiency and calls the print function

			broc::Cuts *cuts;
			broc::CutsSet *cuts_set;
			eire::HandleHolder *handle_holder;
			eire::ConfigReader *config_reader;

			//tionscadaldorcha::ClassificationManager *event_classifier;
			//broc::EventWeightManager *weight_manager;

			std::string identifier;

			bool enable_kinematic_fit_module;
			bool enable_hitfit_combi_seed;

			double event_weight;
                        double cuts_passed_counter;
                        double cuts_overall_counter;

			bool last_cutset;

			std::vector<int> hitfit_combi_seed;
	};
}

#endif
