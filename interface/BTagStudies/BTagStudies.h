#ifndef BCLIB_BTAGSTUDIES_H
#define BCLIB_BTAGSTUDIES_H

/***
 * \class BTagStudies
 *
 * \brief Creates output btag_system8 root file
 *
 * Creates output file and calls tree producer
 *
 * \author walsh
 ***/

#include "System8TreeProducer.h"
#include "../../src/ConfigReader/ConfigReader.h"
#include "../EventSelection/HandleHolder.h"
#include "../EventSelection/CutsSet.h"

namespace bclib{
	class BTagStudies{
		public:
			BTagStudies(broc::CutsSet *cuts_set, eire::HandleHolder *handle_holder);
			~BTagStudies();

			void fill_trees();
		private:
			broc::CutsSet *cuts_set;
			eire::HandleHolder *handle_holder;
			bclib::System8TreeProducer *btag_sys8_tree_producer;

			TFile *ntuple_file;
	};
}

#endif
