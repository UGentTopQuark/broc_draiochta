#ifndef MORIDCHECKER_H
#define MORIDCHECKER_H

/** \class mor::IDChecker
 * 
 * \brief Translates bitwise information stored in long long ints in ROOT
 * ntuples to strings.
 *
 * To reduce the size of ntuples, boolean information is stored in certain
 * situations as single bits of long long int variables. This class provides an
 * interface to enter strings identifying the bit position and returns the
 * value of the bit at the according position. The position of bits can change
 * randomly and is accessible through a name map which is updated every time
 * there is a change in the selected strings of which the information is saved.
 * An example is the information about an event being triggered by a certain
 * trigger.
 *
 * \warning Because the information is stored in long long ints not more than
 * 64 strings can be mapped to a single long long int. This means, it is not
 * possible to write out for example the event trigger information of more than
 * 64 triggers at a time.
 * 
 * \authors klein
 */


#include <vector>
#include <map>
#include <string>

#include <iostream>
#include <cstdlib>

namespace mor{
	class IDChecker{
		public:
			IDChecker();
			~IDChecker();

			bool id_passed(std::string ID_name, unsigned long long lepton_id);
			bool id_passed(std::string ID_name, std::vector<bool> lepton_id);
			void set_id_vector(std::vector<std::string> *names_vec);
			inline std::vector<std::string>* get_id_vector(){ return names_vec;};
			template <class TYPE>
			TYPE id_value(std::string ID_name, std::vector<TYPE> *id_values);
			template <class TYPE>
			std::vector<TYPE> id_value(std::string ID_name, std::vector<std::vector<TYPE> > *id_values);
			bool id_exists(std::string ID_name);
			int get_index(std::string ID_name);
		private:
			void print_id_not_available(std::string ID_name);
			std::map<std::string, unsigned long long> id_map;

			std::vector<std::string> *names_vec;

			static int error_counter;
			const static bool verbose = false;
	};
}

#endif
