#ifndef ANALYSER_H
#define ANALYSER_H

/**
 * \class Analyser
 * 
 * \brief Main analyser and program control
 *
 * The class is the interface between the user input (configuration file), the
 * read in ntuples (FileRunner, EventRunner) and the executed Analysis (CutSelector).
 * 
 * \authors walsh, klein
 */

#include <vector>
#include <iostream>
#include "../../externals/interface/BeagObjects/TriggerObject.h"
#include "../../externals/interface/BeagObjects/Jet.h"
#include "../../externals/interface/BeagObjects/Particle.h"
#include "../../externals/interface/BeagObjects/Electron.h"
#include "../../externals/interface/BeagObjects/Muon.h"
#include "../../externals/interface/BeagObjects/MET.h"
#include "../MorObjects/MJet.h"
#include "../MorObjects/MTriggerObject.h"
#include "../MorObjects/MElectron.h"
#include "../MorObjects/MParticle.h"
#include "../MorObjects/MMuon.h"
#include "../MorObjects/MMET.h"
#include "../../externals/interface/BeagObjects/TriggerObject.h"
#include "../../externals/interface/BeagObjects/Trigger.h"
#include "../../externals/interface/BeagObjects/TTbarGenEvent.h"
#include "../MorObjects/MTTbarGenEvent.h"
#include "../MorObjects/MTrigger.h"
#include "../MorObjects/MPrimaryVertex.h"
#include "../../externals/interface/BeagObjects/PrimaryVertex.h"
#include "EventRunner.h"
#include "NameMappingRunner.h"
#include "../MorObjects/MEventInformation.h"
#include "../../externals/interface/BeagObjects/EventInformation.h"
#include "../EventSelection/CutSelector.h"
#include "../AnalysisTools/HistoWriter.h"
#include "../AnalysisTools/RunSelector.h"
#include "../AnalysisTools/PDFWeightProvider.h"
#include "../EventSelection/JetCorrectionService.h"
#include "../EventSelection/PrescaleProvider.h"
#include "../../src/ConfigReader/ConfigReader.h"

#include "../EventSelection/HandleHolder.h"
#include "../EventSelection/PrescaleProvider.h"
#include "../EventSelection/TriggerModuleManager.h"

#include "TFile.h"

class Analyser{
        public:
		Analyser(std::string dataset_name, eire::ConfigReader *config_reader, TFile *outfile, bool process_mc);
                ~Analyser();
                void set_file_names(std::vector<std::string> *file_names);
                void set_max_events(int max_events);
                void set_outfile(TFile *outfile);
                void analyse();
        private:
                bool change_event();

		/// fill vectors of mor:: objects for various object collections
		template <class beag_type, class mor_type>
		void prepare_mor_collections(typename std::vector<beag_type> *beag_objs, typename std::vector<mor_type> *mor_objs);

		template <class mor_type>
		void assign_lepton_id_checkers(typename std::vector<mor_type> *mor_objs, mor::IDChecker *id_checker, mor::IDChecker *trigger_checker, mor::IDChecker *pfiso_conesize_checker);

		void assign_jet_id_checkers(std::vector<mor::Jet> *mor_jets, mor::IDChecker *btag_checker);
		std::vector<std::string>* fill_unversioned_trigger_names(std::vector<std::string> *names);
		void assign_el_iso_rho(std::vector<mor::Electron> *electrons);
		
		eire::JetCorrectionService *jet_corrector;
		eire::ConfigReader *config_reader;
                EventRunner *evt_runner;
		NameMappingRunner *trigger_name_runner;
		NameMappingRunner *tag_name_runner;
		NameMappingRunner *weight_runner;
                std::vector<beag::TriggerObject> *beag_trigger_objects;
                std::vector<beag::Particle> *beag_gen_particles;
                std::vector<beag::Jet> *beag_jets;
                std::vector<beag::Jet> *beag_noe_jets;
                std::vector<beag::Jet> *beag_nomu_jets;
                std::vector<beag::Electron> *beag_electrons;
                std::vector<beag::Electron> *beag_noniso_electrons;
                std::vector<beag::Muon> *beag_muons;
                std::vector<beag::Muon> *beag_noniso_muons;
                std::vector<beag::MET> *beag_mets;
                std::vector<beag::Trigger> *beag_triggers;
                std::vector<beag::TTbarGenEvent> *beag_gen_evts;
                std::vector<beag::PrimaryVertex> *beag_pvertices;
                std::vector<beag::EventInformation> *beag_event_information;
		std::vector<std::string> *trigger_names;
		std::vector<std::vector<std::string> > *trigger_module_names;
		std::vector<unsigned int> *l1_prescales;
		std::vector<unsigned int> *hlt_prescales;
		std::vector<std::string> *btag_names;
		std::vector<std::string> *pfiso_conesize_names;
		std::vector<std::string> *eID_names;
		std::vector<std::string> *muID_names;
		std::vector<std::vector<double> > *pdf_evt_weights;
		std::vector<std::string> *pdf_names;

                std::vector<mor::TriggerObject> *trigger_objects;
                std::vector<mor::Particle> *gen_particles;
                std::vector<mor::Jet> *uncorrected_jets;
                std::vector<mor::Jet> *uncorrected_noe_jets;
                std::vector<mor::Jet> *uncorrected_nomu_jets;
                std::vector<mor::Jet> *jets;
                std::vector<mor::Jet> *noe_jets;
                std::vector<mor::Jet> *nomu_jets;
                std::vector<mor::Electron> *electrons;
                std::vector<mor::Electron> *noniso_electrons;
                std::vector<mor::Muon> *muons;
                std::vector<mor::Muon> *noniso_muons;
                std::vector<mor::MET> *mets;
                std::vector<mor::PrimaryVertex> *pvertices;

		mor::TTbarGenEvent *gen_evt;
		mor::Trigger *trigger;
		mor::EventInformation *event_information;

		mor::IDChecker *eID_checker;
		mor::IDChecker *muID_checker;
		mor::IDChecker *btag_checker;
		mor::IDChecker *pfiso_conesize_checker;
		mor::IDChecker *trigger_checker;
		mor::IDChecker *l1seed_checker;
		mor::IDChecker *pdf_checker;
		
		RunSelector *run_selector;	/// accept or reject lumi sections based on json files
		eire::PDFWeightProvider *pdf_weight_prov;

		CutSelector *cut_selector;

                TFile *outfile;

		HistoWriter *histo_writer;

                int event_number;
                int max_events;

		bool first_event_call;
		
		bool process_mc;
		bool process_trigger_objects;
		bool process_prescales;
		bool process_gen_particles;
		bool read_pdf_weights;
		bool process_all_pdf_sets;		// clone cutssets for all pdf sets and weights
		bool process_noniso_muons;
		bool process_noniso_electrons;
		int obj_type;				//jl 14.01.11: switch btw pf/tc/calo
		std::string pdf_set;

		eire::HandleHolder *handle_holder;

		broc::PrescaleProvider *prescale_prov;
		broc::TriggerModuleManager *trigger_module_manager;

		std::string identifier;
};

#endif
