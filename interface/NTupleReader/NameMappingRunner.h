#ifndef NAMEMAPPINGRUNNER_H
#define NAMEMAPPINGRUNNER_H

#include "FileRunner.h"
#include "TTree.h"
#include <vector>

#ifdef __MAKECINT__
#pragma link C++ class std::vector<std::vector<double> >+;
#endif

class NameMappingRunner{
	public:
		NameMappingRunner();
		~NameMappingRunner();

		template <class beagObj>
		void assign_mapper(beagObj *&mapper, std::string branch_name);

		void set_file_runner(FileRunner *file_runner, std::string tree_name="name_mapping");
		void next_mapping();
	private:
		FileRunner *file_runner;

        	double current_file_event;
        	double current_file_max_events;
		std::string tree_name;

		static const bool verbose = false;

		TTree *tree;
};

#endif
