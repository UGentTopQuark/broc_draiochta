#ifndef FILERUNNER_H
#define FILERUNNER_H

/**
 * \class FileRunner
 * 
 * \brief Handles lists of ROOT ntuple files and provides information from the files.
 *
 * FileRunner manages a list of ROOT ntuple files. It allows the switching of
 * the files once one of the files has been processed completly and provides an
 * interface to the content of the ntuple file, eg. Trees.
 * 
 * \authors klein
 */


#include <vector>
#include "TFile.h"
//#include "TDCacheFile.h"
#include "TTree.h"
#include <iostream>

class FileRunner{
        public:
                FileRunner();
                ~FileRunner();
                void set_file_names(std::vector<std::string> *file_names);
                bool has_next();
                TTree* get_next_tree(std::string tree_name, bool change_file=true);
                void cd_infile();
        private:
                std::vector<std::string> *file_names;
                //TDCacheFile *infile;
                TFile *infile;
                std::vector<std::string>::iterator current_file;
                TTree *tree;
                static const bool verbose = true;
};

#endif
