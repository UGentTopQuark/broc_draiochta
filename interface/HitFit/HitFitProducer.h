#ifndef HITFIT_HITFITPRODUCER_H
#define HITFIT_HITFITPRODUCER_H

// needs more objects!
#include <string>
#include "Core/RunHitFit.hpp"
#include "Core/JetTranslatorBase.hpp"
#include "Core/LeptonTranslatorBase.hpp"
#include "Core/METTranslatorBase.hpp"

#include "../TagAndProbe/TreeProducer.h"
#include "../EventSelection/HandleHolder.h"
#include "../MorObjects/MTTbarGenEvent.h"

#include "../MorObjects/MEventInformation.h"

typedef hitfit::RunHitFit<mor::Electron,mor::Muon,mor::Jet,mor::MET> BrocHitFit;

namespace hitfit{
	/// semi-leptonic parton enum used to define the order 
	/// in the vector for lepton and jet combinatorics
	enum { LightQ, LightQBar, HadB, LepB, Lepton };

	class HitFitProducer : public clibisfiosraigh::TreeProducer{
		public:
			HitFitProducer(eire::HandleHolder *handle_holder);
			~HitFitProducer();

			virtual void book_branches();
			virtual bool fill_branches();
			inline void set_chisq_cut(float cut){ this->chisq_cut = cut; };
			std::vector<int> get_hitfit_combination();
			double get_hitfit_chisq();
			std::vector<mor::Particle>* get_hitfit_jets();
			std::vector<mor::Particle>* get_hitfit_muons();
			std::vector<mor::Particle>* get_hitfit_electrons();
			std::vector<mor::Particle>* get_hitfit_mets();
			void set_hitfit_combination(std::vector<int> combo);
			void set_hitfit_chisq(double chisq);
			void set_hitfit_jets(std::vector<mor::Particle> *jet_coll);
			void set_hitfit_muons(std::vector<mor::Particle> *muon_coll);
			void set_hitfit_electrons(std::vector<mor::Particle> *electron_coll);
			void set_hitfit_mets(std::vector<mor::Particle> *met_col);
			void combine_jet(int index_jet_tobe_combined, int index);

		private:
			eire::HandleHolder *handle_holder;
			mor::TTbarGenEvent *gen_evt;
			mor::EventInformation *event_information;

			int hitfit_state;
			std::vector<int> hitfit_combi_seed;
			
			bool use_smeared_partons;
			bool use_mc_truth_seed;

			BrocHitFit *HitFit;
			
			float chisq_cut;

			Int_t maxNComb_;
			static const Int_t maxNCombStatic = 120;

			struct FitResult {
				int Status;
				double Chi2;
				double Prob;
				double MT;
				double SigMT;

				mor::Particle HadB;
				mor::Particle HadP;
				mor::Particle HadQ;
				mor::Particle LepB;
				mor::Particle LepL;
				mor::Particle LepN;

				std::vector<int> JetCombi;
				bool operator< (const FitResult& rhs) { return Chi2 < rhs.Chi2; };
			};

			struct TTGenEvent {
				mor::Particle HadB;
				mor::Particle HadP;
				mor::Particle HadQ;
				mor::Particle LepB;
				mor::Particle LepL;
				mor::Particle LepN;

				std::vector<int> JetCombi;
			};

	
			struct FitResultContainer {
				Int_t Combi_LightQ[maxNCombStatic];
				Int_t Combi_LightQBar[maxNCombStatic];
				Int_t Combi_HadB[maxNCombStatic];
				Int_t Combi_LepB[maxNCombStatic];
				Int_t Status[maxNCombStatic];
				Double_t Chi2[maxNCombStatic];
				Double_t Prob[maxNCombStatic];
				Double_t MT[maxNCombStatic];
				Double_t SigMT[maxNCombStatic];

				Int_t nSol;
				Double_t HadB_pt[maxNCombStatic];
				Double_t HadB_eta[maxNCombStatic];
				Double_t HadB_phi[maxNCombStatic];
				Double_t HadB_m[maxNCombStatic];

				Double_t HadP_pt[maxNCombStatic];
				Double_t HadP_eta[maxNCombStatic];
				Double_t HadP_phi[maxNCombStatic];
				Double_t HadP_m[maxNCombStatic];

				Double_t HadQ_pt[maxNCombStatic];
				Double_t HadQ_eta[maxNCombStatic];
				Double_t HadQ_phi[maxNCombStatic];
				Double_t HadQ_m[maxNCombStatic];

				Double_t LepB_pt[maxNCombStatic];
				Double_t LepB_eta[maxNCombStatic];
				Double_t LepB_phi[maxNCombStatic];
				Double_t LepB_m[maxNCombStatic];

				Double_t LepL_pt[maxNCombStatic];
				Double_t LepL_eta[maxNCombStatic];
				Double_t LepL_phi[maxNCombStatic];
				Double_t LepL_m[maxNCombStatic];

				Double_t LepN_pt[maxNCombStatic];
				Double_t LepN_eta[maxNCombStatic];
				Double_t LepN_phi[maxNCombStatic];
				Double_t LepN_m[maxNCombStatic];

				Int_t matched[maxNCombStatic];
			            Double_t ExtraJet_pt[maxNCombStatic];
			            Double_t ExtraJet_eta[maxNCombStatic];
			            Double_t ExtraJet_phi[maxNCombStatic];

			            Double_t HadB_pt_orig[maxNCombStatic];
				Double_t HadB_eta_orig[maxNCombStatic];
				Double_t HadB_phi_orig[maxNCombStatic];
				Double_t HadB_m_orig[maxNCombStatic];

				Double_t HadP_pt_orig[maxNCombStatic];
				Double_t HadP_eta_orig[maxNCombStatic];
				Double_t HadP_phi_orig[maxNCombStatic];
				Double_t HadP_m_orig[maxNCombStatic];

				Double_t HadQ_pt_orig[maxNCombStatic];
				Double_t HadQ_eta_orig[maxNCombStatic];
				Double_t HadQ_phi_orig[maxNCombStatic];
				Double_t HadQ_m_orig[maxNCombStatic];

				Double_t LepB_pt_orig[maxNCombStatic];
				Double_t LepB_eta_orig[maxNCombStatic];
				Double_t LepB_phi_orig[maxNCombStatic];
				Double_t LepB_m_orig[maxNCombStatic];

				Double_t LepL_pt_orig[maxNCombStatic];
				Double_t LepL_eta_orig[maxNCombStatic];
				Double_t LepL_phi_orig[maxNCombStatic];
				Double_t LepL_m_orig[maxNCombStatic];

				Double_t LepN_pt_orig[maxNCombStatic];
				Double_t LepN_eta_orig[maxNCombStatic];
				Double_t LepN_phi_orig[maxNCombStatic];
				Double_t LepN_m_orig[maxNCombStatic];
			}fitResult;

			/// maximal number of jets (-1 possible to indicate 'all')                                                                                                                                                                     
			int maxNJets_;

			/// maximum eta value for muons, needed to limited range in which resolutions are provided                                                                                                                                     
			double maxEtaMu_;
			/// maximum eta value for electrons, needed to limited range in which resolutions are provided                                                                                                                                 
			double maxEtaEle_;
			/// maximum eta value for jets, needed to limited range in which resolutions are provided                                                                                                                                      
			double maxEtaJet_;

			/// input tag for b-tagging algorithm                                                                                                                                                                                          
			std::string bTagAlgo_;
			/// min value of bTag for a b-jet                                                                                                                                                                                              
			double minBTagValueBJet_;
			/// max value of bTag for a non-b-jet                                                                                                                                                                                          
			double maxBTagValueNonBJet_;
			/// switch to tell whether to use b-tagging or not                                                                                                                                                                             
			bool useBTag_;

			/// constraints                                                                                                                                                                                                                
			double mW_;
			double mTop_;

			/// jet correction level                                                                                                                                                                                                       
			std::string jetCorrectionLevel_;

			/// jet energy scale                                                                                                                                                                                                           
			double jes_;
			double jesB_;

			// mc truth information

			double mc_HadB_pt;
			double mc_HadB_eta;
			double mc_HadB_phi;

			double mc_LepB_pt;
			double mc_LepB_eta;
			double mc_LepB_phi;

			double mc_Q_pt;
			double mc_Q_eta;
			double mc_Q_phi;

			double mc_QBar_pt;
			double mc_QBar_eta;
			double mc_QBar_phi;

			int    mc_LepL_charge;
			double mc_LepL_pt;
			double mc_LepL_eta;
			double mc_LepL_phi;

			double mc_LepN_pt;
			double mc_LepN_eta;
			double mc_LepN_phi;


			LeptonTranslatorBase<mor::Electron> electronTranslator_;
			LeptonTranslatorBase<mor::Muon>     muonTranslator_;
			JetTranslatorBase<mor::Jet>         jetTranslator_;
			METTranslatorBase<mor::MET>         metTranslator_;

			std::string hitfitDefault_;
			std::string hitfitElectronResolution_;
			std::string hitfitMuonResolution_;
			std::string hitfitUdscJetResolution_;
			std::string hitfitBJetResolution_;
			std::string hitfitMETResolution_;

			std::vector<mor::Particle> *pPartonsHadP;
			std::vector<mor::Particle> *pPartonsHadQ;
			std::vector<mor::Particle> *pPartonsHadB;
			std::vector<mor::Particle> *pPartonsLepB;
			std::vector<mor::Particle> *pLeptons;
			std::vector<mor::Particle> *pNeutrinos;

			std::vector<std::vector<int> > *pCombi;
			std::vector<double> *pChi2;
			std::vector<double> *pProb;
			std::vector<double> *pMT;
			std::vector<double> *pSigMT;
			std::vector<int> *pStatus;
			
			std::vector<mor::Jet> *jets;
			std::vector<mor::Jet> *original_jets;
			std::vector<beag::Jet> *beagjets;
			std::vector<mor::Muon> *muons;
			std::vector<mor::Electron> *electrons;
			std::vector<mor::MET> *mets;

			std::vector<mor::Particle> *hitfit_jets;
			std::vector<mor::Particle> *hitfit_electrons;
			std::vector<mor::Particle> *hitfit_muons;
			std::vector<mor::Particle> *hitfit_mets;
			double hitfit_chisq;

			Int_t mc_Combi[4];
			
			double event_number;
			double lumi_block;
			double run;			
			double event_weight;
	};
}

#endif
