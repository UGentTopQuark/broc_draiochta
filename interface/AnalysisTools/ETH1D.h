#ifndef EIRE_TH1D_H
#define EIRE_TH1D_H

/** \class eire::TH1D
 * 
 * \brief Extends functionality of default ROOT TH1D
 *
 * eire::TH1D extends the functionality of the default ROOT TH1D class. It
 * allows that a global weight can be set which is then applied to all
 * histograms that are currently processed. This is useful if one global
 * weights as for example event weights and wants to fill all histograms with
 * the same weight. It is still possible set own weights when filling
 * individual histograms.
 * 
 * \authors klein
 */

#include "TH1D.h"
#include <iostream>

namespace eire{
	class TH1D: public ::TH1D{
		public:
			TH1D(const char *name,const char *title,Int_t nbins,const Double_t *xbins);
			TH1D(const char *name,const char *title,Int_t nbins,const Float_t *xbins);
			TH1D(const char *name,const char *title,Int_t nbins,Double_t xlow,Double_t xup);
			static void set_global_weight(double weight);
			Int_t Fill(double value, double weight=-1);
		private:
			static double global_weight;
	};
}

#endif
