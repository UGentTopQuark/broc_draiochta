#ifndef EIRE_TH2D_H
#define EIRE_TH2D_H

/** \class eire::TH2D
 * 
 * \brief Extends functionality of default ROOT TH2D
 *
 * eire::ETH2D extends the functionality of the default ROOT TH2D class. It
 * allows that a global weight can be set which is then applied to all
 * histograms that are currently processed. This is useful if one global
 * weights as for example event weights and wants to fill all histograms with
 * the same weight. It is still possible set own weights when filling
 * individual histograms.
 * 
 * \authors klein
 */


#include "TH2D.h"

namespace eire{
	class TH2D: public ::TH2D{
		public:
			TH2D(const char *name,const char *title,Int_t nbinsx,Double_t xlow,Double_t xup
			           ,Int_t nbinsy,Double_t ylow,Double_t yup);
			static void set_global_weight(double weight);
			Int_t Fill(double xvalue, double yvalue, double weight=-1);
		private:
			static double global_weight;
	};
}

#endif
