#ifndef RUNSELECTOR_H
#define RUNSELECTOR_H

/** \class RunSelector
 * 
 * \brief Selects or rejects runs and lumi sections according to json files.
 *
 * For each event RunSelector checks to which lumi section and run the event
 * belongs and returns according to a good-data json file if the event should
 * be considered or not.
 *
 * \author klein
 */

#include "../MorObjects/MEventInformation.h"

class RunSelector{
	public:
		RunSelector();
		~RunSelector();

		void set_event_information(mor::EventInformation *evt_info);
		bool is_good();
		void accept_all(bool accept);
	private:
		mor::EventInformation *evt_info;

		bool accept_all_events;
};

#endif
